#!/bin/sh

# Set a flag so Gemfile can limit gems
export FRAMEWORK_FLAG=true

# Get a reference to our current directory and script name
SOURCE="$0"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  test ${SOURCE#/} = ${SOURCE} && SOURCE="$DIR/$SOURCE"
done
BASE="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

NAME=$(basename "$0")

# Set up the environment
. "${BASE}/scripts/setenv.sh"

# Check if this is a framework script
if [ -f "${BASE}/apps/pro/msf3/${NAME}" ]; then
  exec "${BASE}/apps/pro/msf3/${NAME}" "$@"
fi

# Or a pro script
if [ -f "${BASE}/apps/pro/engine/${NAME}" ]; then
  unset FRAMEWORK_FLAG
  exec "${BASE}/apps/pro/engine/${NAME}" "$@"
fi

# Or a rails script
if [ -f "${BASE}/apps/pro/ui/script/${NAME}" ]; then
  unset FRAMEWORK_FLAG
  exec "${BASE}/apps/pro/ui/script/${NAME}" "$@"
fi

# Everything else should be on the path
exec "${NAME}" "$@"
