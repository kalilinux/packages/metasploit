#!/bin/bash -e

function needs_sudo () {
  [[ $EUID -eq 0 ]] && return 1
  type gksudo >/dev/null 2>&1 || return 2
  return 0
}

if needs_sudo; then
  gksudo /etc/init.d/postgresql start
  gksudo /etc/init.d/metasploit start
else
  /etc/init.d/postgresql start
  /etc/init.d/metasploit start
fi
