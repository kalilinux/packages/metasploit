#!/bin/bash -e

function msp_running () {
  /etc/init.d/metasploit status >/dev/null
  return $?
}

function needs_sudo () {
  [[ $EUID -eq 0 ]] && return 1
  type gksudo >/dev/null 2>&1 || return 2
  return 0
}

URL=/opt/metasploit/apps/pro/ui/public/boot.html

if msp_running; then
  URL=https://localhost:3790/
else
  if needs_sudo; then
    gksudo /opt/metasploit/scripts/start.sh
  else
    /etc/init.d/postgresql start >/dev/null
    /etc/init.d/metasploit start >/dev/null
  fi
fi

if type xdg-open >/dev/null 2>&1; then
  xdg-open $URL
else
  firefox $URL
fi
