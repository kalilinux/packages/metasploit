#!/bin/sh

RAILS_ENV="${RAILS_ENV:-production}"
export RAILS_ENV

BUNDLE_GEMFILE="${BUNDLE_GEMFILE:-/opt/metasploit/apps/pro/Gemfile}"
export BUNDLE_GEMFILE

MSF_DATABASE_CONFIG="${MSF_DATABASE_CONFIG:-/opt/metasploit/apps/pro/ui/config/database.yml}"
export MSF_DATABASE_CONFIG

# Bootstrap bundler
if ! echo "$GEM_PATH" | egrep -q "/opt/metasploit/apps/pro/vendor/bundle" ; then
  GEM_PATH="${GEM_PATH:+"$GEM_PATH:"}/opt/metasploit/apps/pro/vendor/bundle/ruby/2.1.0"
fi
export GEM_PATH

exec "/opt/metasploit/ruby/bin/ruby" "$@"
