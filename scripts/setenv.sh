#!/bin/sh

METASPLOIT_ROOT="%DESTDIR%"
export METASPLOIT_ROOT


if ! echo $PATH | egrep "${METASPLOIT_ROOT}" > /dev/null ; then
  
  PATH="${METASPLOIT_ROOT}/ruby/bin:$PATH"
  export PATH
fi
if ! echo $LD_LIBRARY_PATH | egrep "${METASPLOIT_ROOT}" > /dev/null ; then
  
  LD_LIBRARY_PATH="${METASPLOIT_ROOT}/ruby/lib:$LD_LIBRARY_PATH"
  export LD_LIBRARY_PATH
fi


RAILS_ENV=production
export RAILS_ENV

BUNDLE_GEMFILE="${BUNDLE_GEMFILE:-${METASPLOIT_ROOT}/apps/pro/Gemfile}"
export BUNDLE_GEMFILE

MSF_DATABASE_CONFIG="${METASPLOIT_ROOT}/apps/pro/ui/config/database.yml"
export MSF_DATABASE_CONFIG

# Bootstrap bundler
if ! echo "$GEM_PATH" | egrep -q "${METASPLOIT_ROOT}/apps/pro/vendor/bundle" ; then
  GEM_PATH="${GEM_PATH:+"$GEM_PATH:"}${METASPLOIT_ROOT}/apps/pro/vendor/bundle/ruby/2.1.0"
fi
export GEM_PATH
