# -*- coding: binary -*-
require 'spec_helper'

describe Metasploit::Pro::Engine::Rpc do
  include_context 'Metasploit::Pro::Engine::Application#framework'

  subject(:metasploit_pro_engine_rpc) {
    described_class.new(service)
  }

  #
  # lets
  #

  let(:options) {
    {}
  }

  let(:service) {
    double(
        'Msf::RPC::Service',
        framework: metasploit_pro_engine_application_framework,
        options: options,
        tokens: tokens,
        users: users
    )
  }

  let(:tokens) {
    []
  }

  let(:users) {
    []
  }

  #
  # let!s
  #

  let!(:rpc_user) {
    FactoryGirl.create(
        :mdm_user,
        admin: true
    )
  }

  it_should_behave_like 'Metasploit::Pro::Engine::Rpc::Exports'
  it_should_behave_like 'Metasploit::Pro::Engine::Rpc::Import'
  it_should_behave_like 'Metasploit::Pro::Engine::Rpc::Reports'
  it_should_behave_like 'Metasploit::Pro::Engine::Rpc::Tasks'
end
