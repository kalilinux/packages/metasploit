require 'spec_helper'

describe Metasploit::Pro::Metamodules::RunStats do
  
  subject(:object) { Object.new.extend(described_class) }

  let(:stats) { [:a, :b, :c] }
  let(:task)  { FactoryGirl.create(:mdm_task) }

  before do
    object.stub(:mdm_task).and_return(task)
  end

  describe '#initialize_run_stats' do
    subject(:run_stats) { object.run_stats }

    context 'run_stats before being called' do
      it { should be_nil }
    end

    context 'run_stats after being called' do

      before { object.initialize_run_stats(stats) }
      
      it { should_not be_empty }

      it 'contains the correct keys' do
        expect(run_stats.keys).to match_array(stats)
      end

      describe '#run_stats structure' do

        it 'the keys map to Hash instances' do
          expect(run_stats.values.all? { |val| val.is_a?(Hash) }).to be_true
        end

        it 'the keys map to Hash instances with :mutex and :model keys' do
          expect(run_stats.values.all? { |val| val.has_key?(:mutex) and val.has_key?(:model) }).to be_true
        end

        it 'each :model subkey contains a RunStat model' do
          expect(run_stats.values.all? { |val| val[:model].is_a?(RunStat) }).to be_true
        end

        it 'each :model subkey contains a persisted RunStat model' do
          expect(run_stats.values.all? { |val| val[:model].persisted? }).to be_true
        end

        it 'each :model subkey contains a persisted RunStat model whose data is 0' do
          expect(run_stats.values.all? { |val| val[:model].data.zero? }).to be_true
        end

        it 'each :mutex subkey contains a Mutex' do
          expect(run_stats.values.all? { |val| val[:mutex].is_a?(Mutex) }).to be_true
        end

      end
    end
  end

  describe '#update_stat' do
    before do
      object.initialize_run_stats(stats)
    end

    it 'updates the specified stat' do
      object.update_stat(:a, 55)
      expect(object.run_stats[:a][:model].data).to eq(55)
    end

    it 'updates and saves the specified stat' do
      object.update_stat(:a, 55)
      expect(object.run_stats[:a][:model].reload.data).to eq(55)
    end

    context 'when passed an unknown stat name' do
      it 'raises an ArgumentError' do
        expect { object.update_stat(:QQ, 50) }.to raise_error(KeyError)
      end
    end
  end

  describe '#increment_stat' do
    before do
      object.initialize_run_stats(stats)
    end

    it 'increments the specified stat' do
      object.increment_stat(:a)
      expect(object.run_stats[:a][:model].data).to eq(1)
    end

    it 'increments and saves the specified stat' do
      object.increment_stat(:a)
      expect(object.run_stats[:a][:model].reload.data).to eq(1)
    end

    context 'when passed an unknown stat name' do
      it 'raises an ArgumentError' do
        expect { object.increment_stat(:QQ) }.to raise_error(KeyError)
      end
    end
  end

end
