shared_examples_for 'Metasploit::Pro::Engine::Rpc::Exports' do
  it { should respond_to :rpc_export_download }
  it { should respond_to :rpc_export_list }
  it { should respond_to :rpc_start_export }
end