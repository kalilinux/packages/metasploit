shared_examples_for 'Metasploit::Pro::Engine::Rpc::Import' do
  it { should respond_to :rpc_import_file }
  it { should respond_to :rpc_import_data }
end
