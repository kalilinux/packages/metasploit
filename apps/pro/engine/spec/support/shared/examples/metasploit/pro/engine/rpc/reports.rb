shared_examples_for 'Metasploit::Pro::Engine::Rpc::Reports' do
  it { should respond_to :rpc_report_list }
  it { should respond_to :rpc_report_artifact_download }
  it { should respond_to :rpc_report_download }
  it { should respond_to :rpc_start_report }
end