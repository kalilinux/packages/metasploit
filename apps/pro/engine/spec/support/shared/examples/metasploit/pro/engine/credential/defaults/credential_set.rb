shared_examples_for 'Metasploit::Pro::Engine::Credential::Defaults::CredentialSet' do

  let(:usernames) { described_class::USERNAMES }
  let(:passwords) { described_class::PASSWORDS }
  let(:combinations) { described_class::COMBINATIONS }

  subject(:cred_set) { described_class.new }

  it 'defines a USERNAMES constant' do
    expect(described_class.const_defined?(:USERNAMES)).to be true
  end

  it 'defines a PASSWORDS constant' do
    expect(described_class.const_defined?(:PASSWORDS)).to be true
  end

  it 'defines a COMBINATIONS constant' do
    expect(described_class.const_defined?(:COMBINATIONS)).to be true
  end

  it 'defines a COUNT constant' do
    expect(described_class.const_defined?(:COUNT)).to be true
  end

  context '#each' do
    it 'yields once for each combination' do
      expect { |b| cred_set.each(&b) }.to yield_control.exactly(combinations.count)
    end

    it 'yields Metasploit::Framework::Credentials' do
      cred_set.each do |cred|
        expect(cred).to be_kind_of Metasploit::Framework::Credential
      end
    end

    it 'yields valid credentials' do
      cred_set.each do |cred|
        expect(cred).to be_valid
      end
    end

  end

end
