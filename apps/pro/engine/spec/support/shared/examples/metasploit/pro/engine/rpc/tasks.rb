shared_examples_for 'Metasploit::Pro::Engine::Rpc::Tasks' do
  let(:workspace){ FactoryGirl.create(:mdm_workspace) }
  let(:user){ FactoryGirl.create(:mdm_user) }
  let(:mdm_task){ FactoryGirl.create(:mdm_task, workspace: workspace, created_by: user.fullname) }

  let(:fake_module_fullname){ "auxiliary/scan/awesomesauce/super-duper-scan" }
  let(:fake_module_description){ "Scans all the Awesomesauce Endpoints for SuperDuper" }

  let(:fake_aux_module) do
    double("MSF aux module",
           init_ui: true,
           datastore: {},
           uuid: 'abcsdf123'
    )
  end

  let(:tasks){ {} }

  let(:pro_task){ double('Pro::ProTask') }

  let(:config) do
    {
      'workspace'   => workspace.name,
      'username'    => user.fullname,
      'description' => fake_module_description,
      workspace: workspace,
      username: user.fullname,
      mdm_task: mdm_task
    }
  end

  before(:each) do
    # Because tacking an '=' on method names above just doesn't work when declaring stub responses on double
    pro_task.stub(:description=)
    pro_task.stub(:workspace=)
    pro_task.stub(:username=)
    pro_task.stub(:requested_task_action=)
    pro_task.stub(:metasploit_module=)
    pro_task.stub(:start)
    pro_task.stub(:task_id).and_return mdm_task.id
    pro_task.stub(:valid?).and_return true
    pro_task.stub(:task_proc).and_return{ Proc.new { puts 'nonsense'} }
    pro_task.stub(:task_proc=)

    metasploit_pro_engine_rpc.stub(:tasks).and_return tasks
    tasks.stub(:[]).and_return pro_task
    tasks.stub(:create).and_return mdm_task.id

    metasploit_pro_engine_application_framework.stub_chain(:db, :workspace).and_return workspace
    metasploit_pro_engine_application_framework.stub_chain(:db, :active).and_return true
    metasploit_pro_engine_application_framework.stub_chain(:auxiliary, :create).and_return fake_aux_module
    metasploit_pro_engine_application_framework.stub(:esnecil_invalid?).and_return false
  end


  describe "_start_module_task" do
    it 'responds_to rpc_task_stop'  do
      expect(metasploit_pro_engine_rpc.respond_to?(:rpc_task_stop)).to eq(true)
    end

    context "setting up the ProTask" do
      context "metadata" do
        context "#description" do
          context "when a description is provided in the configuration options" do
            it 'should take description from the passed-in hash' do
              expect(pro_task).to receive(:description=).with(config['description'])
              metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)
            end
          end
        end

        context "#workspace" do
          context "when a workspace is provided in the config" do
            it 'should use the provided workspace' do
              expect(pro_task).to receive(:workspace=).with(config['workspace'])
              metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)
            end
          end
        end

        context "#username" do
          context "when a username is provided in the config" do
            it 'should set the username from the config hash' do
              expect(pro_task).to receive(:username=).with(config['username'])
              metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)
            end
          end

          context "when a username is NOT provided in the config" do
            before(:each) do
              config['username'] = nil
            end

            it 'should be set to a blank string' do
              expect(pro_task).to receive(:username=).with('unknown')
              metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)
            end
          end
        end
      end
    end

    context "creating the Mdm::Task record" do
      context "without a task_id field in the passed-in args" do
        before(:each) do
          config['task_id'] = nil
        end

        it 'should create a new Mdm::Task' do
          expect{metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)}.to change(Mdm::Task, :count).by(1)
        end
      end

      context "with a task_id in the args" do
        before(:each) do
          config['task_id'] = mdm_task.id
        end

        it 'should call Mdm::Task::find with the supplied task_id' do
          expect(Mdm::Task).to receive(:find).with(mdm_task.id).and_call_original
          metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)
        end

        it 'should not create a new Mdm::Task' do
          expect{metasploit_pro_engine_rpc.send(:_start_module_task, config, fake_module_fullname, fake_module_description)}.to_not change(Mdm::Task, :count)
        end
      end
    end
  end

end
