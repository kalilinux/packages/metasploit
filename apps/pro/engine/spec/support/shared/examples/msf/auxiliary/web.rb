shared_examples_for 'Msf::Auxiliary::Web' do
  context 'methods' do
    context '#details' do
      let(:details) do
        subject.details
      end

      context 'category' do
        let(:category) do
          details[:category]
        end

        it 'should not be nil' do
          category.should_not be_nil
        end

        it 'should be a Web::VulnCategory::Metasploit#name' do
          Web::VulnCategory::Metasploit.where(:name => category).should_not be_empty
        end
      end
    end
  end
end