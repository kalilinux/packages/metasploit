shared_examples_for 'an anti-debugging function' do

  describe '#prototype' do
    it 'should return a valid ANSI C function prototype' do
      function.prototype.should eq prototype
    end
  end

  describe '#definition' do
    it 'should return a valid ANSI C function definition' do
      expect(function.definition).to eq definition
    end
  end

  describe '#invocation' do
    it 'should call the function with no arguments' do
      expect(function.invocation).to eq invocation
    end
  end

end
