shared_examples_for 'Pro::DynamicStagers::Code::Loop' do

  it { should respond_to :generate_for_loop }

  describe '#generate_for_loop' do

    it 'generates a for loop with 1-4 operations inside' do
      expect(function.generate_for_loop).to match(/#{for_loop_regex}/)
    end

    context 'when passed an empty scope' do
      it 'generates new vars in the scope' do
        function.generate_for_loop
        expect(function.scope.vars.keys.count).to eq (Pro::DynamicStagers::Code::Scope::DEFAULT_MIN_VARS)
      end
    end
  end

end