shared_examples_for 'Pro::DynamicStagers::Code::Conditional' do

  it { should respond_to :generate_if_else }
  it { should respond_to :generate_conditional_action }

  describe '#generate_conditional_action' do
    context 'with no functions in scope' do
      it 'generates with random operations' do
        expect(function.generate_conditional_action).to match(/^{\n#{operation_regex}\n}\n$/)
      end
    end

    context 'with a function in scope' do
      let(:scope) {
        Pro::DynamicStagers::Code::Scope.new({
          'Foo' =>  Pro::DynamicStagers::Code::Function.new(name: 'Foo', type: 'void', arguments: [], body: "int a=1;")
        })
      }

      context 'when rand(1) returns a 0 ' do
        it 'uses the function caller instead of an operation' do
          function.stub(:rand).and_call_original
          function.stub(:rand).with(1) { 0 }
          caller_exp = Regexp.escape(scope['Foo'].invocation)
          expect(function.generate_conditional_action).to match(/^{\n#{caller_exp};\n}\n$/)
        end
      end

      context 'when rand(1) returns a 1 ' do
        it 'generates with random operations' do
          function.stub(:rand).and_call_original
          function.stub(:rand).with(1) { 1 }
          expect(function.generate_conditional_action).to match(/^{\n#{operation_regex}\n}\n$/)
        end
      end

    end
  end

  describe '#generate_if_else' do
    context 'when passed an empty scope' do
      let(:scope) {Pro::DynamicStagers::Code::Scope.new  }

      it 'generates new vars in the scope' do
        function.generate_if_else
        expect(function.scope.vars.keys.count).to eq Pro::DynamicStagers::Code::Scope::DEFAULT_MIN_VARS
      end

      it 'instantiates the new vars before the branch' do
        expect(function.generate_if_else).to match(/\A(#{assignment_regex})+/)
      end

      it 'produces an expected if/else statement' do
        expect(function.generate_if_else).to match(/\A#{if_else_regex}\Z/)
      end
    end
  end

end