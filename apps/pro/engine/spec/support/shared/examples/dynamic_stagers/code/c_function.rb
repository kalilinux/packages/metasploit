shared_examples_for 'C Function' do

  it { should respond_to :name}
  it { should respond_to :type}
  it { should respond_to :arguments}
  it { should respond_to :body}
  it { should respond_to :random_arguments }
  it { should respond_to :random_type }
  it { should respond_to :random_body }
  it { should respond_to :random_action }

  it 'should compile cleanly' do
    expect(function).to compile_in_metasm32_template
  end
end