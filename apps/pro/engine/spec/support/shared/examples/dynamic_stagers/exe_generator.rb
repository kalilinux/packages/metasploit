shared_examples_for 'a compilable dynamic payload' do
  context 'executable' do
    let(:service) { false }

    it 'compiles the source code cleanly' do
      expect{exe_generator.compile}.to_not raise_error
    end

    it 'returns a Metasm PE object' do
      expect(exe_generator.compile).to be_kind_of Metasm::PE
    end

    it 'returns a PE object with a manifest file' do
      mype = exe_generator.compile
      expect(mype.resource.to_hash).to have_key "MANIFEST"
    end
  end

  context 'service' do
    let(:service) { true }

    it 'compiles the source code cleanly' do
      expect{exe_generator.compile}.to_not raise_error
    end

    it 'returns a Metasm PE object' do
      expect(exe_generator.compile).to be_kind_of Metasm::PE
    end

    it 'returns a PE object with a manifest file' do
      mype = exe_generator.compile
      expect(mype.resource.to_hash).to have_key "MANIFEST"
    end
  end
end
