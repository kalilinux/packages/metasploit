shared_examples_for 'Pro::DynamicStagers::Code::Operation' do

  let(:variables) { %w{ foo bar baz bat} }

  it { should respond_to :generate_operation }
  it { should respond_to :generate_expression }


  describe '#generate_expression' do
    it 'returns a string' do
      expect(function.generate_expression(variables).class).to eq String
    end

    it 'strips trailing whitespace' do
      expect(function.generate_expression(variables)).to_not match(/\s$/)
    end

    it 'creates a string in an expected format' do
      expect(function.generate_expression(variables)).to match(/^#{expression_regex}$/)
    end
  end

  describe '#generate_operation' do
    context 'when passed a scope with at least 3 variables' do
      let(:scope){
        Pro::DynamicStagers::Code::Scope.new({
         "Foo" => Pro::DynamicStagers::Code::Var.new(name: 'Foo', type: 'int', value: '8'),
         "Bar" => Pro::DynamicStagers::Code::Var.new(name: 'Bar', type: 'int', value: '8'),
         "Baz" => Pro::DynamicStagers::Code::Var.new(name: 'Baz', type: 'int', value: '8')
        })
      }

      it 'generates an expression assigned to one of the vars' do
        expect(function.generate_operation).to match(/^(Foo|Bar|Baz) = #{expression_regex};$/)
      end

      it 'does not add anything to the scope' do
        function.generate_operation
        expect(function.scope.vars.keys).to match_array ["Foo", "Bar", "Baz"]
      end

    end

    context 'when pass a scope with no variables' do
      let(:scope) { Pro::DynamicStagers::Code::Scope.new }

      it 'declares new variables before the expression' do
        expect(function.generate_operation).to match(/^#{operation_regex}$/)
      end

      it 'adds the new variables to the scope' do
        function.generate_operation
        expect(function.scope.vars.keys.count).to eq 3
      end
    end

  end

end