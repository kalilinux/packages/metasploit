shared_context 'Metasploit::Pro::Engine::Application#framework' do
  include_context 'Msf::Simple::Framework'

  let(:metasploit_pro_engine_application_framework) {
    framework.tap { |framework|
      framework.extend(::Pro::License::Product)

      license_pathname = Rails.root.join('license')
      license_path = license_pathname.to_s
      framework.esnecil_init(license_path)
      ::Pro::Hooks::Loader.start(framework)
    }
  }
end