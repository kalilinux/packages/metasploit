shared_context "dynamic stager regex matchers" do


  let(:assignment_regex) {'\s*\w+ \w+ = \w+;\s*'}
  let(:expression_regex) {'\s*\w+ [*+&<>|-]{1,2} \w+( [*+&<>|-]{1,2} \w+){0,2}\s*'}
  let(:operation_regex)  {'\s*('+assignment_regex+')*\w+ = ' + expression_regex + ';'}
  let(:if_else_regex)    {'\s*('+ assignment_regex + '){0,3}if\(' + expression_regex + '|\w+\(\);\)\s*{\s*' + operation_regex + '\s*}\s*else{\s*' + operation_regex + '\s*}\s*'}
  let(:for_loop_regex)   {'\s*(' + assignment_regex + '){0,4}for\(\w+ (\w+)=0; \2 < \d+; \2\+\+\){\s*('+ operation_regex + '\s*|\w+\(\);\s*){1,4}}\s*'}
end