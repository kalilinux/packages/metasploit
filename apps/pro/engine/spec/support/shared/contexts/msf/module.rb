shared_context 'Msf::Module' do
  include_context 'Msf::Simple::Framework'

  # Assumes that the describe is passed the module_full_name like so
  #
  # @example
  #   describe 'auxiliary/pro/social_engineering/web_phish' do
  #     include_context 'Msf::Module'
  #   end
  #
  module_full_name_parts = description.split('/')
  module_type = module_full_name_parts[0]
  module_reference_name = module_full_name_parts[1 .. -1].join('/')
  module_base_name = module_full_name_parts[-1]

  let(:described_class) do
    subject.class
  end

  let(:loader) do
    loader = framework.modules.send(:loaders).find { |loader|
      loader.loadable?(modules_path)
    }

    # Override load_error so that rspec will print it instead of going to framework log
    def loader.load_error(module_path, error)
      raise error
    end

    loader
  end

  let(:module_set) do
    framework.send(module_type)
  end

  let(:modules_path) do
    Rails.application.paths['modules'].existent_directories.first
  end

  subject(module_base_name) do
    # make sure the module is loaded into the module_set
    loaded = loader.load_module(modules_path, module_type, module_reference_name)

    unless loaded
      module_path = loader.module_path(parent_path, type, module_reference_name)

      fail "#{description} failed to load from #{module_path}"
    end

    module_set.create(module_reference_name)
  end
end