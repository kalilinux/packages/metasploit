#define IPPROTO_TCP 6
#define AF_INET 2
#define HTTP_QUERY_CONTENT_LENGTH 5
#define HTTP_QUERY_FLAG_NUMBER 0x20000000
#define INTERNET_FLAG_IGNORE_CERT_CN_INVALID 0x00001000
#define INTERNET_FLAG_IGNORE_CERT_DATE_INVALID 0x00002000
#define INTERNET_FLAG_SECURE 0x00800000
#define INTERNET_OPEN_TYPE_PRECONFIG 0
#define INTERNET_OPTION_SECURITY_FLAGS 31
#define INTERNET_SERVICE_HTTP 3
#define INVALID_SOCKET (SOCKET)(~0)
#define MAKEWORD(a, b) ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MEM_COMMIT 0x1000
#define NULL ((void *)0)
#define PAGE_EXECUTE_READWRITE 0x40
#define RegisterServiceCtrlHandler RegisterServiceCtrlHandlerA
#define RtlZeroMemory(Destination, Length) memset((Destination),0,(Length))
#define SECURITY_FLAG_IGNORE_UNKNOWN_CA 0x00000100
#define SERVICE_ACCEPT_SHUTDOWN 0x00000004
#define SERVICE_ACCEPT_STOP 0x00000001
#define SERVICE_CONTROL_SHUTDOWN 0x00000005
#define SERVICE_CONTROL_STOP 0x00000001
#define SERVICE_RUNNING 0x00000004
#define SERVICE_START_PENDING 0x00000002
#define SERVICE_STOPPED 0x00000001
#define SERVICE_WIN32_SHARE_PROCESS 0x00000020
#define SOCKET_ERROR (-1)
#define SOCK_STREAM 1
#define StartServiceCtrlDispatcher StartServiceCtrlDispatcherA
#define TRUE 1
#define FALSE 0
#define VOID void
#define WINAPI __stdcall
#define _tWinMain WinMain
#define s_addr S_un.S_addr
#define APIENTRY WINAPI
#define ZeroMemory RtlZeroMemory

typedef int BOOL;
typedef unsigned char BYTE;
typedef char CHAR;
typedef unsigned long DWORD;
typedef __stdcall int (*FARPROC)();

struct HINSTANCE__ {
	int unused;
};
typedef void *LPVOID;

struct SERVICE_STATUS_HANDLE__ {
	int unused;
};
typedef unsigned char UCHAR;
typedef unsigned int UINT;
typedef unsigned int UINT_PTR;
typedef unsigned long ULONG;
typedef unsigned long ULONG_PTR;
typedef unsigned short USHORT;
typedef unsigned short WORD;
__noreturn __cdecl void exit(int _Code);

struct hostent {
	char *h_name;
	char **h_aliases;
	short h_addrtype;
	short h_length;
	char **h_addr_list;
};
typedef unsigned int size_t;
typedef unsigned short u_short;


typedef ULONG_PTR DWORD_PTR;
__noreturn __stdcall void ExitProcess __attribute__((dllimport))(UINT uExitCode);
__stdcall DWORD GetLastError __attribute__((dllimport))(void);
typedef struct HINSTANCE__ *HINSTANCE;
typedef LPVOID HINTERNET;
typedef WORD INTERNET_PORT;
typedef const CHAR *LPCSTR;
typedef DWORD *LPDWORD;
typedef __stdcall void (*LPHANDLER_FUNCTION)(DWORD dwControl);
typedef CHAR *LPSTR;
typedef struct SERVICE_STATUS_HANDLE__ *SERVICE_STATUS_HANDLE;
typedef UINT_PTR SOCKET;
__stdcall void Sleep __attribute__((dllimport))(DWORD dwMilliseconds);
typedef BOOL* PBOOL;
typedef void * HANDLE;

struct WSAData {
	WORD wVersion;
	WORD wHighVersion;
	char szDescription[257];
	char szSystemStatus[129];
	unsigned short iMaxSockets;
	unsigned short iMaxUdpDg;
	char *lpVendorInfo;
};

struct _SERVICE_STATUS {
	DWORD dwServiceType;
	DWORD dwCurrentState;
	DWORD dwControlsAccepted;
	DWORD dwWin32ExitCode;
	DWORD dwServiceSpecificExitCode;
	DWORD dwCheckPoint;
	DWORD dwWaitHint;
};

struct in_addr {

	union {

		struct {
			UCHAR s_b1;
			UCHAR s_b2;
			UCHAR s_b3;
			UCHAR s_b4;
		} S_un_b;

		struct {
			USHORT s_w1;
			USHORT s_w2;
		} S_un_w;
		ULONG S_addr;
	} S_un;
};
__cdecl void *memset(void *_Dst, int _Val, size_t _Size);

const struct sockaddr {
	u_short sa_family;
	CHAR sa_data[14];
};

typedef __stdcall void (*LPSERVICE_MAIN_FUNCTIONA)(DWORD dwNumServicesArgs, LPSTR *lpServiceArgVectors);

struct _SERVICE_TABLE_ENTRYA {
	LPSTR lpServiceName;
	LPSERVICE_MAIN_FUNCTIONA lpServiceProc;
};

typedef struct _SERVICE_TABLE_ENTRYA SERVICE_TABLE_ENTRYA;
typedef struct _SERVICE_STATUS *LPSERVICE_STATUS;
typedef struct _SERVICE_STATUS SERVICE_STATUS;


typedef HINSTANCE HMODULE;
typedef struct in_addr IN_ADDR;
typedef struct _SERVICE_STATUS *LPSERVICE_STATUS;
typedef struct WSAData *LPWSADATA;
__stdcall SERVICE_STATUS_HANDLE RegisterServiceCtrlHandlerA __attribute__((dllimport))(LPCSTR lpServiceName, LPHANDLER_FUNCTION lpHandlerProc);
typedef struct _SERVICE_STATUS SERVICE_STATUS;
typedef const struct sockaddr SOCKADDR;
typedef struct WSAData WSADATA;


__stdcall FARPROC GetProcAddress __attribute__((dllimport))(HMODULE hModule, LPCSTR lpProcName);
__stdcall HMODULE LoadLibraryA __attribute__((dllimport))(LPCSTR lpLibFileName);
__stdcall BOOL SetServiceStatus __attribute__((dllimport))(SERVICE_STATUS_HANDLE hServiceStatus, LPSERVICE_STATUS lpServiceStatus);
__stdcall BOOL StartServiceCtrlDispatcherA __attribute__((dllimport))(const SERVICE_TABLE_ENTRYA *lpServiceStartTable __attribute__((in)));

struct sockaddr_in {
	short sin_family;
	USHORT sin_port;
	IN_ADDR sin_addr;
	CHAR sin_zero[8];
};


#ifndef _METASPLOIT_PAYLOAD_COMMON_COMMON_H
#define _METASPLOIT_PAYLOAD_COMMON_COMMON_H

// Define this symbol to disable string obfuscation in the source.
//#define DISABLE_HIDE_STRINGS

#ifdef DISABLE_HIDE_STRINGS
#define XOR_KEY (char)0
#define XOR(s)
#define LIBLOAD(var, name) HMODULE var = LoadLibraryA(name)
#define RDLLOAD(var, type, module, name) type var = (type)GetProcAddress(module, name)
#else
#define XOR_KEY (char)0xFF
#define XOR(s) {for(int _c= 0;_c<(sizeof(s)/sizeof(s[0]));++_c){s[_c]^=XOR_KEY;}}
#define LIBLOAD(var, name) XOR(name);HMODULE var = LoadLibraryA(name);XOR(name)
#define RDLLOAD(var, type, module, name) XOR(name);type var = (type)GetProcAddress(module, name);XOR(name)
#endif

#define CRYPT1(c1) (c1 ^ XOR_KEY)
#define CRYPT2(c1,c2) CRYPT1(c1),CRYPT1(c2)
#define CRYPT3(c1,c2,c3) CRYPT1(c1),CRYPT2(c2,c3)
#define CRYPT4(c1,c2,c3,c4) CRYPT1(c1),CRYPT3(c2,c3,c4)
#define CRYPT5(c1,c2,c3,c4,c5) CRYPT1(c1),CRYPT4(c2,c3,c4,c5)
#define CRYPT6(c1,c2,c3,c4,c5,c6) CRYPT1(c1),CRYPT5(c2,c3,c4,c5,c6)
#define CRYPT7(c1,c2,c3,c4,c5,c6,c7) CRYPT1(c1),CRYPT6(c2,c3,c4,c5,c6,c7)
#define CRYPT8(c1,c2,c3,c4,c5,c6,c7,c8) CRYPT1(c1),CRYPT7(c2,c3,c4,c5,c6,c7,c8)
#define CRYPT9(c1,c2,c3,c4,c5,c6,c7,c8,c9) CRYPT1(c1),CRYPT8(c2,c3,c4,c5,c6,c7,c8,c9)
#define CRYPT10(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10) CRYPT1(c1),CRYPT9(c2,c3,c4,c5,c6,c7,c8,c9,c10)
#define CRYPT11(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11) CRYPT1(c1),CRYPT10(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11)
#define CRYPT12(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12) CRYPT1(c1),CRYPT11(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12)
#define CRYPT13(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13) CRYPT1(c1),CRYPT12(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13)
#define CRYPT14(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14) CRYPT1(c1),CRYPT13(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14)
#define CRYPT15(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15) CRYPT1(c1),CRYPT14(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15)
#define CRYPT16(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16) CRYPT1(c1),CRYPT15(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16)
#define CRYPT17(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17) CRYPT1(c1),CRYPT16(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17)
#define CRYPT18(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18) CRYPT1(c1),CRYPT17(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18)
#define CRYPT19(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19) CRYPT1(c1),CRYPT18(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19)
#define CRYPT20(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20) CRYPT1(c1),CRYPT19(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20)
#define CRYPT21(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21) CRYPT1(c1),CRYPT20(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21)
#define CRYPT22(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22) CRYPT1(c1),CRYPT21(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22)
#define CRYPT23(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23) CRYPT1(c1),CRYPT22(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23)
#define CRYPT24(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24) CRYPT1(c1),CRYPT23(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24)
#define CRYPT25(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25) CRYPT1(c1),CRYPT24(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25)
#define CRYPT26(c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26) CRYPT1(c1),CRYPT25(c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26)




#endif


%{DEFINITION}

void main(void)
{
    %{CALLER};
}