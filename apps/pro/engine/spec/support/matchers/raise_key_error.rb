RSpec::Matchers.define :raise_key_error do |key, err_msg=nil|
  match do |actual|
    err_msg ||= "key not found: :#{key.to_s}"
    expect(actual).to raise_error(KeyError, err_msg)
  end

  failure_message_for_should do |actual|
    "expected calling #initialize with a :#{key} => nil to raise a KeyError"
  end
end
