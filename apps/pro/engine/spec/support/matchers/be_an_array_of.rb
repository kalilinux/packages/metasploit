RSpec::Matchers.define :be_an_array_of do |expected|
  define_method :is_array_of? do |actual|
    @errors = []
    retval = true
    unless actual.kind_of? Array
      @errors << "* Expected #{actual} to be Array, got a #{actual.class}"
      return false
    end

    actual.each_with_index do |element, index|
      unless element.kind_of? expected
        retval = false
        @errors << "* Expected element at index #{index} to be a #{expected}, got #{element.class}"
      end
    end
    retval
  end

  match do |actual|
    is_array_of?(actual)
  end

  failure_message_for_should do |actual|
    "Expected #{actual} to be an Array of #{expected} :\n\n#{@errors.join("\n")}"
  end

end