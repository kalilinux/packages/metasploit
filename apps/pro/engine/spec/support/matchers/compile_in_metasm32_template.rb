RSpec::Matchers.define :compile_in_metasm32_template do
  match do |actual|
    matched = true
    template_pathname = Pathname.new(__FILE__).parent.parent.join('file_fixtures', 'basic_exe_template.c')
    begin
      template = template_pathname.read
      sub_hash = {
          DEFINITION: function.definition,
          CALLER: function.invocation
      }
      code = template % sub_hash
      cpu = Metasm::Ia32.new
      Metasm::PE.compile_c(cpu, code)
    rescue ::Exception => e
      @compile_error = e.message
      matched = false
    end
    matched
  end

  failure_message_for_should do |actual|
    "#{@compile_error}\n Function Definition: \n\n #{actual.definition}"
  end
end