require 'spec_helper'

describe 'auxiliary/pro/social_engineering/web_phish' do
  include_context 'Msf::Module'

  context '#create_phishing_result!' do
    before(:each) do
      request.stub(:web_page).and_return web_page
    end

    let(:json_data) do
      JSON.generate({})
    end

    let(:web_page) do
      FactoryGirl.create(:social_engineering_web_page)
    end

    let(:request) do
      request = Rex::Proto::Http::Request.new
      request.extend described_class::VictimRequest

      request
    end

    context 'with trackable SocialEngineering::VisitRequest' do
      it 'should create a SocialEngineering::PhishingResult' do
        expect {
          web_phish.create_phishing_result!(json_data, request)
        }.to change(SocialEngineering::PhishingResult, :count).by(1)
      end

      context 'phishing result' do
        let(:client_address) do
          '127.0.0.1'
        end

        let(:body) do
          '<html></html>'
        end

        let(:phishing_result) do
          SocialEngineering::PhishingResult.last
        end


        before(:each) do
          request.stub(
              :body => body,
              :client_address => client_address,
              :web_page => web_page
          )

          web_phish.create_phishing_result!(json_data, request)
        end

        it 'should have address from request client_address' do
          phishing_result.address.should == client_address
        end

        it 'should have data from json_data' do
          phishing_result.data.should == json_data
        end

        it 'should have raw_data from the request body' do
          phishing_result.raw_data.should == body
        end

        it 'should have web_page from the request web_page' do
          phishing_result.web_page.should == web_page
        end

        context 'with user agent' do
          let(:encoded_operating_system_name) do
            'Windows'
          end

          # User-Agent string for Windows 7
          let(:encoded_operating_system_flavor) do
            'Windows NT 6.1'
          end

          let(:json_data) do
            JSON.generate(
                {
                    'User-Agent' => user_agent
                }
            )
          end

          let(:operating_system_flavor) do
            '7'
          end

          let(:operating_system_name) do
            'Windows'
          end

          let(:user_agent) do
            "Mozilla/5.0 " \
            "(#{encoded_operating_system_name}; " \
            "U; " \
             "#{encoded_operating_system_flavor}; " \
             "en-US" \
            ") " \
            "AppleWebKit/534.3 (KHTML, like Gecko) " \
            "#{user_agent_name}/#{user_agent_version} " \
            "Safari/534.3"
          end

          let(:user_agent_name) do
            'Chrome'
          end

          let(:user_agent_version) do
            '6.0.472.63'
          end

          it 'should parse user agent with #fingerprint_user_agent' do
            web_phish.should_receive(:fingerprint_user_agent).with(user_agent).and_call_original

            web_phish.create_phishing_result!(json_data, request)
          end

          it 'should have browser_name from user_agent :ua_name' do
            phishing_result.browser_name.should == user_agent_name
          end

          it 'should have browser_version from user_agent :ua_ver' do
            phishing_result.browser_version.should == user_agent_version
          end

          it 'should have os_name from user_agent :os_name' do
            phishing_result.os_name.should == "#{operating_system_name} #{operating_system_flavor}"
          end
        end

        context 'without user agent' do
          let(:json_data) do
            JSON.generate({})
          end

          it "should have 'Unknown' for browser_name" do
            phishing_result.browser_name.should == 'Unknown'
          end

          it 'should have nil for browser_version' do
            phishing_result.browser_version.should be_nil
          end

          it "should have 'Unknown' for os_name" do
            phishing_result.os_name.should == 'Unknown'
          end
        end
      end
    end

    context 'without trackable SocialEngineering::VisitRequest' do
      it 'should not create any SocialEngineering::PhishingResults' do
        expect {
          web_phish.create_phishing_result!(json_data, request)
        }.to_not change(SocialEngineering::PhishingResult, :count).by(0)
      end
    end
  end
end