require 'spec_helper'

describe 'auxiliary/pro/social_engineering/portable_file_generate' do
  include_context 'Msf::Module'

  it 'can be instantiated' do
    expect {
      portable_file_generate
    }.not_to raise_error
  end
end