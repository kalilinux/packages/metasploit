require 'spec_helper'

describe 'auxiliary/pro/social_engineering/campaign_commander' do
  include_context 'Msf::Module'

  it 'can be instantiated' do
    expect {
      campaign_commander
    }.not_to raise_error
  end
end