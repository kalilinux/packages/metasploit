require 'spec_helper'

describe 'auxiliary/pro/social_engineering/usb_key_generate_exe' do
  include_context 'Msf::Module'

  it 'can be instantiated' do
    expect {
      usb_key_generate_exe
    }.not_to raise_error
  end
end