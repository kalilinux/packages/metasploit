require 'spec_helper'

describe 'auxiliary/pro/apps/ssh_key' do
  include_context 'Msf::Module'

  it 'can be instantiated' do
    expect {
      ssh_key
    }.not_to raise_error
  end
end