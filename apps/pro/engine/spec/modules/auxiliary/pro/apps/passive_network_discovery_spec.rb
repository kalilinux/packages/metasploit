require 'spec_helper'

describe 'auxiliary/pro/apps/passive_network_discovery' do
  include_context 'Msf::Module'

  it 'can be instantiated' do
    expect {
      passive_network_discovery
    }.not_to raise_error
  end
end