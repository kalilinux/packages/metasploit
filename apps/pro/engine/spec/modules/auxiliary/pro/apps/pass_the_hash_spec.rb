require 'spec_helper'

describe 'auxiliary/pro/apps/pass_the_hash' do
  include_context 'Msf::Module'

  it 'can be instantiated' do
    expect {
      pass_the_hash
    }.not_to raise_error
  end
end