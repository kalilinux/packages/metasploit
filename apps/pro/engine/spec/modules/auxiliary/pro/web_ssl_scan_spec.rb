require 'spec_helper'

describe 'auxiliary/pro/web_ssl_scan' do
  include_context 'Msf::Module'

  let(:rhost) do
    FactoryGirl.generate :mdm_ipv4_address
  end

  let(:rport) do
    FactoryGirl.generate :port
  end

  let(:workspace) do
    FactoryGirl.create(:mdm_workspace)
  end

  before(:each) do
    web_ssl_scan.datastore['RHOST'] = rhost
    web_ssl_scan.datastore['RPORT'] = rport
    # WORKSPACE option is the Mdm::Workspace#name since you can't store an Mdm::Workspace in the datastore.
    web_ssl_scan.datastore['WORKSPACE'] = workspace.name
  end

  context '#base_vuln_opts' do
    subject(:base_vuln_opts) do
      web_ssl_scan.base_vuln_opts
    end

    its([:blame]) { should == 'App Developer' }

    context 'category' do
      subject(:category) do
        base_vuln_opts[:category]
      end

      it { should_not be_nil }

      it 'should be a Web::VulnCategory::Metasploit#name' do
        Web::VulnCategory::Metasploit.where(:name => category).should_not be_empty
      end
    end

    its([:confidence]) { should == 90 }
    its([:method]) { should == 'GET' }
    its([:params]) { should == [] }
    its([:path]) { should == '/' }
    its([:pname]) { should == 'server' }
    its([:risk]) { should == 2 }

    it "should use datastore['RHOST'] for host" do
      base_vuln_opts[:host].should == rhost
    end

    it "should use datastore['RHOST'] for vhost" do
      base_vuln_opts[:vhost].should == rhost
    end

    it "should use datastore['RPORT'] for port" do
      base_vuln_opts[:port].should == rport
    end
  end

  context '#run' do
    def run
      web_ssl_scan.run
    end

    let(:report_weak) do
      false
    end

    let(:result) do
      Rex::SSLScan::Result.new
    end

    let(:ssl_required) do
      false
    end

    before(:each) do
      Rex::SSLScan::Scanner.any_instance.stub(
          :scan => result,
          :valid? => true
      )

      Msf::DBManager.send(:include, Web::VulnCategory::Mixin)
      framework.db.stub(:active => true)
      web_ssl_scan.datastore['SSL_REQUIRED'] = ssl_required
      web_ssl_scan.datastore['REPORT_WEAK'] = report_weak
    end

    it 'should have Pro report_web_vuln monkey patch' do
      framework.db.should be_a Web::VulnCategory::Mixin
    end

    it 'should print_status when SSL Scan is Inititated' do
      web_ssl_scan.should_receive(:print_status) do |status|
        status.should include(rhost.to_s)
        status.should include(rport.to_s)
        status.should include('SSL Scan Initiated')
      end

      # ignore other calls to print_status
      web_ssl_scan.should_receive(:print_status).at_least(1)

      run
    end

    it 'should create a Rex::SSLScan::Scanner' do
      Rex::SSLScan::Scanner.should_receive(:new).with(
          rhost,
          rport,
          hash_including(
              'Msf' => framework,
              'MsfExploit' => web_ssl_scan
          )
      ).and_call_original

      run
    end

    it 'should use scanner to scan host port' do
      Rex::SSLScan::Scanner.any_instance.should_receive(:scan).and_return(result)

      run
    end

    it 'should print the scan result' do
      web_ssl_scan.should_receive(:print_status).once

      web_ssl_scan.should_receive(:print_status) do |result_string|
        result_string.should == result.to_s
      end

      run
    end

    context 'SSL_REQUIRED' do
      context 'with true' do
        let(:ssl_required) do
          true
        end

        before(:each) do
          result.stub(:supports_ssl? => supports_ssl?)
        end

        context 'with supports ssl' do
          let(:supports_ssl?) do
            true
          end

          it 'should not create an Mdm::WebVuln' do
            expect {
              run
            }.to_not change(Mdm::WebVuln, :count)
          end
        end

        context 'without supports ssl' do
          let(:supports_ssl?) do
            false
          end

          it 'should create an Mdm::WebVuln' do
            expect {
              run
            }.to change(Mdm::WebVuln, :count).by(1)
          end

          it 'should use #proof_table for proof' do
            proof = "Proof Table"
            web_ssl_scan.should_receive(:proof_table).with(result).and_return(proof)

            run

            web_vuln = Mdm::WebVuln.last
            web_vuln.proof.should == proof
          end

          it 'should print good that server is not running over ssl' do
            web_ssl_scan.should_receive(:print_good) do |good|
              good.should include(rhost.to_s)
              good.should include(rport.to_s)
              good.should include('VULNERABLE Server is not running over SSL')
            end

            run
          end

          context 'created Mdm::WebVuln' do
            subject(:web_vuln) do
              Mdm::WebVuln.first
            end

            before(:each) do
              run
            end

            context 'category' do
              subject(:category) do
                web_vuln.category
              end

              it "should be Web::VulnCategory::Metasploit with name 'Transport-Layer-Encryption'" do
                category.should == Web::VulnCategory::Metasploit.where(:name => 'Transport-Layer-Encryption').first
              end
            end

            context 'web_site' do
              subject(:web_site) do
                web_vuln.web_site
              end

              it 'should get vhost from RHOST' do
                web_site.vhost.should == rhost
              end

              context 'service' do
                subject(:service) do
                  web_site.service
                end

                context 'host' do
                  subject(:host) do
                    service.host
                  end

                  it 'should get name from RHOST' do
                    host.name.should == rhost
                  end

                  it 'should get address from RHOST' do
                    host.address.should == rhost
                  end
                end

                it 'should get port from RPORT' do
                  service.port.should == rport
                end
              end
            end

            it 'should get blame from base_vuln_opts' do
              web_vuln.blame.should == web_ssl_scan.base_vuln_opts[:blame]
            end

            its(:description) { should == 'The specified site is running over an unencrypted transport layer' }

            it 'should get method from base_vuln_opts' do
              web_vuln.method.should == web_ssl_scan.base_vuln_opts[:method]
            end

            its(:name) { should == 'Unencrypted Transport Layer' }

            it 'should get params from base_vuln_opts' do
              web_vuln.params.should == web_ssl_scan.base_vuln_opts[:params]
            end

            it 'should get path from base_vuln_opts' do
              web_vuln.path.should == web_ssl_scan.base_vuln_opts[:path]
            end

            it 'should get pname from base_vuln_opts' do
              web_vuln.pname.should == web_ssl_scan.base_vuln_opts[:pname]
            end

            it 'should get risk from base_vuln_opts' do
              web_vuln.risk.should == web_ssl_scan.base_vuln_opts[:risk]
            end
          end
        end
      end

      context 'with false' do
        let(:ssl_required) do
          false
        end

        it 'should not check if ssl is supported in result' do
          # need to stub to_s because it calls supprts_ssl? internally and want to check that run doesn't call
          # supports_ssl? directly, not indirectly.
          result.stub(:to_s => 'Result')

          result.should_not_receive(:supports_ssl?)

          run
        end
      end
    end

    context 'REPORT_WEAK' do
      context 'with true' do
        let(:report_weak) do
          true
        end

        before(:each) do
          result.stub(:standards_compliant? => standards_compliant?)
        end

        context 'with standards compliant' do
          let(:standards_compliant?) do
            true
          end

          it 'should not create an Mdm::WebVuln' do
            expect {
              run
            }.to_not change(Mdm::WebVuln, :count)
          end
        end

        context 'without standards compliant' do
          let(:standards_compliant?) do
            false
          end

          it 'should create an Mdm::WebVuln' do
            expect {
              run
            }.to change(Mdm::WebVuln, :count).by(1)
          end

          it 'should use #proof_table for proof' do
            proof = "Proof Table"
            web_ssl_scan.should_receive(:proof_table).with(result).and_return(proof)

            run

            web_vuln = Mdm::WebVuln.last
            web_vuln.proof.should == proof
          end

          it 'should print good that server is using weak ciphers' do
            web_ssl_scan.should_receive(:print_good) do |good|
              good.should include(rhost.to_s)
              good.should include(rport.to_s)
              good.should include('VULNERABLE Server is using Weak Ciphers')
            end

            run
          end

          context 'created Mdm::WebVuln' do
            subject(:web_vuln) do
              Mdm::WebVuln.first
            end

            before(:each) do
              run
            end

            context 'category' do
              subject(:category) do
                web_vuln.category
              end

              it "should be Web::VulnCategory::Metasploit with name 'Transport-Layer-Encryption'" do
                category.should == Web::VulnCategory::Metasploit.where(:name => 'Transport-Layer-Encryption').first
              end
            end

            context 'web_site' do
              subject(:web_site) do
                web_vuln.web_site
              end

              it 'should get vhost from RHOST' do
                web_site.vhost.should == rhost
              end

              context 'service' do
                subject(:service) do
                  web_site.service
                end

                context 'host' do
                  subject(:host) do
                    service.host
                  end

                  it 'should get name from RHOST' do
                    host.name.should == rhost
                  end

                  it 'should get address from RHOST' do
                    host.address.should == rhost
                  end
                end

                it 'should get port from RPORT' do
                  service.port.should == rport
                end
              end
            end

            it 'should get blame from base_vuln_opts' do
              web_vuln.blame.should == web_ssl_scan.base_vuln_opts[:blame]
            end

            its(:description) {
              should == 'The specified site supports weak versions of SSL and/or weak encryption ciphers'
            }

            it 'should get method from base_vuln_opts' do
              web_vuln.method.should == web_ssl_scan.base_vuln_opts[:method]
            end

            its(:name) { should == 'Weak Encryption Supported' }

            it 'should get params from base_vuln_opts' do
              web_vuln.params.should == web_ssl_scan.base_vuln_opts[:params]
            end

            it 'should get path from base_vuln_opts' do
              web_vuln.path.should == web_ssl_scan.base_vuln_opts[:path]
            end

            it 'should get pname from base_vuln_opts' do
              web_vuln.pname.should == web_ssl_scan.base_vuln_opts[:pname]
            end

            it 'should get risk from base_vuln_opts' do
              web_vuln.risk.should == web_ssl_scan.base_vuln_opts[:risk]
            end
          end
        end
      end

      context 'with false' do
        let(:report_weak) do
          false
        end

        it 'should not check if standards compliant in result' do
          result.should_not_receive(:standards_compliant?)

          run
        end
      end
    end
  end
end
