require 'spec_helper'

describe 'modules/payloads' do
  modules_pathname = Pathname.new(__FILE__).parent.parent.parent.parent.join('modules')

  include_context 'untested payloads', modules_pathname: modules_pathname
end
