require 'spec_helper'

describe Msf::Simple::Framework do
  include_context 'Msf::Simple::Framework'

  context '#init_module_paths' do
    subject(:init_module_paths) {
      framework.init_module_paths
    }

    it 'adds pro/modules to module paths' do
      init_module_paths

      expect(framework.modules.send(:module_paths)).to include(Rails.root.parent.join('modules').expand_path.to_path)
    end
  end
end