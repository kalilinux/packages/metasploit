require 'spec_helper'

module Pro
  describe ProTask do
    #We remove transactions for this spec b/c we spawn a new thread when we call pro_task.start
    self.use_transactional_fixtures = false
    DatabaseCleaner.strategy = :truncation, {only: %w[workspaces users tasks ]}

    it { expect(::Pro::ProTask::Status::DONE).to eql(:done) }
    it { expect(::Pro::ProTask::Status::NEW).to eql(:new) }
    it { expect(::Pro::ProTask::Status::RUNNING).to eql(:running) }

    # Used to represent fake attrs for a ProTask
    let(:fake_proc){ Proc.new{|t| t.nil?} }
    let(:mdm_task){ FactoryGirl.create(:mdm_task) }
    let(:metasploit_module){ double("metasploit module") }
    let(:metasploit_module_refname){ "auxiliary/fake/something-awesome" }
    let(:metasploit_module_fullname){ "Something Awesome" }
    let(:thread){ double("thread") }

    subject(:pro_task){ Pro::ProTask.new(fake_proc, {}) }


    before(:each) do
      DatabaseCleaner.clean
      # Speed up the spec runs
      stub_const("Pro::ProTask::METASPLOIT_MODULE_CLEANUP_SLEEP_DURATION", 0.01)

      # No reason to create notifications during the test runs
      pro_task.stub(:create_notification).and_return true

      pro_task.stub(:on_stop).and_return fake_proc
      pro_task.stub(:metasploit_module).and_return metasploit_module

      metasploit_module.stub(:refname).and_return metasploit_module_refname
      metasploit_module.stub(:fullname).and_return metasploit_module_fullname
      metasploit_module.stub(:stop_task).and_return true

      pro_task.thread = thread
      thread.stub(:kill)
      thread.stub(:alive?).and_return true
    end


    describe "#start and #stop" do

      before(:each) do
        metasploit_module.stub(:cleanup).and_return true
        metasploit_module.stub(:user_output).and_return StringIO.new
        metasploit_module.stub(:user_output=).and_return nil
      end

      after(:all) do
        DatabaseCleaner.clean
        self.use_transactional_fixtures = true
      end


      describe "#start" do

        before(:each) do
          pro_task.stub(:requested_task_action).and_return Pro::ProTask::START
          pro_task.record = mdm_task
        end

        it 'should set self.thread' do
          expect(pro_task).to receive(:thread=)
          pro_task_thread = pro_task.start
          pro_task_thread.join
        end

        it 'should call cleanup' do
          expect(pro_task).to receive(:cleanup)
          pro_task_thread = pro_task.start
          pro_task_thread.join
        end

        it 'should call sync' do
          expect(pro_task).to receive(:sync)
          pro_task_thread = pro_task.start
          pro_task_thread.join
        end

        it 'should set completed_at' do
          expect(pro_task).to receive(:completed_at=)
          pro_task_thread = pro_task.start
          pro_task_thread.join
        end

      end

    end

  end
end
