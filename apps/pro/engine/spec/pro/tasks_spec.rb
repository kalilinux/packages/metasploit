require 'spec_helper'

module Pro
  describe ProTask do
    it { expect(::Pro::ProTask::Status::DONE).to eql(:done) }
    it { expect(::Pro::ProTask::Status::NEW).to eql(:new) }
    it { expect(::Pro::ProTask::Status::RUNNING).to eql(:running) }

    # Used to represent fake attrs for a ProTask
    let(:fake_proc){ Proc.new{|t| t.nil?} }
    let(:mdm_task){ FactoryGirl.create(:mdm_task) }
    let(:metasploit_module){ double("metasploit module") }
    let(:metasploit_module_refname){ "auxiliary/fake/something-awesome" }
    let(:metasploit_module_fullname){ "Something Awesome" }
    let(:thread){ double("thread") }

    subject(:pro_task){ Pro::ProTask.new(fake_proc, {}) }

    before(:each) do
      # Speed up the spec runs
      stub_const("Pro::ProTask::METASPLOIT_MODULE_CLEANUP_SLEEP_DURATION", 0.01)

      # No reason to create notifications during the test runs
      pro_task.stub(:create_notification).and_return true
      pro_task.stub(:record).and_return mdm_task

      pro_task.stub(:on_stop).and_return fake_proc
      pro_task.stub(:metasploit_module).and_return metasploit_module

      metasploit_module.stub(:refname).and_return metasploit_module_refname
      metasploit_module.stub(:fullname).and_return metasploit_module_fullname
      metasploit_module.stub(:stop_task).and_return true

      pro_task.thread = thread
      thread.stub(:kill)
      thread.stub(:alive?).and_return true
    end

    describe "#status" do
      context "when there is no thread" do
        before(:each) do
          pro_task.thread = nil
        end

        it 'should be NEW' do
          expect(pro_task.status).to eq(::Pro::ProTask::Status::NEW)
        end
      end

      context "when there is a thread but #alive? doesn't return true" do
        before(:each) do
          thread.stub(:alive?).and_return false
        end

        it 'should be DONE' do
          expect(pro_task.status).to eq(::Pro::ProTask::Status::DONE)
        end
      end

      context "when there is an alive thread" do
        before(:each) do
          thread.stub(:alive?).and_return true
        end

        it 'should be RUNNING' do
          expect(pro_task.status).to eq(::Pro::ProTask::Status::RUNNING)
        end
      end
    end

    describe "#start and #stop" do

      before(:each) do
        metasploit_module.stub(:cleanup).and_return true
        metasploit_module.stub(:user_output).and_return StringIO.new
        metasploit_module.stub(:user_output=).and_return nil
      end



      describe "#stop" do
        # Otherwise we are stuck in the wait loop
        before(:each) do
          pro_task.stub(:status).and_return Pro::ProTask::Status::DONE
        end

        it 'should call #cleanup' do
          expect(pro_task).to receive(:cleanup)
          pro_task.stop
        end

        it 'should call #sync' do
          expect(pro_task).to receive(:sync)
          pro_task.stop
        end

        context "when there is a Metasploit module in the #module attribute" do
          it 'should call the cleanup method for the module' do
            expect(pro_task.metasploit_module).to receive(:cleanup)
            pro_task.stop
          end
        end

        context "when there is a thread in the #thread attribute" do
          it 'should call #kill on the thread' do
            expect(pro_task.thread).to receive(:kill)
            pro_task.stop
          end
        end
      end

    end

    describe "#sync" do
      let(:new_info){ "reticulating splines" }
      let(:new_description){ "new description" }
      let(:new_progress){ 85 }
      let(:new_error){ "some bad error happened" }
      let(:new_result){ "new_result!" }

      context "when the record state is 'running' and the status is DONE" do
        before(:each) do
          pro_task.record.stub(:running?).and_return true
          pro_task.stub(:status).and_return Pro::ProTask::Status::DONE

          pro_task.info        = new_info
          pro_task.description = new_description
          pro_task.progress    = new_progress
          pro_task.error       = new_error
          pro_task.result      = new_result

          pro_task.record.info        = "old_value"
          pro_task.record.description = "old_value"
          pro_task.record.progress    = "old_value"
          pro_task.record.error       = "old_value"
          pro_task.record.result      = "old_value"

          pro_task.sync
        end

        it 'should update the info attribute on the DB record' do
          expect(pro_task.record.info).to eql(new_info)
        end

        it 'should update the description attribute on the DB record' do
          expect(pro_task.record.description).to eql(new_description)
        end

        it 'should update the progress attribute on the DB record' do
          expect(pro_task.record.progress).to eql(new_progress)
        end

        it 'should update the error attribute on the DB record' do
          expect(pro_task.record.error).to eql(new_error)
        end

        it 'should update the result attribute on the DB record' do
          expect(pro_task.record.result).to eql(new_result)
        end
      end

      context "when the record state is not 'running' or the status is not DONE" do
        context "and record.info doesn't match #info" do
          before(:each) do
            pro_task.record.info =  "initializing splines"
            pro_task.info = new_info
            pro_task.sync
          end

          it 'should update record.info with the value of #info' do
            expect(pro_task.record.info).to eql(new_info)
          end
        end

        context "and record.description doesn't match #description" do
          before(:each) do
            pro_task.record.description = "old description"
            pro_task.description = new_description
            pro_task.sync
          end

          it 'should update record.description with the value of #description' do
            expect(pro_task.record.description).to eql(new_description)
          end
        end

        context "and record.progress doesn't match #progress" do
          before(:each) do
            pro_task.record.progress = 75
            pro_task.progress = new_progress
            pro_task.sync
          end

          it 'should update record.progress with the value of #progress' do
            expect(pro_task.record.progress).to eql(new_progress)
          end
        end

        context "and record.error doesn't match #error" do
          before(:each) do
            pro_task.record.error = nil
            pro_task.error = new_error
            pro_task.sync
          end

          it 'should update record.error with the value of #error' do
            expect(pro_task.record.error).to eql(new_error)
          end
        end

        context "and record.result doesn't match #result" do
          before(:each) do
            pro_task.record.result = "mah old result"
            pro_task.result = new_result
            pro_task.sync
          end

          it 'should update record.result with the value of #result' do
            expect(pro_task.record.result).to eql(new_result)
          end
        end
      end
    end

    describe "pausing and resuming" do
      before(:each) do
        pro_task.metasploit_module.stub(:blank?).and_return false
        pro_task.metasploit_module.stub(Metasploit::Pro::Engine::Task::PauseResume::PAUSE_METHOD_NAME)
        pro_task.metasploit_module.stub(Metasploit::Pro::Engine::Task::PauseResume::RESUME_METHOD_NAME)
        pro_task.stub(:end_task_thread)
      end

      describe "#task_supports_pause_resume?" do
        context "when the task's #metasploit_module supports pause and resume" do
          it { expect(pro_task.supports_pause_resume?).to eq(true) }
        end
      end

      describe "#pause" do
        context "when the task's #metasploit_module doesn't support pause/resume" do
          before(:each) do
            pro_task.stub(:supports_pause_resume?).and_return false
          end

          it 'should raise TaskModuleInterfaceError' do
            expect{pro_task.pause}.to raise_error Pro::ProTask::TaskModuleInterfaceError
          end
        end
      end

    end

    # TODO: fill these in at some point when the ProTask class is more sane,
    # like if we make it an ActiveModel.
    describe "#valid?" do
      it 'should be false without a #task_id'
      it 'should be false without a #record'
      it 'should be false without a #metasploit_module'
      it 'should be false without a #proc'
      it 'should be false without a #username'
      it 'should be false without a #workspace'

      context "#requested_action" do
        context "when #requested_action is in VALID_REQUESTED_ACTIONS" do
          context ":start" do
            before(:each) do
              pro_task.requested_task_action = Pro::ProTask::START
            end

            it { expect(pro_task).to be_valid }
          end

          context ":stop" do
            before(:each) do
              pro_task.requested_task_action = Pro::ProTask::STOP
            end

            it { expect(pro_task).to be_valid }
          end

          context ":pause" do
            before(:each) do
              pro_task.requested_task_action = Pro::ProTask::PAUSE
            end

            it { expect(pro_task).to be_valid }
          end
        end

        context "when #requested_action is not in VALID_REQUESTED_ACTIONS" do
          it 'should not be valid' do
            pro_task.requested_task_action = :dance
            expect(pro_task).to_not be_valid
          end
        end

      end

    end
  end
end
