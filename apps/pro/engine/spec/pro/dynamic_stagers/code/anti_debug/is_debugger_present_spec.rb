require 'spec_helper'

describe Pro::DynamicStagers::Code::AntiDebug::IsDebuggerPresent do

  subject(:function){
    described_class.new
  }

  let(:definition){
    "void is_debugger_present() {\n\t          "+
    "typedef BOOL (__stdcall *IsDebuggerPresentProto)(void);\n"+
    "          char cbKernel[] = { CRYPT12('k','e','r','n','e','l','3','2','.','d','l','l'),XOR_KEY };\n"+
    "          char cbIsDebuggerPresent[] = { CRYPT17('I','s','D','e','b','u','g','g','e','r','P','r',"+
    " 'e', 's', 'e', 'n', 't'),XOR_KEY };\n          LIBLOAD(hKernel, cbKernel);\n"+
    "          RDLLOAD(IsDebuggerPresentRDL, IsDebuggerPresentProto, hKernel, cbIsDebuggerPresent);\n"+
    "          if (IsDebuggerPresentRDL()){\n            exit(0);\n          }\n\n}"
  }

  let(:prototype){ "void is_debugger_present();" }
  let(:invocation) {"is_debugger_present()" }


  it_should_behave_like 'C Function'
  it_should_behave_like 'an anti-debugging function'


end