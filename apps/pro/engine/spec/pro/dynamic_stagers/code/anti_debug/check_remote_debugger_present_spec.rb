require 'spec_helper'

describe Pro::DynamicStagers::Code::AntiDebug::CheckRemoteDebuggerPresent do

  subject(:function){
    described_class.new
  }

  let(:definition){
    "void check_remote_debugger_present() {\n\t"+
    "          typedef BOOL (__stdcall *CheckRemoteDebuggerPresentProto)"+
    "(HANDLE,PBOOL);\n          typedef HANDLE (__stdcall *GetCurrentProcessProto)(void);\n\n"+
    "          char cbKernel[] = { CRYPT12('k','e','r','n','e','l','3','2','.','d','l','l'),XOR_KEY };\n"+
    "          char cbCheckRemoteDebuggerPresent[] = { CRYPT26('C','h','e','c','k','R','e','m','o','t','e'"+
    ",'D', 'e', 'b', 'u', 'g', 'g', 'e', 'r', 'P', 'r', 'e', 's', 'e', 'n', 't'),XOR_KEY };\n"+
    "          char cbGetCurrentProcess[] = { CRYPT17('G','e','t','C','u','r','r','e','n','t','P','r', 'o',"+
    " 'c', 'e', 's', 's'),XOR_KEY };\n\n          LIBLOAD(hKernel, cbKernel);\n\n"+
    "          RDLLOAD(GetCurrentProcessRDL, GetCurrentProcessProto, hKernel, cbGetCurrentProcess);\n"+
    "          RDLLOAD(CheckRemoteDebuggerPresentRDL, CheckRemoteDebuggerPresentProto, hKernel,"+
    " cbCheckRemoteDebuggerPresent);\n\n          BOOL remotedbg;\n\n          "+
    "CheckRemoteDebuggerPresentRDL(GetCurrentProcessRDL(), &remotedbg );\n\n"+
    "          if (remotedbg){\n            exit(0);\n          }\n\n}"
  }
  let(:prototype) { "void check_remote_debugger_present();" }
  let(:invocation) { "check_remote_debugger_present()"  }

  it_should_behave_like 'C Function'
  it_should_behave_like 'an anti-debugging function'


end