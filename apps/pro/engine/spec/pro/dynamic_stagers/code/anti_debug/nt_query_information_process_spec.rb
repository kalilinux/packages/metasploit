require "spec_helper"

describe Pro::DynamicStagers::Code::AntiDebug::NtQueryInformationProcess do
  subject(:function) { described_class.new }

  let(:prototype) { "void nt_query_information_process();" }
  let(:invocation) { "nt_query_information_process()"  }
  let(:definition) {
    "void nt_query_information_process() {\n\t          "+
    "char cbKernel[] = { CRYPT12('k','e','r','n','e','l','3','2','.','d','l','l'),XOR_KEY };\n"+
    "          LIBLOAD(hKernel, cbKernel);\n\n          // begin NtQueryInformationProcess\n" +
    "          char cbNtdll[] = { CRYPT9('n', 't', 'd', 'l', 'l', '.', 'd', 'l', 'l'), XOR_KEY };\n"+
    "          LIBLOAD(hNtdll, cbNtdll);\n\n          "+
    "char cbNtQueryInformationProcess[] ="+
    " { CRYPT25('N', 't', 'Q', 'u', 'e', 'r', 'y', 'I', 'n', 'f', 'o', 'r', 'm', 'a', 't', 'i', 'o', 'n', 'P', 'r', 'o', 'c', 'e', 's', 's'), XOR_KEY };\n"+
    "          char cbGetCurrentThread[] = { CRYPT16('G', 'e', 't', 'C', 'u', 'r', 'r', 'e', 'n', 't', 'T', 'h', 'r', 'e', 'a', 'd'), XOR_KEY };\n"+
    "          char cbNtSetInformationThread[] = { CRYPT22('N', 't', 'S', 'e', 't', 'I', 'n', 'f', 'o', 'r', 'm', 'a', 't', 'i', 'o', 'n', 'T', 'h', 'r', 'e', 'a', 'd'), XOR_KEY };\n\n"+
    "          typedef VOID(__stdcall *NtQueryInformationProcessProto)(HANDLE, int, LPVOID, ULONG, ULONG_PTR);\n"+
    "          RDLLOAD(NtQueryInformationProcessRDL, NtQueryInformationProcessProto, hNtdll, cbNtQueryInformationProcess);\n\n" +
    "          typedef HANDLE(__stdcall *GetCurrentThreadProto)(VOID);\n          "+
    "RDLLOAD(GetCurrentThreadRDL, GetCurrentThreadProto, hKernel, cbGetCurrentThread);\n\n"+
    "          // if any debuggers are still attached, lets just die.\n          int debuggerAttached;\n"+
    "          NtQueryInformationProcessRDL((HANDLE )-1, 0x07, (LPVOID *)&debuggerAttached, 4, NULL);\n\n"+
    "          if (debuggerAttached != 0) {\n            exit(0); // show's over.\n          }\n\n}"
  }

  it_should_behave_like 'C Function'
  it_should_behave_like 'an anti-debugging function'

end
