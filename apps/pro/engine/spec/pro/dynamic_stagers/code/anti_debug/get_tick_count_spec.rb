require 'spec_helper'

describe Pro::DynamicStagers::Code::AntiDebug::GetTickCount do

  subject(:function){
    described_class.new
  }

  let(:definition){
    "void get_tick_count() {\n\t          DWORD tickcount;\n"+
    "          typedef DWORD (__stdcall *GetTickCountProto)(void);\n"+
    "          char cbKernel[] = { CRYPT12('k','e','r','n','e','l','3','2','.','d','l','l'),XOR_KEY };\n"+
    "          char cbGetTickCount[] = { CRYPT12('G','e','t','T','i','c','k','C','o','u','n','t'),XOR_KEY };\n"+
    "          LIBLOAD(hKernel, cbKernel);\n          RDLLOAD(GetTickCountRDL, GetTickCountProto, hKernel, cbGetTickCount);\n"+
    "          tickcount = GetTickCountRDL();\n\t        Sleep(650);\n          if (((GetTickCountRDL() - tickcount) / 300) != 2)"+
    "{\n            exit(0);\n          }\n\n}"
  }
  let(:prototype) { "void get_tick_count();" }
  let(:invocation) { "get_tick_count()" }

  it_should_behave_like 'C Function'
  it_should_behave_like 'an anti-debugging function'

end