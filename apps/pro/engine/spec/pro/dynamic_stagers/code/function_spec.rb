require "spec_helper"

describe Pro::DynamicStagers::Code::Function do
  let(:type) { 'void' }
  let(:body) { '' }
  let(:arguments) { [] }
  let(:name) { 'foo' }
  let(:scope) { Pro::DynamicStagers::Code::Scope.new({"foobar" => 1}) }
  let(:defaults) { {type: type, body: body, arguments: arguments, name: name, scope: scope} }
  subject(:function) { described_class.new(defaults) }

  include_context "dynamic stager regex matchers"

  it_should_behave_like 'C Function'
  it_should_behave_like 'Pro::DynamicStagers::Code::Operation'
  it_should_behave_like 'Pro::DynamicStagers::Code::Loop'
  it_should_behave_like 'Pro::DynamicStagers::Code::Conditional'

  context 'creating a CFunction' do
    subject(:initialized_function) { -> { described_class.new(defaults) } }

    context 'when :name is nil' do
      let(:name) { nil }
      it { should raise_key_error(:name) }
    end

    context 'when :body is nil' do
      let(:body) { nil }
      it { should raise_key_error(:body) }
    end

    context 'when :type is nil' do
      let(:type) { nil }
      it { should raise_key_error(:type) }
    end

    context 'when :arguments is nil' do
      let(:arguments) { nil }
      it { should raise_key_error(:arguments) }
    end

    context 'when :name key is missing' do
      before { defaults.delete(:name) }
      it { should raise_key_error(:name) }
    end

    context 'when :body key is missing' do
      before { defaults.delete(:body) }
      it { should raise_key_error(:body) }
    end

    context 'when :type key is missing' do
      before { defaults.delete(:type) }
      it { should raise_key_error(:type) }
    end

    context 'when :arguments key is missing' do
      before { defaults.delete(:arguments) }
      it { should raise_key_error(:arguments) }
    end

    context 'with an invalid return type' do
      let(:type) { 'LOLZ' }
      it { should raise_key_error(:type, "Type must be one of: int, long, short, DWORD, void") }
    end

    context 'with a String for its arguments' do
      let(:arguments) { 'LOLZ' }
      it { should raise_key_error(:arguments, ":arguments must be an Array") }
    end
  end

  context 'creating an empty CFunction' do
    describe '#prototype' do
      it 'should return a valid ANSI C function prototype' do
        function.prototype.should eq "void foo();"
      end
    end

    describe '#definition' do
      it 'should return a valid ANSI C function definition' do
        expect(function.definition).to match(/void foo\(\)\s*{\s*}/)
      end
    end

    describe '#invocation' do
      it 'should call the function with no arguments' do
        expect(function.invocation).to eq("foo()")
      end
    end
  end

  context 'creating a CFunction that accepts one int parameter' do
    let(:int_cvar) {
      Pro::DynamicStagers::Code::Var.new({ name: 'Foo', type: 'int', value: 8 })
    }
    let(:arguments) { [int_cvar] }

    describe '#prototype' do
      it 'should return the correct prototype' do
        function.prototype.should eq "void foo(int Foo);"
      end
    end

    describe '#definition' do
      it 'should return a valid ANSI C function definition' do
       expect(function.definition).to match(/void foo\(int Foo\)\s*{\s*}/)
      end
    end

    describe '#invocation' do
      it 'should call the function with the cvar\'s default value' do
        expect(function.invocation).to eq("foo(8)")
      end
    end

    describe '#scope' do
      it 'adds the arguments to the local scope' do
        expect(function.scope.vars.keys).to include 'Foo'
      end
    end
  end

  context 'creating a CFunction with a non-blank body' do
    let(:body) { 'printf("Hello World\n"); '}

    describe '#definition' do
      it 'should return a valid ANSI C function definition' do
       expect(function.definition).to match(/void foo\(\)\s*{\s*#{Regexp.escape(body)}\s*}/)
      end
    end
  end

  context 'creating a random C Function' do
    let(:defaults) { { random: true, scope: scope } }

    describe '#random_type' do
      it 'randomly picks a type from the list of valid return types' do
        expect( Pro::DynamicStagers::Code::Function::RETURN_TYPES).to include function.random_type
      end
    end

    describe '#random_arguments' do

      it 'returns an array of Var objects' do
        expect(function.random_arguments.class).to eq Array
        function.random_arguments.each do |arg|
          expect(arg.class).to eq Pro::DynamicStagers::Code::Var
        end
      end

    end

    describe '#random_action' do
      context 'when a function is in scope' do
        let(:test_function) { described_class.new(name: name, type: type, body: body, arguments: arguments)}
        let(:scope) { Pro::DynamicStagers::Code::Scope.new({"foo" => test_function}) }

        it 'generates an operation when rand produces a 0' do
          function.stub(:rand).and_call_original
          function.stub(:rand).with(4) { 0 }
          expect(function.random_action).to match(/\A#{operation_regex}\Z/)
        end

        it 'generates a loop when rand produces 1' do
          function.stub(:rand).and_call_original
          function.stub(:rand).with(4) { 1 }
          expect(function.random_action).to match(/\A#{for_loop_regex}\Z/)
        end

        it 'generates a if/else when rand produces 2' do
          function.stub(:rand).and_call_original
          function.stub(:rand).with(4) { 2 }
          expect(function.random_action).to match(/#{if_else_regex}/)
        end

        it 'generates a function call when rand produces 3' do
          function.stub(:rand).and_call_original
          function.stub(:rand).with(4) { 3 }
          expect(function.random_action).to include "foo();"
        end
      end
    end

    it 'should compile properly inside a test template' do
      expect(function).to compile_in_metasm32_template
    end


  end

end
