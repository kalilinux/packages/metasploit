require "spec_helper"

describe Pro::DynamicStagers::Code::Scope do

  let(:values) { {} }
  subject(:scope) { described_class.new(values) }

  it { should respond_to :vars }
  it { should respond_to :functions }
  it { should be_empty }

  context 'when not empty' do
    let(:values) {
      {
          "Foo" => Pro::DynamicStagers::Code::Function.new(name: 'Foo', type: 'void', arguments: [], body: "int a=1;"),
          "Bar" => Pro::DynamicStagers::Code::Function.new(name: 'Bar', type: 'void', arguments: [], body: "int a=1;"),
          "Baz" => Pro::DynamicStagers::Code::Var.new(name: 'Baz', type: 'int', value: '8')
      }
    }

    it 'returns the object given the name as a key' do
      expect(scope["Foo"]).to eq values["Foo"]
    end

    describe '#functions' do
      it 'returns a hash of only the functions' do
        expect(scope.functions.keys).to match_array ["Foo", "Bar"]
      end
    end

    describe '#vars' do
      it 'returns a hash of only the vars' do
        expect(scope.vars.keys).to match_array ["Baz"]
      end
    end

    describe '#random_key' do
      before {
        count = 0
        ::Rex::Text.stub(:rand_text_alpha) do
          if (count+=1) < 2 then "Foo" else "Bat" end
        end
      }

      it 'generates a unique name' do
        expect(scope.random_key).to eq 'Bat'
      end
    end
  end
end