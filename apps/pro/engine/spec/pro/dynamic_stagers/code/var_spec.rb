require "spec_helper"

describe Pro::DynamicStagers::Code::Var do
  let(:type) { 'int' }
  let(:value) { 8 }
  let(:name) { 'Foo' }
  let(:defaults) { {name: name, type: type, value: value} }

  subject(:int_variable) {
    described_class.new( defaults )
  }

  it { should respond_to :name}
  it { should respond_to :type}
  it { should respond_to :value}
  it { should respond_to :scope}
  it { should respond_to :value}

  context 'creating a CVar' do
    subject(:initialized_cvar) { -> { described_class.new(defaults) } }

    context 'when an integer is passed to #initialize' do
      let(:defaults) { 1 }
      it { should raise_error(ArgumentError, "Invalid Argument Type: Fixnum") }
    end

    context 'when :name key is missing' do
      before { defaults.delete(:name) }
      it { should raise_key_error(:name) }
    end

    context 'when :type key is missing' do
      before { defaults.delete(:type) }
      it { should raise_key_error(:type) }
    end

    context 'when :value key is missing' do
      before { defaults.delete(:value) }
      it { should raise_key_error(:value) }
    end

    context 'when :name is nil' do
      let(:name) { nil }
      it { should raise_key_error(:name) }
    end

    context 'when :type is nil' do
      let(:type) { nil }
      it { should raise_key_error(:type) }
    end

    context 'when :value is nil' do
      let(:value) { nil }
      it { should raise_key_error(:value) }
    end

    context 'when :type is invalid' do
      let(:type) { 'baz' }
      let(:msg) { "Type must be one of: #{Pro::DynamicStagers::Code::Var::TYPES.join(',')}" }
      it { should raise_error(ArgumentError, msg) }
    end
  end

  context '#declarator' do
    it 'should return the expected declaration string' do
      int_variable.declarator.should == "int Foo = 8;"
    end
  end

  describe '#random_type' do
    it 'should return one of the approved types' do
      expect(Pro::DynamicStagers::Code::Var::TYPES.include? int_variable.random_type).to be_true
    end
  end

  describe '#random_value' do

    context 'when type is int' do
      it 'should return a string of a number' do
        expect(int_variable.random_value).to match(/^\d+$/)
      end
    end

    context 'when type is DWORD' do
      subject(:dword_variable) { described_class.new(type: 'DWORD', random: true)}
      it 'should return a string with a hex representation of a number' do
        expect(dword_variable.random_value).to match(/^0x[0-9a-f]{4}$/)
      end
    end
  end

  describe 'setting random to true' do
    context 'when not passed a name' do
      let(:defaults) { {random: true, type: type, value: value} }
      it 'should generate a random name from the Scope' do
        Pro::DynamicStagers::Code::Scope.any_instance.stub(:random_key).and_return( 'RandomName')
        expect(int_variable.name).to eq 'RandomName'
      end
    end

    context 'when not passed a type' do
      let(:defaults) { {random: true, name: name, value: value} }

      it 'should randomly pick an approved type' do
        Pro::DynamicStagers::Code::Var.any_instance.stub(:random_type).and_return('long')
        expect(int_variable.type).to eq 'long'
      end
    end

    context 'when not passed a value' do
      let(:defaults) { {random: true, name: name, type: type} }

      it 'should randomly pick a valid default value' do
        Pro::DynamicStagers::Code::Var.any_instance.stub(:rand).and_return(1337)
        expect(int_variable.value).to eq '1337'
      end
    end

  end

end
