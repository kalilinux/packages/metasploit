require 'spec_helper'

describe Pro::DynamicStagers::EXEGenerator do
  include_context "dynamic stager regex matchers"

  subject(:exe_generator) { described_class.new(defaults) }
  let(:host) { "192.168.172.1" }
  let(:port) { "8001" }
  let(:arch) { :x86 }
  let(:stager) { "reverse_tcp" }
  let(:service) { false }
  let(:defaults) { {host: host, port: port, arch: arch, stager: stager } }

  it { should respond_to :arch }
  it { should respond_to :cpu }
  it { should respond_to :host }
  it { should respond_to :port }
  it { should respond_to :stager }
  it { should respond_to :source_code }
  it { should respond_to :ssl }
  it { should respond_to :template }
  it { should respond_to :uri }
  it { should respond_to :xor_key }
  it { should respond_to :generate_source_code! }
  it { should respond_to :compile }

  context 'when creating a new generator' do
    subject(:exe_generator) { -> { described_class.new(defaults) } }

    context 'when an integer is passed to #initialize' do
      let(:defaults) { 1 }
      it { should raise_error(ArgumentError, "Invalid Argument Type: Fixnum") }
    end

    context 'when :host key is missing' do
      before { defaults.delete(:host) }
      it { should raise_key_error(:host) }
    end

    context 'when :host key is nil' do
      let(:host) { nil }
      it { should raise_key_error(:host) }
    end

    context 'when :port key is missing' do
      before { defaults.delete(:port) }
      it { should raise_key_error(:port) }
    end

    context 'when :port key is nil' do
      let(:port) { nil }
      it { should raise_key_error(:port) }
    end

    context 'when :stager key is missing' do
      before { defaults.delete(:stager) }
      it { should raise_key_error(:stager) }
    end

    context 'when :stager key is nil' do
      let(:stager) { nil }
      it { should raise_key_error(:stager) }
    end

    context 'when :arch is set to an invalid value' do
      let(:arch) { :xJoe }
      it 'raises an ArgumentError' do
        expect(exe_generator).to raise_error(ArgumentError, /Arch must be one of/)
      end
    end

    context 'when :stager is set to an invalid value' do
      let(:stager) { 'reverse_gopher' }
      it 'raises an ArgumentError' do
        expect(exe_generator).to raise_error(ArgumentError, /Stager must be one of/)
      end
    end
  end

  context 'when :arch key is missing' do
    before { defaults.delete(:arch) }
    it "defaults to :x86 for the arch" do
      expect(exe_generator.arch).to eq :x86
    end
  end

  context 'when :arch key is nil' do
    let(:arch) { nil }
    it "defaults to :x86 for the arch" do
      expect(exe_generator.arch).to eq :x86
    end
  end

  context 'when :service key is missing' do
    before { defaults.delete(:service) }
    it "defaults to false" do
      expect(exe_generator.service).to be_false
    end
  end

  context 'when :service key is nil' do
    let(:service) { nil }
    it "defaults to false" do
      expect(exe_generator.service).to be_false
    end
  end

  context 'when :arch is :x64' do
    let(:arch) { :x64 }

    it 'sets the cpu object as an X86_64 processor' do
      expect(exe_generator.cpu.class).to eq Metasm::X86_64
    end
  end

  context 'when :arch is :x64' do
    it 'sets the cpu object as an Ia32 processor' do
      expect(exe_generator.cpu.class).to eq Metasm::Ia32
    end
  end

  describe '#ssl' do
    context 'when stager is reverse_https' do
      let(:stager) { "reverse_https"}

      it 'returns true' do
        expect(exe_generator.ssl).to be_true
      end
    end

    context 'when stager is not reverse_https' do
      let(:stager) { "reverse_tcp"}

      it 'returns false' do
        expect(exe_generator.ssl).to be_false
      end
    end
  end

  describe '#xor_key' do
    it 'returns a hex string of a value from 1-255' do
      expect(exe_generator.xor_key).to match(/\A0x[0-9a-fA-F]{2}\Z/)
    end
  end

  describe '#uri' do
    it 'resolves back to :init_native' do
      random_uri = exe_generator.uri
      expect(exe_generator.process_uri_resource(random_uri)[:mode]).to eq :init_native
    end
  end

  describe '#template' do
    Pro::DynamicStagers::EXEGenerator::SUPPORTED_STAGERS.each do | chosen_stager |
      context "when stager is #{chosen_stager}" do
        let(:stager) { chosen_stager }

        it 'returns a non-empty string' do
          expect(exe_generator.template).to be_present
        end

        it 'returns the correct template for the stager' do
          expect(exe_generator.template).to include "#{exe_generator.stager}_rdl("
        end
      end
    end
  end

  describe '#randomized_functions' do
    it 'returns an array of Function objects' do
      random_functions = exe_generator.randomized_functions
      expect(random_functions).to be_an_array_of Pro::DynamicStagers::Code::Function
    end
  end

  describe '#generate_source_code!' do
    let(:stager){ "reverse_https"}
    let(:fake_template) {
      <<-EOS
        %{DEFINITIONS}
        int main()
        {
          char host[32] = "%{HOST}";
          short port = %{PORT};
          char uri[32] = "%{URI}";
          bool ssl = %{SSL};
          %{VARS1}
          %{FUNCTIONS1}
        }
      EOS
    }
    before(:each) do
      described_class.any_instance.stub(:grab_template => fake_template)
    end

    it 'inserts the host into the template' do
      expect(exe_generator.generate_source_code!).to include "char host[32] = \"#{host}\";"
    end

    it 'inserts the port into the template' do
      expect(exe_generator.generate_source_code!).to include "short port = #{port};"
    end

    it 'inserts the uri into the template' do
      expect(exe_generator.generate_source_code!).to include "char uri[32] = \"#{exe_generator.uri}\";"
    end

    it 'sets the SSL flag appropriately' do
      expect(exe_generator.generate_source_code!).to include "bool ssl = #{exe_generator.ssl.to_s.upcase};"
    end

    context 'when inserting vars' do
      let(:fake_template){
        <<-EOS
          int main()
          {
            %{VARS1}
          }
        EOS
      }
      it 'inserts random variables in the template' do
        random_vars_match = /(#{assignment_regex}){0,4}/
        expect(exe_generator.generate_source_code!).to match(random_vars_match)
      end
    end

  end

  describe '#compile' do
    context 'reverse_tcp' do
      let(:stager) { "reverse_tcp" }
      it_should_behave_like 'a compilable dynamic payload'
    end

    context 'bind_tcp' do
      let(:stager) { "bind_tcp" }
      it_should_behave_like 'a compilable dynamic payload'
    end

    context 'reverse_http' do
      let(:stager) { "reverse_tcp" }
      it_should_behave_like 'a compilable dynamic payload'
    end

    context 'reverse_https' do
      let(:stager) { "reverse_https" }
      it_should_behave_like 'a compilable dynamic payload'
    end

  end

end
