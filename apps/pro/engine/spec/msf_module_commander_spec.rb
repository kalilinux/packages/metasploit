require "spec_helper"

require 'msf_module_commander'

describe MsfModuleCommander do
  # Represents the commander module itself -- the place where we mixin the MsfModuleCommander
  let(:module_class){ Class.new }

  # Represents the persisted object that implements a state machine with events triggered
  # by MsfModuleCommander actions
  let(:command_object){double "command_object"}

  def set_command_mode(subject, mode)
    subject.instance_variable_set("@command_mode", mode)
  end

  def set_command_object(subject, object)
    subject.instance_variable_set("@command_object", object)
  end

  before(:each) do
    module_class.send(:include, MsfModuleCommander)
  end

  describe "starting the module run" do
    subject { module_class.new }

    before(:each) do
      pending("Marking all of the tests as pending since module_class.new is missing an arg")
      subject.stub(:task_progress=)
    end

    describe "when no command mode has been set" do
      before(:each) do
        set_command_mode(subject, nil)
      end

      it "should raise an error" do
        expect{subject.validate_and_kickoff!}.to raise_error MsfModuleCommander::InvalidCommandModeError
      end
    end

    describe "when the command mode has been set to an invalid value" do
      before(:each) do
        set_command_mode(subject, :not_a_valid_command_mode)
      end

      it "should raise an error" do
        expect{subject.validate_and_kickoff!}.to raise_error MsfModuleCommander::InvalidCommandModeError
      end
    end

    describe "when no #command_object has been set" do
      before(:each) do
        set_command_object(subject, nil)
        set_command_mode(subject, MsfModuleCommander::VALID_COMMAND_MODES.first) # just get one you know will be valid
      end

      it "should raise an error" do
        expect{subject.validate_and_kickoff!}.to raise_error MsfModuleCommander::ConfigError
      end
    end

    describe "in single-threaded mode" do
      before(:each) do
        set_command_mode(subject, :single_threaded)
        set_command_object(subject, command_object)
      end

      it "should pass execution to the Procedure object" do
        command_object.should_receive(:execute_with_commander).with(subject)
        subject.validate_and_kickoff!
      end

    end

    describe "in multi-threaded mode" do
      before(:each) do
        set_command_mode(subject, :multi_threaded)
        set_command_object(subject, command_object)
        subject.instance_variable_set("@start_state_event_method", "start")
        subject.instance_variable_set("@finish_state_event_method", "finish")
      end

      it "should raise an error if there is no #start_state_event_method_name set" do
        subject.instance_variable_set("@start_state_event_method", nil)
        expect{subject.validate_and_kickoff!}.to_not raise_error MsfModuleCommander::ConfigError

      end

      it "should raise an error if there is no #finish_state_event_method_name set" do
        subject.instance_variable_set("@finish_state_event_method", nil)
        expect{subject.validate_and_kickoff!}.to raise_error MsfModuleCommander::ConfigError
      end
    end

  end

end
