# Must be required first so that all other required files will collect coverage information
require 'simplecov'

ENV['RAILS_ENV'] = 'test'

require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'

rails_engines = [Rails, Metasploit::Framework::Engine]

rails_engines.each do |rails_engine|
  Dir[rails_engine.root.join('spec', 'support', '**', '*.rb')].each do |f|
    require f
  end
end

RSpec.configure do |config|
  
  SimpleCov.command_name "RSpec Engine"
  
  config.mock_with :rspec
  config.order = :random
  config.use_transactional_fixtures = true

  config.deprecation_stream = Metasploit::Pro::UI::Engine.root.parent.join('test_reports','spec', "deprecations_engine.txt")
  
  #
  # Callbacks
  #

  config.before(:each) do
    ActiveRecord::Base.observers.disable :all
  end
end
