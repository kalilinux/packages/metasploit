#
# Project
#

require 'msf/pro/mime'
require 'msf/pro/locations'
require 'metasploit/pro/lport_mapper'

module Msf
###
#
# This module provides shared methods for top-level Pro modules
#
###
module Pro
module Task

  include Msf::Auxiliary::Report
  include Msf::Pro::Locations
  include Msf::Pro::MIME

  IPV4_LOCALHOST_IP = '0.0.0.0'
  IPV6_LOCALHOST_IP = '::'

  def initialize(info)
    super(info)

    register_options([
      OptAddressRange.new('WHITELIST_HOSTS', [ false, "Allowed target ranges, leave blank to allow all"]),
      OptAddressRange.new('BLACKLIST_HOSTS', [ false, "Ignored target ranges, leave blank to allow all"]),
      OptBool.new('VERBOSE', [ false, "Enable verbose module output in the task log", true]),
      OptBool.new('REALLY_VERBOSE', [ false, "Enable really verbose module output in the task log", false]),
      OptString.new('WORKSPACE', [false, "The name of the active workspace"]),
      OptString.new('PROUSER', [false, "The name of the user who launched this task"])
    ], self.class)

    register_advanced_options([
      OptBool.new('EnableReverseHTTPS', [ false, "Enable reverse_https and reverse_http stagers for configured modules", false]),
      OptBool.new('EnableReverseHTTP', [ false, "Enable reverse_http stagers for configured modules", false])
    ], self.class)

    @step_current = 0
    @lport_mapper = Metasploit::Pro::LportMapper.new
    @lport_mapper.use_rpc = false
  end


  module SessionDetectedPlatform
    attr_accessor :detected_platform
  end

  # This is used basically as a flag for report.rb#marshalize().
  class BinaryString < String
  end

  attr_accessor :step_count, :step_current
  attr_accessor :session_total
  attr_accessor :included_hosts


  # This method memoizes an mdm task for us. If one wasn't created for this module run
  # we create one now. This allows us to test properly from the console.
  def mdm_task
    @mdm_task ||= self[:task].nil? ? Mdm::Task.create!(workspace: myworkspace) : self[:task].record
  end


  def next_step(info=nil)
    return if not @step_count
    @step_current ||= 0
    return if @step_current >= @step_count
    @step_current += 1
    if(self[:task])
      pct = (((@step_current) / @step_count.to_f) * 100).to_i
      self[:task].progress = pct
      self[:task].info = info || "Step #{@step_current} of #{@step_count}"

      print_good("Workspace:#{myworkspace.name} Progress:#{@step_current}/#{@step_count} (#{self[:task].progress}%) #{self[:task].info}")
    end
  end

  def next_substep(spct)
    return if not @step_count
    @step_current ||= 0
    return if @step_current >= @step_count
    if(self[:task])
      pct = (((@step_current + spct) / @step_count.to_f) * 100).to_i
      self[:task].progress = pct.to_i
      # print_good("WORKSPACE=#{myworkspace.name} STEP=#{@step_current} DONE=%#{self[:task].progress} INFO=#{self[:task].info}")
    end
  end

  def task_sync
    return if not @step_count
    @step_current ||= 0
    return if @step_current >= @step_count
    if(self[:task])
      self[:task].progress = (((@step_current) / @step_count.to_f) * 100).to_i
    end
  end

  def task_info
    return if not self[:task]
    self[:task].info
  end

  def task_info=(val)
    return if not self[:task]
    self[:task].info = val
  end

  def task_progress
    return if not self[:task]
    self[:task].progress
  end

  def task_progress=(val)
    return if not self[:task]
    self[:task].progress = val
  end

  def task_result
    return if not self[:task]
    self[:task].result
  end

  def task_result=(result)
    return if not self[:task]
    self[:task].result = result
  end

  def task_error
    return if not self[:task]
    self[:task].error
  end

  def task_error=(e)
    return if not self[:task]
    self[:task].error = e
  end

  # Override
  def stop_task
  end

  def vprint_debug(msg)
    return if not datastore['REALLY_VERBOSE']
    print_status(msg)
  end

  def vprint_status(msg)
    return if not datastore['VERBOSE']
    print_status(msg)
  end

  def vprint_error(msg)
    return if not datastore['VERBOSE']
    print_error(msg)
  end

  def vprint_good(msg)
    return if not datastore['VERBOSE']
    print_good(msg)
  end

  def print_autotag_status
    if datastore['AUTOTAG_OS']
      print_status "Hosts will be automatically tagged by operating system."
    end
    if datastore['AUTOTAG_TAGS'] and not datastore['AUTOTAG_TAGS'].strip.empty?
      print_status "Hosts will be automatically tagged as: #{datastore['AUTOTAG_TAGS']}"
    end
  end

  # Builds a complete list of target hosts given a workspace, a whitelist, and a blacklist.
  # Report expects hosts that are not alive as well, so it is the oddman out
  #TODO: Refactor the gd whitelist functionality as it is bollocks
  def gen_included_hosts()
    ::ActiveRecord::Base.connection_pool.with_connection {
      if self.refname.include? "pro/report"
        if @whitelist.empty?
          if @blacklist.empty?
            @included_hosts = myworkspace.hosts
          else
            @included_hosts = myworkspace.hosts.where("address not in (?)", @blacklist.keys)
          end
        else
          @included_hosts = myworkspace.hosts.where(:address => @whitelist.keys )
        end
      else
        if @whitelist.empty?
          @included_hosts = myworkspace.hosts.where(:state => 'alive')
        else
          @included_hosts = myworkspace.hosts.where(:state => 'alive', :address => @whitelist.keys )
        end
      end
    }
  end

  def process_whitelist
    whitelist_conf = datastore['WHITELIST_HOSTS']
    blacklist_conf = datastore['BLACKLIST_HOSTS']

    @whitelist = {}
    @blacklist = {}

    if(whitelist_conf and not whitelist_conf.strip.empty?)
      r = Rex::Socket::RangeWalker.new(whitelist_conf)

      if r.num_ips > 65536
        raise RuntimeError, "IP address range is too large (maximum is 65,536 hosts)"
      end

      while(ip = r.next_ip)
        @whitelist[ip] = true
      end
    end

    if(blacklist_conf and not blacklist_conf.strip.empty?)
      r = Rex::Socket::RangeWalker.new(blacklist_conf)

      if r.num_ips > 65536
        raise RuntimeError, "IP address range is too large (maximum is 65,536 hosts)"
      end

      while(ip = r.next_ip)
        @blacklist[ip] = true
        @whitelist.delete(ip)
      end
    end

    if @whitelist['127.0.0.1']
      @whitelist.delete('127.0.0.1')
      print_error("Scanning 127.0.0.1 is not currently supported")
    end

    gen_included_hosts()
  end

  def host_allowed?(host)
    if host.kind_of? Mdm::Host
      ip = host.address
    else
      ip = host
    end
    ((@whitelist.empty? or @whitelist[ip]) and ! @blacklist[ip])
  end

  def myworkspace
    return @myworkspace if @myworkspace
    ::ActiveRecord::Base.connection_pool.with_connection {
      @myworkspace = Mdm::Workspace.find_by_name(datastore['WORKSPACE']) || framework.db.workspace
    }
  end

  # XXX: Move this or have something like it into Rex instead, this is the
  # third time i've created this function at least.
  def ascii_safe_hex(str,ws=false)
    if ws
      str.gsub(/([\x00-\x20\x80-\xFF])/n){ |x| "\\x%.2x" % x.unpack("C*")[0] }
    else
      str.gsub(/([\x00-\x08\x0b\x0c\x0e-\x1f\x80-\xFF])/n){ |x| "\\x%.2x" % x.unpack("C*")[0]}
    end
  end

  def configure_module(mod, num = nil)
    mod.import_defaults
    mod.register_parent(self)
    mod.datastore['VERBOSE']         = true
    mod.datastore['ShowProgress']    = false
    mod.datastore['TimestampOutput'] = true
    mod.datastore['ExploitNumber']   = num
    mod.datastore['AutoRunScript']   = self.datastore['AutoRunScript'] if self.datastore['AutoRunScript']
    mod[:task]                       = self[:task]
    # Force any TCP listeners to use the local communication channel only
    mod.datastore['ListenerComm'] = 'local'

    configure_module_exe_templates(mod)

    mod.init_ui(self.user_input, self.user_output)
  end

  def configure_module_exe_templates(mod)
    if framework.esnecil_support_ca_signed?
      mod.datastore['EXE::Path'] = pro_exe_template_directory
      mod.datastore['EXE::FallBack'] = true
      mod.datastore['EXE::OldMethod'] = true
    end
  end

  def configure_payload(mod, rhost, opts={})
    return if not mod.respond_to?(:compatible_payloads)

    ::ActiveRecord::Base.connection_pool.with_connection {

      if self.datastore.has_key? 'DynamicStager' and mod.datastore.has_key? 'DynamicStager'
        mod.datastore['DynamicStager'] = self.datastore['DynamicStager']
      end

      # if we are using a dynamic stager upping the delay to 5min
      if mod.datastore['DynamicStager']
        print_status("Increasing WfsDelay to 5 minutes for Dynamic Stagers")
        mod.datastore['WfsDelay'] = 300
      end

      # Make sure this is in compressed form for IPv6
      rhost = Rex::Socket.compress_address(rhost)

      # Create a toggle for IPv6 payload mode
      ipv6  = Rex::Socket.is_ipv6?(rhost)

      # Find compatible payload modules
      pset = mod.compatible_payloads.map{|x| x[0] }

      # Figure out what connection method to use
      conn = get_payload_method(rhost)

      # Attempt to find a database record for this host
      hinf = myworkspace.hosts.find_by_address(rhost)

      # Handle reverse connect payload choices
      if conn == :reverse

        # Force the listener to always bind to the ANY address
        mod.datastore['ReverseListenerBindAddress'] = '0.0.0.0'

        # Force the listener to use the local communication channel only
        mod.datastore['ReverseListenerComm'] = 'local'

        pref_ipv4 = [
          #
          # Most desirable payloads for IPv4
          #
          'windows/meterpreter/reverse_tcp',
          'java/meterpreter/reverse_tcp',
          'php/meterpreter/reverse_tcp',
          'php/meterpreter_reverse_tcp',
          'ruby/shell_reverse_tcp',
          'cmd/unix/interact',
          'cmd/unix/reverse',
          'cmd/unix/reverse_perl',
          'cmd/unix/reverse_netcat',
          'cmd/unix/reverse_bash',
          #
          # Least desirable payloads for IPv4
          #
          'windows/meterpreter/reverse_nonx_tcp',
          'windows/meterpreter/reverse_ord_tcp',
          'windows/shell/reverse_tcp',
          'generic/shell_reverse_tcp'
        ]

        pref_ipv6 = [
          #
          # Most desirable payloads for IPv6
          #
          'windows/meterpreter/reverse_ipv6_tcp',
          'bsd/x86/shell_reverse_tcp_ipv6',
          'bsd/x86/shell/reverse_ipv6_tcp',

# TODO:
#         'java/meterpreter/reverse_ipv6_tcp',
          'php/meterpreter/reverse_tcp',
          'php/meterpreter_reverse_tcp',
          'php/reverse_tcp',
          'php/reverse_perl',
          'ruby/shell_reverse_tcp',


          'cmd/unix/interact',

          # Telnet supports IPv6 on compatible platforms
          'cmd/unix/reverse',

          # Perl supports IPv6 on newer version and can use the normal payload
          'cmd/unix/reverse_perl',

          # Netcat supports IPv6 on compatible platforms
          'cmd/unix/reverse_netcat',

          # Bash supports IPv6 for TCP redirects (if the OS supports it)
          'cmd/unix/reverse_bash',

          #
          # Least desirable payloads for IPv6
          #
          'windows/shell/reverse_ipv6_tcp',
# TODO:
#         'generic/shell_reverse_ipv6_tcp'
        ]

        pref = ipv6 ? pref_ipv6 : pref_ipv4

        if datastore['EnableReverseHTTPS'] or opts[:enable_reverse_https]
          if ipv6
            pref.unshift("windows/meterpreter/reverse_ipv6_http")
            pref.unshift("windows/meterpreter/reverse_ipv6_https")
          else
            pref.unshift("windows/meterpreter/reverse_http")
            pref.unshift("windows/meterpreter/reverse_https")
          end
        else
          if datastore['EnableReverseHTTP'] or opts[:enable_reverse_http]
            if ipv6
              pref.unshift("windows/meterpreter/reverse_ipv6_http")
            else
              pref.unshift("windows/meterpreter/reverse_http")
            end
          end
        end

        if datastore['PAYLOAD_TYPE'].to_s.downcase != 'meterpreter'
          pref.reject!{|x| x.index('meterpreter') }
        end

        # Tune the payload set for exploits using an automatic target
        if mod.datastore['TARGET'] =~ /Automatic/i or
           (mod.respond_to?(:default_target) and
            mod.default_target and
            mod.targets[ mod.default_target ].name =~ /Automatic/i)

          # Move Java to the top of the list when we can't assume a windows target
          if pset.include?('java/meterpreter/reverse_tcp') and (hinf.nil? or hinf.os_name !~ /windows/i)
            pref.delete('java/meterpreter/reverse_tcp')
            pref.unshift('java/meterpreter/reverse_tcp')
          end
        end

        pref.each do |n|
          if(pset.include?(n))
            mod.datastore['PAYLOAD'] = n

            lport = find_usable_lport(ipv6, opts['payload_ports'])
            if not lport
              print_error("Fatal: Could not find a viable listener port")
              return
            end

            mod.datastore['LPORT'] = lport.to_s

            if datastore['PAYLOAD_LHOST'].present?
              lhost = datastore['PAYLOAD_LHOST']
            elsif opts['payload_lhost'].present?
              lhost = opts['payload_lhost']
            else
              lhost = Rex::Socket.source_address(rhost)
            end

            # IPv6 specific fixups
            if ipv6
              # Force the listener to always bind to the ANY IPv6 address
              mod.datastore['ReverseListenerBindAddress'] = '::'
            end

            mod.datastore['LHOST'] = lhost

            mod.datastore['EnableStageEncoding'] = self.datastore['EnableStageEncoding']

            return
          end
        end

        print_error("No reverse connect payloads available for #{mod.fullname}")

        # Only admit defeat if the user specifically chose bind/reverse, otherwise fallthrough to bind
        if ['reverse', 'bind'].include?( datastore['PAYLOAD_METHOD'] )
          return
        end
      end


      # Handle bind connections
      if conn == :bind
        pref_ipv4 = [
          #
          # Most desirable payloads for IPv4
          #
          'windows/meterpreter/bind_tcp',
          'java/meterpreter/bind_tcp',
          'php/meterpreter/bind_tcp',
          'ruby/shell_bind_tcp',
          'cmd/unix/interact',
          'cmd/unix/bind_perl',
          'cmd/unix/bind_inetd',
          'cmd/unix/bind_netcat',
          'cmd/unix/bind_ruby',

          #
          # Least desirable payloads for IPv4
          #
          'windows/meterpreter/bind_nonx_tcp',
          'windows/shell/bind_tcp',
          'generic/shell_bind_tcp'
        ]

        pref_ipv6 = [
          #
          # Most desirable payloads for IPv6
          #
          'windows/meterpreter/bind_ipv6_tcp',
          'bsd/x86/shell_bind_tcp_ipv6',
          'bsd/x86/shell/bind_ipv6_tcp',
          'php/meterpreter/bind_ipv6_tcp',
          'php/bind_php_ipv6',
          'php/bind_perl_ipv6',
          'ruby/shell_bind_tcp_ipv6',

          'cmd/unix/interact',

          'cmd/unix/bind_perl_ipv6',
          'cmd/unix/bind_netcat_ipv6',
          'cmd/unix/bind_ruby_ipv6',

# TODO:
#         'cmd/unix/bind_inetd_ipv6',

          #
          # Least desirable payloads
          #
          'windows/shell/bind_ipv6_tcp'
# TODO:
#         'generic/shell_bind_ipv6_tcp'
        ]

        pref = ipv6 ? pref_ipv6 : pref_ipv4

        if datastore['PAYLOAD_TYPE'].to_s.downcase != 'meterpreter'
          pref.reject!{|x| x.index('meterpreter') }
        end

        host = myworkspace.hosts.find_by_address(rhost)

        # Maybe this was manually launched?
        if not host
          # Only print this if its not our fake placeholder host
          if rhost != "50.50.50.50"
            print_status("Choosing a random listener since we have no host information for this target")
          end

          # Choose a random port from the specified range
          mod.datastore['LPORT'] = @lport_mapper.find_usable_lport(ipv6).to_s

          pref.each do |n|
            if(pset.include?(n))
              mod.datastore['PAYLOAD'] = n
              return
            end
          end
          print_error("No bind payloads available for #{mod.fullname}")
          return
        end

        @bind_port_table ||= {}

        if ! @bind_port_table[rhost]
          # Create a list of all allowed ports on the target
          closed = []; opened = []; filtered = [];
          host.services.where(proto: 'tcp', host_id: host[:id]).each do |s|
            case s.state
            when 'open'
              opened << s.port
            when 'closed'
              closed << s.port
            when 'filtered'
              filtered << s.port
            end
          end

          print_status("Host #{rhost} has #{opened.length} open ports, #{closed.length} closed ports, and #{filtered.length} filtered ports")

          @bind_port_table[rhost] = [opened, closed, filtered]
        end

        opened, closed, filtered = @bind_port_table[rhost]

        # Closed ports indicate all other ports are filtered or in use
        if closed.length > 0
          mod.datastore['LPORT'] = closed[ rand(closed.length) ].to_s
          print_status("Using closed port #{mod.datastore['LPORT']} for #{rhost} due to firewall rules")
          pref.each do |n|
            if(pset.include?(n))
              mod.datastore['PAYLOAD'] = n
              return
            end
          end
          print_error("No bind payloads available for #{mod.fullname}")
        end

        ports  = configure_allowed_ports
        ports -= filtered
        ports -= opened

        pref.each do |n|
          if(pset.include?(n))
            mod.datastore['PAYLOAD'] = n
            mod.datastore['LPORT']   = ports[ rand(ports.length) ].to_s
            print_status("Using a random high port (#{mod.datastore['LPORT']}) for #{rhost}")
            mod.datastore['EnableStageEncoding'] = self.datastore['EnableStageEncoding']
            return
          end
        end

        print_error("No bind payloads available for #{mod.fullname})")
        return
      end

      print_error("No payloads were compatible with #{mod.fullname})")
    }
  end


  # This method is used to configure SRVPORT for exploits that need a dynamic listener
  # for the callback process. The PHP RFI modules are one example of this.
  def configure_exploit_callback(mod, rhost)

    return if not mod.datastore['SRVPORT']

    conn = get_payload_method(rhost)

    # Fail here because the exploit will not work
    if conn == :bind
      print_error("Fatal: Could not select a callback port when bind connections are specified")
      return
    end

    lport = find_usable_lport( Rex::Socket.is_ipv6?(rhost) )
    if not lport
      print_error("Fatal: Could not find a viable callback port")
      return
    end

    # Configure the socket
    mod.datastore['SRVPORT'] = lport.to_s
  end

  # Returns the sessions that have active routes.
  def msf3_routes
    sessions = framework.sessions.values.reject {|s| s.routes.nil? || s.routes.empty?}
    routes = []
    return routes if sessions.empty?
    sessions.each do |s|
      routes.concat(s.routes.split(","))
    end
    return routes.flatten.uniq
  end

  def print_routes
    unless msf3_routes.empty?
      print_status "Active routes: #{msf3_routes.join(", ")}"
    end
  end

  module ProScript
    attr_accessor :pro
  end

  def shell_execute_script(session, path, *args)

    if not framework.sessions[session.sid]
      print_error("Could not run script #{path} on session #{session.sid}: session is not available")
      return
    end

    obj = Rex::Script::Shell.new(session, path)
    obj.extend(ProScript)
    obj.pro = self
    obj.sink = self.user_output
    obj.workspace = myworkspace

    begin
      obj.run(args)
    rescue ::Interrupt
      raise $!
    rescue ::Exception => e
      print_error("Error running script #{path} on session #{session.sid}: #{e}")
    end
  end

  #
  # Return the address associated with a session
  #
  def session_host(s)
    # Allow an IP address to be used when no session is available
    return s if s.class == ::String
    s.session_host
  end


  def shell_detect_platform(s)

    s.extend(SessionDetectedPlatform) if not s.respond_to?('detected_platform')

    # Check to see if we already know the platform
    return s.detected_platform if s.detected_platform

    print_status("Detecting the system platform for session #{s.sid} - #{session_host(s)} #{s.info.to_s}")
    # Read any pending data on the shell
    buff = s.rstream.get_once(-1, 0.01)
    if(buff and buff =~ /Windows/)
      s.detected_platform = "windows"
      return s.detected_platform
    end

    buff ||= ""

    1.upto(2) do
      temp = s.shell_command("uname -a")
      buff << temp if temp
      if (temp = s.rstream.get_once(-1, 3))
        buff << temp
      end
    end

    # Match Cisco IOS
    if (buff and buff =~ /Invalid input detected at/)
      s.detected_platform = "ios"
      return s.detected_platform
    end

    # Match Windows
    if (buff and buff =~ /or batch file/)
      s.detected_platform = "windows"
      return s.detected_platform
    end

    # Match Unix
    if (buff and buff =~ /sunos|bsd|linux|hp-?ux|aix|darwin|irix/i)
      # Output of "uname -a" on various systems, mostly gathered via google:
      #   SunOS sol9 5.11 snv_107 sun4u sparc SUNW,Sun-Blade-1000
      #   FreeBSD server.example.com 5.3-RELEASE FreeBSD 5.3-RELEASE #1: Fri Apr 29 23:04:18 EEST 2005
      #   Linux maat 2.6.32-30-generic #59-Ubuntu SMP Tue Mar 1 21:30:46 UTC 2011 x86_64 GNU/Linux
      #   HP-UX myhost A.09.01 C 9000/750 2015986034 32-user license
      #   AIX hal-ibmp610-1 2 5 0007B76A4C00
      #   Darwin macbookpro0341 10.4.0 Darwin Kernel Version 10.4.0: Fri Apr 23 18:27:12 PDT 2010; root:xnu-1504.7.4~1/RELEASE_X86_64 x86_64
      #   IRIX64 blueberry 6.5 10181058 IP27

      # Explicitly disable echo
      s.shell_command("stty -echo > /dev/null 2> /dev/null")
      s.detected_platform = "unix"
      return s.detected_platform
    end

    # Likely a device
    s.detected_platform = "device"
  end

  # We need to keep track of established sessions for both LimitSessions
  # and to ensure we don't try more than one sessions per host/service
  # (regardless of workspace) for exploit and websploit.
  # This picks up the ports the sessions are on -- but /not/ how they
  # got there. If this is a tunnel to port 22, there's no real difference,
  # but if you popped the machine on port 445 and now have a meterpreter
  # session on two arbitrary ports, I don't see a way to tell that
  # you originally got there via 445. Shouldn't matter, but this
  # may be unexpected for users?
  def find_established_sessions
    @established_sessions ||= {}
    framework.sessions.values.map do |s|
      addr,port = target_or_peer_host_and_port(s)
      uuid = s.uuid || nil
      wspace = s.workspace || nil
      @established_sessions[uuid] = [addr,wspace,port.to_i]
    end
    return @established_sessions
  end

  # Canonically determines host and port, regardless of session type. Takes
  # a framework session object, returns an array of [host,port].
  def target_or_peer_host_and_port(s)
    [s.session_host, s.session_port]
  end

  def get_payload_method(rhost)
    internet = /^(((25[0-5]|2[0-4][0-9]|19[0-1]|19[3-9]|18[0-9]|17[0-1]|17[3-9]|1[0-6][0-9]|1[1-9]|[2-9][0-9]|[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]))|(192\.(25[0-5]|2[0-4][0-9]|16[0-7]|169|1[0-5][0-9]|1[7-9][0-9]|[1-9][0-9]|[0-9]))|(172\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|1[0-5]|3[2-9]|[4-9][0-9]|[0-9])))\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])$/

    case datastore['PAYLOAD_METHOD'].downcase
    when 'reverse'
      conn = :reverse
    when 'bind'
      conn = :bind
    else
      # The rules for automatic payload choices:
      # * Prefer reverse connections
      # * Choose bind if NAT is detected
      saddr,daddr = Rex::Socket.source_address(rhost),rhost

      # Default to reverse
      conn = :reverse

      if Rex::Socket.is_ipv4?(rhost)
        # Bind if 192.168.0.1 -> 4.4.4.4
        if daddr =~ internet and saddr !~ internet
          conn = :bind
        end
      end

      # TODO: Prefer bind for Linux targets if RHOST =~ /^fe80::/
    end
    conn
  end

  def configure_allowed_ports(lport_range=nil)
    @lport_mapper.allowed_ports = (lport_range || datastore['PAYLOAD_PORTS'] || "1024-65535")
    @lport_mapper.allowed_ports.dup
  end

  def find_usable_lport(ipv6=false, lport_range=nil)
    configure_allowed_ports(lport_range)
    @lport_mapper.find_usable_lport(ipv6)
  end

  def lport_available?(lport, ipv6=false)
    configure_allowed_ports
    @lport_mapper.lport_available?(lport,ipv6)
  end

  # Takes an array of hosts and a string (space-delimited by default) of tag names,
  # and applies all the supplied tags to the hosts (technically, adds
  # the hosts to those tags).
  #
  # Generally, autotagging should(?) fail silently rather than raise an
  # error.

  def autotag_hosts(hosts,taglist, regex=/\s+/)

    ::ActiveRecord::Base.connection_pool.with_connection {
      return unless hosts.respond_to? :each
      return unless taglist.respond_to? :split
      hosts.each do |host|
        if host.kind_of?(::Mdm::Host)
          this_host = host
        else
          this_host = myworkspace.hosts.find_by_address(host)
        end
          next unless host
        taglist.split(regex).each do |tagname|
          tag = find_or_create_host_tag(tagname)
          next if tag.hosts.include?(this_host)
          tag.hosts ||= []
          tag.hosts << this_host unless tag.hosts.include? this_host

          if tag.changed?
            tag.save
            taglist_formatted = taglist.split(regex).map {|t| "##{t}"}.join(", ")
            print_status "Tagging %s with %s" % [this_host.address, taglist_formatted]
          end

          tag.save if tag.changed?
        end
      end
    }
  end

  # Shorthand for autotagging single host.
  def autotag_host(host,taglist)
    autotag_hosts([host],taglist)
  end

  def find_or_create_host_tag(tagname)
    ::ActiveRecord::Base.connection_pool.with_connection {
      possible_tag = myworkspace.host_tags.select {|t| t.name == tagname}.first
      tag = possible_tag || ::Mdm::Tag.new
      tag.name = tagname
      return tag
    }
  end

  def autotag_os(host)
    return unless host && host.kind_of?(::Mdm::Host)
    tagname = "os_#{detect_os(host)}"
    ::ActiveRecord::Base.connection_pool.with_connection {
      tag = find_or_create_os_tag(tagname)
      return tag if tag.hosts.include?(host)
      clear_host_os_tags(host) # Prevent multiple os tags
      tag.hosts ||= []
      tag.hosts << host unless tag.hosts.include? host
      tag.save if tag.changed?
      return tag
    }

  end

  def clear_host_os_tags(host)
    ::ActiveRecord::Base.connection_pool.with_connection {
      host.tags.each do |tag|
        next unless possible_os_autotags().include?(tag.name)
        tag.hosts.delete(host)
        tag.save
      end
      return host
    }
  end

  # This is unique to just OS labels.
  def find_or_create_os_tag(tagname)
    ::ActiveRecord::Base.connection_pool.with_connection {
      possible_tag = myworkspace.host_tags.select {|t| t.name == tagname}.first
      tag = possible_tag || ::Mdm::Tag.new
      tag.name = tagname

      # Come up with some kind of automatic description?
      # tag.desc = "Automatically tagged" # Pretty redundant
      return tag
    }

  end

  def possible_os_autotags
    [:aix, :apple, :beos, :openbsd, :freebsd, :bsd,
      :cisco, :hp, :ibm, :linux, :printer, :router,
      :solaris, :sunos, :windows, :unknown].map {|x| "os_#{x}"}
  end

  def ds_passthrough(mod, keys)
    keys.each do |x|
      mod.datastore[x] = datastore[x] unless datastore[x].nil?
    end
  end
end
end
end
