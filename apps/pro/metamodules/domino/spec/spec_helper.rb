# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'

# explicitly setup bundle before requiring environment so simplecov is available prior to loading environment
require 'rubygems'
require 'bundler'
Bundler.setup(:default, :test)

# Require simplecov before loading anything else because only code loaded after simplecov will have coverage numbers
require 'simplecov'

app = ENV['APP']

unless app
  raise ArgumentError,
        "The APP environment variable must be set to 'engine' or 'ui' to specify which application should host domino."
end

require File.expand_path("../../../../#{app}/config/environment", __FILE__)
require 'rspec/rails'

if app == 'ui'
  ui_rails_engine = Pro::Application
else
  ui_rails_engine = Metasploit::Pro::UI::Engine
end

engines = [
  Metasploit::Model::Engine,
  MetasploitDataModels::Engine,
  Metasploit::Credential::Engine,
  ui_rails_engine
]

engines.each do |engine|
  support_glob = engine.root.join('spec', 'support', '**', '*.rb')

  Dir[support_glob].each { |f|
    require f
  }
end

RSpec.configure do |config|
  SimpleCov.command_name "RSpec Domino[#{app}]"
  
  # has to be before filter_run_including, so a symbol can be passed
  config.treat_symbols_as_metadata_keys_with_true_values = true

  # only run specs compatible with the given app
  config.filter_run_including app.to_sym

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'
  
  config.deprecation_stream = Metasploit::Pro::UI::Engine.root.parent.join('test_reports','spec', 'deprecations_mm_domino.txt')

  config.use_transactional_fixtures = true
end
