require 'spec_helper'

describe  Apps::Domino::TaskConfigController, :ui do
  include_context 'ApplicationController'

  before do
    License.stub_chain(:get, :supports_vuln_validation?).and_return(true)
    skip_required_checks_on(controller)
    skip_admin_check_on(controller)
    stub_workspace_with(workspace)
    controller.stub(:current_user => user)
  end

  it_behaves_like 'filter values endpoint' do
    let(:controller_action) { :filter_values}
    let(:facet_keys) do
      [
        'address',
        'name',
        'os_name'
      ]
    end
    let(:host) { FactoryGirl.create(:mdm_host, workspace: workspace) }
    let(:vuln) { FactoryGirl.create(:mdm_vuln, host: host) }

    let(:opts) do
      {
          workspace_id: workspace.id,
      }
    end
  end

end