# -*- encoding: utf-8 -*-
# stub: domino 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "domino"
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Joe Vennix"]
  s.date = "2015-07-14"
  s.description = "Credentials domino is the king, all hail"
  s.email = ["joev@metasploit.com"]
  s.files = ["app/assets", "app/assets/javascripts", "app/assets/javascripts/domino", "app/assets/javascripts/domino/domino.coffee.erb", "app/concerns", "app/concerns/apps", "app/concerns/apps/app_run", "app/concerns/apps/app_run/domino_tracking.rb", "app/concerns/metasploit", "app/concerns/metasploit/credential", "app/concerns/metasploit/credential/core", "app/concerns/metasploit/credential/core/domino_edges.rb", "app/concerns/metasploit/credential/core/domino_nodes.rb", "app/concerns/metasploit/credential/login", "app/concerns/metasploit/credential/login/domino_edges.rb", "app/concerns/metasploit/pro", "app/concerns/metasploit/pro/engine", "app/concerns/metasploit/pro/engine/rpc", "app/concerns/metasploit/pro/engine/rpc/domino.rb", "app/concerns/pro", "app/concerns/pro/client", "app/concerns/pro/client/start_domino_metamodule.rb", "app/controllers", "app/controllers/apps", "app/controllers/apps/domino", "app/controllers/apps/domino/task_config_controller.rb", "app/models", "app/models/apps", "app/models/apps/domino", "app/models/apps/domino/edge.rb", "app/models/apps/domino/node.rb", "app/models/apps/domino/task_config.rb", "app/presenters", "app/presenters/tasks", "app/presenters/tasks/domino_presenter.rb", "app/views", "app/views/apps", "app/views/apps/domino", "app/views/apps/domino/task_config", "app/views/apps/domino/task_config/_form.html.erb", "config/metamodule.yml", "config/routes.rb", "lib/apps", "lib/apps/domino", "lib/apps/domino.rb", "lib/apps/domino/engine.rb", "spec/controllers", "spec/controllers/apps", "spec/controllers/apps/domino", "spec/controllers/apps/domino/task_config_controller_spec.rb", "spec/factories", "spec/factories/apps", "spec/factories/apps/domino", "spec/factories/apps/domino/edges.rb", "spec/factories/apps/domino/nodes.rb", "spec/models", "spec/models/apps", "spec/models/apps/domino", "spec/models/apps/domino/edge_spec.rb", "spec/models/apps/domino/node_spec.rb", "spec/spec_helper.rb"]
  s.homepage = "https://github.com/rapid7/pro/metamodules/domino"
  s.licenses = ["Proprietary"]
  s.rubygems_version = "2.4.3"
  s.summary = "Credential domino is the boss"
  s.test_files = ["spec/controllers", "spec/controllers/apps", "spec/controllers/apps/domino", "spec/controllers/apps/domino/task_config_controller_spec.rb", "spec/factories", "spec/factories/apps", "spec/factories/apps/domino", "spec/factories/apps/domino/edges.rb", "spec/factories/apps/domino/nodes.rb", "spec/models", "spec/models/apps", "spec/models/apps/domino", "spec/models/apps/domino/edge_spec.rb", "spec/models/apps/domino/node_spec.rb", "spec/spec_helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activerecord>, ["< 4.1.0", ">= 4.0.9"])
      s.add_runtime_dependency(%q<metasploit-concern>, [">= 0"])
      s.add_runtime_dependency(%q<railties>, ["< 4.1.0", ">= 4.0.9"])
    else
      s.add_dependency(%q<activerecord>, ["< 4.1.0", ">= 4.0.9"])
      s.add_dependency(%q<metasploit-concern>, [">= 0"])
      s.add_dependency(%q<railties>, ["< 4.1.0", ">= 4.0.9"])
    end
  else
    s.add_dependency(%q<activerecord>, ["< 4.1.0", ">= 4.0.9"])
    s.add_dependency(%q<metasploit-concern>, [">= 0"])
    s.add_dependency(%q<railties>, ["< 4.1.0", ">= 4.0.9"])
  end
end
