# -*- encoding: utf-8 -*-
# stub: firewall_egress 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "firewall_egress"
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Fernando Arias", "Matt Buck", "Samuel Huckins", "Luke Imhoff", "David Maloney", "Lance Sanchez", "Joe Vennix"]
  s.date = "2015-07-14"
  s.description = "Test firewall egress"
  s.email = ["ferando_arias@rapid7.com", "matt_buck@rapid7.com", "samuel_huckins@rapid7.com", "luke_imhoff@rapid7.com", "david_maloney@rapid7.com", "lance_sanchez@rapid7.com", "joev@metasploit.com"]
  s.files = ["app/assets", "app/assets/javascripts", "app/assets/javascripts/firewall_egress", "app/assets/javascripts/firewall_egress/firewall_egress.coffee.erb", "app/concerns", "app/concerns/metasploit", "app/concerns/metasploit/pro", "app/concerns/metasploit/pro/engine", "app/concerns/metasploit/pro/engine/rpc", "app/concerns/metasploit/pro/engine/rpc/firewall_egress.rb", "app/concerns/pro", "app/concerns/pro/client", "app/concerns/pro/client/start_firewall_egress_testing.rb", "app/controllers", "app/controllers/apps", "app/controllers/apps/firewall_egress", "app/controllers/apps/firewall_egress/task_config_controller.rb", "app/models", "app/models/apps", "app/models/apps/firewall_egress", "app/models/apps/firewall_egress/result_range.rb", "app/models/apps/firewall_egress/task_config.rb", "app/views", "app/views/apps", "app/views/apps/firewall_egress", "app/views/apps/firewall_egress/task_config", "app/views/apps/firewall_egress/task_config/_form.html.erb", "config/metamodule.yml", "config/routes.rb", "lib/apps", "lib/apps/app_presenters", "lib/apps/app_presenters/firewall_egress.rb", "lib/apps/firewall_egress", "lib/apps/firewall_egress.rb", "lib/apps/firewall_egress/engine.rb", "lib/apps/firewall_egress/scan.rb", "spec/controllers", "spec/controllers/apps", "spec/controllers/apps/firewall_egress", "spec/controllers/apps/firewall_egress/task_config_controller_spec.rb", "spec/factories", "spec/factories/apps", "spec/factories/apps/firewall_egress", "spec/factories/apps/firewall_egress/result_ranges.rb", "spec/lib", "spec/lib/apps", "spec/lib/apps/firewall_egress", "spec/lib/apps/firewall_egress/scan_spec.rb", "spec/models", "spec/models/apps", "spec/models/apps/firewall_egress", "spec/models/apps/firewall_egress/result_range_spec.rb", "spec/spec_helper.rb"]
  s.homepage = "https://github.com/rapid7/pro/metamodules/firewall_egress"
  s.licenses = ["Proprietary"]
  s.rubygems_version = "2.4.3"
  s.summary = "Test firewall egress"
  s.test_files = ["spec/controllers", "spec/controllers/apps", "spec/controllers/apps/firewall_egress", "spec/controllers/apps/firewall_egress/task_config_controller_spec.rb", "spec/factories", "spec/factories/apps", "spec/factories/apps/firewall_egress", "spec/factories/apps/firewall_egress/result_ranges.rb", "spec/lib", "spec/lib/apps", "spec/lib/apps/firewall_egress", "spec/lib/apps/firewall_egress/scan_spec.rb", "spec/models", "spec/models/apps", "spec/models/apps/firewall_egress", "spec/models/apps/firewall_egress/result_range_spec.rb", "spec/spec_helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activerecord>, ["< 4.1.0", ">= 4.0.9"])
      s.add_runtime_dependency(%q<metasploit-concern>, [">= 0"])
      s.add_runtime_dependency(%q<railties>, ["< 4.1.0", ">= 4.0.9"])
    else
      s.add_dependency(%q<activerecord>, ["< 4.1.0", ">= 4.0.9"])
      s.add_dependency(%q<metasploit-concern>, [">= 0"])
      s.add_dependency(%q<railties>, ["< 4.1.0", ">= 4.0.9"])
    end
  else
    s.add_dependency(%q<activerecord>, ["< 4.1.0", ">= 4.0.9"])
    s.add_dependency(%q<metasploit-concern>, [">= 0"])
    s.add_dependency(%q<railties>, ["< 4.1.0", ">= 4.0.9"])
  end
end
