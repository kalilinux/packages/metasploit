require 'spec_helper'
require 'apps/firewall_egress/result_range'

describe Apps::FirewallEgress::ResultRange, :engine, :ui do
  subject(:result_range) {FactoryGirl.build(:firewall_egress_result_range)}

  describe "validity" do
    describe "factory fixture" do
      it {should be_valid}
    end

    describe "for target host" do
      it 'should not be empty' do
        result_range.target_host = nil
        result_range.should validate_presence_of(:target_host)
      end
    end

    describe "for port state" do
      it { should validate_inclusion_of(:state).in_array described_class::VALID_PORT_STATES}
    end

    describe "for port bounds" do
      [:start_port, :end_port].each do |port_range_bound|
        it "should not allow negative port numbers for #{port_range_bound}" do
          result_range.send("#{port_range_bound}=", -1)
          result_range.should_not be_valid
        end

        it "should not allow floats for #{port_range_bound}" do
          result_range.send("#{port_range_bound}=", 3.1415)
          result_range.should_not be_valid
        end

        it "should not allow numbers bigger than 65535 for #{port_range_bound}" do
          result_range.send("#{port_range_bound}=", 70000)
          result_range.should_not be_valid
        end
      end

    end

  end

end
