package org.metasploit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRXhtmlExporter;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;

import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

import org.yaml.snakeyaml.Yaml;

public class GenerateReport {

	public static void main(String[] args) {

		//
		// print usage if the required args are missing
		//
		if (args.length < 5 || Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			printUsage();
			System.exit(1);
			return;
		}

		//
		// pull out the args
		//
		// This is starting to suck a lot. Someone ought to rewrite this with an OptParse
		// or something.

		String jrxmlFilename = args[0];
		String reportFilename = args[1];
		String dbConfigFile = args[2];
		String paramConfigFile = args[3];
		String imgDir = args[4]; // This is only used for html exports, so this required arg is totally wrong
		String virtDir = args[5]; // Location for swap file for virtualization
		String railsEnvironment = (args.length > 6) ? args[6] : "production";

        //
        // determine file type
        //

        String nameExt[];
        nameExt = reportFilename.split("\\.");
        String reportType = nameExt[nameExt.length-1];

		//
		// open the input file or use stdin if "-" was given
		//

		InputStream in;
		if (jrxmlFilename.equals("-")) {
			in = System.in;
		} else {
			try {
				in = new FileInputStream(new File(jrxmlFilename));
			} catch (FileNotFoundException e) {
				System.err.println(e.getMessage());
				System.exit(1);
				return;
			}
		}

		//
		// open the output file or use stdout if "-" was given
		//

		OutputStream out;
		if (reportFilename.equals("-")) {
			out = System.out;
		} else {
			try {
				out = new FileOutputStream(new File(reportFilename));
			} catch (FileNotFoundException e) {
				System.err.println(e.getMessage());
				System.exit(1);
				return;
			}
		}

		try {
			//
			// connect to the db
			//
			
			Connection conn;
			try {
				conn = getConnection(dbConfigFile, railsEnvironment);
			} catch (ClassNotFoundException e) {
				System.err.println("Failed to load database driver: " + e.getMessage());
				e.printStackTrace();
				System.exit(1);
				return;
			} catch (SQLException e) {
				System.err.println("Failed to connect to database: " + e.getMessage());
				e.printStackTrace();
				System.exit(1);
				return;
			}

			//
			// compile the report
			//
			
			JasperDesign design = JRXmlLoader.load(in);
			JasperReport report = JasperCompileManager.compileReport(design);

			//
			// fill the report
			//
            Map<String,Object> parameters;
            try {
                parameters = loadParamConfig(paramConfigFile);
            } catch (FileNotFoundException e) {
				System.err.println("Failed to read parameters from parameter file: " + e.getMessage());
				e.printStackTrace();
				System.exit(1);
				return;
			}
			
      
			// Virtualization
			File jasperDir = new File(virtDir);
			if (!jasperDir.exists())
			{
				jasperDir.mkdir();
			}

			// target directory for disk file, initial size of file, growth size:
			JRSwapFile swapDir = new JRSwapFile(virtDir, 1024, 100);
			// maxSize (in JRVirtualizable objects) of the pages in cache
			// (before writing to disk),
			// directory for temp files, bool delete files upon completion.
			// Note: maxSize is shared when multiple reports are being generated.
			JRVirtualizer virtualizer = new JRSwapFileVirtualizer(100, swapDir, true);

			parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			//System.out.println("Using virtualizer in " + virtDir);
			// end Virtualization
	   
			JasperPrint print = JasperFillManager.fillReport(report, parameters, conn);


			//
			// export to reportType
			//
		
            if(reportType.equals("pdf")) {
                System.out.println("Writing PDF Report to " + reportFilename);
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                exporter.exportReport();
            } else if(reportType.equals("docx")) {
                System.out.println("Writing Microsoft Word 2007 Report to " + reportFilename);
                JRDocxExporter exporter = new JRDocxExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                exporter.exportReport();
            } else if(reportType.equals("odt")) {
                System.out.println("Writing OpenOffice ODT Report to " + reportFilename);
                JROdtExporter exporter = new JROdtExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                exporter.exportReport();
            } else if(reportType.equals("rtf")) {
                System.out.println("Writing Rich Text Format RTF Report to " + reportFilename);
                JRRtfExporter exporter = new JRRtfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                exporter.exportReport();
            } else if(reportType.equals("html")) {
								File imgDirIo = new File(imgDir);
                // Limiting to just new web app report until issues resolved with others:
                JRExporter exporter = null;
                if(jrxmlFilename.contains("webapp_assessment")) {
                  exporter = new JRXhtmlExporter();
                  System.out.println("Writing XHTML Report to " + reportFilename);
                } else {
                  exporter = new JRHtmlExporter();
                  System.out.println("Writing HTML Report to " + reportFilename);
                }
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                exporter.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.TRUE);
                exporter.setParameter(JRHtmlExporterParameter.IMAGES_DIR, imgDirIo);
                System.out.println("Images will be saved to  " + imgDir);
                exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, imgDir);
                exporter.exportReport();
            } else {
                System.out.println("Unknown file type, defaulting to PDF.");
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                exporter.exportReport();
            }

		} catch (JRException e) {
			System.err.println("Failed to generate report: " + e.getMessage());
		}

		System.exit(0);
	}

	private static void printUsage() {
		System.out.println("Usage: GenerateReport <jrxml_file> <pdf_file> <database_yml_file> <parameter_yml_file> [environment]");
	}

	@SuppressWarnings("unchecked")
	private static Map<String, Object> loadDatabaseConfig(String databaseYmlFile, String environment)
			throws FileNotFoundException {
		Yaml yaml = new Yaml();
		@SuppressWarnings("rawtypes")
		Map obj = (Map) yaml.load(new FileInputStream(new File(databaseYmlFile)));
		return (Map<String, Object>) obj.get(environment);
	}

    /**
     * @param paramConfigFile the path to jasperparams.yml
     * @return the params hashmap
     */
	@SuppressWarnings("unchecked")
    private static Map<String, Object> loadParamConfig(String paramYmlFile)
            throws FileNotFoundException {
        Yaml yaml = new Yaml();
        @SuppressWarnings("rawtypes")
            Map obj = (Map) yaml.load(new FileInputStream(new File(paramYmlFile)));
        return (Map<String, Object>) obj;
    }


	/**
	 * @param host the host name
	 * @param database the database name
	 * @param port the port number (or null to use the default port)
	 * @return JDBC URL
	 */
	private static String getJdbcUrl(String host, String database, Integer port) {
		String url = "jdbc:postgresql://" + host;
		if (port != null) {
			url += ":" + port;
		}
		url += "/" + database;
		return url;
	}

	/**
	 * @param user
	 * @param password
	 * @return connection Properties
	 */
	private static Properties getConnectionProperties(String user, String password) {
		Properties props = new Properties();
		if (user != null) {
			props.setProperty("user", user);
		}
		if (password != null) {
			props.setProperty("password", password);
		}
		return props;
	}

	
	/**
	 * @param dbConfigFile the path to database.yml
	 * @param railsEnvironment rails environment (production, development, test)
	 * 
	 * @return a db connection to the postgres database
	 * 
	 * @throws ClassNotFoundException if postgres driver isn't in the classpath
	 * @throws SQLException if connection fails
	 */
	private static Connection getConnection(String dbConfigFile, String railsEnvironment) throws ClassNotFoundException,
			SQLException {

		// load the database config
		Map<String, Object> databaseConfig;
		try {
			databaseConfig = loadDatabaseConfig(dbConfigFile, railsEnvironment);
		} catch (FileNotFoundException e1) {
			throw new SQLException("Failed to load " + railsEnvironment + " configuration from " + dbConfigFile);
		}
	
		Object hostValue = databaseConfig.get("host");
		String host = (hostValue == null) ? "localhost" : String.valueOf(databaseConfig.get("host"));
		String database = String.valueOf(databaseConfig.get("database"));
		String username = String.valueOf(databaseConfig.get("username"));
		String password = String.valueOf(databaseConfig.get("password"));
		Integer port = (Integer) databaseConfig.get("port"); // may be null

		// load the database driver class
		Class.forName("org.postgresql.Driver");

		String url = getJdbcUrl(host, database, port);
		Properties props = getConnectionProperties(username, password);

		Connection conn = DriverManager.getConnection(url, props);
		return conn;
	}

}

