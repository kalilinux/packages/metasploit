package org.metasploit.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

public class ScriptletUtil extends JRDefaultScriptlet
{
	/**
	 * Returns a rough copy of the content of a text file.
	 *
	 * @param textFilePath The FQP of the text file.
	 * 
	 * @return The string contents of the text file.
	 * 
	 * @throws JRScriptletException If loading the file fails.
	 */
	public String loadTextFile(String textFilePath) throws JRScriptletException
	{
		String text = "";
		
		/*
		 * For some reason Jasper did not like backslash in file name paths
		 */
		if (textFilePath.contains("\\"))
			textFilePath = textFilePath.replaceAll("\\\\", "/");
		
		try
		{
			final File textFile = new File(textFilePath);
			if (!textFile.canRead())
				throw new IOException("Cannot read file: " + textFilePath);
			
			final BufferedReader reader = new BufferedReader(new FileReader(textFile));
			String input = "";
			while ((input = reader.readLine()) != null)
			{
				if  (!input.isEmpty())
					text += "\n";				
				text += input;					
			}
		}
		catch (Exception e)
		{
			throw new JRScriptletException(e);
		}
		
		return text;
	}
}
