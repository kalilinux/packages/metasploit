package org.metasploit.datatransform;
import java.sql.Array;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * Used within Jasper reports to transform vuln refs.
 * 
 * @author Christopher Lee.
 */
public class VulnRefTransformer extends JRDefaultScriptlet
{	
	
	/**
	 * Shortens module names
	 * 
	 * @param name The full module name eg: exploit/windows/smb/psexec
	 * 
	 * @return The shortened form exploit/windows/smb/psexec => psexec
	 */
	public String getShortenedModuleName(final String name)
	{
		final int lastIndex = name.lastIndexOf("/");
		if (lastIndex < 0)
			return name;
		else
			return name.substring(lastIndex+1, name.length());
	}
	
	/**
	 * Converts an SQL Array type to a vertical format.
	 * 
	 *                 1
	 * ie: {1,2,3} =>  2
	 *                 3
	 * 
	 * @param array The SQL array input.
	 * 
	 * @return The transformed string.
	 * 
	 * @throws JRScriptletException
	 */
	public String verticallyFormatTimeString(String input) throws JRScriptletException
	{
		if (input == null)
			return "";
		
		try
		{
			String transform = "";
			final String[] values = input.split(",");
			for (String value : values)
			{
				transform += value + "\n";
			}
			
			return transform;
		}
		catch (Exception e)
		{
			throw new JRScriptletException(e);
		}
	}
	
	/**
	 * Converts an array of vuln ref IDs into a string with description and URL.
	 * 
	 * @param vulnRefKey The vulnerability reference keys for a specific vulnerability
	 * as stored by the metasploit database.
	 * 
	 * @return The transformed string for a properly formatted reference key.
	 * 
	 * @throws JRScriptletException
	 */
	public String getVulnRefTransform(Array vulnRefKeys) throws JRScriptletException
	{
		if (vulnRefKeys == null)
			return "";
		
		try
		{
			String[] castedKeys = (String [])vulnRefKeys.getArray();
			String transform = "";
			for (String refKey : castedKeys)
			{	
				if (!transform.trim().isEmpty())
					transform += "\n";
				
				String[] typeAndID = getTypeAndID(refKey);
				String description = getDescription(typeAndID[0], typeAndID[1]);
				String url = getURL(typeAndID[0], typeAndID[1]);
				String s = "";

        if (url != "") {
          s = description + " - " + url;
        } else {
          s = description;
        }
				transform += s;
			}
			
			// Add new line at end to fix jasper RTF problem which writes last line into
			// the table line.
			transform += "\n";
			
			return transform;
		}
		catch (Exception e)
		{
			throw new JRScriptletException(e);
		}
		
	}
	
	/*
	 * Fix so that when jasper cuts through long URL's it will
	 * stil be selectable.
	 */
	private String quoteURL(String url)
	{
		return "\"" + url + "\"";
	}
	
	/*
	 * Breaks up a vuln ref into a type and its ID.
	 */
	private String[] getTypeAndID(String vulnRefKey)
	{
		final String[] parsedRef = vulnRefKey.split("-", 2);
		String id = parsedRef[0].trim();
		String value = "";
		
		if (parsedRef.length < 2)
		{
			value = id;
			id = "GENERIC";
		}
		else
		{
			value = parsedRef[1].trim();
		}
		
		parsedRef[0] = id;
		parsedRef[1] = value;
		return parsedRef;
	}
	
	/*
	 * parses the URL for a particular vuln.
	 */
	private String getURL(String type, String ref)
	{
		if ("OSVDB".equalsIgnoreCase(type))
			return "http://www.osvdb.org/" + ref;
		else if ("CVE".equalsIgnoreCase(type))
			return "http://cve.mitre.org/cgi-bin/cvename.cgi?name=" + ref;
		else if ("BID".equalsIgnoreCase(type))
			return "http://www.securityfocus.com/bid/" + ref;
		else if ("MSB".equalsIgnoreCase(type))
			return "http://www.microsoft.com/technet/security/bulletin/" + ref + ".mspx";
		else if ("MIL".equalsIgnoreCase(type))
			return "http://milw0rm.com/metasploit/" + ref;
		else if ("WVE".equalsIgnoreCase(type))
			return "http://www.wirelessve.org/entries/show/WVE-" + ref;
		else if ("US-CERT-VU".equalsIgnoreCase(type))
			return "http://www.kb.cert.org/vuls/id/" + ref;
		else if ("BPS".equalsIgnoreCase(type))
			return "https://strikecenter.bpointsys.com/bps/advisory/BPS-" + ref;
		else if ("NEXPOSE".equalsIgnoreCase(type))
			return "http://www.rapid7.com/vulndb/lookup/" + ref;
		else if ("SECUNIA".equalsIgnoreCase(type))
			return "http://secunia.com/advisories/" + ref;
		else if ("NSS".equalsIgnoreCase(type))
			return "http://www.nessus.org/plugins/index.php?view=single&id=" + ref;
		else if ("XF".equalsIgnoreCase(type))
		{
			 Pattern pattern = Pattern.compile("[^-][0-9]+[^-]");
			 Scanner scanner = new Scanner(ref);
			 String last = "";
			 while (scanner.hasNext(pattern))
				 last = scanner.next(pattern);
			return "http://xforce.iss.net/xforce/xfdb/" + last;
		}
		else if ("URL".equalsIgnoreCase(type))
			return ref;
		else if ("GENERIC".equalsIgnoreCase(type))
			return "#";
		else
			return "";
	}
	
	/*
	 * Get the description for a vuln ref.
	 */
	private String getDescription(String type, String ref)
	{
		if ("NEXPOSE".equalsIgnoreCase(type))
			return "Rapid7 VulnDB";
		else if ("MSB".equalsIgnoreCase(type))
		{
			 Pattern pattern = Pattern.compile(".*(MS[0-9]{2}-[0-9]{3}).*");
			 Matcher matcher = pattern.matcher(ref);
			 if (matcher.matches())
				 return matcher.group(1);
			 else
				 return type + "-" + ref;
		}
		else if ("URL".equalsIgnoreCase(type))
			return "URL";
		else if (type == null || "GENERIC".equalsIgnoreCase(type))
			return ref;
		else
			return type + "-" + ref;
	}	
}
