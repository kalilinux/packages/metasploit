package org.metasploit;
/*
 * An XML Jasper interface; Takes XML data from the standard input
 * and uses JRXmlDataSource to generate Jasper reports in the
 * specified output format using the specified compiled Jasper design.
 * 
 * Inspired by the xmldatasource sample application provided with
 * jasperreports-1.1.0
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRVirtualizer;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;

import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

/**
 * @author Jonas Schwertfeger (jonas at schwertfeger dot ch)
 * @version $Id: XmlJasperInterface.java 97 2005-11-23 14:48:15Z js $
 */
public class XmlJasperInterface {
  private static final String TYPE_PDF = "pdf";
  private static final String TYPE_XML = "xml";
  private static final String TYPE_RTF = "rtf";
  private static final String TYPE_XLS = "xls";
  private static final String TYPE_CSV = "csv";
  private static final String TYPE_HTML = "html";
  
  private String outputType;
  private String compiledDesign;
  private String selectCriteria;
  private String imgDir;
  private String virtDir;

  public static void main(String[] args) {

    String outputType = null;
    String compiledDesign = null;
    String selectCriteria = null;
    String imgDir = null;
    String virtDir = null;

    if (args.length < 4) {
      printUsage();
      return;
    }

    for (int k = 0; k < args.length; ++k) {
      if (args[k].startsWith("-o"))
        outputType = args[k].substring(2);
      else if (args[k].startsWith("-f"))
        compiledDesign = args[k].substring(2);
      else if (args[k].startsWith("-x"))
        selectCriteria = args[k].substring(2);
			else if (args[k].startsWith("-i"))
				imgDir = args[k].substring(2);
			else if (args[k].startsWith("-v"))
				virtDir = args[k].substring(2);
    }
    
    XmlJasperInterface jasperInterface = new XmlJasperInterface(outputType, compiledDesign, selectCriteria, imgDir, virtDir);

    if (!jasperInterface.report()) {
      System.exit(1);
    }
  }
  
  public XmlJasperInterface(
      String outputType,
      String compiledDesign,
      String selectCriteria,
			String imgDir,
			String virtDir) {
    this.outputType = outputType;
    this.compiledDesign = compiledDesign;
    this.selectCriteria = selectCriteria;
		this.imgDir = imgDir;
		this.virtDir = virtDir;
  }
  
  public boolean report() {
		try {

			// Virtualization
			final Map<String, Object> parameters = new HashMap<String, Object>();
			File jasperDir = new File(virtDir);
			if (!jasperDir.exists())
			{
				jasperDir.mkdir();
			}

			// target directory for disk file, initial size of file, growth size:
			JRSwapFile swapDir = new JRSwapFile(virtDir, 1024, 100);
			// maxSize (in JRVirtualizable objects) of the pages in cache
			// (before writing to disk),
			// directory for temp files, bool delete files upon completion.
			// Note: maxSize is shared when multiple reports are being generated.
			JRVirtualizer virtualizer = new JRSwapFileVirtualizer(100, swapDir, true);

			parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			// end Virtualization

      JasperPrint jasperPrint = JasperFillManager.fillReport(compiledDesign, parameters, new JRXmlDataSource(System.in, selectCriteria));
      
      if (TYPE_PDF.equals(outputType)) {
        JasperExportManager.exportReportToPdfStream(jasperPrint, System.out);
      }
      else if (TYPE_XML.equals(outputType)) {
        JasperExportManager.exportReportToXmlStream(jasperPrint, System.out);
      }
      else if (TYPE_RTF.equals(outputType)) {
        JRRtfExporter rtfExporter = new JRRtfExporter();
        rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, System.out);
        rtfExporter.exportReport();
      }
      else if (TYPE_XLS.equals(outputType)) {
        JRXlsExporter xlsExporter = new JRXlsExporter();
        xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, System.out);
        xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
        xlsExporter.exportReport();
      }
      else if (TYPE_CSV.equals(outputType)) {
        JRCsvExporter csvExporter = new JRCsvExporter();
        csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        csvExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, System.out);
        csvExporter.exportReport();
			}
      else if (TYPE_HTML.equals(outputType)) {
				File imgDirIo = new File(imgDir);
        JRHtmlExporter htmlExporter = new JRHtmlExporter();
        htmlExporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
        htmlExporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, System.out);
				htmlExporter.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.TRUE);
				htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_DIR, imgDirIo);
				htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, imgDir);
        htmlExporter.exportReport();
      } else {
        printUsage();
      }
    } catch (JRException e) {
      e.printStackTrace();
      return false;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  private static void printUsage() {
    System.out.println("XmlJasperInterface usage:");
    System.out.println("\tjava XmlJasperInterface -oOutputType -fCompiledDesign -xSelectExpression -iImageDirectory -vVirtualizationDirectory < input.xml > report\n");
    System.out.println("\tOutput types:\t\tpdf | xml | rtf | xls | csv | html");
    System.out.println("\tCompiled design:\tFilename of the compiled Jasper design");
    System.out.println("\tSelect expression:\tXPath expression that specifies the select criteria");
    System.out.println("\tImage Directory:\tDirectory to store images (html format only)");
    System.out.println("\tVirtualization Directory:\tDirectory to store virtualization swap file.");
    System.out.println("\t\t\t\t(See net.sf.jasperreports.engine.data.JRXmlDataSource for further information)");
    System.out.println("\tStandard input:\t\tXML input data");
    System.out.println("\tStandard output:\tReport generated by Jasper");
  }
}
