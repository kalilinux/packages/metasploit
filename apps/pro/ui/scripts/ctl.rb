#!/usr/bin/env ruby

def get_thin_port(base)
	res = 3001
	cnf = File.join(base, "apps", "pro", "ui", "conf", "metasploit.conf")
	return res if not File.exist?(cnf)
	dat = File.read(cnf)
	if dat =~ /BalancerMember\s+http:\/\/127.0.0.1:(\d+)/
		res = $1.to_i
	end
	res
end

def get_thin_pid(base)
	pidfile = File.join(base, "apps", "pro", "ui", "log", "thin.pid")
	return nil unless ::File.exist?(pidfile)
	pid = ::File.read(pidfile).strip.to_i
	return nil unless pid > 0
	pid
end

def thin_opts
	['-e', 'production', '-l', 'log/thin.log', '-P', 'log/thin.pid']
end

def start_thin(base, port)
	Dir.chdir(File.join(base, "apps", "pro", "ui"))
	ARGV.push('start', '-p', port.to_s, '-a', '127.0.0.1', '-d', *thin_opts)
	require 'rubygems'
	require 'bundler/setup'
	version = '>= 0'
	gem 'thin', version
	load Gem.bin_path('thin', 'thin', version)
end

# Get our bearings
@cwd = File.dirname(__FILE__)
@pro = File.expand_path(File.join(@cwd, "..", "..", "..", ".."))
@prt = get_thin_port(@pro)
@pid = get_thin_pid(@pro)

# Parse arguments
@act = ARGV.shift

case @act
when "start"
	start_thin(@pro, @prt)
when "stop"
	# The normal thin stop command throws a nasty stack trace if the
	# service is already stopped.
	# Stop manually a little more graceful
	if @pid
		::Process.kill(9, @pid) rescue nil
	end
	system("pkill -9 -f thin")
	puts "metasploit is stopped"
when "status"
	if @pid
		if (::Process.kill(0, @pid).to_i rescue 0) == 1
			puts "metasploit is running"
		else
			puts "metasploit is stopped"
		end
	else
		puts "metasploit is stopped"
	end
else
	exit(0)
end
