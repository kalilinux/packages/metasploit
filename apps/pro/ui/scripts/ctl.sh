#!/bin/sh

# Get a reference to our current directory and script name
SOURCE="$0"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  test ${SOURCE#/} = ${SOURCE} && SOURCE="$DIR/$SOURCE"
done
BASE="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd "${BASE}"

# Run as daemon
if [ $(id -u) -eq 0 ]; then
  exec su daemon -s /bin/sh -c "$0 $@"
fi

# Set up the environment
. "${BASE}/../../../../scripts/setenv.sh"

exec ruby "${BASE}/ctl.rb" $@
