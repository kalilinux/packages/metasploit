require 'action_mailer'

class SmtpConfigurer
  def initialize(smtp_settings)
    @settings = smtp_settings
  end

  def load_configuration
    mailer_hash = @settings.to_mailer
    ActionMailer::Base.smtp_settings = mailer_hash
  end
end
