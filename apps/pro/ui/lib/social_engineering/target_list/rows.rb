module SocialEngineering::TargetList::Rows
  extend ActiveSupport::Concern

  load 'social_engineering/target_list/rows/CSV.rb'
  include SocialEngineering::TargetList::Rows::CSV    
  include SocialEngineering::TargetList::Rows::QuickAdd

  included do
    #
    # Callbacks
    #

    before_save :process_rows, :if => :any_targets_present?
  end

  private

  def any_targets_present?
    csv_targets_present? || quick_add_targets_present?
  end

  # Pulls out email addresses from extracted list,
  # searches for matches amongst current HTs.
  # In the case of a match: only add join table entry.
  # Otherwise, add HT as usual, along with join table entry.
  def process_rows
    # combine csv_uniq_quick_add and csv_uniq_csv members
    @uniq_quick_add_members ||= []
    @uniq_csv_members ||= []

    uniq_members = (@uniq_quick_add_members + @uniq_csv_members)

    # fail here if there are NO rows to process
    if uniq_members.empty?
      errors.add(:base, "No valid human targets to add.")
      return false
    end

    uniq_members.each do |member|
      begin
        human_target = SocialEngineering::HumanTarget.where(
            :workspace_id => self.workspace.id,
            :email_address => member[:email_address],
            :first_name => member[:first_name],
            :last_name => member[:last_name]
        ).first_or_create!
      rescue
        errors.add(:base, "Could not create Human Target.")
        return false
      end

      unless human_target.valid?
        errors.add(:base, "Invalid human target.")
        return false
      end

        self.target_list_targets.build(
          :target_list_id => self.id,
          :human_target_id => human_target.id
        )
    end
  end

  def validate_row(row, row_cnt=0, filename='')
    # Email must be somewhat recognizable as an email:
    email = row["email_address"]
    email_pat = /.+@.+\..+/
    email_valid = (email =~ email_pat) == 0 ? true : false

    if not email or email == ''
      errors.add(:base, "Error: Undefined email address for human target at row #{row_cnt} in #{filename}.")
    elsif not email_valid
      errors.add(:base, "Error: Email address not properly formatted at row #{row_cnt} in #{filename}.")
    else
      return true
    end
    false
  end
end