module Web::RequestEngine
  extend Web::RequestEngine::Parts

  uses :enclosers
  uses :escapers
  uses :evaders
  uses :executors
end