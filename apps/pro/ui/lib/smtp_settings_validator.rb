require 'net/smtp'
class SmtpSettingsValidator
  SMTP_VALIDATION_TIMEOUT = 3

  def initialize(opts= {})
    @server      = opts[:server]
    @port        = opts[:port]
    @use_ssl     = opts[:ssl] || false
    @username    = opts[:username] || ''
    @password    = opts[:password] || ''
    @domain      = opts[:domain] || 'localhost'
    @auth        = opts[:auth].to_sym || :plain

    # Why?
    @domain = 'localhost' if @domain.blank?
  end

  def errors
    @errors ||= []
  end

  # @return [Boolean] if we are able to connect to the specified SMTP
  #   server. Adds error messages to the @errors array, a la AR.
  def valid?
    if @server.blank? 
      errors << 'Invalid host'
    elsif @port.blank?
      errors << 'Invalid port'
    else
      smtp = Net::SMTP.new(@server, @port)
      smtp.open_timeout = SMTP_VALIDATION_TIMEOUT
      smtp.read_timeout = SMTP_VALIDATION_TIMEOUT

      if @use_ssl
        smtp.enable_ssl
      else
        smtp.disable_ssl
      end

      begin
        if @username.blank? && @password.blank?
          smtp.start
        else
          smtp.start(@domain, @username, @password, @auth)
        end
      rescue OpenSSL::SSL::SSLError
        errors << 'SSL error'
      rescue  Timeout::Error, IOError
        errors << 'Connection timeout error'
      rescue  Net::SMTPAuthenticationError
        errors << 'Authentication error'
      rescue SocketError, Errno::ECONNREFUSED, Errno::EADDRNOTAVAIL
        errors << 'Could not connect to host'
      ensure
        smtp.finish if smtp.started?
      end
      
    end
    errors.empty?
  end
end