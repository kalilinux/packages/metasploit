class NexposeImporter
  def initialize(opts={})
    @console = opts[:console]
    @current_user = opts[:current_user]
    @workspace = opts[:workspace]
  end

  def run
    # if there exists an ImportRun in the last few minutes, just use that
    @import_run = ::Nexpose::Data::ImportRun.where(
        :nx_console_id => @console.id,
        :state => [:ready_to_import, :acquiring_environment_data]
    ).where(
        "created_at > current_timestamp - interval '" +
            "#{::Nexpose::Data::ImportRun::CACHE_EXPIRATION}' minute"
    )

    @import_run.where(workspace: @workspace) if @workspace

    @import_run = @import_run.first

    if @import_run.blank?
      @import_run = ::Nexpose::Data::ImportRun.create(
          :console => @console,
          :user => @current_user,
          :workspace => @workspace
      )
      @import_run.delay.start! # run asynchronously
    end

    @import_run
  end

end