class CampaignResetter
  def self.reset(campaign)
    SocialEngineering::Visit.remove_for(campaign)
    SocialEngineering::PhishingResult.remove_for(campaign)
    SocialEngineering::EmailOpening.remove_for(campaign)
    SocialEngineering::EmailSend.remove_for(campaign)
  end
end
