class SubmittedFormsFinder < Struct.new(:campaign)
  def find
    selects = [ 'se_phishing_results.id AS phishing_result_id',
                'email_address', 
                'se_phishing_results.created_at AS created_at', 
                'se_human_targets.id AS human_target_id',
                'first_name',
                'last_name',
                'web_page_id',
                'se_web_pages.name AS web_page_name' ]

    # Need to use a LEFT JOIN to get anonymous traffic as well as that 
    # associated with existing HumanTarget objects
    joins = ["LEFT JOIN se_human_targets ON human_target_id = se_human_targets.id", :web_page]
    phishing_results = SocialEngineering::PhishingResult.where(web_page_id: campaign.web_pages)
                                                        .joins(joins)
                                                        .select(selects)

    phishing_results
  end

  # fields that we want to return in the result
  def datatable_columns
    [:email_address, :first_name, :last_name, :web_page_name, :created_at, :phishing_result_id, :human_target_id, :web_page_id]
  end

  # fields that we want to be searchable
  def datatable_search_columns
    [:email_address, :first_name, :last_name, 'se_web_pages.name']
  end
end

