class ActiveRecord::Base
	# TODO: For Rails 3 upgrade, this will become a custom validator.
	def self.validates_ip_address(method)
		validates_format_of method, :with => /(#{Rex::Socket::MATCH_IPV4})|(#{Rex::Socket::MATCH_IPV6})/, :message => "must be an IPv4 or IPv6 address"
	end
end
