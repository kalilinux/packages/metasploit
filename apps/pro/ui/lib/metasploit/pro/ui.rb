#
# Gems
#
# gems must load explicitly any gem declared in gemspec
# @see https://github.com/bundler/bundler/issues/2018#issuecomment-6819359
#
#

require 'active_record'
require 'acts_as_list'
require 'after_commit_queue'
require 'authlogic'
require 'carrierwave'
require 'cookiejar'
require 'delayed_job_active_record'
require 'formtastic'
require 'has_scope'
require 'ice_cube'
require 'liquid'
require 'metasploit/concern'
require 'metasploit/credential'
require 'metasploit_data_models'
require 'network_interface'
require 'pcaprub'
require 'pg'
require 'state_machine'
require 'wicked'
Metasploit::Pro::Metamodules.require

module Metasploit
  module Pro
    # Namespace for this gem.
    module UI
    end
  end
end