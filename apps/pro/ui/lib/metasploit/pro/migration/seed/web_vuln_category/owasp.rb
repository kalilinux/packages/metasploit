# @abstract subclass and define ATTRIBUTES_LIST, TARGET, and VERSION.  ATTRIBUTES_LIST are attributes of seeds (minus
#   TARGET and VERSION) in rank order.  {Web::VulnCategory::OWASP#rank} is derived from
#   the index in ATTRIBUTES_LIST with index `0` mapping to rank `1`, etc.  TARGET should be either 'Application' for
#   normal OWASP or 'Mobile' for the newer Mobile list.  VERSION should be the version of the TARGET's list such as the
#   year, '2010', or year and release status '2013rc1'.
#
# Seeds {Web::VulnCategory::OWASP} for a given `TARGET` and `VERSION`.
class Metasploit::Pro::Migration::Seed::WebVulnCategory::OWASP < Metasploit::Pro::Migration::Seed::Base
  # Yields scope for each {Web::VulnCategory::OWASP} record.
  #
  # @yield [scope]
  # @yieldparam scope [ActiveRecord::Relation] scope that uniquely identifies each record and contains required fields
  #   for creation.
  # @yieldreturn [void]
  # @return [void]
  def each_scope
    # qualify constant off self.class or subclasses won't be able to define constant and have it found
    self.class::ATTRIBUTES_LIST.each_with_index do |attributes, index|
      rank = index + 1

      scope = Web::VulnCategory::OWASP.where(attributes)
      # qualify constant off self.class or subclasses won't be able to define constant and have it found
      scope = scope.where(:rank => rank, :target => self.class::TARGET, :version => self.class::VERSION)

      yield scope
    end
  end
end