# @abstract subclass and define ATTRIBUTES_SET.  ATTRIBUTES_SET are attributes of seeds.
#
# Seeds {Web::VulnCategory::Metasploit}.
class Metasploit::Pro::Migration::Seed::WebVulnCategory::Metasploit < Metasploit::Pro::Migration::Seed::Base
  # Yields a scope for each {Web::VulnCategory::Metasploit} record
  #
  # @yield [scope]
  # @yieldparam scope [ActiveRecord::Relation] scope that uniquely specifies one Web::VulnCategory::Metasploit
  # @yieldreturn [void]
  # @return [void]
  def each_scope
    # qualify constant off self.class or subclasses won't be able to define constant and have it found
    self.class::ATTRIBUTES_SET.each do |attributes|
      scope = Web::VulnCategory::Metasploit.where(attributes)

      yield scope
    end
  end
end