# Migration used to seed (on {#up}) and destroy (on {#down}) seed data for pre-existing installs
# of Metasploit that won't use `rake db:seed`.
#
# @abstract Subclass in a migration in db/migrate_pro and override the {#each_scope} method to yield
#   scopes for the model seeds.
class Metasploit::Pro::Migration::Seed::Base < ActiveRecord::Migration
  # Destroys {#each_scope}.
  #
  # @return [void]
  def down
    say_with_time('Destroying seeds...') do
      each_scope(&:destroy_all)
    end
  end

  # Yields a scope containing the attributes for each seed.
  #
  # @abstract When subclassing {Metasploit::Pro::SeedMigration},
  #
  # @yield [scope] A scope with different attributes for each seed (such as by using `Model.where(attributes)`)
  # @yieldparam scope [ActiveRecord::Relation, #destroy_all, #first_or_create!] a scope that maps to one-and-only one
  #   seed, so that `destroy_all` can be called on it in {#down} and `first_or_create!` can be called on it in {#up}.
  # @yieldreturn [void]
  # @raise [NotImplementedError] if you forgot to override the method and it is still abstract
  def each_scope
    not_implemented
  end

  # Creates {#each_scope} unless it already exists.
  #
  # @return [void]
  def up
    say_with_time('Creating non-existent seeds...') do
      each_scope do |scope|
        begin
          scope.first_or_create!
        rescue ActiveRecord::RecordInvalid => error
          yaml = error.record.to_yaml

          yaml.each_line do |line|
            say(line, true)
          end

          raise
        end
      end
    end
  end
end