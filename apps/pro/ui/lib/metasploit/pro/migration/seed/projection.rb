# Projects a {source_class} onto a {destination_class} using a {projection_class} where there are seeds of
# {source_class} mapped to {destination_class} to make {projection_class} seeds. Use the {project} DSL to setup the
# metadata needed for {#each_scope} used in #up and #down.
#
# @example Mapping 'Vulnerable Version' Metasploit category to 'Using Components with Known Vulnerabilities' OWASP category
#   class SeedWebVulnCategoryProjectionMetasploitOwasps2013 < Metasploit::Pro::Migration::Seed::Projection
#     projects Web::VulnCategory::Metasploit,
#              :onto => Web::VulnCategory::OWASP,
#              :using => Web::VulnCategory::Projection::MetasploitOWASP,
#              :where => {
#                {
#                  :summary => 'Vulnerable Version'
#                } => {
#                  :summary => 'Using Components with Known Vulnerabilities',
#                  :target => 'Application',
#                  :version => '2013rc1'
#                }
#              }
#   end
class Metasploit::Pro::Migration::Seed::Projection < Metasploit::Pro::Migration::Seed::Base
  class << self
    # @!attribute [r] destination_attributes_by_source_attributes
    #   @return [Hash{Hash => Hash}] maps attributes that uniquely identify one {#source_class} seed to the attributes
    #     that uniquely identify one {#destination_class} seed.
    attr_reader :destination_attributes_by_source_attributes

    # @!attribute [r] destination_class
    #   @return [Class] The :onto option passed to {project}.
    attr_reader :destination_class

    # Returns the foreign_key of {destination_class} in belongs_to association on {projection_class}.
    #
    # @return (see foreign_key)
    # @raise (see foreign_key)
    def destination_foreign_key
      @destination_foreign_key ||= foreign_key(destination_class)
    end

    # Declares this Seed migration is meant to project the source class's seeds onto the destination class's seeds.
    #
    # @param source [Class] the class whose attributes are the keys for `:where`.
    # @param options [Hash{Symbol => Object}] keyword arguments
    # @option options [Class] :onto the class whose attributes are the values for `:where`.
    # @option options [Class] :using the class representing the join model between the source and destination (specified
    #   by `:onto`) classes.
    # @option options [Hash{Hash => Hash}}] :where maps source attributes to destination attributes.  The attributes
    #   should uniquely identify one seed for each class or {#check_count!} will raise an exception in {#each_pair}.
    # @return [void]
    #
    # @raise [KeyError] if :onto is not given
    # @raise [KeyError] if :using is not given
    # @raise [KeyError] if :where is not given
    def project(source, options={})
      options.assert_valid_keys(:onto, :using, :where)

      @source_class = source
      @destination_class = options.fetch(:onto)
      @projection_class = options.fetch(:using)
      @destination_attributes_by_source_attributes = options.fetch(:where)
    end

    # @!attribute [r] projection_class
    #   @return [Class] the join model between {destination_class} and {source_class}
    attr_reader :projection_class

    # @!attribute [r] source_class
    #   @return [Class] class passed to {project} that supplies the keys for :where
    attr_reader :source_class

    # Returns the foreign_key of {source_class} in belongs_to association on {projection_class}.
    #
    # @return (see foreign_key)
    # @raise (see foreign_key)
    def source_foreign_key
      @source_foreign_key ||= foreign_key(source_class)
    end

    private

    # Returns foreign_key of klass in belongs_to association on {projection_class}.
    #
    # @param klass [Class<ActiveRecord::Base>] either {destination_class} or {source_class}.
    # @return [String] the ActiveRecord::Reflection::AssociationReflection#foreign_key of the
    #   ActiveRecord::Reflection::AssociationReflection that has klass as its
    #   ActiveRecord::Reflection::AssociationReflection#klass.
    # @raise [ArgumentError] if foreign_key is not found.
    def foreign_key(klass)
      # only belongs_to associations will have foreign key, so pass the macro instead of having to look through
      # has_many and has_one associations too.
      association_reflections = projection_class.reflect_on_all_associations(:belongs_to)
      foreign_key = nil

      association_reflections.each do |association_reflection|
        if association_reflection.klass == klass
          foreign_key = association_reflection.foreign_key

          break
        end
      end

      unless foreign_key
        raise ArgumentError,
              "Cannot determine foreign key: " \
              "#{projection_class} does not have a belongs_to association with klass #{klass}."
      end

      foreign_key
    end
  end

  # Yields a scope for each {projection_class}.
  #
  # @yield (see Metasploit::Pro::Migration::Seed::Base#each_scope)
  # @yieldparam (see Metasploit::Pro::Migration::Seed::Base#each_scope)
  # @yieldreturn (see Metasploit::Pro::Migration::Seed::Base#each_scope)
  # @return (see Metasploit::Pro::Migration::Seed::Base#each_scope)
  def each_scope
    # pull out of loop and into locals
    destination_foreign_key = self.class.destination_foreign_key
    projection_class = self.class.projection_class
    source_foreign_key = self.class.source_foreign_key

    each_pair do |source, destination|
      scope = projection_class.where(
         source_foreign_key => source.id,
         destination_foreign_key => destination.id
      )

      yield scope
    end

  end

  private

  # Turns the attributes hash into a sentence containing the attribute names and values.
  #
  # @param attributes [Hash{Symbol => Object}] Maps attribute names to their values
  # @return [String]
  def attribute_sentence(attributes)
    formatted_attributes = attributes.sort.collect do |name, value|
      "#{name} (#{value.inspect})"
    end

    formatted_attributes.to_sentence
  end

  # Checks that the model class has only one record with the given attributes.
  #
  # @param klass [Class, #where, #to_s] an ActiveRecord::Base suclass.
  # @param attributes [Hash{Symbol => Object}] attributes to query.
  # @return [ActiveRecord::Relation] scope for finding the one record with the given attributes.
  # @raise [ArgumentError] if count != 1
  def check_count!(klass, attributes={})
    scope = klass.where(attributes)
    count = scope.count

    if count != 1
      sentence = attribute_sentence(attributes)

      if count < 1
        raise ArgumentError, "No #{klass} with #{sentence}"
      elsif count > 1
        raise ArgumentError, "Multiple (#{count}) #{klass.to_s.pluralize} with #{sentence}"
      end
    end

    scope
  end

  # Yields {source_class} instance and {destination_class} instance pairs derived from
  # {destination_attributes_by_source_attributes}.
  #
  # @yield [source, destination]
  # @yieldparam source [source_class]
  # @yieldparam owasp [destination_class]
  # @yieldreturn [void]
  # @return [void]
  def each_pair
    self.class.destination_attributes_by_source_attributes.each do |source_attributes, destination_attributes|
      source_scope = check_count!(self.class.source_class, source_attributes)
      source = source_scope.first

      destination_scope = check_count!(self.class.destination_class, destination_attributes)
      destination = destination_scope.first

      yield source, destination
    end
  end
end