module Metasploit::Pro::FinderMethods
  extend ActiveSupport::Concern

  included do
    alias_method_chain :construct_limited_ids_condition, :distinct_columns
    # Ruby 2 cannot call this method if it is protected (the
    # original method is protected) so we make it public.
    # TODO: Better way to handle this?
    send(:public, :construct_limited_ids_condition)
  end

  # @see https://github.com/Empact/rails/blob/dbe40957bceffb04fb5b6e4dc7c8239b0c36eee6/activerecord/lib/active_record/relation/finder_methods.rb#L246
  def construct_limited_ids_condition_with_distinct_columns(relation)
    orders = relation.order_values.map { |val| val.presence }.compact
    values = @klass.connection.distinct_columns("#{quoted_table_name}.#{quoted_primary_key}", orders)

    # @see https://github.com/rails/rails/pull/6523/files#L0R243 for rails 3.2 compatibility
    # distinct! is not defined so use uniq(true)
    relation = relation.dup.select(values).uniq(true)

    id_rows = @klass.connection.select_all(relation.arel, 'SQL', relation.bind_values)
    ids_array = id_rows.map {|row| row[primary_key]}

    ids_array.empty? ? raise(ActiveRecord::ThrowResult) : table[primary_key].in(ids_array)
  end
end