require 'pathname'
formatting_results = false

#
# Monkey patch opening of files to set internal encoding in addition to external encoding to prevent
# Encoding::UndefinedConversionError
#


log_path = File.join('log', 'simplecov.log')
# Use w+ so the last run is all that is in the log so it doesn't get bloated
$log = File.open(log_path, 'w+')

module SimpleCov
  class SourceFile
    def initialize(filename, coverage)
      @filename, @coverage = filename, coverage

      # capture stderr so that "warning: Ignoring internal encoding UTF-8: it is identical to external encoding UTF-8"
      # is not printed again and again since its a known bug that simplecov's author has already submitted to ruby.
      original_stderr = $stderr.clone
      $stderr.reopen($log)

      begin
        File.open(filename, 'r:UTF-8:UTF-8') { |f|
          @src = f.readlines
        }
      ensure
        # restore stderr
        $stderr.reopen(original_stderr)
      end

    end
  end
end

if ENV['COVERAGE']
  root = Pathname.new(__FILE__).parent
  pro = root.parent
  SimpleCov.root(pro.to_path)
    
  formatting_results = true

  require 'simplecov-rcov'

  SimpleCov.start 'rails' do
    formatter(
        SimpleCov::Formatter::MultiFormatter[
            SimpleCov::Formatter::HTMLFormatter,
            SimpleCov::Formatter::RcovFormatter,
        ]
    )
  end
end

# detect we are being loaded from Rubymine.app/rb/report/simplecov_generator.rb
if ARGV[0] =~ /Library\/Caches\/RubyMine.*\/coverage\//
  formatting_results = true

  SimpleCov.load_adapter('rails')
end

if formatting_results
  SimpleCov.configure do
    # ignore this file
    add_filter '.simplecov'
    add_filter 'msf3'
    
    coverage_dir pro.join('test_reports','coverage')
    
    merge_timeout 7200
    
    #
    # Changed Files in Git Group
    # @see http://fredwu.me/post/35625566267/simplecov-test-coverage-for-changed-files-only
    #

    untracked = `git ls-files --exclude-standard --others`
    unstaged = `git diff --name-only`
    staged = `git diff --name-only --cached`
    all = untracked + unstaged + staged
    changed_filenames = all.split("\n")

    add_group 'Changed' do |source_file|
      changed_filenames.detect { |changed_filename|
        source_file.filename.end_with?(changed_filename)
      }
    end

    #
    # Simple Rails convention groups not handled by 'rails' adapter
    #

    add_group 'Presenters', 'app/presenters'
    add_group 'Uploaders', 'app/uploaders'
    add_group 'Validators', 'app/validators'
  end
end
