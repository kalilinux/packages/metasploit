class AddAddressToEmailOpening < ActiveRecord::Migration
  def change
    add_column :se_email_openings, :address, :inet
  end
end
