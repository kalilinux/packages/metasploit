class CreateSocialEngineeringCampaignFiles < ActiveRecord::Migration
  def change
    create_table :se_campaign_files do |t|
      t.references :attachable, :polymorphic => true
      t.string :attachment

      t.timestamps null: false
    end
  end
end
