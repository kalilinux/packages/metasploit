class AddPhishingRedirectOriginToWebPage < ActiveRecord::Migration
  def change
  	add_column :se_web_pages, :phishing_redirect_origin, :string
  end
end
