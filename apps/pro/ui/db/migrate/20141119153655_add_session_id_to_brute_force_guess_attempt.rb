class AddSessionIdToBruteForceGuessAttempt < ActiveRecord::Migration
  def change
    add_column :brute_force_guess_attempts, :session_id, :integer
  end
end
