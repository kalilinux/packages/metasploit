class AddingStatusMessageToSeEmailSend < ActiveRecord::Migration
  def change
    add_column :se_email_sends, :sent, :boolean
    add_column :se_email_sends, :status_message, :string
  end
end