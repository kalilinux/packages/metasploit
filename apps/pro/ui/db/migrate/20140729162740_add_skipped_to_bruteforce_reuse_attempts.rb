class AddSkippedToBruteforceReuseAttempts < ActiveRecord::Migration
  def change
    add_column :brute_force_reuse_attempts, :skipped, :boolean
  end
end
