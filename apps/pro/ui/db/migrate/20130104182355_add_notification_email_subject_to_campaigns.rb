class AddNotificationEmailSubjectToCampaigns < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :notification_email_subject, :string
  end
end
