# Seeds {Web::VulnCategory::OWASP} where {Web::VulnCategory::OWASP#target} is {TARGET 'Application'} and
# {web::VulnCategory::OWASP#version} is {VERSION '2010'}.
class SeedWebVulnCategoryOwasps < Metasploit::Pro::Migration::Seed::WebVulnCategory::OWASP
  #
  # CONSTANTS
  #

  # Attributes of seeds (minus {TARGET} and {VERSION}) in rank order.  {Web::VulnCategory::OWASP#rank} is derived from
  # the index in {ATTRIBUTES_LIST} with index `0` mapping to rank `1`, etc.
  ATTRIBUTES_LIST = [
      {
          :detectability => 'average',
          :exploitability => 'easy',
          :impact => 'severe',
          :prevalence => 'common',
          :summary => 'Injection'
      },
      {
          :detectability => 'easy',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'very widespread',
          :summary => 'Cross-Site Scripting (XSS)'
      },
      {
          :detectability => 'average',
          :exploitability => 'average',
          :impact => 'severe',
          :prevalence => 'common',
          :summary => 'Broken Authentication and Session Management'
      },
      {
          :detectability => 'easy',
          :exploitability => 'easy',
          :impact => 'moderate',
          :prevalence => 'common',
          :summary => 'Insecure Direct Object References'
      },
      {
          :detectability => 'easy',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'widespread',
          :summary => 'Cross-Site Request Forgery (CSRF)'
      },
      {
          :detectability => 'easy',
          :exploitability => 'easy',
          :impact => 'moderate',
          :prevalence => 'common',
          :summary => 'Security Misconfiguration'
      },
      {
          :detectability => 'difficult',
          :exploitability => 'difficult',
          :impact => 'severe',
          :prevalence => 'uncommon',
          :summary => 'Insecure Cryptographic Storage'
      },
      {
          :detectability => 'average',
          :exploitability => 'easy',
          :impact => 'moderate',
          :prevalence => 'uncommon',
          :summary => 'Failure to Restrict URL Access'
      },
      {
          :detectability => 'easy',
          :exploitability => 'difficult',
          :impact => 'moderate',
          :prevalence => 'common',
          :summary => 'Insufficient Transport Layer Protection'
      },
      {
          :detectability => 'easy',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'uncommon',
          :summary => 'Unvalidated Redirect and Forwards'
      }
  ]

  # These seeds are only for Application security and do not cover the newer Mobile targetted Top Ten.
  TARGET = 'Application'
  # The latest version of the OWASP Application Security Top Ten.
  VERSION = '2010'
end
