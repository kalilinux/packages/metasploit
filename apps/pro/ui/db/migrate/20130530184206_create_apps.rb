class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string  :name
      t.text    :description
      t.float   :rating
    end
  end
end
