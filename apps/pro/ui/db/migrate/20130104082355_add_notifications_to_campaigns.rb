class AddNotificationsToCampaigns < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :notification_enabled, :boolean
    add_column :se_campaigns, :notification_email_address, :string
    add_column :se_campaigns, :notification_email_message, :text
  end
end
