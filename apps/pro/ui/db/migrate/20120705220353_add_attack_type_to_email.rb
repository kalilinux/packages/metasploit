class AddAttackTypeToEmail < ActiveRecord::Migration
  def change
    add_column :se_emails, :attack_type, :string
  end
end
