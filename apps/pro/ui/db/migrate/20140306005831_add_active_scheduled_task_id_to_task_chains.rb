class AddActiveScheduledTaskIdToTaskChains < ActiveRecord::Migration
  def change
    add_column :task_chains, :active_scheduled_task_id, :integer
  end
end
