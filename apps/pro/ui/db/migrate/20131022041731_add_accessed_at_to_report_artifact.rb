class AddAccessedAtToReportArtifact < ActiveRecord::Migration
  def change
    add_column :report_artifacts, :accessed_at, :timestamp
  end
end
