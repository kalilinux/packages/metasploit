class AddStartedAtToExport < ActiveRecord::Migration
  def change
    add_column :exports, :started_at, :timestamp
  end
end
