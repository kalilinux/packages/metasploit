class AddKindToNotificationMessages < ActiveRecord::Migration
  def change
    add_column :notification_messages, :kind, :string
  end
end
