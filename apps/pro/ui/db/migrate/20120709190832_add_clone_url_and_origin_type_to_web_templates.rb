class AddCloneUrlAndOriginTypeToWebTemplates < ActiveRecord::Migration
  def change
  	add_column :se_web_templates, :clone_url, :string
  	add_column :se_web_templates, :origin_type, :string
  end
end
