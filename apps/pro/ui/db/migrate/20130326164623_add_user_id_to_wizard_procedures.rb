class AddUserIdToWizardProcedures < ActiveRecord::Migration
  def change
    add_column :wizard_procedures, :user_id, :integer
  end
end
