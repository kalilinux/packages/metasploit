class AddFileSizeToSeCampaignFiles < ActiveRecord::Migration
  def change
    add_column :se_campaign_files, :file_size, :integer
  end
end
