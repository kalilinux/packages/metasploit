class ChangeSinglePasswordMmName < ActiveRecord::Migration
  class Apps::App < ActiveRecord::Base; end

  def up
    single_password = Apps::App.where(name: 'Single Password Testing').first
    if single_password
      single_password.update_attribute(:name, 'Single Credentials Testing')
    end
  end

  def down
  end
end
