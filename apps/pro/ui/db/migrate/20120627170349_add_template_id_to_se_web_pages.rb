class AddTemplateIdToSeWebPages < ActiveRecord::Migration
  def up
    add_column :se_web_pages, :template_id, :integer
  end

  def down
    remove_column :se_web_pages, :template_id
  end
end
