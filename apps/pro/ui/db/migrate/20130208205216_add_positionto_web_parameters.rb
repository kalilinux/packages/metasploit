class AddPositiontoWebParameters < ActiveRecord::Migration
  def down
    change_table :web_parameters do |t|
      t.remove :position
    end
  end

  def up
    change_table :web_parameters do |t|
      t.integer :position, :null => false
    end
  end
end
