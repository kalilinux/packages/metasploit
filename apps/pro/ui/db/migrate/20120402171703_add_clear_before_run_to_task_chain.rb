class AddClearBeforeRunToTaskChain < ActiveRecord::Migration
  def change
    add_column :task_chains, :clear_workspace_before_run, :boolean
  end
end
