# Seeds {Web::VulnCategory::Projection::MetasploitOWASP} between {Web::VulnCategory::Metasploit} and
# {Web::VulnCategory::OWASP}.
class SeedWebVulnCategoryProjectionMetasploitOwaspsMissingSeeds < Metasploit::Pro::Migration::Seed::Projection
  #
  # CONSTANT
  #

  # The {Web::VulnCategory::OWASP#target} is the same for all {Web::VulnCategory::OWASP} seeds in this migration, so
  # make it a constant.
  TARGET = 'Application'
  # The {Web::VulnCategory::OWASP#version} is the same for all {Web::VulnCategory::OWASP} seeds in this migration, so
  # make it a constant.
  VERSION = '2010'

  project Web::VulnCategory::Metasploit,
          :onto => Web::VulnCategory::OWASP,
          :using => Web::VulnCategory::Projection::MetasploitOWASP,
          :where => {
            {
              :summary => 'Cross-Site Request Forgery (CSRF)'
            } => {
              :summary => 'Cross-Site Request Forgery (CSRF)',
              :target => TARGET,
              :version => VERSION
            },
            {
              :summary => 'Direct Object Reference'
            } => {
              :summary => 'Insecure Direct Object References',
              :target => TARGET,
              :version => VERSION
            },
            {
              :summary => 'Session fixation'
            } => {
              :summary => 'Broken Authentication and Session Management',
              :target => TARGET,
              :version => VERSION
            },
            {
              :summary => 'Unvalidated redirect'
            } => {
              :summary => 'Unvalidated Redirect and Forwards',
              :target => TARGET,
              :version => VERSION
            },
            {
              :summary => 'Unauthorized access'
            } => {
              :summary => 'Failure to Restrict URL Access',
              :target => TARGET,
              :version => VERSION
            }
          }
end
