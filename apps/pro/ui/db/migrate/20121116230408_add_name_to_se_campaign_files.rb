class AddNameToSeCampaignFiles < ActiveRecord::Migration
  def change
    add_column :se_campaign_files, :name, :string
  end
end
