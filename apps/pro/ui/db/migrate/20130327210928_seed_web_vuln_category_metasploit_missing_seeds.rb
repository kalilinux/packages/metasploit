# Seeds {Web::VulnCategory::Metasploit Web::VulnCategory::Metasploit's} web_vuln_category_metasploits table.
class SeedWebVulnCategoryMetasploitMissingSeeds < Metasploit::Pro::Migration::Seed::WebVulnCategory::Metasploit
  #
  # CONSTANTS
  #

  # Set of attribute Hashes for {Web::VulnCategory::Metasploit} records.
  ATTRIBUTES_SET = Set.new(
    [
      {
        :name => 'CSRF',
        :summary => 'Cross-Site Request Forgery (CSRF)'
      },
      {
        :name => 'Direct-Object-Reference',
        :summary => 'Direct Object Reference'
      },
      {
        :name => 'Session-Fixation',
        :summary => 'Session fixation'
      },
      {
        :name => 'Unauthorized-Access',
        :summary => 'Unauthorized access'
      },
      {
        :name => 'Unvalidated-Redirect',
        :summary => 'Unvalidated redirect'
      }
    ]
  )
end
