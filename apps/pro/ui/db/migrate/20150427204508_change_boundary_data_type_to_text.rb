class ChangeBoundaryDataTypeToText < ActiveRecord::Migration
  def up
    change_column :workspaces, :boundary, :text
  end

  def down
    change_column :workspaces, :boundary, :string, :limit => 4096
  end
end
