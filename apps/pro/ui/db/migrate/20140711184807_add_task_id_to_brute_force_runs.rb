class AddTaskIdToBruteForceRuns < ActiveRecord::Migration
  def change
    add_column :brute_force_runs, :task_id, :integer
  end
end
