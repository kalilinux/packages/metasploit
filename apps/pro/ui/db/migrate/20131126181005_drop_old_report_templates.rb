class DropOldReportTemplates < ActiveRecord::Migration
  def up
    drop_table :report_templates
  end
end
