class AddContentDispositionToCampaignFile < ActiveRecord::Migration
  def change
    add_column :se_campaign_files, :content_disposition, :string
  end
end
