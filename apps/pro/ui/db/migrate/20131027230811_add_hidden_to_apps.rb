class AddHiddenToApps < ActiveRecord::Migration
  def change
    add_column :apps, :hidden, :boolean, default: false
  end
end
