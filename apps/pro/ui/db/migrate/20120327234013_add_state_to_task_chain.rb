class AddStateToTaskChain < ActiveRecord::Migration
  def change
    add_column :task_chains, :state, :string, :default => 'ready'
  end
end
