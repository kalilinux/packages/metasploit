class AddStateToRun < ActiveRecord::Migration
  def change
    add_column :automatic_exploitation_runs, :state, :string
  end
end
