class AddStateMachineToAppRun < ActiveRecord::Migration
  def change
    add_column :app_runs, :state, :string
  end
end
