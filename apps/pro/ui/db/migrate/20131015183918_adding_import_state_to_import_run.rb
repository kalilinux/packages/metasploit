class AddingImportStateToImportRun < ActiveRecord::Migration
  def change 
    add_column :nexpose_data_import_runs, :import_state, :string
  end

end