class AddActiveReportIdToTaskChain < ActiveRecord::Migration
  def change
    add_column :task_chains, :active_report_id, :integer
  end
end
