class AddErrorAndClassToGeneratedPayload < ActiveRecord::Migration
  def change
    add_column :generated_payloads, :generator_error, :string
    add_column :generated_payloads, :payload_class, :string
  end
end
