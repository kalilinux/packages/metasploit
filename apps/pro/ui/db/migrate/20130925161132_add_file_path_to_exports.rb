class AddFilePathToExports < ActiveRecord::Migration
  def change
    add_column :exports, :file_path, :string, :limit => 1024
  end
end
