class AddLogoPathToReports < ActiveRecord::Migration
  def change
    add_column :reports, :logo_path, :text
  end
end
