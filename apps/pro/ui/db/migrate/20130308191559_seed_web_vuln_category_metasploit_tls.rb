# Seeds {Web::VulnCategory::Metasploit Web::VulnCategory::Metasploit's} web_vuln_category_metasploits table.
class SeedWebVulnCategoryMetasploitTls < Metasploit::Pro::Migration::Seed::WebVulnCategory::Metasploit
  #
  # CONSTANTS
  #

  # Set of attribute Hashes for {Web::VulnCategory::Metasploit} records.
  ATTRIBUTES_SET = Set.new(
      [
          {
              :name => 'Transport-Layer-Encryption',
              :summary => 'Insufficient Transport Layer Security'
          },

      ]
  )
end