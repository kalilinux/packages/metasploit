class AddSerializedScheduleToTaskChain < ActiveRecord::Migration
  def change
    add_column :task_chains, :schedule_hash, :text
  end
end
