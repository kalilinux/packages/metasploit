class AddFileUploadToScheduledTask < ActiveRecord::Migration
  def change
    add_column :scheduled_tasks, :file_upload, :string
  end
end
