class AddCompletedAtToExport < ActiveRecord::Migration
  def change
    add_column :exports, :completed_at, :timestamp
  end
end
