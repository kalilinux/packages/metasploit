class AddReportHashtoSchedule < ActiveRecord::Migration
  def change
    add_column :scheduled_tasks, :report_hash, :text
  end
end
