class AddEditorTypeToEmail < ActiveRecord::Migration
  def change
  	add_column :se_emails, :editor_type, :string
  end
end
