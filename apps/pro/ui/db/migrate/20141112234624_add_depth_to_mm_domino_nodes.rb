class AddDepthToMmDominoNodes < ActiveRecord::Migration
  def change
    add_column :mm_domino_nodes, :depth, :integer, default: 0
  end
end
