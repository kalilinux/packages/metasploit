class AddCampaignIdToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :campaign_id, :integer
  end
end
