class RemoveHiddenFromTaskChains < ActiveRecord::Migration
  def change
    remove_column :task_chains, :hidden
  end
end
