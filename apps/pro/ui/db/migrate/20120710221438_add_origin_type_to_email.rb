class AddOriginTypeToEmail < ActiveRecord::Migration
  def change
    add_column :se_emails, :origin_type, :string
  end
end
