class AddNxAssetIdToHost < ActiveRecord::Migration
  def change
    add_column :hosts, :nexpose_data_asset_id, :integer
  end
end
