# Seeds {Web::VulnCategory::Projection::MetasploitOWASP} between {Web::VulnCategory::Metasploit} and
# {Web::VulnCategory::OWASP}.
class SeedWebVulnCategoryProjectionMetasploitOwasps < Metasploit::Pro::Migration::Seed::Projection
  #
  # CONSTANT
  #

  # The {Web::VulnCategory::OWASP#target} is the same for all {Web::VulnCategory::OWASP} seeds in this migration, so
  # make it a constant.
  TARGET = 'Application'
  # The {Web::VulnCategory::OWASP#version} is the same for all {Web::VulnCategory::OWASP} seeds in this migration, so
  # make it a constant.
  VERSION = '2010'

  project Web::VulnCategory::Metasploit,
          :onto => Web::VulnCategory::OWASP,
          :using => Web::VulnCategory::Projection::MetasploitOWASP,
          :where => {
              {
                  :summary =>  'Command Injection'
              } => {
                  :summary => 'Injection',
                  :target => TARGET,
                  :version => VERSION
              },
              {
                  :summary => 'SQL Injection'
              } => {
                  :summary => 'Injection',
                  :target => TARGET,
                  :version => VERSION
              },
              {
                  :summary => 'Publicly Writable Directory'
              } => {
                  :summary => 'Security Misconfiguration',
                  :target => TARGET,
                  :version => VERSION
              },
              {
                  :summary => 'Vulnerable Version'
              } => {
                  :summary => 'Security Misconfiguration',
                  :target => TARGET,
                  :version => VERSION
              },
              {
                  :summary => 'Cross-site scripting'
              } => {
                  :summary => 'Cross-Site Scripting (XSS)',
                  :target => TARGET,
                  :version => VERSION
              }
          }
end
