class AddStateToSeCampaigns < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :state, :string, :default => 'unconfigured'
  end
end
