class AddMaskCredentialsToExports < ActiveRecord::Migration
  def change
    add_column :exports, :mask_credentials, :boolean, :default => false
  end
end
