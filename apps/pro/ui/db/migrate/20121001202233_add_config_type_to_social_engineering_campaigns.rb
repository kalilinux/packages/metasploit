class AddConfigTypeToSocialEngineeringCampaigns < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :config_type, :string
  end
end
