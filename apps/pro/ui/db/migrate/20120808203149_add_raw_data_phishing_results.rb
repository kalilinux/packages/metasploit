class AddRawDataPhishingResults < ActiveRecord::Migration
  def change
    add_column :se_phishing_results, :raw_data, :text
  end
end
