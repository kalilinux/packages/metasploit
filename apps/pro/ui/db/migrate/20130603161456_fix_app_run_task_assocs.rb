class FixAppRunTaskAssocs < ActiveRecord::Migration
  def change
    add_column :tasks, :app_run_id, :integer
    remove_column :app_runs, :task_id
  end
end
