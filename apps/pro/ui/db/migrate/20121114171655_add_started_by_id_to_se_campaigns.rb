class AddStartedByIdToSeCampaigns < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :started_by_user_id, :integer
  end
end
