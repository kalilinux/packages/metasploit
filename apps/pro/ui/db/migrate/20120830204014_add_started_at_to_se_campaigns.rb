class AddStartedAtToSeCampaigns < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :started_at, :datetime
  end
end
