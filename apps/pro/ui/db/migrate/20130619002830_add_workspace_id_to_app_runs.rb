class AddWorkspaceIdToAppRuns < ActiveRecord::Migration
  def change
    add_column :app_runs, :workspace_id, :integer
    add_index :app_runs, :workspace_id
  end
end
