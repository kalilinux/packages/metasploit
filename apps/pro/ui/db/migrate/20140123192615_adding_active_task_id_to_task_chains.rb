class AddingActiveTaskIdToTaskChains < ActiveRecord::Migration
  def change
    add_column :task_chains, :active_task_id, :integer
  end
end