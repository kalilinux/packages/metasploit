class ChangeCloneUrlToString < ActiveRecord::Migration
  def up
    change_column :se_web_pages, :clone_url, :string
  end

  def down
    change_column :se_web_pages, :clone_url, :text
  end
end
