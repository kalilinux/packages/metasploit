class CreateRunStats < ActiveRecord::Migration
  def change
    create_table :run_stats do |t|
      t.string :name
      t.float :data
      t.references :measurable, :polymorphic => true
      t.timestamps null: false
    end
  end
end
