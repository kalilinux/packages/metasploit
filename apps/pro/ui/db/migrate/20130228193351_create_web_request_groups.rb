class CreateWebRequestGroups < ActiveRecord::Migration
  def change
    create_table :web_request_groups do |t|
      t.timestamps null: false
    end
  end
end
