class AddSslCertIdtoSeCampaign < ActiveRecord::Migration
  def change
    add_column :se_campaigns, :ssl_cert_id, :integer
  end
end
