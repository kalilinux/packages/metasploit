class AddingAttackToWebRequest < ActiveRecord::Migration
  def change
    add_column :web_requests, :attack, :boolean, :default => :true
  end
end