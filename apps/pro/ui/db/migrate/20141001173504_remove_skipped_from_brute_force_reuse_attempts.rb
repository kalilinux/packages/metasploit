class RemoveSkippedFromBruteForceReuseAttempts < ActiveRecord::Migration
  def change
    remove_column :brute_force_reuse_attempts, :skipped
  end
end
