class AddHiddenToAppRuns < ActiveRecord::Migration
  def change
    add_column :app_runs, :hidden, :boolean, default: false
  end
end
