class AddHighValueToDominoNodes < ActiveRecord::Migration

  def change
    add_column :mm_domino_nodes, :high_value, :boolean, default: false
  end

end
