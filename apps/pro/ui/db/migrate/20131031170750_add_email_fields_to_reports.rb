class AddEmailFieldsToReports < ActiveRecord::Migration
  def change
    add_column :reports, :email_recipients, :text
  end
end
