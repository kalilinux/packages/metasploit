class AddDurationToReport < ActiveRecord::Migration
  def change
    add_column :reports, :started_at, :timestamp
    add_column :reports, :completed_at, :timestamp
  end
end
