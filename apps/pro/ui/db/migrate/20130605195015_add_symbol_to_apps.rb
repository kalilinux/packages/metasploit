class AddSymbolToApps < ActiveRecord::Migration
  def change
    add_column :apps, :symbol, :string
  end
end
