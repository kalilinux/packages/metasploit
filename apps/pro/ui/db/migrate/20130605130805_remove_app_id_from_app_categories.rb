class RemoveAppIdFromAppCategories < ActiveRecord::Migration
  def change
    remove_column :app_categories, :app_id
  end
end
