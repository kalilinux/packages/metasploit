class AddCapturedCredsCountToMmDominoNodes < ActiveRecord::Migration

  def change
    add_column :mm_domino_nodes, :captured_creds_count, :integer, default: 0
  end

end
