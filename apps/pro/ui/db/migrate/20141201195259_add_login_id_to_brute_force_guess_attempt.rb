class AddLoginIdToBruteForceGuessAttempt < ActiveRecord::Migration
  def change
    add_column :brute_force_guess_attempts, :login_id, :integer
  end
end
