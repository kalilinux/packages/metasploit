# Seeds {Web::VulnCategory::OWASP} where {Web::VulnCategory::OWASP#target} is {TARGET 'Application'} and
# {web::VulnCategory::OWASP#version} is {VERSION '2013rc1'}.
class SeedWebVulnCategoryOwasps2013 < Metasploit::Pro::Migration::Seed::WebVulnCategory::OWASP
  #
  # CONSTANTS
  #

  # Attributes of seeds (minus {TARGET} and {VERSION}) in rank order.  {Web::VulnCategory::OWASP#rank} is derived from
  # the index in {ATTRIBUTES_LIST} with index `0` mapping to rank `1`, etc.
  ATTRIBUTES_LIST = [
      {
          :detectability => 'average',
          :exploitability => 'easy',
          :impact => 'severe',
          :prevalence => 'common',
          :summary => 'Injection'
      },
      {
          :detectability => 'average',
          :exploitability => 'average',
          :impact => 'severe',
          :prevalence => 'common',
          :summary => 'Broken Authentication and Session Management'
      },
      {
          :detectability => 'easy',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'very widespread',
          :summary => 'Cross-Site Scripting (XSS)'
      },
      {
          :detectability => 'easy',
          :exploitability => 'easy',
          :impact => 'moderate',
          :prevalence => 'common',
          :summary => 'Insecure Direct Object References'
      },
      {
          :detectability => 'easy',
          :exploitability => 'easy',
          :impact => 'moderate',
          :prevalence => 'common',
          :summary => 'Security Misconfiguration'
      },
      {
          :detectability => 'average',
          :exploitability => 'difficult',
          :impact => 'severe',
          :prevalence => 'uncommon',
          :summary => 'Sensitive Data Exposure'
      },
      {
          :detectability => 'average',
          :exploitability => 'easy',
          :impact => 'moderate',
          :prevalence => 'common',
          :summary => 'Missing Function Level Access Control'
      },
      {
          :detectability => 'easy',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'widespread',
          :summary => 'Cross-Site Request Forgery (CSRF)'
      },
      {
          :detectability => 'difficult',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'widespread',
          :summary => 'Using Components with Known Vulnerabilities'
      },
      {
          :detectability => 'easy',
          :exploitability => 'average',
          :impact => 'moderate',
          :prevalence => 'uncommon',
          :summary => 'Unvalidated Redirect and Forwards'
      }
  ]

  # These seeds are only for Application security and do not cover the newer Mobile targetted Top Ten.
  TARGET = 'Application'
  # The latest version of the OWASP Application Security Top Ten.
  VERSION = '2013rc1'
end