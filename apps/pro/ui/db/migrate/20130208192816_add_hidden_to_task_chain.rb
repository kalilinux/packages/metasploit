class AddHiddenToTaskChain < ActiveRecord::Migration
  def change
    add_column :task_chains, :hidden, :boolean, :default => false
  end
end
