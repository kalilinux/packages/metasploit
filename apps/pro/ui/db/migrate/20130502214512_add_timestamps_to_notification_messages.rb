class AddTimestampsToNotificationMessages < ActiveRecord::Migration
  def change
    add_column(:notification_messages, :created_at, :datetime)
  end
end
