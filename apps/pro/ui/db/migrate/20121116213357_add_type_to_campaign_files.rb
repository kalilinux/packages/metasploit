class AddTypeToCampaignFiles < ActiveRecord::Migration
  def change
    add_column :se_campaign_files, :type, :string
  end
end
