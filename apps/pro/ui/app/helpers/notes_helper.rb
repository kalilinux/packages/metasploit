module NotesHelper
	# Generate the markup for the Mdm::Note's row checkbox.
	#
	# note - The Mdm::Note to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_host_checkbox_tag(note)
		markup = ""
		markup += check_box_tag("host_ids[]", note.host.id, false, :id => nil, :class => 'hosts')
		markup += check_box_tag("note_ids[]", note.id, false, :id => nil, :class => 'invisible notes')
		markup += "<div class='invisible'>"
		note.host.tags.each do |tag|
			markup += tag_html(tag)
		end
		markup += "</div>"
		markup.to_json.html_safe
	end

	# Generate the markup for the Mdm::Note's Mdm::Host's name.
	#
	# note - The Mdm::Note to fetch the Mdm::Host's attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_host_name_html(note)
		name = h(note.host.name ? (json_data_scrub(note.host.name)) : note.host.address)
		link_to(name, note.host).to_json.html_safe
	end

	# Generate the markup for the Mdm::Note type.
	#
	# note - The Mdm::Note to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_type_html(note)
		if action_name == 'index'
			link_to(json_data_scrub(note.ntype), workspace_notes_path(@workspace, :search => note.ntype)).to_json.html_safe
		else
			json_data_scrub(h(note.ntype)).to_json.html_safe
		end
	end

	# Generate the markup for the Mdm::Note data.
	#
	# note - The Mdm::Note to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_data_html(note)
		if note.data.kind_of?(Hash)
			"<div class='note-data'>#{render('hash', hash: note.data)}</div><a href='' class='note-data-view'>View</a> #{json_data_scrub(truncate(note.data.inspect, :length => 60))}".to_json.html_safe
		else
			json_data_scrub(truncate(h(note.data), :length => 60)).to_json.html_safe
		end
	end

	# Generate the markup for the Mdm::Note's +created_at+ date.
	#
	# note - The Mdm::Note to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_created_at_html(note)
		note.created_at.to_s(:long).to_json.html_safe
	end

	# Generate the markup for the Mdm::Note's status.
	#
	# note - The Mdm::Note to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_status_html(note)
		(note.flagged? ? icon('flag_red') : "").to_json.html_safe
	end

	# Generate the markup for the Mdm::Note's host_count.
	#
	# note - The Mdm::Note to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def note_host_count_html(note)
		link_to(note.host_count, workspace_notes_path(@workspace, :search => note.ntype), :class => 'datatables-search').to_json.html_safe
	end
end
