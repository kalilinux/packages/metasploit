module RefHelper

	def link_for_ref(type,ref)
		vulnref = lookup_url(type,ref)
		url = vulnref[0]
		desc = vulnref[1]
		short_desc = vulnref[2]
		img = vulnref[3]

		return h(desc) unless url
		if img
			link_to(
				(image_tag(img, :height => "18px", :class => "left-badge")  + h(short_desc)),
				h(url),
				:title => h(desc),
				:target => "_new",
        :class => "image-left"
			)
		else
			link_to(
				h(desc),
				h(url),
				:title  => h(desc),
				:target => "_new"
			)
		end
	end

	def lookup_url(type,ref)
		url = case type
			when 'OSVDB'      then 'http://www.osvdb.org/' + ref
			when 'EDB'        then 'http://www.exploit-db.com/exploits/' + ref
			when 'CVE'        then 'http://cvedetails.com/cve/' + ref + '/'
			when 'BID'        then 'http://www.securityfocus.com/bid/' + ref
			when 'MSB'        then 'http://www.microsoft.com/technet/security/bulletin/' + ref + '.mspx'
			when 'MIL'        then 'http://milw0rm.com/metasploit/' + ref
			when 'WVE'        then 'http://www.wirelessve.org/entries/show/WVE-' + ref
			when 'US-CERT-VU' then 'http://www.kb.cert.org/vuls/id/' + ref
			when 'BPS'        then 'https://strikecenter.bpointsys.com/bps/advisory/BPS-' + ref
			when 'NEXPOSE'    then 'http://www.rapid7.com/vulndb/lookup/' + ref
			when 'SECUNIA'    then 'http://secunia.com/advisories/' + ref
			when 'NSS'        then 'http://www.nessus.org/plugins/index.php?view=single&id=' + ref
			when 'XF'
				xf_url = 'http://xforce.iss.net/xforce/xfdb/'
				# Deal with Nexpose's weird name-and(parens) as well as plain numbers
				xf_id = ref.scan(/[^-][0-9]+[^-]/).last
				xf_id ? (xf_url << xf_id) : nil
			when 'URL'        then ref
			when 'GENERIC'    then '#'
			else
				''
			end

		desc = case type
			when 'NEXPOSE'
				'Rapid7 VulnDB'
			when 'MSB'
				if ref =~ /(MS[0-9]{2}-[0-9]{3})/
					$1
				else
					"#{type}-#{ref}"
				end
			when 'URL'
				begin
					URI.parse(ref).host
				rescue ::Exception
					ref.to_s
				end
			when nil
				ref.to_s
			when 'GENERIC'
				"#{ref}"
			else
				"#{type}-#{ref}"
			end

		short_desc = desc[/^(.*)\.(.*)\.(com|net|org|gov|fr)/] ? $2 : desc

		if type == "URL"
			img = case desc.to_s.downcase
				when /blogs\.(msdn|technet)\.com/
					ref_to_icon('msb')
				when /oracle\.com/
					ref_to_icon('oracle')
				when /securityfocus\.com/
					ref_to_icon('bid')
				when /vupen\.com/, /frsirt\.com/
					ref_to_icon('vupen')
				when /lists\.grok\./, /marc\.info/
					ref_to_icon('email')
				when /red-database-security\.com/
					ref_to_icon('red')
				when /mysql\.com/
					ref_to_icon('mysql')
				when /apache\.org/
					ref_to_icon('apache')
				when /php\.net/
					ref_to_icon('php')
				when /debian\./
					ref_to_icon('debian')
				when /ubuntu/
					ref_to_icon('ubuntu')
				else
					ref_to_icon(type)
			end
		else
			img = ref_to_icon(type)
		end

		[url,desc,short_desc,img]
	end

	def ref_to_icon(str)
		ico = case str.to_s.downcase
					when /^cve/
						'cve_logo.png'
					when /^osvdb/
						'osvdb_logo.png'
					when /^edb/
						'exploit-db_logo.png'
					when /^bid/
						'securityfocus_logo.png'
					when /^msb/
						'microsoft_logo.png'
					when /^nexpose/
						'rapid7_logo.png'
					when /^secunia/
						'secunia_logo.png'
					when "oracle"
						'oracle_logo.png'
					when "vupen"
						'vupen_logo.png'
					when "email"
						'email.png'
					when "red"
						'red_logo.png'
					when "apache"
						'apache_logo.png'
					when "php"
						'php_logo.png'
					when "mysql"
						'mysql_logo.png'
					when "debian"
						'debian_logo.png'
					when "ubuntu"
						'ubuntu_logo.png'
					when "nss"
						'tenable.png'
					else
						'w3_logo.png'
					end
		'icons/ref/' + ico
	end


end

