module ServicesHelper
	# Generate the markup for the Mdm::Service's row checkbox.
	#
	# service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_host_checkbox_tag(service)
		markup = ""
		markup += check_box_tag("host_ids[]", service.host.id, false, :id => nil, :class => 'hosts')
		markup += check_box_tag("service_ids[]", service.id, false, :id => nil, :class => 'invisible services')
		markup += "<div class='invisible'>"
		service.host.tags.each do |tag|
			markup += tag_html(tag)
		end
		markup += "</div>"
		markup.to_json.html_safe
	end

	# Generate the markup for the Mdm::Service's Mdm::Host's name.
	#
	# service - The Mdm::Service to fetch the Mdm::Host's attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_host_name_html(service)
		raw_name = service.host.name
		# if the hostname was populated from a wildcard cert, we don't want
		#   to see a huge list of e.g. *.metasploit.com
		name_is_safe = raw_name.present? && !raw_name.starts_with?("*")
		name = name_is_safe ? (h(json_data_scrub(raw_name))) : service.host.address
		link_to(name, service.host).to_json.html_safe
	end

	# Generate the markup for the Mdm::Service name.
	#
	# service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_name_html(service)
		h(json_data_scrub(service.name)).to_json.html_safe
	end

	# Generate the markup for the Mdm::Service protocol.
	#
	# service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_protocol_html(service)
		h(json_data_scrub(service.proto)).to_json.html_safe
	end

	# Generate the markup for the Mdm::Service protocol.
	#
	# Mdm::Service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_port_html(service)
		service.port.to_json.html_safe
	end

	# Generate the markup for the Mdm::Service info.
	#
	# note - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_info_html(service)
		h(json_data_scrub(truncate(service.info, :length => 70))).to_json.html_safe
	end

	# Generate the markup for the Mdm::Service state.
	#
	# service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_state_html(service)
		state = service.state
		"<div class='service-state #{h state}'>#{h state.try(:capitalize)}</div>".to_json.html_safe
	end

	# Generate the markup for the Mdm::Service's +updated_at+ date.
	#
	# service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_updated_at_html(service)
		service.updated_at.to_s(:long).to_json.html_safe
	end

    # Generate the markup for the Mdm::Service's host_count.
	#
	# service - The Mdm::Service to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def service_host_count_html(service)
		search = "#{service.info} proto:#{h service.proto} name:#{h service.name} port:#{h service.port}"
		link_to(service.host_count, workspace_services_path(@workspace, :search => search), :class => 'datatables-search').to_json.html_safe
	end
end
