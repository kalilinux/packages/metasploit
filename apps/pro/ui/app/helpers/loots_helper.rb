module LootsHelper
	# Generate the markup for the Mdm::Loot's row checkbox.
	#
	# loot - The Mdm::Loot to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_host_checkbox_tag(loot)
		markup = ""
		if loot.host_id
			markup += check_box_tag("host_ids[]", loot.host.id, false, :id => nil, :class => 'hosts')
			markup += check_box_tag("loot_ids[]", loot.id, false, :id => nil, :class => 'invisible loots')
			markup += "<div class='invisible'>"
			loot.host.tags.each do |tag|
				markup += tag_html(tag)
			end
		end
		markup += "</div>"
		markup.to_json.html_safe
	end

	# Generate the markup for the Mdm::Loot's Mdm::Host's name.
	#
	# loot - The Mdm::Loot to fetch the Mdm::Host's attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_host_name_html(loot)
		if loot.host_id
		# TODO: Should probably be using the host.title method for these.
		name = loot.host.name ? (h(json_data_scrub(loot.host.name))) : loot.host.address
			link_to(name, loot.host).to_json.html_safe
		else
			h("<none>").to_json.html_safe
		end
	end

	# Generate the markup for the Mdm::Loot type.
	#
	# loot - The Mdm::Loot to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_type_html(loot)
		h(json_data_scrub(loot.ltype)).to_json.html_safe
	end

	# Generate the markup for the Mdm::Loot name.
	#
	# loot - The Mdm::Loot to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_name_html(loot)
		h(json_data_scrub(loot.name)).to_json.html_safe
	end

	# Generate the markup for the Mdm::Loot size.
	#
	# loot - The Mdm::Loot to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_size_html(loot)
		"#{loot.size} bytes".to_json.html_safe
	end

	# Generate the markup for the Mdm::Loot info.
	#
	# loot - The Mdm::Loot to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_info_html(loot)
		h(json_data_scrub(loot.info)).to_json.html_safe
	end


	# Generate the markup for the Mdm::Loot data.
	#
	# loot - The Mdm::Loot to fetch the attribute from.
	#
	# Returns the String markup html, escaped for json.
	def loot_data_html(loot)
		data = if loot.image?
			"<a href='#{h loot_path(loot)}'><img src='#{h loot_path(loot)}'' width='640' height='480' border='0'></a>"
		elsif loot.binary?
			h loot.sniff.to_s[0,32].unpack("C*").map{|x| "%.2x" % x}.join(" ")
		else
			h loot.sniff
		end
		data_html = "<div class='loot-data invisible'>#{data}</div>"
		data_html += "<div style='float:left;'>"
		data_html += link_to("View", '#', :class => "loot-data-view report_view")
		data_html += "&nbsp;|&nbsp;"
    	data_html += link_to("Download", loot_path(loot, :disposition => 'attachment'), :class => "report_download")
    	data_html.to_json.html_safe
	end
end
