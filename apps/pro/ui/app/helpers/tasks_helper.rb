module TasksHelper

	def autotag_checkbox(host_tag)
		if host_tag.kind_of? Mdm::Tag
			check_box_tag controller.action_name.split("_",2).last + "_task[autotag_tags][]", host_tag.id, false, :id => "tag_#{host_tag.id}"
		else
			check_box_tag controller.action_name.split("_",2).last + "_task[autotag_tags][]", -1, false, :id => nil
		end
	end

	def next_task
		if Mdm::Task.count > 0
			next_id = Mdm::Task.last.id + 1
		else
			next_id = 1
		end
		this_action = case controller.action_name
			when "new_scan"
				"discover"
			when "new_collect_evidence"
				"collect"
			when /^new_(.*)/
				$1.gsub(/[^A-Za-z0-9]/,"_")
			when /^(new|edit)$/
				unless params[:kind].empty?
          case params[:kind]
            when 'scan'
              "discover"
            else
              params[:kind]
          end
        else
          if controller.class.to_s =~ /^Campaign/
            "campaign"
          else
            controller.class.to_s
          end
        end
			else
				controller.action_name
			end
		[this_action,next_id].join("_")
	end

	# add an invisible element that responds to the ".end" selector at the
	# end of a tasklog to mark the task as completed on the client (js) side
	def tasklog_cap(task)
		unless task.running? || task.paused?
			'<span class="end" style="display:none;"></span>'.html_safe
		else
			''
		end
	end

	def tasklog_prettify(line)
		ltype = case line[0,3]
		when '[-]'
			'error'
		when '[+]'
			'good'
		else
			'normal'
		end

		case line
		when /Workspace:.*Progress:/
			ltype = 'status'
		end

		content_tag :div, h(line), :class => "logline_#{ltype}"
	end

	# Generate the options hash for the Nexpose console +input+ call.
	#
	# consoles - The Array of available consoles for selection.
	#
	# Returns the Hash options.
	def nexpose_console_select_options(consoles)
		options = { collection: options_for_select(consoles) }
		if consoles.size > 1
			options.merge! prompt: "Select a Nexpose console below"
		else
			options.merge! include_blank: false
		end
	end

end
