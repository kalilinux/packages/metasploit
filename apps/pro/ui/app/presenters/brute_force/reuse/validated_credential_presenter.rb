module BruteForce
  module Reuse
    class ValidatedCredentialPresenter < DelegateClass(Attempt)

      def as_json(opts={})
        super.merge!(
          'public' => self['public'],
          'private' => self['private'],
          'realm' => self['realm']
        )
      end

    end
  end
end
