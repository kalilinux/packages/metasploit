module DataResponder
  def load_filtered_records(klass, params)
    relation = apply_search_scopes(
        klass,
        params.merge(klass: klass)
    )
    relation = apply_scopes(apply_sort(relation, params))
    relation
  end

  def apply_search_scopes(relation, opts)
    if params[:search] && !params[:search][:custom_query].blank?
      query = params[:search][:custom_query]

      relation = MetasploitDataModels::Search::Visitor::Relation.new(
          query: Metasploit::Model::Search::Query.new(
              formatted: query,
              klass: class_for_relation(relation)
          )
      ).visit

      relation = apply_base_scopes(relation, opts)
    else
      relation = apply_base_scopes(relation, opts)
    end

    relation = selected_records(relation,opts[:selections]) unless opts[:selections].nil?

    return relation

    # TODO
    # * be sure to add the includes, scope also when searching (put into helper method)
    # * support on-model attributes
    # * support associations
    # * support boolean searches
    # * support checkbox/textfield search (username/password)
    # * support select menu search
  end

  # TODO: the below helpers originally in table_responder.rb
  # and needed to get logins_controller_specs to pass
  # Probably don't need after integrating visual search on other pages

  # Apply the scopes common to every search.
  #
  # @param relation [ActiveRecord::Relation] the relation against which searches
  #   will be appended
  #
  # @return [ActiveRecord::Relation] the scoped relation
  def apply_base_scopes(relation, opts)
    # Ensure all query conditions of the original relation are applied to the response relation.
    if @original_relation.present? && @original_relation.is_a?(ActiveRecord::Relation) && search_params_present?
      # TODO: This should all live in a nice helper in the Mdm::...::Relation class

      # joins
      if @original_relation.join_sources.present?
        relation = relation.joins(@original_relation.join_sources)
      end

      # select
      if @original_relation.ast.present? && @original_relation.ast.cores.present?
        projections = @original_relation.ast.cores.map(&:projections).flatten
        relation = relation.select(projections)
      end

      # group
      if @original_relation.group_values.present?
        relation = relation.group(@original_relation.group_values)
      end

      # where
      if @original_relation.where_values.present?
        # Have to use the raw sql here, as I can't find a better way to grab this.
        @original_relation.where_values.collect(&:to_sql).each do |where_clause|
          relation = relation.where(where_clause)
        end
      end

      # eager_load
      if @original_relation.eager_load_values.present?
        eager_load_tables = @original_relation.eager_load_values
        relation = relation.eager_load(*@original_relation.eager_load_values)
      end

      # from
      if @original_relation.from_value.present?
        relation.from_value = @original_relation.from_value
      end

      # offset
      if @original_relation.offset_value.present?
        relation.offset_value = @original_relation.offset_value
      end

      # limit
      if @original_relation.limit_value.present?
        relation.limit_value = @original_relation.limit_value
      end

      # uniq
      if @original_relation.uniq_value.present?
        relation.limit_value = @original_relation.uniq_value
      end

      # having
      if @original_relation.having_values.present?
        # Have to use the raw sql here, as I can't find a better way to grab this.
        @original_relation.having_values.collect(&:to_sql).each do |having_clause|
          relation = relation.having(having_clause)
        end
      end

      # order
      if @original_relation.order_values.present?
        @original_relation.order_values.each do |order_clause|
          relation = relation.order(order_clause)
        end
      end
    end

    relation = relation.includes(opts.fetch(:includes, []))
    relation = opts[:scope].call(relation) if opts[:scope]

    relation
  end

  # @return [Boolean] true if there are any search parameters present, false otherwise
  def search_params_present?
    return false unless params[:search]

    return true if custom_search_params_present?

    return false
  end

  # @return [Boolean] true if there are any custom search params present, false otherwise
  def custom_search_params_present?
    params[:search].is_a?(Hash) && params[:search][:custom_query] &&
        !params[:search][:custom_query].blank?
  end

  # @param relation [ActiveRecord::Relation] the relation to sort
  # @param opts [Hash] @see #as_table opts
  # @return [ActiveRecord::Relation] a sorted relation
  def apply_sort(relation, opts={})
    sort = params[:sort_by]
    if sort.blank?
      sort = 'id DESC'
    end

    fqdn, dir = sort.split

    klass = class_for_relation(relation)

    domains = fqdn.split('.')
    domains[0...-1].each do |col|
      # re-map the reflections to use string keys
      reflections = Hash[klass.reflections.map {|k, v| [k.to_s, v] }]
      # that way we don't have to convert input to a symbol, which is a possible OOM DOS
      klass = reflections[col.to_s].try(:klass)
    end

    if ['asc', 'desc'].include?(dir.downcase)
      if klass.present? and klass.columns_hash.has_key?(domains.last)
        relation = relation.order("#{klass.table_name}.#{domains.last} #{dir}")
      elsif domains.length == 1 and domains.last =~ /\A[a-z0-9\._\-,]+\Z/i
        relation = relation.order(domains.last.split(',').map { |d| "#{d} #{dir}" })
      end
    end

    relation
  end
end