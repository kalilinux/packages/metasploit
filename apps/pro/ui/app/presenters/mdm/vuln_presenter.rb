module Mdm
  class VulnPresenter < DelegateClass(Vuln)
    include Mdm::VulnAttempt::Status

    def as_json(opts={})
      super.merge!(
       'action' => attempt_action,
       'status' => vuln_attempt_status,
       'status_title' => status_title
      )
    end

    def attempt_action
      #TODO: When we add the required data model to track Imports with a Vuln we determine the action type
      'Exploit'
    end

    def status_title
      fail_reason if vuln_attempt_status == Mdm::VulnAttempt::Status::FAILED
    end

  end
end