host = @vuln.service.try(:host) || @vuln.host

json.id @vuln.id
json.workspace_id host.workspace_id
json.name @vuln.name
json.pushable @vuln.pushable
json.not_exploitable @vuln.not_exploitable
json.markable @vuln.markable
json.not_pushable_reason @vuln.not_pushable_reason
json.match_result_state MetasploitDataModels::AutomaticExploitation::MatchResult.for_vuln_id(@vuln.id).first.try(:state)
json.pushed @vuln.pushed

if @vuln.matches.present?
  json.match_id @vuln.matches.first.id
end

if @vuln.service
  json.service do
    json.name @vuln.service.name
  end
end

json.host do
  json.id host.id
  json.name host.name
  json.address host.address
  json.os_icon_html host_os_icon_html(host, json: false)
  json.is_vm_guest host.is_vm_guest?
end

json.notes @vuln.notes do |note|
  json.comment note.data[:comment]
end

@vuln.refs.sort_by! { |r| r.name || '' }

# Need to drop the empty ref created by build_ref in the controller.
json.refs @vuln.refs.select { |r| !r.new_record? } do |ref|
  json.html_link link_for_ref(*ref.link_info)
end



