define [
  'base_model'
], ->
  @Pro.module "Entities", (Entities, App) ->

    class Entities.Vuln extends App.Entities.Model

      url: ->
        #If model has new_vuln_attempt status we update the status of the last vuln attempt on this vuln
        if @get('new_vuln_attempt_status')?
          Routes.update_last_vuln_attempt_status_workspace_vuln_path(@get('workspace_id'),@get('id'))
        else if @get('restore_vuln_attempt_status')?
          Routes.restore_last_vuln_attempt_status_workspace_vuln_path(@get('workspace_id'),@get('id'))
        else
          Routes.workspace_vuln_path(@get('workspace_id') ? WORKSPACE_ID, @id, format: 'json')

      updateLastVulnStatus: (status) ->
        #For now we send the whole model. When we move to Rails 4 we should use the patch option instead.
        @save({'new_vuln_attempt_status':status},
          success:(model) ->
            model.unset('new_vuln_attempt_status')

        )

      restoreLastVulnStatus: ->
        #For now we send the whole model. When we move to Rails 4 we should use the patch option instead.
        @save({'restore_vuln_attempt_status':true},
          success: (model) ->
            model.unset('restore_vuln_attempt_status')
        )


    #
    # API
    #

    API =
      getVuln: (id) ->
        new Entities.Vuln(id: id)

      newVuln: (attributes = {}) ->
        new Entities.Vuln(attributes)

    #
    # REQUEST HANDLERS
    #

    App.reqres.setHandler "vuln:entity", (id) ->
      API.getException id

    App.reqres.setHandler "new:vuln:entity", (attributes) ->
      API.newVuln attributes