define [
  'base_model'
  'base_collection'
], ->
  @Pro.module "Entities", (Entities, App) ->

    #
    # ENTITY CLASSES
    #

    class Entities.Host extends App.Entities.Model

        url: ->
          "/hosts/#{@id}.json"

    class Entities.HostsCollection extends App.Entities.Collection

      # @property accessLevels [Array<String>] a sorted list of unique values
      #   of the `access_level` property across the models in this collection

      initialize: (models, opts) ->
        {@workspace_id}  = opts

        @url = ->
          "/workspaces/#{@workspace_id}/hosts/json"

      model: Entities.Host

    #
    # API
    #

    API =
    # @return [Collection] of all Cred objects in a given workspace
      getHosts: (model,opts) ->
        hosts = new Entities.HostsCollection(model,opts)
        hosts


    App.reqres.setHandler "hosts:entities", (model,opts={}) ->
      throw new Error("missing workspace_id") unless opts.workspace_id?
      API.getHosts(model,opts)