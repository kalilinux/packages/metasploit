define [
  'base_layout'
  'base_view'
  'base_itemview'
  'apps/imports/index/templates/index_layout'
], () ->
  @Pro.module 'ImportsApp.Index', (Index, App, Backbone, Marionette, $, _) ->

    class Index.Layout extends App.Views.Layout
      template: @::templatePath 'imports/index/index_layout'

      className: 'foundation'

      ui:
        importBtn: '.import-btn'
        tagsLabel: '.tags-label'
        tagsPane: '.tags-pane'
        expandArrow: 'span.expand'
        collapseArrow: 'span.collapse'
        autoTagOs: '[name="imports[autotag_os]"]'
        importType: '[name="imports[type]"]'
        selectedImportType: '[name="imports[type]"]:checked'
        preserveHosts: '#update_hosts'

      events:
        'click @ui.tagsLabel' : '_toggleTags'
        'change @ui.importType' : '_importTypeChanged'

      triggers:
        'click @ui.importBtn':'import:start'

      regions:
        nexposeImportRegion: '.nexpose-import-region'
        tagsRegion: '.tags-region'


      _importTypeChanged: =>
        #Since the checked selector is cached
        @_bindUIElements()
        @trigger('import:typeChange',{view:@})

      _toggleTags: () ->
        @ui.expandArrow.toggle()
        @ui.collapseArrow.toggle()
        @ui.tagsPane.slideToggle('fast')

      enableImportButton: () ->
        @ui.importBtn.removeClass('disabled')

      disableImportButton: () ->
        @ui.importBtn.addClass('disabled')

      isFileImport: () ->
        @ui.selectedImportType.val() == 'import-from-file'

      expandTagSection: ->
        @ui.expandArrow.hide()
        @ui.collapseArrow.show()
        @ui.tagsPane.show()


