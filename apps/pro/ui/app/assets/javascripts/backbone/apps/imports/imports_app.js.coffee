define [
  'lib/utilities/navigation'
  'apps/imports/index/index_controller'
], ->
  @Pro.module 'ImportsApp', (ImportsApp, App)->

    class ImportsApp.Router extends Marionette.AppRouter
      appRoutes:
        "": "import"

    API =
      import: () ->
        new ImportsApp.Index.Controller()

    App.addInitializer ->
      new ImportsApp.Router(controller: API)

    # Sets the mainRegion, which is used by default when #show if called
    # without explicitly setting a region.
    App.addRegions
      mainRegion: "#imports-main-region"