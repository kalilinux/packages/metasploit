define [
  'jquery'
  'base_itemview'
  'lib/shared/cve_cell/templates/view'
  'lib/shared/cve_cell/templates/modal_view'
], ($) ->
  @Pro.module "Shared.CveCell", (CveCell, App, Backbone, Marionette, $, _) ->

    #
    # CveCell View
    #
    class CveCell.View extends App.Views.Layout
      template: @::templatePath "cve_cell/view"

      className: 'shared cve-cell'

      triggers:
        'click a' : 'refs:clicked'

    #
    # CveModal View
    #
    class CveCell.ModalView extends App.Views.ItemView
      template: @::templatePath "cve_cell/modal_view"

      className: 'shared cve-cell modal-content'

