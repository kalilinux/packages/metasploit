define [
  'base_controller'
  'lib/components/filter/filter_view'
  'css!css/components/filter'
], ->
  @Pro.module "Components.Filter" , (Filter, App) ->

    class Filter.Controller extends App.Controllers.Application
      # Hash of default options for controller
      defaults: ->
        {}

      # Creates a new instance of the FilterController and adds it to the
      # region passed in through the options hash
      #
      # @param [Object] options the option hash
      # @option opts region [Marionette.Region] the region into which we should render
      #   the filter
      initialize: (options = {}) ->
        # Set default key/values if key not defined in options hash
        # @config = _.defaults options, @_getDefaults()

        @filterView = @getFilterView(options)
        @setMainView(@filterView)

        # Setup listener for query search
        @listenTo @filterView, 'filter:query:new', (query) ->
          @trigger 'filter:query:new', query

      # Create view for this filter
      #
      # @param [Object] options the option hash
      # @return [Filter.FilterView] a view for the filter
      getFilterView: (options) ->
        new Filter.FilterView options

    # Create a filter component Controller
    App.reqres.setHandler "filter:component", (options={})->
      new Filter.Controller options