define [], ->
  @Pro.module "Concerns", (Concerns,App,Backbone, Marionette, $, _) ->

    Concerns.VulnAttemptStatuses =
      STATUSES:
        EXPLOITED: 'Exploited'

      #
      # @return [Boolean] true if the status of the attempt is "exploited"
      isExploited: ->
        @get('status') == @STATUSES.EXPLOITED || @get('vuln_attempt_status') == @STATUSES.EXPLOITED