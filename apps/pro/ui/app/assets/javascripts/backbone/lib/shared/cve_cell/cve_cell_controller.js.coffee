define [
  'base_controller'
  'entities/module_detail'
  'lib/components/modal/modal_controller'
  'lib/shared/cve_cell/cve_cell_views'
  'css!css/shared/cve_cell'
], () ->
  @Pro.module "Shared.CveCell", (CveCell, App) ->

    #
    # Contains the CveCell
    #
    class CveCell.Controller extends App.Controllers.Application

      # Hash of default options for controller
      defaults: ->
        {}

      # Create a new instance of the CveCell Controller and adds it to the
      # region passed in through the options hash
      #
      # @param [Object] options the option hash
      initialize: (options = {}) ->
        config = _.defaults options, @_getDefaults()
        @model = new Backbone.Model(config).get('model')
        view = new CveCell.View()

        # Add Mutator to parse Refs
        _.extend(@model,{
          mutators:
            parsedRefs:
              get: ->
                this.get('ref_names')
            refCount:
              get: ->
                this.get('ref_count')
        })

        view.model = @model

        @setMainView(view)

        @listenTo @_mainView, 'refs:clicked', =>

          moduleDetail = App.request 'module:detail:entity', {id:@model.id,refsOnly:true}

          dialogView = new CveCell.ModalView(
            model: moduleDetail
          )

          moduleDetail.fetch()

          App.execute 'showModal', dialogView,
            modal:
              title: 'References'
              description: ''
              width: 260
              height: 300
            buttons: [
              { name: 'Close', class: 'close'}
            ]
            loading:true


    # Register an Application-wide handler for a tooltip controller
    App.reqres.setHandler 'cveCell:component', (options={})->
      new CveCell.Controller options