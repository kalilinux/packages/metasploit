define [
  'lib/components/table/table_view'
], ->
  @Pro.module "Concerns", (Concerns, App, Backbone, Marionette, $, _) ->
    #
    # Pro specific carpenter controller concern
    #
    Concerns.ProCarpenter =

      initialize: ->
        @initializeFilter() if @filterOpts

        # Tell the collection NOT to fetch if we are rendering static data
        @collection.bootstrap() if @static

        @carpenterRadio.on 'error:search', (message) =>
          App.execute 'flash:display',
            title:   'Error in search'
            style:   'error'
            message: message || 'There is an error in your search terms.'

      #
      # Setup for filter component to work
      #
      initializeFilter: ->
        # This set's the filters searchBox text to the initial query (but doesn't execute query)
        # Note VisualSearch requires a space after the facet key to handle quoted values
        # Model::Search syntax, however, requires NO spaces
        # ex, "private.type: 'Metasploit::Credential::SSHKey'", the space allows proper tokenization
        if @search
          @fetch = false
          @filterOpts.query = @addWhiteSpaceToQuery(@search)

        @staticFacets = @filterOpts.staticFacets

        @filter = App.request 'filter:component', @

        # Apply intial filter query (must be done after filter:component requested)
        @applyCustomFilter(@search) if @search

        # Listen to the filter's query event
        @listenTo @filter, 'filter:query:new', (query) =>
          @toggleInteraction false
          @applyCustomFilter(query)

        @show @filter, region: (@filterOpts.filterRegion || @getMainView().filterRegion)

      #
      # applies a custom filter String (of the form "key1:val1 key2:val2") to the Collection
      # @param filter [String] the custom query string to parse and apply as a filter
      #
      applyCustomFilter: (query) ->
        @list.setSearch
          attributes:
            custom_query: query

      #
      # Formats query string to be compatible with VisualSearch
      # (simply adds whitespace to a single ':' separator
      # @param query [String]
      # example: query = "private.type:'Metasploit::Credential::Password'"
      # example output:  "private.type: 'Metasploit::Credential::Password'"
      #                                ^ whitespace inserted
      #
      addWhiteSpaceToQuery: (query) ->
        query.replace(/([^:]):([^:\s])/g, "$1: $2")
