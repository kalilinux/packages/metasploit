define [
  'css!css/concerns/placeholder'
], ->
  @Pro.module "Concerns", (Concerns, App, Backbone, Marionette, $, _) ->

    Concerns.Placeholder =

      events:
        'focusout @ui.textArea' : '_focusOutTextArea'
        'focusin @ui.textArea' : '_focusInTextArea'


      onShow: () ->
        @ui.textArea.val(@ui.textArea.data('placeholder'))
        @ui.textArea.addClass('placeholder')

      #
      # Show View on hover after time delay
      #
      _focusOutTextArea: (e) ->
        if @ui.textArea.val().match(/^$|^\s%/)?.length > 0
          @ui.textArea.val(@ui.textArea.data('placeholder'))
          @ui.textArea.addClass('placeholder')


      _focusInTextArea: (e) ->
        if @ui.textArea.data('placeholder')==@ui.textArea.val()
          @ui.textArea.removeClass('placeholder')
          @ui.textArea.val('')