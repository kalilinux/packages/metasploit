define [
  'jquery'
  'base_controller'
  'lib/concerns/controllers/pro_carpenter'
  'carpenter'
], ($) ->

  @Pro.module "Components.Table", (Table, App) ->
    Marionette.Carpenter.Controller.include('ProCarpenter')

    # Register an Application-wide handler for rendering a table component
    App.reqres.setHandler 'table:component', (options={}) ->
      # Not sure how to add this check in the ProCarpenter concern, it didn't work when I tried
      options.fetch = false if options.search
      new Marionette.Carpenter.Controller options