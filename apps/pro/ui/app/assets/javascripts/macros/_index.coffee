jQuery ($) ->
  $(document).ready ->
   # Module delete button should confirm deletion and verify that modules have
    # been selected.
    $('#macro-delete-submit').multiDeleteConfirm
      tableSelector:    '#macro_list'
      pluralObjectName: 'macros'
