jQuery ($) ->

  sortVulns = ->
    # Vuln attempts table should be searchable and sortable.
    $("#vuln-attempts-tbl").table
        searchInputHint:      "Search exploit attempts"
        datatableOptions:
          bDestroy: true
          "aaSorting":        [[0, 'desc']]
          "aoColumns": [
            null
            null
            null
            null
            null
          ]
          "oLanguage":
            "sEmptyTable":      "No attempts have been records for this vulnerability."

  window.moduleLinksInit=(moduleRunPathFragment) ->
      formId = "#new_module_run"

      $('a.module-name').click (event) ->
        if $(this).attr('href') isnt "#"
          return true
        else
          pathPiece = $(this).attr('module_fullname')
          modAction = "#{moduleRunPathFragment}/#{pathPiece}"
          theForm = $(formId)
          theForm.attr('action', modAction)
          theForm.submit()
          return false

  $(document).ready ->

    $("#tabs").tabs
      selected: 0
      spinner:  'Loading...'
      cache:    true
      load:     (e, ui) ->
        href = ui.tab.find('a').attr('href')
        $(ui.panel).find(".tab-loading").remove();
        if href.indexOf("vuln") > -1
          sortVulns()
      select:   (e, ui) ->
        $panel = $(ui.panel);
        if $panel.is(":empty")
          $panel.append("<div class='tab-loading'></div>")
