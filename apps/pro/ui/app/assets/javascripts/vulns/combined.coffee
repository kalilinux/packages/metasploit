jQuery ($) ->
  $ ->
    vulnsCombinedPath = $('#vulns-combined-path').html()
    $vulnsTable = $('#vulns-table')

    # Enable DataTable for the hosts list.
    $vulnsDataTable = $vulnsTable.table
      analysisTab: true
      setFilteringDelay: true
      controlBarLocation: $('.analysis-control-bar')
      searchInputHint:   "Search vulnerabilities"
      datatableOptions:
        "oLanguage":
          "sEmptyTable":    "No vulnerabilities are associated with this Project."
        "sAjaxSource":      vulnsCombinedPath
        "aaSorting":        [[1, 'desc']]
        "aoColumns": [
          {"mDataProp": "checkbox", "bSortable": false}
          {"mDataProp": "name"}
          {"mDataProp": "refs", "bSortable": false}
          {"mDataProp": "host_count", "sWidth": "50px"}
        ]