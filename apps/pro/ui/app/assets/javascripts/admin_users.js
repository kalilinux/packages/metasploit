document.observe("dom:loaded", function() {

	if ($('user_admin')) {
		//
		// Disable & check all Project Access checkboxes when Administrator is selected
		//
		var set_project_access_state = function() {
			var user_admin = $('user_admin');
			var project_access_checkboxes = $$('#workspaces input[type=checkbox]');
			if (user_admin.checked) {
				project_access_checkboxes.each(function(e) {
					e.checked = true;
					e.disable();
				});
			} else {
				project_access_checkboxes.each(function(e) {
					e.enable();
				});
			}
		};
		set_project_access_state();

		$('user_admin').observe('click', set_project_access_state);
	};
	
});
