jQuery ($) ->
  $ ->
    notesPath = $('#notes-path').html()
    $notesTable = $('#notes-table')

    # Enable DataTable for the hosts list.
    $notesDataTable = $notesTable.table
      analysisTab: true
      searchInputHint:   "Search Notes"
      datatableOptions:
        "oLanguage":
          "sEmptyTable":    "No Notes are associated with this Project."
        "sAjaxSource":      notesPath
        "aaSorting":      [[2, 'desc']]
        "aoColumns": [
          {"mDataProp": "checkbox", "bSortable": false}
          {"mDataProp": "host", "sWidth": "125px", "bSortable": false},
          {"mDataProp": "type", "sWidth": "150px"},
          {"mDataProp": "data", "bSortable": false},
          {"mDataProp": "created_at", "sWidth": "170px"},
          {"mDataProp": "status", "sWidth": "20px", "bSortable": false}
        ]

    # View note data in a dialog.
    $(document).on 'click', 'a.note-data-view', (e) ->
      $dialog = $("<div style='display:hidden'>#{$(this).siblings('.note-data').html()}</div>").appendTo('body')
      $dialog.dialog
        title: "Note data"
        buttons:
          "Close": -> 
            $(this).dialog('close')
        # Workaround for inability to set maxHeight
        # from http://bugs.jqueryui.com/ticket/4820
        open: (event, ui) ->
          $(this).css 'max-height': 400, 'overflow-y': 'auto'
          if $(this).parent().position().top == 0
            $('.ui-resizable').css 'top': '100px'
      e.preventDefault()

    $notesDataTable.addCollapsibleSearch()