jQuery ($) ->
  $ ->
    servicesCombinedPath = $('#services-combined-path').html()
    $servicesTable = $('#services-table')

    # Enable DataTable for the services list.
    $servicesDataTable = $servicesTable.table
      analysisTab: true
      searchInputHint:   "Search Services"
      datatableOptions:
        "oLanguage":
          "sEmptyTable":    "No Services are associated with this Project."
        "aaSorting":        [[4, 'desc']]
        "sAjaxSource":      servicesCombinedPath
        "aoColumns": [
          {"mDataProp": "name", "sWidth": "125px"},
          {"mDataProp": "protocol", "sWidth": "60px"},
          {"mDataProp": "port", "sWidth": "50px"},
          {"mDataProp": "info"},
          {"mDataProp": "host_count", "sWidth": "150px"}
        ]
