jQuery ($) ->
  $ ->
    servicesPath = $('#services-path').html()
    $servicesTable = $('#services-table')

    # Enable DataTable for the hosts list.
    $servicesDataTable = $servicesTable.table
      analysisTab: true
      controlBarLocation: $('.analysis-control-bar')
      searchInputHint:   "Search Services"
      datatableOptions:
        oLanguage:
          sEmptyTable:    "No Services are associated with this Project."
        sAjaxSource:      servicesPath
        aaSorting:      [[4, 'asc']]
        aoColumns: [
          {mDataProp: "checkbox", bSortable: false}
          {mDataProp: "host" },
          {mDataProp: "name", sWidth: "125px"},
          {mDataProp: "protocol", sWidth: "60px"},
          {mDataProp: "port", sWidth: "50px"},
          {mDataProp: "info"},
          {mDataProp: "state", sWidth: "125px"},
          {mDataProp: "updated_at", sWidth: '200px' }
        ]
        
    # Display the search bar when the search icon is clicked
    $servicesDataTable.addCollapsibleSearch()
