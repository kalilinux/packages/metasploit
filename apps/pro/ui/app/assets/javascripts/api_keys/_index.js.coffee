jQuery ($) ->
  $(document).ready ->

	 $(document).on 'click','a.reveal', (e) ->
	      $dialog = $('<div style="display:hidden"></div>').appendTo('body')
	      $dialog.load $(this).parent().parent().attr('rurl'), { 'id': $(this).parent().parent().attr('rid')}, (responseText, textStatus, xhrRequest) =>
	        $dialog.dialog
	          title: "Unobfuscated API Key"
	          height: 100
	          width: 300
	      e.preventDefault()
