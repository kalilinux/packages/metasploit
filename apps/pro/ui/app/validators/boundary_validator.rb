class BoundaryValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    workspace = record.workspace

    unless workspace
      record.errors[:workspace] << "must be present to validate #{attribute} is within boundary"
    else
      unless record.workspace.allow_actions_on?(value)
        record.errors[attribute] << 'must be inside workspace boundaries'
      end
    end
  end
end