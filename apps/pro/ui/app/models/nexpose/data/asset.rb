# This class represents a networked device tracked by a Nexpose instance. It is imported over
# an active connection to an {Mdm::NexposeConsole} as part of activity tracked by an associated
# {Nexpose::Data::ImportRun}.
class ::Nexpose::Data::Asset < ActiveRecord::Base

  #
  # Associations
  #

  # @!attribute site
  #   The Nexpose site this asset belongs to
  #
  #   @return [Nexpose::Data::Site]
  belongs_to :site,
             class_name: "::Nexpose::Data::Site",
             foreign_key: :nexpose_data_site_id


  # @!attribute exceptions
  #   The exception objects that have been created for this asset
  #
  #   @return [ActiveRecord::Relation<Nexpose::Result::Exception>]
  has_many :exceptions,
           class_name: "::Nexpose::Result::Exception",
           as: :nx_scope

  # @!attribute ip_addresses
  #   The IP addresses associated with this Asset
  #
  #   @return [ActiveRecord::Relation<Nexpose::Data::IpAddress>]
  has_many :ip_addresses,
           class_name: "::Nexpose::Data::IpAddress",
           foreign_key: :nexpose_data_asset_id



  # @!attribute mdm_hosts
  #   The Metasploit host objects that correspond to this asset
  #
  #   @return [ActiveRecord::Relation<Mdm::Host>]
  has_many :mdm_hosts,
           class_name: "Mdm::Host",
           foreign_key: :nexpose_data_asset_id


  # @!attribute vulnerability_instances
  #   The Nexpose-tracked instances of particular vulnerabilities found on this asset
  #
  #   @return [ActiveRecord::Relation<Nexpose::Data::VulnerabilityInstance>]
  has_many :vulnerability_instances,
           class_name: "::Nexpose::Data::VulnerabilityInstance",
           foreign_key: :nexpose_data_asset_id

  #
  # Nested Attributes
  #
  accepts_nested_attributes_for :ip_addresses

  #
  # Attributes
  #

  # @!attribute asset_id
  #   The primary key for this asset in the Nexpose instance's database
  #   @return [String]

  # @!attribute host_names
  #   The serialized host names on this asset
  #   @return [String]
  serialize :host_names, Array

  # @!attribute last_scan_date
  #   The date and time of the last scan of this asset by Nexpose
  #   @return [DateTime]

  # @!attribute last_scan_id
  #   The Nexpose database primary key of the last scan of this asset
  #   @return [Fixnum]

  # @!attribute mac_addresses
  #   The serialized Media Access Control addresses (http://en.wikipedia.org/wiki/MAC_address)
  #   known to Nexpose for this asset
  #   @return [String]
  serialize :mac_addresses, Array

  # @!attribute next_scan_date
  #   The date and time of the next scheduled scan of this asset
  #   @return [DateTime]

  # @!attribute os_name
  #   The operating system this asset is running
  #   @return [String]

  #
  # Scopes
  #

  scope :asset_id, lambda {|asset_id| where(asset_id: asset_id) }
  scope :last_scan_id, lambda {|last_scan_id| where(last_scan_id: last_scan_id) }

  #
  # Attribute Validations
  #
  validates :asset_id,
            presence: true,
            uniqueness: { scope: :nexpose_data_site_id }

  validates :nexpose_data_site_id,
            numericality: true,
            presence: true

  #
  # Rails 4 compatibility, manually create accessible attributes
  #
  ACCESSIBLE_ATTRS = [
    'asset_id',
    'host_names',
    'ip_addresses_attributes',
    'last_scan_date',
    'last_scan_id',
    'mac_address',
    'mac_addresses',
    'next_scan_date',
    'os_name',
    'url'
  ]


  # @returns[::Nexpose::Data::Asset] a valid ::Nexpose::Data::Asset made from the json hash
  # we get from nexpose
  def self.object_from_json(object_attributes)
    asset_attributes         = object_attributes.slice(*ACCESSIBLE_ATTRS)
    ip_addresses_attributes  = object_attributes["addresses"].collect {|address| {"address" => address} }
    asset_attributes["ip_addresses_attributes"] = ip_addresses_attributes

    self.asset_id(object_attributes["asset_id"]).first_or_create(asset_attributes)
  end

  # Iterate over the Asset's collection of ::Nexpose::Data::Vulnerability objects, find Asset's previously saved
  # Mdm::Host objects whose IP address matches the ::Nexpose::Data::Vulnerability#asset_ip_address
  # and create Mdm::Services, using +name+ and +port+ as uniqueness keys. Then create matching Mdm::Vuln
  # objects on the found-or-created Mdm::Service.
  # @return [void]
  def create_services_and_vulns_for_mdm_hosts
    # returning nil if the hosts we have created for this asset have been reassigned to another asset
    return nil unless mdm_hosts(true).present?
    # returning nil if this host doest have any vulns with real ports
    return nil if vulnerability_instances.with_real_ports.blank?

    # creating mdm vulns for each vuln expose found with real ports
    vulnerability_instances.with_real_ports.find_each do |vi|
      host         = mdm_hosts.where(address: vi.asset_ip_address).first

      next if host.nexpose_asset != self

      vuln_def     = vi.vulnerability.vulnerability_definition
      vuln_service = host.services.where(:name => vi.service, :port => vi.port).first_or_create(
        port:  vi.port,
        name:  vi.service,
        state: "open",       # If NX found a vuln on it, the service is 'open' as far as Nmap is concerned
        proto: vi.protocol
      )
      vuln = vuln_service.vulns.where(:name => vuln_def.title).first_or_create(
        host: host,
        name: vuln_def.title,
        info: vuln_def.description,
        nexpose_vulnerability_definition: vuln_def
      )

      vuln.origin = self.site.import_run

      vuln_def.vulnerability_references.each do |vuln_ref|
        module_refs = Mdm::Module::Ref.where(:name => vuln_ref.ref_lookup)

        if module_refs.any? && vuln_ref.ref_lookup
          ref = Mdm::Ref.where(:name => vuln_ref.ref_lookup).first_or_create
          vuln.refs << ref
          vuln.save
        end

      end

    end
  end

  # An array of valid {Mdm::Host} objects made from corresponding {Asset} data
  # and scoped to the same {Mdm::Workspace} as the parent {Nexpose::Data::ImportRun object}.
  # Since an Asset on the Nexpose side can have multiple IPs, we have one {Mdm::Host} per.
  # @param blacklist [Array<String>] an array of blacklist ip strings
  # @return [Array<Mdm::Host>]
  def to_mdm_hosts(blacklist=[])
    ip_addresses.pluck(:address).collect do |ip|
      unless blacklist.include? ip
        Mdm::Host.where(address: ip, workspace_id: site.import_run.workspace_id).
          first_or_create(
          name: host_names.first,
          mac:  mac_addresses.first,
          os_name: os_name,
          nexpose_asset: self,
          state: "alive"
        )
      end
    end
  end

  # A string representation of the asset: a hostname if there is one, otherwise
  # the first IP.
  # @return[String]
  def to_s
    if host_names.any? && !host_names.first.blank? && !os_name.blank?
      "#{host_names.first} - #{os_name}"
    elsif host_names.any?
      host_names.first
    elsif ip_addresses.any? && !os_name.blank?
      "#{ip_addresses.first.address} - #{os_name}"
    else
      ip_addresses.first.address
    end
  end

end


