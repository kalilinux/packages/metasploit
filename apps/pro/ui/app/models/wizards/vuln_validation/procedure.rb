require 'ostruct'

class Wizards::VulnValidation::Procedure < Wizards::BaseProcedure
  # @attr [OpenStruct] run_stats key/value store for relevant RunStats
  attr_reader :run_stats

  # @attr [::Nexpose::Data::ImportRun] import_run
  attr_reader :import_run

  #
  # State Machine
  #
  # actual logic of the wizard lives here

  before_save :save_custom_tag

  state_machine :state, initial: :ready, use_transactions: false do
    # build a map of state -> state for the next! event
    event :next do
      transition ready: :scanning, if: :assets_from_scan?
      transition ready: :importing, if: :assets_from_import?
      transition scanning: :finished, if: :no_hosts_imported?
      transition scanning: :building_matches
      transition importing: :finished, if: :no_hosts_imported?
      transition importing: :building_matches
      transition building_matches: :paused, if: :dry_run?
      transition building_matches: :reporting, if: :no_vulns_found?
      transition building_matches: :exploiting
      transition exploiting: :collecting_evidence, if: :should_collect_evidence?
      transition [:exploiting, :collecting_evidence] => :cleaning_up, :if => :should_cleanup?
      transition [:exploiting, :collecting_evidence, :cleaning_up] => :reporting, :if => :should_report?
      transition [:exploiting, :collecting_evidence, :cleaning_up] => :finished
      transition reporting: :finished
    end

    # Procedure is "paused" while user verifies he wants to validate the listed vulns
    event :continue do
      transition paused: :exploiting
    end

    event :error_occurred do |error_str|
      transition any => :error
    end

    # No matter what state Procedure starts from, RunStats get initialized & memoized
    before_transition any => any do |procedure, transition|
      procedure.initialize_run_stats
      # print your status!
      if procedure.commander.present?
        procedure.commander.print_good transition.to.humanize.capitalize
      end
    end

    #
    # "Bail" transitions
    #

    before_transition :ready => :importing do |procedure|
      procedure.import_run.choose_sites(procedure.config_hash[:nexpose_sites])
      procedure.save_host_count if procedure.run_stats.total_hosts.data.zero?
    end

    # Scanning did not return any results. Print an error and finish!
    before_transition :scanning => :finished do |procedure|
      procedure.commander.print_error "Scan did not find any hosts. Ending Vuln Validation run."
    end

    # Scanning did not return any results. Print an error and finish!
    before_transition :importing => :finished do |procedure|
      procedure.commander.print_error "Import did not find any hosts. Ending Vuln Validation run."
    end

    # run the scan task
    state :scanning do
      def run
        commander.run_module(
          'auxiliary/pro/nexpose/scan_and_import',
          config_hash[:nexpose_scan_task].merge(
            'SCAN' => true,
            'IMPORT_RUN_ID' => import_run.id,
            'AUTOTAG_OS' => config_hash[:tag_by_os],
            'AUTOTAG_TAGS' => custom_tag_names
          )
        )
        # now ensure that the vulns count matches up :)
        run_stats.vulns_found.update_attributes(:data => workspace.vulns.count)
      end
    end

    # Import straight from a console
    state :importing do
      def run
        total_hosts = run_stats.total_hosts
        total_hosts.data = import_run.sites.map { |site| site.summary['assets_count'] }.reduce(:+)
        commander.run_module(
          'auxiliary/pro/nexpose/scan_and_import', {
            'IMPORT_RUN_ID' => import_run.id,
            'AUTOTAG_OS' => config_hash[:tag_by_os],
            'AUTOTAG_TAGS' => custom_tag_names
          }
        )
        # now ensure that the vulns count matches up :)
        run_stats.vulns_found.update_attributes(:data => workspace.vulns.count)
      end
    end

    # build an AutoExploitation::MatchSet for running the validation_commander.rb task on
    state :building_matches do
      def run
        vulns     = vulns_to_exploit
        min_rank  = config_hash[:exploit_task]['DS_MinimumRank']
        match_set = MetasploitDataModels::AutomaticExploitation::MatchSet.create_with_matches_for_vulns(
          vulns,
          workspace: workspace,
          user: user,
          minimum_module_rank: min_rank
        )
        config_hash[:match_set_id] = match_set.id
        save
        run_stats.potential_exploits.update_attributes(data: match_set.matches.size)
      end
    end

    # Run the validation_commander.rb module to exploit stuff (or noop in dry-run)
    state :exploiting do
      def run
        # if we've reached this point in a dry_run, then the user has clicked Continue.
        match_set = MetasploitDataModels::AutomaticExploitation::MatchSet.find(config_hash[:match_set_id])
        exploitation_options = config_hash[:exploit_task].merge({
          'WORKSPACE_ID' => workspace.id,
          'MATCH_SET_ID' => match_set.id,
          'USER_ID' => user.id,
          'DS_OnlyMatch' => false
        })
        # break and smash things
        commander.run_module 'auxiliary/pro/nexpose/validation_commander', exploitation_options
      end
    end

    state :collecting_evidence do
      def run
        # put the pieces together with our bleeding fingers
        filter_sessions_in_config(config_hash[:collect_evidence_task])
        commander.run_module 'auxiliary/pro/collect', config_hash[:collect_evidence_task]
      end
    end

    state :cleaning_up do
      def run
        # wrap those fingers up champ
        commander.run_module 'auxiliary/pro/cleanup', config_hash[:cleanup_task]
      end
    end

    state :reporting do
      def run
        report = Report.find(config_hash[:report_id])
        report.generate_delayed
      end
    end
  end

  # Steps through the BaseProcedure's entire state
  #   machine, taking transitions based on user-defined options,
  #   which causes subtasks to execute in a specific sequence.
  #
  # @param comm [Class<Msf::Module>] commander module
  def execute_with_commander(comm)
    self.commander = comm
    continue! if paused?
    while can_next?
      begin
        reload # (ugh)
        run
      rescue NoMethodError
      end
      next!
    end
  end

  # @return [Array<Mdm::Vuln>] vulns that exist on Hosts that we want to exploit
  def vulns_to_exploit
    blacklist = (config_hash[:exploit_task]['DS_BLACKLIST_HOSTS'] || '').split(/\s+/).reject(&:blank?)
    bad_hosts = workspace.hosts.where('address IN (?)', blacklist).pluck(:id)
    if bad_hosts.present?
      workspace.vulns.where('host_id NOT IN (?)', bad_hosts)
    else
      workspace.vulns
    end
  end

  # @return [Boolean] if user wants to print match set, but not actually exploit
  def dry_run?
    config_hash[:exploit_task]['DS_OnlyMatch'] == true
  end

  def save_host_count
    total_hosts = run_stats.total_hosts
    total_hosts.data = import_run.sites.map { |site| site.summary['assets_count'] }.reduce(:+)
    total_hosts.save
  end

  # Returns true if the assets to be acted on in the procedure
  # are to come from a Nexpose scan of a live network.
  # @return [Boolean]
  def assets_from_scan?
    config_hash[:nexpose_gather_type].to_s.to_sym == :scan
  end

  # Returns true if the assets to be acted on in the procedure
  # come from a Nexpose import
  # @return [Boolean]
  def assets_from_import?
    config_hash[:nexpose_gather_type].to_s.to_sym == :import
  end

  # @return [Array<String>] of tag names to add to all hosts
  def custom_tag_names
    if config_hash[:use_custom_tag] and config_hash[:tagging_enabled]
      tag = Mdm::Tag.find_by_id(config_hash[:custom_tag])
      if tag.present? then tag.name else '' end
    else
      ''
    end
  end

  # @return [Mdm::Task] the attached task
  def mdm_task
    @mdm_task ||= Mdm::Task.find(self.commander[:task].task_id)
  end

  # Returns true if the system should run the collect evidence module,
  # which will run multiple scripts to gather passwords, screenshots,
  # dump hashes, etc on compromised systems, via combination of Meterpreter-based scripts
  # and auxiliary modules.
  # @return [Boolean]
  def should_collect_evidence?
    config_hash[:collect_evidence]
  end

  # Helper for state_machine transition conditions
  # @return [Boolean] whether we should generate a report
  def should_report?
    # TODO This needs a better check than just hosts, should use report
    # validations:
    config_hash[:report_enabled] and workspace.hosts(true).exists?
  end

  # Helper for state_machine transition condition
  # Checks for a user selected cleanup task and
  # whether there are active sessions in the workspace
  # @return [Boolean] whether we should run cleanup
  def should_cleanup?
    config_hash[:cleanup_enabled] and workspace.sessions.alive.exists?
  end

  # Ran in a before_save callback
  # Saves the Mdm::Tag object on form.custom_tag, if applicable
  def save_custom_tag
    if config_hash[:custom_tag].present? and config_hash[:use_custom_tag]
      config_hash[:custom_tag].save if config_hash[:custom_tag].new_record?
    end
  end

  # @return [::Nexpose::Data::ImportRun] the ImportRun to use
  def import_run
    @import_run ||= if assets_from_scan?
      console_id = config_hash[:nexpose_scan_task]['DS_NEXPOSE_CID'].to_i
      new_import_run = ::Nexpose::Data::ImportRun.create(
        console: Mdm::NexposeConsole.find(console_id),
        workspace: workspace,
        user: user
      )
      new_import_run
    else
      # user already kicked off an ImportRun to load sites
      new_import_run = ::Nexpose::Data::ImportRun.find(config_hash[:import_run_id].to_i)
      if new_import_run.workspace.blank?
        new_import_run.update_attributes(workspace: workspace)
      end
      new_import_run
    end
  end

  def no_hosts_imported?
    workspace.hosts(true).empty?
  end

  def no_vulns_found?
    workspace.vulns(true).empty?
  end

  # Creates & memoizes the necessary RunStats
  def initialize_run_stats
    return if @run_stats.present?
    stats_hash = {
      :hosts_imported => RunStat.where(
        name: :hosts_imported,
        task_id: mdm_task.id
      ).first_or_create,
      :total_hosts => RunStat.where(
        name: :total_hosts,
        task_id: mdm_task.id
      ).first_or_create,
      :vulns_found => RunStat.where(
        name: :vulns_found,
        task_id: mdm_task.id
      ).first_or_create,
      :potential_exploits => RunStat.where(
        name: :potential_exploits,
        task_id: mdm_task.id
      ).first_or_create,
      :exploit_matches => RunStat.where(
        name: :exploit_matches,
        task_id: mdm_task.id
      ).first_or_create,
      :vuln_validations => RunStat.where(
        name: :vuln_validations,
        task_id: mdm_task.id
      ).first_or_create,
      :vuln_exceptions => RunStat.where(
        name: :vuln_exceptions,
        task_id: mdm_task.id
      ).first_or_create
    }
    stats_hash.each do |key, run_stat|
      if run_stat.data.nil?
        run_stat.data = 0
        run_stat.save!
      end
    end
    @run_stats = OpenStruct.new(stats_hash)
  end
end
