class CredFile < ActiveRecord::Base
  #
  # Constants
  #

  DEFAULT_FILE_TYPE = 'userpass'
  FILE_TYPE_LABEL_BY_FILE_TYPE = {
      'pass' => 'Passwords',
      'pwdump' => 'PWDump',
      'ssh_keys' => 'SSH Keys',
      'user' => 'Usernames',
      'userpass' => 'UserPass'
  }

  #
  # Associations
  #

  belongs_to :workspace, :class_name => 'Mdm::Workspace'

  #
  # Validations
  #

  validates :ftype, :inclusion => { :in => FILE_TYPE_LABEL_BY_FILE_TYPE.keys }
  validates :workspace_id, :presence => true

  #
  # Methods
  #

	def display_name
		if name.present?
			name
    elsif path.present?
      File.basename(path)
    else
      "Nameless and pathless #{self.class}"
		end
	end
end
