require "csv"

class SocialEngineering::TargetList < ActiveRecord::Base
  include SocialEngineering::TargetList::Rows

  self.table_name = :se_target_lists

  #
  #
  # Associations - sorted by name
  #
  #

  has_many :emails, :class_name => 'SocialEngineering::Email', :dependent => :restrict
  has_many :target_list_targets, :class_name => 'SocialEngineering::TargetListHumanTarget', :dependent => :destroy
  belongs_to :user, :class_name => 'Mdm::User'
  belongs_to :workspace, :class_name => 'Mdm::Workspace'

  #
  # Through :target_list_targets
  #

  has_many :human_targets, :class_name => 'SocialEngineering::HumanTarget', :through => :target_list_targets

  #
  # Callbacks
  #

  after_save  :save_related

  #
  # Scopes
  #

  scope :for_workspace, lambda { |workspace| where(:workspace_id => workspace.id) }
  scope :workspace_id, lambda { |workspace_id| where(:workspace_id => workspace_id) }

  #
  # Validations
  #

  validates :name, :presence => true, :uniqueness => { :scope => :workspace_id }
  validates :user, :presence => true
  validates :workspace, :presence => true

  private

  # Saves related new HTs and ALHT refs.
  def save_related
    self.class.transaction do
      self.human_targets.each { |ht| ht.save if ht.new_record? }
      self.target_list_targets.each { |ref| ref.save if ref.new_record? }
    end
  end
end
