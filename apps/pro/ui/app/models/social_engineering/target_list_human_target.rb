class SocialEngineering::TargetListHumanTarget < ActiveRecord::Base
  self.table_name = :se_target_list_human_targets

  #
  # Associations
  #

  belongs_to :human_target, :class_name => 'SocialEngineering::HumanTarget'
  belongs_to :target_list, :class_name => 'SocialEngineering::TargetList'

  after_destroy :destroy_orphans

  validates :human_target_id,
            :uniqueness => {
                :scope => :target_list_id
            }

  def destroy_orphans
    SocialEngineering::HumanTarget.all.each do |ht|
      if ht.target_lists.empty?
        ht.destroy
      end
    end
  end
end
