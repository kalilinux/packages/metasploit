class SocialEngineering::Visit < ActiveRecord::Base
  self.table_name = :se_visits

  include SocialEngineering::TargetInteractionCallbacks

  #
  # Attributes
  #

  # @!attribute [rw] address
  #   The originating IP address of the request for this visit. Necessary to
  #   avoid coercion to an `IPAddr` object.
  #
  #   @return [String]
  def address
    self[:address].to_s
  end

  #
  # Associations
  #

  belongs_to :email, :class_name => 'SocialEngineering::Email'
  belongs_to :human_target, :class_name => 'SocialEngineering::HumanTarget'
  belongs_to :web_page, :class_name => 'SocialEngineering::WebPage'

  #
  # Scopes
  #

  scope :for_human_target, lambda{|ht| where(:human_target_id => ht.id).includes(:email => :campaign) }
  scope :campaign_id, lambda{ |campaign_id| joins(:email).where(:se_emails => {:campaign_id => campaign_id})  }

  #
  # Validations
  #

  validates :human_target, :presence => true

  def self.remove_for(campaign)
    visits = []

    if campaign.web_campaign?
      visits << where(web_page_id: campaign.web_pages)
    end

    if campaign.email_campaign?
      visits << where(email_id: campaign.emails)
    end

    destroy(visits.flatten.uniq.collect(&:id))
  end
end
