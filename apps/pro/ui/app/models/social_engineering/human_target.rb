class SocialEngineering::HumanTarget < ActiveRecord::Base
  self.table_name = :se_human_targets

  #
  #
  # Associations - sorted by association name
  #
  #

  has_many :phishing_results, :class_name => 'SocialEngineering::PhishingResult'
  belongs_to :user, :class_name => 'Mdm::User'
  belongs_to :workspace, :class_name => 'Mdm::Workspace'
  has_many :target_list_targets, :class_name => 'SocialEngineering::TargetListHumanTarget'

  #
  # Has Many Through :target_list_targets
  #

  has_many :target_lists, :class_name => 'SocialEngineering::TargetList', :through => :target_list_targets

  #
  # Validations
  #

  validates :email_address, 
            :presence => true, 
            :uniqueness => {
                :scope => [:first_name, :last_name, :workspace_id],
                :case_sensitive => false
            }
  
  validates :workspace, :presence => true


  # @return[Array] HumanTargets which have corresponding PhishingResults in the DB
  scope :phished, lambda { joins(:phishing_results) }
  scope :workspace_id, lambda { |workspace_id| where(:workspace_id => workspace_id) }
  
  # returns array containing [@email,@first_name,@last_name]
  def build_csv_row
    [email_address, first_name, last_name]
  end

  # returns a dummy human target with default values for name & email
  # used by se/emails_controller#preview
  def self.dummy
    target = SocialEngineering::HumanTarget.new
    target.email_address = 'example@example.com'
    target.first_name = 'John'
    target.last_name  = 'Doe'
    target.id = 0
    target
  end

  def name
    "#{last_name}, #{first_name}"
  end
end
