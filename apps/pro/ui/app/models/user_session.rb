# Explicit requires for engine/prosvc.rb
require 'authlogic'

class UserSession < Authlogic::Session::Base
  logout_on_timeout(Rails.env.production? || Rails.env.test?)
  consecutive_failed_logins_limit 5
  failed_login_ban_for 10.minutes

  authenticate_with Mdm::User
  httponly true
  secure true

  before_destroy :reset_persistence_token
  before_create :reset_persistence_token

  private

  # Reset the cookie's persistence token.
  def reset_persistence_token
    record.reset_persistence_token!
  end
end
