class ReportArtifact < ActiveRecord::Base

  include Metasploit::Pro::Report::Utils

  belongs_to :report

  scope :unaccessed, lambda { |workspace_id|
                     where(["accessed_at is null and reports.workspace_id = ?", workspace_id]).includes(:report)
  }

  #
  # Validations
  #
  validates_presence_of :file_path
  validate              :artifact_file_exists
  validates_presence_of :report_id
  validates_presence_of :report

  #
  # Callbacks
  #
  before_destroy :remove_file, :update_parent_report

  #
  # Methods
  #

  def artifact_file_exists
    # If there is a file path
    if errors[:file_path].blank?
      unless File.exists? file_path
        errors[:artifact_file_path] << "Artifact file not found at #{file_path}"
      end
    end
  end

  def logger
    report_logger
  end

  # Delete artifact file from filesystem
  def remove_file
    begin
      FileUtils.rm_f(file_path)
      logger.info("Artifact file deleted: #{file_path}")
    rescue SystemCallError => e
      logger.error("Unable to remove artifact file #{file_path}: #{e}")
    end
  end

  # Removes deleted artifact format from parent report format list
  def update_parent_report
    self.report.file_formats.delete(file_format)
    self.report.save
  end

  # @return [String] the format of the artifact file
  def file_format
    format = File.extname(self.file_path).gsub('.', '')
    ['doc', 'docx'].include?(format) ? 'word' : format
  end
end