class ImportCredsTask < TaskConfig
  include ActiveModel::Validations

  #
  # Validations
  #

  validates :path, :credentials_file => true, :presence => true

  #
  # Methods
  #

  def desc
    @desc ||= ''
  end

  def desc=(description)
    @desc = description.to_s.strip
  end

  def file_type
    @file_type ||= CredFile::DEFAULT_FILE_TYPE
  end

  attr_writer :file_type

  def initialize(attributes={})
    super(attributes)

    [:desc, :file_type, :name, :orig_file_name, :path].each do |attribute_name|
      setter = "#{attribute_name}="
      value = attributes[attribute_name]

      send(setter, value)
    end
  end

  def name
    @name ||= ''
  end

  def name=(name)
    @name = name.to_s.strip
  end

  def orig_file_name
    @orig_file_name ||= ''
  end

  attr_writer :orig_file_name

  def path
    @path ||= ''
  end

  attr_writer :path

  def rpc_call
    client.start_import_creds(
        'DS_DESC'            => desc,
        'DS_FTYPE'           => file_type,
        'DS_IMPORT_PATH'     => path,
        'DS_NAME'            => name,
        'DS_ORIG_FILE_NAME'  => orig_file_name,
        'DS_REMOVE_FILE'     => true,
        'username'           => username,
        'workspace'          => workspace.name,
    )
  end
end

