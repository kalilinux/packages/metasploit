module Mdm::Session::SupportedActions
  def supports_collect?
    ['shell', 'meterpreter'].include? stype
  end

  def supports_collect_other_files?
    ['meterpreter'].include? stype
  end

  def supports_search?
    (['meterpreter'].include? stype and platform =~ /win32|win64/)
  end

  def supports_shell?
    ['shell', 'meterpreter'].include? stype
  end

  def supports_vnc?
    (['meterpreter'].include?(stype) and platform =~ /win32|win64/)
  end

  def supports_vpn_pivot?
    (is_admin? and ['meterpreter'].include? stype and platform =~ /win32|win64/)
  end
end
