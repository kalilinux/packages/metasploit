module Mdm::Vuln::Scopes
  extend ActiveSupport::Concern

  included do
    scope :with_table_data, lambda {
      select([
               Mdm::Vuln[:id].as('vuln_id'),
               Mdm::VulnAttempt[:id].as('id'),
               Mdm::Vuln[:name],
               Mdm::Module::Detail[:name].as('description'),
               Mdm::VulnAttempt[:username].as('username'),
               Mdm::VulnAttempt[:attempted_at],
               Mdm::VulnAttempt[:exploited],
               Mdm::VulnAttempt[:fail_reason],
               Mdm::VulnAttempt[:last_fail_reason],
               Mdm::VulnAttempt[:module]
             ])
    }

    scope :event_history, lambda { |vuln|
      joins(
        Mdm::Vuln.join_association(:vuln_attempts),
        Mdm::Vuln.join_association(:refs),
        Mdm::Ref.join_association(:module_refs),
        Mdm::Module::Ref.join_association(:detail)
      ).where(
        Mdm::Vuln[:id].eq(vuln.id)
      ).group([
                Mdm::Vuln[:id],
                Mdm::VulnAttempt[:id],
                Mdm::Vuln[:name],
                Mdm::Module::Detail[:name]
              ]
      )
    }

  end

end