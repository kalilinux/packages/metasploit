module Mdm::Task::Log
  # returns each line from the logfile, starting with the specified line number
  def log_lines(from = 0)
    if path and File.exist?(path)
      lines = File.readlines(path)[from..-1]
      return [] if lines.blank?
      lines.map(&:strip)
    else
      []
    end
  end
end