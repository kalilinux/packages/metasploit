module Mdm::Listener::Decorator
  extend ActiveSupport::Concern

  include Mdm::Listener::Task

  module ClassMethods
    def supported_payloads
      [
          [ "IPv4: Windows Meterpreter (TCP)", "windows/meterpreter/reverse_tcp"],
          [ "IPv4: Windows Meterpreter (HTTPS)", "windows/meterpreter/reverse_https"],
          [ "IPv4: Windows Meterpreter (HTTP)", "windows/meterpreter/reverse_http"],
          [ "IPv4: Windows x64 Meterpreter (TCP)", "windows/x64/meterpreter/reverse_tcp"],
          [ "IPv4: Java Meterpreter (TCP)", "java/meterpreter/reverse_tcp"],
          [ "IPv4: PHP Meterpreter (TCP)", "php/meterpreter/reverse_tcp"],
          [ "IPv4: Command Shell (TCP)", "generic/shell_reverse_tcp"],
          [ "IPv6: Windows Meterpreter (TCP)", "windows/meterpreter/reverse_ipv6_tcp"],
          [ "IPv6: Windows Meterpreter (HTTPS)", "windows/meterpreter/reverse_ipv6_https"],
          [ "IPv6: Windows Meterpreter (HTTP)", "windows/meterpreter/reverse_ipv6_http"]
      ]
    end
  end

	def status
		active? ? "Active" : "Inactive"
  end

  def name
		"#{self.address}:#{self.port}"
	end
end

