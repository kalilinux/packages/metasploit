# Explicit requires for engine/prosvc.rb
require 'authlogic'

module Mdm::User::Authentic
  extend ActiveSupport::Concern

  included do
    acts_as_authentic do |c|
      c.session_class ::UserSession
      c.validate_email_field = false
      c.merge_validates_length_of_password_field_options minimum: 8
      c.merge_validates_length_of_password_confirmation_field_options minimum: 8
      c.logged_in_timeout = 30.minutes
    end

    #
    # Validations
    #

    validates :password, password_is_strong: true
    validates :password_confirmation, password_is_strong: true
  end
end
