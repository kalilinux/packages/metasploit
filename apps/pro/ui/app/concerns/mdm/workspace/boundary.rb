module Mdm::Workspace::Boundary
  extend ActiveSupport::Concern

  included do
    # Must be defined here in order to override the definition in the base class as the
    # base class is always the first class ancestor, before any included modules.
    def boundary_must_be_ip_range
      if boundary
        begin
          boundaries = Shellwords.split(boundary)
        rescue ArgumentError
          boundaries = []
        end

        boundaries.each do |range|
          unless valid_ip_or_range?(range)
            errors.add(:boundary, "must be a valid IP range")
          end
        end
      end
    end
  end

  def addresses
    (boundary || "").split("\n")
  end
end