module Mdm::Workspace::CredFiles
  extend ActiveSupport::Concern

  included do
    #
    # Associations
    #

    has_many :cred_files, :class_name => 'CredFile', :dependent => :destroy
  end
end
