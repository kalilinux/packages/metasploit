module Mdm::Module::Detail::Scopes
  extend ActiveSupport::Concern

  included do
    scope :with_table_data, lambda {
      select([
               Mdm::Module::Detail[:id],
               Mdm::Module::Detail[:mtype],
               Mdm::Module::Detail[:rank],
               Mdm::Module::Detail[:name],
               Mdm::Module::Detail[:name].as('description'),
               Mdm::Module::Detail[:fullname].as('module'),
               'array_to_json(array_agg(module_platforms.name)) as platform_names',
               'array_to_json(array_agg(module_targets.name)) as target_names',
               'array_to_json(array_agg(module_actions.name)) as action_names',
               'array_to_json(array_agg(module_refs.name)) as ref_names',
               'count(module_refs.name) as ref_count'
             ])
    }

    scope :related_modules, lambda { |vuln|
      joins(
        Mdm::Module::Detail.join_association(:actions, Arel::OuterJoin),
        Mdm::Module::Detail.join_association(:targets, Arel::OuterJoin),
        Mdm::Module::Detail.join_association(:platforms),
        Mdm::Module::Detail.join_association(:refs),
        Mdm::Module::Ref.join_association(:refs),
        Mdm::Ref.join_association(:vulns)
      ).where(
        Mdm::Vuln[:id].eq(vuln.id)
      ).group([
                Mdm::Module::Detail[:id]
              ]
      )
    }

  end

end