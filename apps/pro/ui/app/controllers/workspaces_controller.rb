class WorkspacesController < ApplicationController
  include AnalysisSearchMethods
  include Metasploit::Pro::AttrAccessor::Boolean
  include Metasploit::Pro::AddressUtils

  before_filter :require_membership, :only => [:show]
  before_filter :require_manageability, :only => [:edit, :update]

  require 'shellwords'

  def index
    @workspaces = if (not current_user.admin) and License.get.multi_user?
      current_user.accessible_workspaces.sort_by(&:updated_at).reverse
    else
      Mdm::Workspace.order('updated_at DESC')
    end
    @feed_serial   = License.get.product_serial
    @feed_version  = License.get.product_version
    @feed_revision = License.get.product_revision
    @feed_edition  = License.get.edition.downcase
    @feed_ts       = Time.now.to_i.to_s
    @feed_orig_version = License.get.product_orig_version
    @supports_quick_start_menu = License.get.supports_quick_start_and_global_tools?
    @enable_news_feed = set_default_boolean(current_profile.settings['enable_news_feed'], true)
  end

  def new
    @workspace = Mdm::Workspace.new(
      :boundary => default_ip_range,
      :users => Mdm::User.all,
      :owner => current_user
    )
  end

  def create
    params[:workspace][:user_ids] ||= []
    @workspace = Mdm::Workspace.new(params[:workspace])
    if @workspace.save
      respond_to do |format|
        format.html { redirect_to workspace_path(@workspace) }
        format.json do
          render :json => { :path => workspace_path(@workspace) }
        end
      end
    else
      respond_to do |format|
        format.html { render :action => 'new' }
        format.json do
          render :json   => { :errors => @workspace.errors },
                 :status => :bad
        end
      end
    end
  end

  def edit
    @workspace = Mdm::Workspace.find(params[:id])
  end

  def update
    params[:workspace][:user_ids] ||= []
    if @workspace.update_attributes(params[:workspace])
      flash[:notice] = "Project '#{@workspace.name}' updated"
      redirect_to workspaces_url
    else
      render :action => 'edit'
    end
  end

  def destroy
    workspaces = if params[:id]
      [ Mdm::Workspace.find(params[:id]) ]
    elsif params[:workspace_ids]
      Mdm::Workspace.find(params[:workspace_ids])
    else
      []
    end

    workspaces.each do |workspace|
      unless workspace.manageable_by?(current_user)
        render(:text => "You are not the owner of this project", :layout => "forbidden", :status => 403) and return
      end

      workspace.tasks.each { |t| t.rpc_stop }
      workspace.sessions.each { |s| s.stop rescue nil }
      workspace.destroy
    end

    if workspaces.empty?
      flash[:error] = "No Projects selected to remove"
    else
      flash[:notice] = view_context.pluralize(workspaces.length, "Project") + " removed"
    end

    respond_to do |format|
      format.html { redirect_to workspaces_url }
      format.xml  { head :ok }
    end
  end

  def show
    @workspace = Mdm::Workspace.find(params[:id])
    @tasks = @workspace.tasks.running
    @disco_running       = @tasks.any? { |t| t.discovery? }
    @penetration_running = @tasks.any? { |t| t.penetration? }
    @collection_running  = @tasks.any? { |t| t.collection? }
    @stats = @workspace.stats

    respond_to do |format|
      format.html
    end
  end

  def search
    @workspace = Mdm::Workspace.find(params[:workspace_id]) if params[:workspace_id]
    @search = params[:search]
    @search_terms = ::Shellwords.shellwords(@search) rescue @search.split(/\s+/)
    @search_terms.reject! { |t| t.blank? }

    if @workspace and not current_user.accessible_workspaces.include?(@workspace)
      render :text => "You are not a member of this project", :layout => "forbidden", :status => 403 # Forbidden
      return
    end

    @workspaces = ( @workspace ? [@workspace] : current_user.accessible_workspaces )

    @workspaces = @workspaces.inject({}) do |hash, workspace|
      hosts = pro_search(:hosts,false,@search,workspace)
      services = pro_search(:services,false,@search,workspace)
      vulns = pro_search(:vulns,false,@search,workspace)


      hash[workspace] = {
        :hosts => hosts.sort_by(&:address_int),
        :vulns => vulns.sort_by(&:name),
        :services => services.sort{|a,b| [a.port, a.host.address_int] <=> [b.port, b.host.address_int]},
      }
      hash
    end
  end

protected

  def require_membership
    @workspace = Mdm::Workspace.find(params[:id])
    unless @workspace.usable_by?(current_user)
      render :text => "You are not a member of this project", :layout => "forbidden", :status => 403 # Forbidden
    end
  end

  def require_manageability
    @workspace = Mdm::Workspace.find(params[:id])
    unless @workspace.manageable_by?(current_user)
      render :text => "You are not the owner of this project", :layout => "forbidden", :status => 403 # Forbidden
    end
  end
end
