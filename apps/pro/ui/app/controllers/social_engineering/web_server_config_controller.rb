module SocialEngineering
class WebServerConfigController < ServerConfigController
  CUSTOM_HOST_VALUE_PLACEHOLDER = 'custom_value'

  def edit
    compute_hosts
  end

  def update
    @campaign.web_host     = params[:social_engineering_campaign][:web_host]
    @campaign.web_port     = params[:social_engineering_campaign][:web_port]
    @campaign.web_bap_port = params[:social_engineering_campaign][:web_bap_port]
    @campaign.web_ssl      = (params[:social_engineering_campaign][:web_ssl].to_i == 1)

    @campaign.errors.clear

    file = params[:social_engineering_campaign][:file]
    ssl_error = false

    if file.present?
      begin
        ssl_cert = SSLCert.create!(workspace_id: @campaign.workspace_id, file: file)
        @campaign.ssl_cert = ssl_cert
      rescue ActiveRecord::RecordInvalid, OpenSSL::X509::CertificateError => e
        ssl_error = true
      end
    end

    if @campaign.save && ssl_error == false
      render :partial => 'shared/iframe_transport_response', :locals => {
        :data => {
          success: true,
          json: SocialEngineering::CampaignSummary.new(@campaign).to_json
        }
      }
    else
      # TODO: rationalize handling of failure condition
      if ssl_error == true
        @campaign.errors.add(:file, "is not a valid SSL Cert")
      end
      compute_hosts
      errors = render_to_string 'social_engineering/web_server_config/edit', layout: false
      render :partial => 'shared/iframe_transport_response', :locals => { :data => { :success => false, :error => errors } }
    end
  end

  private

  def compute_hosts
    @default_ip = Rex::Socket.source_address()
    @default_host = Socket.gethostname
    if [@default_ip, @default_host].include? @campaign.web_host
      @other_value = ''
    else
      @other_value = @campaign.web_host
      # in order for formtastic to select the correct radio button
      @campaign.web_host = CUSTOM_HOST_VALUE_PLACEHOLDER
    end
  end

end
end
