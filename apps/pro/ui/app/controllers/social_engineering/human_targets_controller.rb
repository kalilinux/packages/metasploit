module SocialEngineering
class HumanTargetsController < ApplicationController

  def show
    @human_target     = HumanTarget.find(params[:id])
    @phishing_results = PhishingResult.for_human_target(@human_target)
    @visits           = Visit.for_human_target(@human_target)
    @openings         = EmailOpening.for_human_target(@human_target)
  end

end
end
