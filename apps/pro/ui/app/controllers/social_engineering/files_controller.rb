module SocialEngineering
  class FilesController < ApplicationController
    before_filter :load_workspace, :except => :download
    layout false
    respond_to :html, :js

    def index
      finder = ::UserSubmittedFilesFinder.new(@workspace)
      render :json => DataTableQueryResponse.new(finder, params).to_json
    end

    def destroy
      file_ids = params[:user_submitted_file_ids] || []
      if file_ids.empty?
        render :json => {:error =>  'No files were selected'}, :status => :error
      else
        begin
          UserSubmittedFile.destroy(file_ids)
          head :ok
        rescue Errno::EACCES => e
          render :json => {:error =>  e.message}, :status => :error
        end
      end
    end

    def new
      @file = UserSubmittedFile.new
    end

    def create
      begin
        @file = UserSubmittedFile.new(params[:social_engineering_user_submitted_file])
        @file.workspace_id = @workspace.id
        @file.user_id = current_user.id
        if @file.save
          render :partial => 'create_success'
        else
          render :action => "new"
        end
      rescue Errno::EACCES
        pathname = Pathname.new SocialEngineering::CampaignFileUploader.new.campaign_files_path
        @file = UserSubmittedFile.new
        @file.errors.add(:base, "Permissions error. Unable to write to #{pathname.realpath}")
        render :action => :new
      end
    end

    def download
      # serve file here!!!!!!!!!!!!!!!~!!~!!!!!~
      @file = CampaignFile.find(params[:id])
      File.open(@file.attachment.path, 'rb') do |file|
        send_data file.read,
        :type => 'application/x-octet-stream',
        :disposition => 'attachment',
        :filename => File.basename(@file.attachment.path)
      end
    end
  end
end

