class SocialEngineering::VisitsController < ApplicationController
  before_filter :find_campaign
  before_filter :load_workspace

  def index
    @visits = @campaign.visits_with_targets
  end

  private

  def find_campaign
    @campaign = SocialEngineering::Campaign.find(params[:campaign_id])
  end

end
