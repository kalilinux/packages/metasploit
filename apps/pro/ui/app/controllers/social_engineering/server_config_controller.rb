module SocialEngineering
class ServerConfigController < ApplicationController
  before_filter :find_campaign
  before_filter :load_workspace

  # Assume we're doing AJAXy stuff that will render w/out layout
  respond_to :js
  layout false


  # Since we're just rendering the default template, the controller-wide 
  # respond_to takes care of anything we'd need to do here.
  def edit
  end

  private

  def find_campaign
    @campaign = Campaign.find(params[:campaign_id])
  end
end
end
