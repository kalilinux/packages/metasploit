class SocialEngineering::TargetListsController < ApplicationController
  before_filter :load_workspace
  before_filter :load_target_list, :only => [:show, :export, :update, :remove_targets]
  
  layout false

  def index
    respond_to do |format|
      format.html do
        @target_lists = SocialEngineering::TargetList.where(:workspace_id => @workspace.id)
      end
      format.json do
        finder = ::TargetListsFinder.new(@workspace)
        render :json => DataTableQueryResponse.new(finder, params).to_json
      end
    end
  end

  def new
    @target_list = SocialEngineering::TargetList.new
  end

  def create
    @target_list = SocialEngineering::TargetList.new(params[:social_engineering_target_list])
    import_file = import_file_from_params
    @target_list.user = current_user
    @target_list.workspace = @workspace
    @target_list.import_file = import_file
    if @target_list.save
      flash.now[:notice] = "Target List '#{@target_list.name}' successfully created."
      @target_list.reload
      render :partial => 'create_success'
    else
      @prev_targets = @target_list.quick_add_targets
      render :new
    end
  end

  def show
  end

  def update
    @target_list.update_attributes params[:social_engineering_target_list]
    @prev_targets = @target_list.quick_add_targets if @target_list.errors.any?
    render :show
  end

  def export
  end

  def destroy
    attack_ids = params[:target_list_ids] || []

    if attack_ids.empty?
      render :json => {:error => "No Target Lists selected to delete"}, :status => :error
    else
      begin
        SocialEngineering::TargetList.destroy(attack_ids)
        head :ok
      rescue ActiveRecord::DeleteRestrictionError
        emails = SocialEngineering::Email.where("target_list_id IN (?)", attack_ids)
        campaigns = emails.collect(&:campaign).uniq
        campaigns_str = campaigns.collect{ |camp| "\"#{camp.name}\"" }.join(", ")
        error_str = "is use in by the #{'campaign'.pluralize(campaigns.size)} #{campaigns_str}"
        render :json => {:error => "Unable to delete Target List because it #{error_str}."}, :status => :error
      end
    end
  end

  # given an array of human targets, remove each target from the current class
  # todo: determine if this leaves hanging refs anywhere
  def remove_targets
    target_ids = params[:human_target_ids] || []
    if target_ids.empty?
      flash[:error] = "No Human Target selected to delete"
    elsif target_ids.size >= @target_list.human_targets.size
      flash[:error] = "You cannot have an empty Target List."
    else
      target_ids.each do |tid|
        SocialEngineering::TargetListHumanTarget.where(:target_list_id => @target_list.id, :human_target_id => tid).first.destroy
      end
      flash[:notice] = view_context.pluralize(target_ids.size, "Human Target") + " deleted"
    end
    render :show
  end

  private 

    # To facilitate specs:
    def import_file_from_params
      if params[:social_engineering_target_list][:import_file].present?
        params[:social_engineering_target_list][:import_file].tempfile
      else
        nil
      end
    end

    def load_target_list
      tid = params[:id] || params[:target_list_id]
      @target_list = SocialEngineering::TargetList.where(:workspace_id => @workspace.id, :id => tid).first
    end

end
