class UsersController < ApplicationController
	skip_before_filter :require_user
	skip_before_filter :require_license
	before_filter :redirect_if_user_exists, :only => [:new, :create]
  before_filter :check_request_verified, :only => [:create]

  # See MSP-12165, prevent initial user creation via CSRF:
  def check_request_verified
    redirect_to root_path unless verified_request?
  end

	def new
		if (request.remote_ip == "127.0.0.1" or request.remote_ip == "::1")
			@user = Mdm::User.new
			@user.time_zone = "UTC" # default to UTC
			render :action => 'new'
		else
			@server_url = "127.0.0.1" + request.port_string
			render "generic/welcome"
		end
	end

	def create
		if (request.remote_ip == "127.0.0.1" or request.remote_ip == "::1")

			@user = Mdm::User.new(params[:user])
			@user.admin = true
			if @user.save
				# flash[:notice] = "User account created"
				@user_session = UserSession.new(:username => @user.username, :password => @user.password)
				if @user_session.save
					redirect_to root_path
				else
					redirect_to login_path
				end
			else
				render :action => 'new'
			end
		else
			redirect_to root_path
		end
	end

	def edit
		@user = current_user
		@current_user = @user
		if not @user
			redirect_to root_path
			return
		end
	end

	def update
		@user = current_user

		if not @user
			redirect_to root_path
			return
		end

		user_attrs = params[:user]

		# Protect these attributes from modification
		user_attrs.delete(:admin)
		user_attrs.delete(:workspace_ids)
		
		# Block a sneaky way to bypass these checks
		user_attrs.delete(:attributes)
		user_attrs.delete('attributes')

		# Require old password to change
		if user_attrs.has_key? :password
			old_password_fail = !(@user.valid_password? user_attrs[:password_old])
			user_attrs.delete :password_old
		end

		if !old_password_fail && @user.update_attributes(user_attrs)
			flash[:notice] = "Updated #{h @user.username}"
			redirect_to root_path
		else
			@user.errors[:current_password] = 'is incorrect.' if old_password_fail
			@current_user = @user
			render :action => 'edit'
		end
	end

	private

	def redirect_if_user_exists
		redirect_to root_path if user_exists?
	end

end

