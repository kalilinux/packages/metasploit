class ApiKeysController < ApplicationController
	before_filter :require_admin
	before_filter :load_api_key, :only => [:edit, :update, :reveal]

	def index
		@api_keys = Mdm::ApiKey.all
	end

	def new
		@api_key = Mdm::ApiKey.new
    @api_key.token = Mdm::ApiKey.generated_token
		render :action => 'new'
	end

	def create
		@api_key = Mdm::ApiKey.new(params[:api_key])
		if @api_key.save
			redirect_to api_keys_path
		else
			render :action => 'new'
		end
	end

	def destroy
		api_keys = if params[:id]
			[ Mdm::ApiKey.find(params[:id]) ]
		elsif params[:api_key_ids]
			Mdm::ApiKey.find(params[:api_key_ids])
		else
			[]
		end

		Mdm::ApiKey.destroy_all(:id => api_keys.map(&:id))
		if api_keys.empty?
			flash[:error] = "No API Keys selected to remove"
		else
			flash[:notice] = view_context.pluralize(api_keys.length, "API Key") + " removed"
		end
		redirect_to api_keys_path
	end

	def edit
		@api_key = Mdm::ApiKey.find(params[:id])
	end

	def update
		@api_key = Mdm::ApiKey.find(params[:id])
		if @api_key.update_attributes(params[:api_key])
			flash[:notice] = "API Key updated"
			redirect_to api_keys_path
		else
			render :action => 'edit'
		end
	end

	def reveal
		render :text => "<strong>#{h(@api_key.token)}</strong>"
	end

	private

	def load_api_key
		@api_key = Mdm::ApiKey.find(params[:id])
	end
end

