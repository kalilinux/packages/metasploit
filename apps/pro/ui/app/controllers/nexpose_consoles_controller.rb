class NexposeConsolesController < ApplicationController
  before_filter :require_admin

  def index
    @consoles = Mdm::NexposeConsole.all
  end

  def new
    @console = Mdm::NexposeConsole.new
    render :action => 'new'
  end

  def create
    @console = Mdm::NexposeConsole.new(params[:nexpose_console])
    @console.owner = current_user.username
    @console.status = "Initializing"
    if @console.save
      # now let's check the connection
      status = @console.check_status
      respond_to do |format|
        format.any(:html, :js) { redirect_to nexpose_consoles_path }
        format.json do
          render json: @console.as_json.merge(status)
        end
      end
    else
      respond_to do |format|
        format.any(:html, :js) { render :action => 'new' }
        format.json do
          render json: { success: false, errors: @console.errors.messages }, status: :error
        end
      end
    end
  end

  def destroy
    consoles = if params[:id]
      [ Mdm::NexposeConsole.find(params[:id]) ]
    elsif params[:nexpose_console_ids]
      Mdm::NexposeConsole.find(params[:nexpose_console_ids])
    else
      []
    end

    Mdm::NexposeConsole.destroy_all(:id => consoles.map(&:id))
    respond_to do |format|
      format.any(:js, :html) do
        if consoles.empty?
          flash[:error] = "No Nexpose Consoles selected to remove"
        else
          flash[:notice] = view_context.pluralize(consoles.length, "Nexpose Console") + " removed"
        end
        redirect_to nexpose_consoles_path
      end
      format.json { render json: { success: true } }
    end
  end

  def edit
    @console = Mdm::NexposeConsole.find(params[:id])
  end

  def update
    @console = Mdm::NexposeConsole.find(params[:id])
    if @console.update_attributes(params[:nexpose_console])
      flash[:notice] = "Nexpose Console"
      redirect_to nexpose_consoles_path
    else
      render :action => 'edit'
    end
  end
end

