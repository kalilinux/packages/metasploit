class UserSessionsController < ApplicationController

  skip_before_filter :require_user
  skip_before_filter :require_license

  layout "login"

  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])

    remote_ip = request.remote_ip
    user_agent = request.env['HTTP_USER_AGENT'].to_s

    if @user_session.save
      user = @user_session.record
      user.last_login_address = remote_ip
      user.session_key = session[:session_id]
      user.save!

      login_event(user.username, true, remote_ip, user_agent)

      redirect_back_or_default root_path
    else
      login_event(@user_session.username, false, remote_ip, user_agent)

      if @user_session.being_brute_force_protected?
        flash[:error] = 'Your account has been locked due to successive invalid login attempts. Please try again later.'
      else
        flash[:error] = 'Invalid username or password.'
      end

      redirect_to login_path
    end
  end

  def destroy
    @user_session = UserSession.find

    logout_event(@user_session.record.username, request.remote_ip, request.env['HTTP_USER_AGENT'].to_s)

    @user_session.destroy
    flash[:notice] = "You have been logged out."
    redirect_to root_path
  end

  def handle_unverified_request
    super
    redirect_to login_path and return
  end

  private

  def login_event(username, success, remote_ip, user_agent)
    ev = Mdm::Event.new(:name => "user_login",
                        :username => username,
                        :info => {:success => success,
                                  :address => remote_ip,
                                  :user_agent => user_agent})
    ev.save!
  end

  def logout_event(username, remote_ip, user_agent)
    ev = Mdm::Event.new(:name => "user_logout",
                        :username => username,
                        :info => {:address => remote_ip,
                                  :user_agent => user_agent})
    ev.save!
  end

end
