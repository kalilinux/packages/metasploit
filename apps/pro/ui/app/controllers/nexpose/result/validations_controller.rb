class ::Nexpose::Result::ValidationsController < ApplicationController
  before_filter :load_workspace

  def create
    export_run = create_export_run
    validations = create_validation_for_export_run(export_run, params[:vuln_id])
    tid = launch_with_export_run(export_run)
    render json: { :success => :ok, :task_id => tid, redirect_url: task_detail_path(@workspace.id, tid), id: validations[0].id }
  end

  def update
    export_run = create_export_run
    validations = create_validation_for_export_run(export_run, params[:vuln_id])
    tid = launch_with_export_run(export_run)
    render json: { :success => :ok, :task_id => tid, redirect_url: task_detail_path(@workspace.id, tid), id: validations[0].id }
  end

  def show
    respond_to do |format|
      format.json{
        validation = Nexpose::Result::Validation.select([
                                                        Nexpose::Result::Validation[:sent_to_nexpose],
                                                        Nexpose::Result::Validation[:nexpose_response]
                                                      ]).joins(
          Nexpose::Result::Validation.join_association(:export_run)
        ).where(
          Nexpose::Result::Validation[:id].eq(params[:id]),
          Nexpose::Result::ExportRun[:workspace_id].eq(@workspace.id)
        )

        render json: validation
      }
    end
  end

  private

  #TODO: Refactor with code in exceptions controller

  # Calls the Push Exceptions module across the RPC bridge, passing the export_run ID
  # @return [Integer] the id of the spawned Mdm::Task
  def launch_with_export_run(export_run)
    # make the RPC call and redirect to task page
    tid = Nexpose::ExceptionValidationPush::TaskConfig.new(
      :workspace => @workspace,
      :export_run_id => export_run.id
    ).rpc_call["task_id"]
  end

  # @return [Nexpose::Result::ExportRun] to pass over the RPC bridge
  def create_export_run
    old_import = Nexpose::Data::ImportRun.find_by_workspace_id(@workspace.id)
    export_run = Nexpose::Result::ExportRun.create(
      :console => old_import.console,
      :workspace => @workspace,
      :user => current_user
    )
  end


  # Coerces the MatchResults[state=succeeded] from a workspace in to Nexpose::Result::Validations
  # @return [Array<Nexpose::Result::Validation>] all validated matches in
  #   the workspace
  def create_validation_for_export_run(export_run,vuln_id)
    validations=[]

    MetasploitDataModels::AutomaticExploitation::MatchResult.for_vuln_id(vuln_id).succeeded.find_each do |match_result|
      validation = Nexpose::Result::Validation.where(automatic_exploitation_match_result_id: match_result.id).first

      unless validation.nil?
        validation.update_attributes(
          export_run: export_run,
          verified_at: match_result.created_at
        )
      else
        validation = Nexpose::Result::Validation.create(
          user: current_user,
          nx_asset: match_result.match.matchable.host.nexpose_asset,
          export_run: export_run,
          match_result: match_result,
          verified_at: match_result.created_at
        )

      end
      validations << validation
    end

    validations
  end

end