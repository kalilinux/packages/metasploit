class ::Nexpose::Result::ExceptionsController < ApplicationController
  before_filter :load_workspace

  def push_validations
    export_run = create_export_run
    create_validations_for_export_run(export_run)
    tid = launch_with_export_run(export_run)
    render json: { :success => :ok, :task_id => tid, redirect_url: task_detail_path(@workspace.id, tid) }
  end

  def show
    respond_to do |format|
      format.json{
        exception = Nexpose::Result::Exception.select([
                                                        Nexpose::Result::Exception[:sent_to_nexpose],
                                                        Nexpose::Result::Exception[:nexpose_response]
                                                      ]).joins(
          Nexpose::Result::Exception.join_association(:export_run)
        ).where(
          Nexpose::Result::Exception[:id].eq(params[:id]),
          Nexpose::Result::ExportRun[:workspace_id].eq(@workspace.id)
        )

        render json: exception
      }
    end
  end

  def update
    respond_to do |format|
      format.json {
        unless params[:vuln_id].blank?
          create_single_exception
        else
          create_bulk_exceptions
        end
      }
    end
  end

  def create
    respond_to do |format|
      format.json {
        unless params[:vuln_id].blank?
          create_single_exception
        else
          create_bulk_exceptions
        end
      }
    end
  end

  private

  # Push a single exception
  def create_single_exception
    export_run = create_export_run
    exceptions = create_exception_for_export_run(export_run,params[:vuln_id])
    tid = launch_with_export_run(export_run)
    render json: { :task_id => tid, redirect_url: task_detail_path(@workspace.id, tid), id: exceptions[0].id}
  end

  # Creates exceptions in bulk from the push exceptions page
  # TODO: Refactor method
  def create_bulk_exceptions
    errors = []
    @result_exceptions = []
    error_count = 0

    params[:nexpose_result_exceptions].values.each do |push_exception_group|
      group_errors = []
      push_exception_group.values.each do |push_exception|
        e = ::Nexpose::Result::Exception.find_by_id(push_exception[:id])
        exception = push_exception.select{|k,v| k !='checkbox'}
        begin
          exception['expiration_date'] = Date.strptime(exception['expiration_date'],"%m/%d/%Y").to_time
        rescue ArgumentError
          exception['expiration_date'] = nil
          @invalid_date = true
        end

        match = MetasploitDataModels::AutomaticExploitation::Match.joins(:match_results).where(automatic_exploitation_match_results: {id:push_exception[:automatic_exploitation_match_result_id]}).first
        data_asset = ::Nexpose::Data::Asset.joins(mdm_hosts: {vulns: :matches}).where(automatic_exploitation_matches: {id: match.id}).first

        if e
          e.assign_attributes(exception.merge(user: current_user, nx_scope: data_asset))
        else
          e = ::Nexpose::Result::Exception.new(exception.merge(
                                                 user: current_user,
                                                 nx_scope: data_asset
                                               ))
        end

        if @invalid_date
          e.add_invalid_date_error
        end

        unless e.valid?
          if push_exception.has_key?('checkbox')
            error_count = error_count+1
          end
        else
          if push_exception.has_key?('checkbox')
            @result_exceptions << e
          end
        end
        if push_exception.has_key?('checkbox')
          group_errors << e.errors
        else
          group_errors << {}
        end
      end
      errors << group_errors
    end

    unless error_count > 0
      export_run = create_export_run
      @result_exceptions.each do |e|
        e.export_run = export_run
        e.save
      end
      tid = launch_with_export_run(export_run)
      render json: { :success => :ok, :task_id => tid, redirect_url: task_detail_path(@workspace.id, tid) }
    else
      render json: { success:false, errors: errors}, status: :error
    end
  end

  # @return [Nexpose::Result::ExportRun] to pass over the RPC bridge
  def create_export_run
    old_import = Nexpose::Data::ImportRun.find_by_workspace_id(@workspace.id)
    Nexpose::Result::ExportRun.create(
      :console => old_import.console,
      :workspace => @workspace,
      :user => current_user
    )
  end

  # Coerces the MatchResults[state=succeeded] from a workspace in to Nexpose::Result::Validations
  # @return [Array<Nexpose::Result::Validation>] all validated matches in
  #   the workspace
  def create_validations_for_export_run(export_run)
    MetasploitDataModels::AutomaticExploitation::MatchResult.by_workspace(@workspace.id).succeeded.find_each do |match_result|
      Nexpose::Result::Validation.create(
        :user => current_user,
        :nx_asset => match_result.match.matchable.host.nexpose_asset,
        :export_run => export_run,
        :match_result => match_result,
        :verified_at => match_result.created_at
      )
    end
  end

  # Coerces the MatchResults[state=failed] from a workspace in to Nexpose::Result::Exception
  # @return [Array<Nexpose::Result::Exception>] all exceptions matches in
  #   the workspace
  def create_exception_for_export_run(export_run,vuln_id)
    exceptions=[]
    MetasploitDataModels::AutomaticExploitation::MatchResult.for_vuln_id(vuln_id).failed.find_each do |match_result|
      exception = Nexpose::Result::Exception.where(automatic_exploitation_match_result_id: match_result.id).first

      unless exception.nil?
        exception.update_attributes(
          reason: params[:reason],
          expiration_date: params[:expiration_date].empty? ? nil : Date.strptime(params[:expiration_date],"%m/%d/%Y").to_time,
          approve: params[:approve],
          export_run: export_run,
          comments: params[:comments]
        )
      else
        exception = Nexpose::Result::Exception.new(
          user: current_user,
          nx_scope: match_result.match.matchable.host.nexpose_asset,
          export_run: export_run,
          reason: params[:reason],
          expiration_date: params[:expiration_date].empty? ? nil : Date.strptime(params[:expiration_date],"%m/%d/%Y").to_time,
          approve: params[:approve],
          comments: params[:comments]
        )
        exception.match_result = match_result
        exception.save
      end
      exceptions << exception
    end
    exceptions
  end


  # Calls the Push Exceptions module across the RPC bridge, passing the export_run ID
  # @return [Integer] the id of the spawned Mdm::Task
  def launch_with_export_run(export_run)
    # make the RPC call and redirect to task page
    Nexpose::ExceptionValidationPush::TaskConfig.new(
      :workspace => @workspace,
      :export_run_id => export_run.id
    ).rpc_call["task_id"]
  end
end