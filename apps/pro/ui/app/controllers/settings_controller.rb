class SettingsController < ApplicationController

  before_filter :require_admin

  def index
    @macros    = Mdm::Macro.all
    @api_keys  = Mdm::ApiKey.all
    @listeners = Mdm::Listener.all
    @consoles  = Mdm::NexposeConsole.all
    @profile   = current_profile
  end

  def update_profile
    pid = params[:profile_id]
    params[:smtp_ssl] = true if params[:smtp_ssl] == "true"

    @profile = Mdm::Profile.find(pid)

    if @profile.blank?
      flash[:error] = "Invalid Mdm::Profile ID"
      redirect_to :action => 'index'
    end

    @profile.settings_list.each do |s|
      key = s[:name].intern
      val = params[key]

      next if val.nil?

      if s[:type] == :boolean
        val = (val.to_s =~ /(true|1|yes)/) ? true : false
      end
      @profile.settings[s[:name]] = val
    end

    update_smtp_settings if smtp_settings_present?
    update_proxy_settings
    @profile.save!

    Mdm::Profile.record_timestamps = false
    @profile.update_attributes :updated_at => Time.now.utc
    Mdm::Profile.record_timestamps = true

    @profile.load_smtp_configuration

    flash[:notice] = "Settings successfully updated."

    redirect_to :action => 'index', :anchor => params[:anchor]
  end

  private

  # Ensure that SMTP information is complete if provided.
  # @return [Boolean]
  def smtp_settings_valid?
    status, error_msg = check_smtp_settings

    if status == :error
      flash[:error] = "SMTP Settings Error: #{error_msg}"
      false
    else
      true
    end
  end

  # Assume the user wants to use an SMTP server if they have anything in the address field
  # @return [Boolean]
  def smtp_settings_present?
    params[:smtp_address].present?
  end


  def update_proxy_settings
    Mdm::Profile::HTTP_PROXY_SETTINGS.each do |setting|
      @profile.settings[setting.to_s] = (@profile.settings["use_http_proxy"] ? params[setting] : nil)
    end
  end

  def update_smtp_settings
    SmtpSettings.new(params).apply_to(@profile.settings)
  end

  # Perform a check of the provided SMTP information to ensure that the server
  # is up and responds to the settings.
  def check_smtp_settings
    use_ssl = params[:smtp_ssl]
    server = params[:smtp_address]
    port = params[:smtp_port].to_i
    username = params[:smtp_username]
    password = params[:smtp_password]
    domain = params[:smtp_domain]
    auth = params[:smtp_authentication]
    if password == SmtpSettings::PASSWORD_UNCHANGED
      password = @profile.settings['smtp_password']
    end
    settings_validator = SmtpSettingsValidator.new(
        :server => server,
        :port => port,
        :ssl => use_ssl,
        :domain => domain,
        :username => username,
        :password => password,
        :auth => auth)
    valid = settings_validator.valid?
    error_msg = settings_validator.errors.join(', ')
    status = valid ? :ok : :error
    [status, error_msg]
  end

end

