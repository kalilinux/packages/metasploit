class BruteForce::Guess::GuessController < ApplicationController
  before_filter :application_backbone, only: [:index,:create]
  before_filter :load_workspace

  def index
    @licensed = License.get.supports_bruteforce?
    respond_to do |format|
      format.html{
        gon.workspace_cred_count = Metasploit::Credential::Core.where(workspace_id: @workspace).count
      }
    end
  end

  # If we are coming from the analysis-host/service screen.
  # Unobstrusive JS helper hits this action :-/
  def create
    @licensed = License.get.supports_bruteforce?
    respond_to do |format|
      format.html{
        gon.workspace_cred_count = Metasploit::Credential::Core.where(workspace_id: @workspace).count
        gon.host_ips = Mdm::Host.select(:address).where(id:params[:host_ids],workspace_id: @workspace)

        render :index
      }
    end
  end


end