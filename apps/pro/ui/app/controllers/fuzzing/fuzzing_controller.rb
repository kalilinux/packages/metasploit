class Fuzzing::FuzzingController < ApplicationController

  before_filter :load_workspace, :only => [ :index ]
  before_filter :my_load_workspace, :except => [ :index ]
  #before_filter :check_license
  before_filter :check_fuzzing_support
  before_filter :redirect_if_not_licensed, :except => [ :index ]


	def index
		
    return redirect_to workspace_url(@workspace, :anchor=>'') unless License.get.supports_fuzzing_frame?

		respond_to do |format|
			format.html
			format.json do
				render :json => {}
			end
		end
	end

  private

  def check_fuzzing_support
    redirect_to workspace_url(@workspace, :anchor=>'') unless License.get.supports_fuzzing_frame?
  end

end