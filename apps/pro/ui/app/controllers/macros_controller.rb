class MacrosController < ApplicationController
	before_filter :require_admin
	before_filter :find_macro, :only => [:module_options, :add_action, :delete_action, :update, :edit]

	def index
		@macros = Mdm::Macro.all
	end

	def new
		@macro = Mdm::Macro.new
		render :action => 'new'
	end

	def create
		@macro = Mdm::Macro.new(params[:macro])
		@macro.owner = current_user.username
		if @macro.save
			redirect_to edit_macro_path(@macro)
		else
			render :action => 'new'
		end
	end

	def destroy
		macros = if params[:id]
			[ Mdm::Macro.find(params[:id]) ]
		elsif params[:macro_ids]
			Mdm::Macro.find(params[:macro_ids])
		else
			[]
		end

		Mdm::Macro.destroy_all(:id => macros.map(&:id))
		if macros.empty?
			flash[:error] = "No Macros selected to remove"
		else
			flash[:notice] = view_context.pluralize(macros.length, "Macro") + " removed"
		end
		redirect_to macros_path
	end

	def edit
		build_module_list
	end

	def update
		if @macro.update_attributes(params[:macro])
			flash[:notice] = "Macro #{@macro.name} updated"
			redirect_to edit_macro_path(@macro)
		else
			render :action => 'edit'
		end
	end

	def module_options
		m = MsfModule.find_by_fullname(params[:module])
		render :partial => 'module_fields', :locals => { :msf_module => m, :macro_id => nil, :action_index => nil }
	end

	def delete_action
		params[:action_ids].each do |action_id|
			@macro.actions[action_id.to_i] = nil
		end
		@macro.actions.delete(nil)
		@macro.save

		flash[:notice] = view_context.pluralize(params[:action_ids].length, "Action") + " removed"
		redirect_to edit_macro_path(@macro)
	end

	def add_action
		mname = params[:module]
		mopts = params[:options] || {}
		@macro.add_action(mname, mopts)
		redirect_to edit_macro_path(@macro)
	end

protected

	def build_module_list
		@module_list = []
		MsfModule.post.each do |m|
			@module_list << m
		end

		@module_list = @module_list.sort_by { |m| m.title }
	end

	def find_macro
		@macro = Mdm::Macro.find(params[:id])
	end

end

