class Admin::UsersController < ApplicationController
	before_filter :require_admin

	def index
		@users = Mdm::User.order('username')
	end

	def new
		@user = Mdm::User.new(:workspaces => Mdm::Workspace.all)
		@user.time_zone = current_user.time_zone
	end

	def create
		params[:user][:workspace_ids] ||= []
		@user = Mdm::User.new(params[:user])
		if @user.save
			redirect_to admin_users_path
		else
			render :action => 'new'
		end
	end

	def edit
		@current_user = current_user
		@user = Mdm::User.find(params[:id])
	end

	# TODO: A lot of this code is duplicated in the users_controller. I don't
	# see a valid reason for keeping these as separate controllers.
	def update
		@user = Mdm::User.find(params[:id])
		@current_user = current_user
		params[:user][:workspace_ids] ||= []

		user_attrs = params[:user]

		# password is only required when an admin is modifying his own password
		if @current_user == @user && user_attrs.has_key?(:password)
			old_password_fail = !(@user.valid_password? user_attrs[:password_old])
			user_attrs.delete :password_old
		end

		if !old_password_fail && @user.update_attributes(user_attrs)
			flash[:notice] = "User '#{@user.username}' updated"
			redirect_to admin_users_path
		elsif old_password_fail
			@user.errors.add :original_password, 'is incorrect.'
			render :action => 'edit'
		else
			render :action => 'edit'
		end
	end

	def destroy
		users = if params[:id]
			[ Mdm::User.find(params[:id]) ]
		elsif params[:user_ids]
			Mdm::User.find(params[:user_ids])
		else
			[]
		end
		attempted_current_user_deletion = users.delete(current_user) # don't allow current_user to be deleted
		Mdm::User.destroy_all(:id => users.map(&:id))
		if attempted_current_user_deletion and users.empty?
			flash[:error] = "You cannot delete the currently logged-in user"
		elsif users.empty?
			flash[:error] = "No users selected to delete"
		else
			flash[:notice] = view_context.pluralize(users.length, "user") + " deleted"
		end

		redirect_to admin_users_path
	end

end
