class ListenersController < ApplicationController
	before_filter :require_admin
    before_filter :find_listener, :only => [:module_options, :start, :stop, :update, :edit]

	def index
		@listeners = Mdm::Listener.all
	end

	def new
		@listener = Mdm::Listener.new
		@listener.address = "0.0.0.0"
		@listener.port    = rand(50000) + 10000
		build_workspace_list
		build_macro_list
		render :action => 'new'
	end

	def create
		@workspace  = Mdm::Workspace.find( params[:listener].delete(:workspace) )
		@listener = Mdm::Listener.new(params[:listener])
		@listener.owner   = current_user.username
		@listener.workspace = @workspace

		if @listener.save
			@listener.start
			redirect_to listeners_path
		else
			build_workspace_list
			build_macro_list
			render :action => 'new'
		end
	end

	def stop
		@listener.stop
		redirect_to listeners_path
	end

	def start
		@listener.start
		redirect_to listeners_path
	end

	def destroy
		listeners = if params[:id]
			[ Mdm::Listener.find(params[:id]) ]
		elsif params[:listener_ids]
			Mdm::Listener.find(params[:listener_ids])
		else
			[]
		end

		listeners.each { |list| list.rpc_stop }

		Mdm::Listener.destroy_all(:id => listeners.map(&:id))
		if listeners.empty?
			flash[:error] = "No Listeners selected to remove"
		else
			flash[:notice] = view_context.pluralize(listeners.length, "Listener") + " removed"
		end
		redirect_to listeners_path
	end

	def edit
		@workspace_list     = Mdm::Workspace.all.map{|x| [x.name, x.id]}
		@workspace_default  = @listener.workspace.id
		@macro_list       = [''] + Mdm::Macro.all.map{|x| x.name }
		@macro_default    = @listener.macro
		build_module_list
	end

	def update
		@workspace  = Mdm::Workspace.find( params[:listener].delete(:workspace) )
		@listener.update_attributes(params[:listener])
		@listener.workspace = @workspace
		@listener.save if @listener.changed?
		@listener.restart if @listener.active?
		redirect_to edit_listener_path(@listener)
	end

	def module_options
		m = MsfModule.find_by_fullname(params[:module])
		render :partial => 'module_fields', :locals => { :msf_module => m, :listener_id => nil  }
	end

protected

	def build_module_list
		@module_list = []
		MsfModule.post.each do |m|
			@module_list << m
		end

		@module_list = @module_list.sort_by { |m| m.title }
	end

	def build_workspace_list
		@workspace_list     = Mdm::Workspace.all.map{|x| [x.name, x.id]}
		@workspace_default  = Mdm::Workspace.last.id
	end

	def build_macro_list
		@macro_list       = [''] + Mdm::Macro.all.map{|x| x.name }
		@macro_default    = ''
	end

	def find_listener
		@listener = Mdm::Listener.find(params[:id])
	end

end

