class VulnsController < ApplicationController
  include AnalysisSearchMethods
  before_filter :find_vuln, :only => [
                            :edit, :destroy, :update, :show,
                            :details, :attempts, :exploits,
                            :history, :related_modules, :related_hosts,
                            :update_last_vuln_attempt_status,:restore_last_vuln_attempt_status
                          ]
  before_filter :find_host, :only => [:new, :edit, :create, :show, :details, :attempts, :exploits ]
  before_filter :find_workspace, :only => [:new, :edit, :show, :details, :attempts, :exploits ]
  before_filter :load_workspace, :only => [:index, :combined, :destroy_multiple, :destroy_multiple_groups]
  before_filter :set_pagination, :only => [:index, :combined]
  before_filter :fix_datatables_search_parameters, :only => [:index]
  before_filter :application_backbone, only: [:show]
  before_filter :set_js_routes,        only: [:show]

  helper RefHelper
  include TableResponder
  include FilterResponder


  def index
    load_vulns if request.format.json?

    session[:return_to] = workspace_vulns_path(@workspace)

    @automatic_exploitation_runs = MetasploitDataModels::AutomaticExploitation::Run.where(workspace_id: @workspace.id)

    respond_to do |format|
      format.html
      format.json { render :partial => 'vulns', :vulns => @vulns }
    end
  end

  def combined
    load_combined_vulns if request.format.json?

    respond_to do |format|
      format.html
      format.json { render :partial => 'vulns_combined', :vulns => @vulns }
    end
  end

  def new
    @vuln = @host.vulns.new
    ref_data
    render layout: !request.xhr?
  end

  def edit
    ref_data
    render layout: !request.xhr?
  end

  def show
    respond_to do |format|
      format.html
      format.json
    end
  end

  def details
    ref_data
    exploit_module_match_data
    respond_to do |format|
      format.html { render :partial => "vuln_details_tab" }
      format.js { render :partial => "vuln_details_tab"}
    end
  end

  def detail
    respond_to do |format|
      format.json do

      end
    end
  end

  def related_hosts
    respond_to do |format|
      format.json{
        render json: as_table(related_hosts_relation,{
          presenter_class: Mdm::RelatedHostPresenter,
          search: params[:search]
        })
      }
    end
  end

  def related_hosts_filter_values
    values = filter_values_for_key(related_hosts_relation, params)
    render json: values.as_json
  end

  def related_modules
    respond_to do |format|
      format.json{
        relation = Mdm::Module::Detail.with_table_data.related_modules(@vuln)
        render json: as_table(relation,{presenter_class: Mdm::Module::DetailPresenter, vuln: @vuln})
      }
    end
  end

  def update_last_vuln_attempt_status
    respond_to do |format|
      format.json{
        vuln_attempt = @vuln.vuln_attempts.last
        vuln_attempt.last_fail_reason = vuln_attempt.fail_reason
        vuln_attempt.fail_reason =  Mdm::VulnAttempt::Status::NOT_EXPLOITABLE
        vuln_attempt.save
        render template: 'vulns/show'
      }
    end
  end

  def restore_last_vuln_attempt_status
    respond_to do |format|
      format.json{
        vuln_attempt = Mdm::VulnAttempt.where(vuln_id:@vuln.id).where('vuln_attempts.last_fail_reason IS NOT NULL').first
        vuln_attempt.fail_reason = vuln_attempt.last_fail_reason
        vuln_attempt.last_fail_reason = nil
        vuln_attempt.save
        render template: 'vulns/show'
      }
    end
  end


  def history
    respond_to do |format|
      format.json{
        relation = Mdm::Vuln.with_table_data.event_history(@vuln)
        render json: as_table(relation, presenter_class: Mdm::VulnPresenter)
      }
    end
  end

  def attempts
    ref_data
    exploit_module_match_data
    respond_to do |format|
      format.html { render :partial => "vuln_attempts_tab" }
      format.js { render :partial => "vuln_attempts_tab" }
    end
  end

  def exploits
    ref_data
    exploit_module_match_data
    respond_to do |format|
      format.html { render :partial => "vuln_exploits_tab" }
      format.js { render :partial => "vuln_exploits_tab" }
    end
  end

  def create
    @vuln = @host.vulns.new(params[:vuln])

    respond_to do |format|
      format.html{
        if @vuln.save
          flash[:notice] = "Vulnerability added"
          redirect_to_hosts_show
        else
          ref_data
          find_host
          find_workspace
          render :action => 'new'
        end
      }
      format.json{
        if @vuln.save
          render json: {success: :ok}
        else
          render json: {success: false, error: @vuln.errors}, status: :error
        end
      }
    end
  end

  def update
    respond_to do |format|
      params[:vuln][:existing_ref_attributes] ||= {}
      format.html {
        if @vuln.update_attributes(params[:vuln])
          flash[:notice] = "Vulnerability added"
          redirect_to_hosts_show
        else
          ref_data
          find_host
          find_workspace
          render :action => 'edit'
        end
      }
      format.json {
        @vuln.assign_attributes(params[:vuln])

        if @vuln.save
          render :json => {success: :ok}
        else
          render :json => {success: false, :error => @vuln.errors}, :status => :error
        end
      }
    end
  end

  def destroy
    @vuln.destroy

    respond_to do |format|
      format.html do
        flash[:notice] = "Vulnerability removed"
        redirect_to_hosts_show
      end
      format.js
      format.json{
        render :json => {sucess: :ok}
      }
    end
  end

  def destroy_multiple
    conditions = [
      "hosts.workspace_id = ? and vulns.id in (?)",
      @workspace.id,
      params[:vuln_ids]
    ]
    vulns = Mdm::Vuln.joins(:host).readonly(false).destroy_all(conditions)

    if vulns.size.zero?
      flash[:error] = "No vulnerabilities selected to delete"
    else
      flash[:notice] = view_context.pluralize(vulns.size, "vuln") + " deleted"
    end

    render_js_redirect(workspace_vulns_path(@workspace))
  end

  def destroy_multiple_groups
    vulns = Mdm::Vuln.where('id IN (?)', params[:vuln_ids]).map do |vuln_id|
      origin_vuln = Mdm::Vuln.find(vuln_id)
      group = Mdm::Vuln.where('vulns.name = ?', origin_vuln.name)
                       .joins(:host).where('hosts.workspace_id = ?', @workspace.id)
      group.readonly(false).destroy_all
    end

    if vulns.flatten.length.zero?
      flash[:error] = "No vulnerabilities selected to delete"
    else
      flash[:notice] = view_context.pluralize(vulns.flatten.length, "vuln") + " deleted"
    end

    render_js_redirect(combined_workspace_vulns_path(@workspace))
  end

  # TODO: For the moment, we're only searching by related hosts in the single
  # vuln view, so we don't need to mess with the base search_operators method.
  # In the future, if we want to search across vulns, we'll need to fix this,
  # because att that point search_operator_class should return Mdm::Vuln.
  def search_operator_class
    Mdm::Host
  end

  private

  def find_vuln
    @vuln = Mdm::Vuln.find(params[:id])
  end

  def find_host
    if params[:host_id]
      @host = Mdm::Host.find(params[:host_id])
    else
      @host = @vuln.host
    end
  end

  def find_workspace
    @workspace = @host.workspace
  end

  # Determine the correct pagination arguments based on the incoming DT parameters.
  #
  # Returns nothing.
  def set_pagination
    if params[:iDisplayLength] == '-1'
      @vulns_per_page = nil
    else
      @vulns_per_page = (params[:iDisplayLength] || 20).to_i
    end
    @vulns_offset = (params[:iDisplayStart] || 0).to_i
  end

  # Load the Vulns with correct sorting and pagination.
  #
  # @return [void]
  def load_vulns
    vuln_set = pro_search(:vulns, false).includes(:vuln_attempts)
    paged_vuln_set = vuln_set.limit(@vulns_per_page).offset(@vulns_offset)
    @vulns = sort_vulns(paged_vuln_set)

    @vulns_total_display_count = @vulns_total_count
  end

  # Load the Vulns with correct sorting and pagination.
  #
  # Returns nothing.
  def load_combined_vulns
    vuln_set = pro_search(:vulns, true)
    @vulns = vuln_set
      .limit(@vulns_per_page)
      .offset(@vulns_offset)
      .order(vulns_combined_sort_option)
    @vulns_total_display_count = vuln_set.length
  end

  # Generate a SQL sort by option based on the incoming DataTables parameter.
  #
  # @param relation [ActiveRecord::Relation]
  # @return [ActiveRecord::Relation]
  def sort_vulns(relation)
    order_predication_class = Arel::Nodes::Descending

    if params[:sSortDir_0] =~ /^A/i
      order_predication_class = Arel::Nodes::Ascending
    end

    case params[:iSortCol_0].to_i
      when 1
        # okay, Mdm::Host has some gotchas to sorting:
        # 1. Generally, sort Hosts by #name (type:string)
        # 2. If Host#name is blank, default to sorting by #address (type:inet)
        relation = relation.order(
          order_predication_class.new(
            Arel::SqlLiteral.new(
              'CASE hosts.name WHEN HOST(hosts.address::inet)::text THEN '+
              'NULL ELSE LOWER(hosts.name) END'
            )
          )
        )

        # 3. If Host#name is EQUAL to Host#address, default to sorting by #address (type:inet)
        #       (otherwise the address will be alphabetically sorted, WRONG)
        relation = relation.order(
          order_predication_class.new(
            Mdm::Host.arel_table[:address]
          )
        )
      when 2
        relation = relation.order(
          order_predication_class.new(
            Mdm::Service.arel_table[:port]
          )
        )
      when 3
        relation = relation.order(
          order_predication_class.new(
            Mdm::Vuln.arel_table[:name]
          )
        )
      else
        vulns = Mdm::Vuln.arel_table
        relation = relation.order(
          order_predication_class.new(
            vulns[:exploited_at]
          ),
          order_predication_class.new(
            vulns[:vuln_attempt_count]
          ).reverse
        )
    end

    relation
  end

  # Generate a SQL sort by option based on the incoming DataTables parameter.
  #
  # Returns the SQL String
  def vulns_combined_sort_option
    column = case params[:iSortCol_0]
               when '1'
                 'name'
               when '3'
                 'host_count'
             end
    column + ' ' + (params[:sSortDir_0] =~ /^A/i ? 'asc' : 'desc') if column
  end


  def redirect_to_hosts_show
    redirect_to host_path(@vuln.host) + "#vulnerabilities"
  end

  def ref_data
    @vuln.refs.build
  end


  # Generate an include option for the tag table when loading records
  # for the Analysis tab.
  #
  # Returns the Array condition.
  def tags_include_option
    [:refs, {:host => :tags}]
  end

  def exploit_module_match_data
    module_details = Mdm::Module::Detail.arel_table

    @exploits = @vuln.module_details.where(
      :mtype => [
        'auxiliary',
        'exploit'
      ]
    ).order(
      module_details[:mtype].desc,
      module_details[:rank].desc
    )
  end

  def related_hosts_relation
    find_vuln
    #TODO: Optimize into one query?
    ref_ids = Mdm::VulnRef.select(:ref_id).where(vuln_id: @vuln.id).pluck(:ref_id)

    relation = Mdm::Host.with_table_data.related_hosts(ref_ids,@vuln.host)
  end

  def set_js_routes
    gon.related_hosts_filter_values_workspace_vuln_path =
        related_hosts_filter_values_workspace_vuln_path
    gon.search_operators_workspace_vuln_path =
        search_operators_workspace_vuln_path
  end
end
