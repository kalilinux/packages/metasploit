require 'securerandom'

# Store the token file in the "tmp" directory
token_file = ::File.expand_path(::File.join(::File.dirname(__FILE__), "..", "..", "tmp", "secret_key_base.txt"))

# Create the file if it not does not exist
if not File.exist?(token_file)

	# Preserve our old umask
	omask = ::File.umask

	# Set the umask to prevent other users from reading this
	::File.umask(077)

	fd = nil
	0.upto(90) do
		fd = ::File.open(token_file, "wb") rescue nil
		break if fd
		sleep(1)
	end

	if fd
		# This is 1.9.2+ stdlib
		fd.write( ::SecureRandom.hex(128) )
		fd.close
		# If we are running in a production environment on linux, set ownership so Rails
		# can open the file
		if !Rails.application.platform.win32? && Rails.application.root.parent.parent.parent.join('apps').directory?
			begin
				FileUtils.chown('daemon', 'root', token_file)
			rescue Errno::EPERM, Errno::EACCES
				# ignore permission denied as sometimes the Rails process (running as non-root) beats the ProSvc process
			end
		end
	else
		# Indicate that we fail
		token_file = nil
	end

	# Restore the umask
	::File.umask(omask)
end

if token_file
	# Read the token out of the file and configure rails with it
	fd = ::File.open(token_file, "rb") rescue nil
	if fd
		Rails.application.config.secret_key_base = fd.read(fd.stat.size)
		fd.close
	else
		token_file = nil
	end
end

if not token_file
	# Better than failing (will have to relogin after restart)
	Rails.application.config.secret_key_base = ::SecureRandom.hex(128)
end
