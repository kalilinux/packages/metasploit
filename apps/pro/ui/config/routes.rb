begin
  # Try Pro::Application first as it won't autoload, so if it doesn't raise a NameError, then this must be called
  # from pro/ui
  engine = Pro::Application
rescue NameError
  # Used in metasploit-pro-engine and metamodules specs
  engine = Metasploit::Pro::UI::Engine
end

#change
engine.routes.draw do
  get "/campaign_files/:id" => "social_engineering/files#download"

  # The RESTful API
  namespace :rest_api do
    namespace :v1 do
      get "/base/" => "base#index"

      namespace :social_engineering do
        resources :target_lists
        resources :human_targets

        resources :campaigns do
          resources :web_pages
          resources :emails
          resources :email_openings
          resources :phishing_results
          resources :visits
        end
      end
    end
    namespace :v2 do
      get "/base/" => "base#index"

      namespace :social_engineering do
        resources :target_lists
        resources :human_targets
      end

      resources :workspaces, only: [:index, :show] do
        resources :campaigns, only: [:index, :show], controller: 'social_engineering/campaigns' do
          resources :web_pages, only: [:index, :show], controller: 'social_engineering/web_pages'
          resources :emails, only: [:index, :show], controller: 'social_engineering/emails'
          resources :email_openings, only: [:index, :show], controller: 'social_engineering/email_openings'
          resources :phishing_results, only: [:index, :show], controller: 'social_engineering/phishing_results'
          resources :visits, only: [:index, :show], controller: 'social_engineering/campaigns/visits'
        end
        resources :hosts, only: [:index, :show] do
          resources :sessions, only: [:index, :show]
          resources :notes, only: [:index, :show]
          resources :services, only: [:index, :show] do
            resources :vulns, only: [:index, :show]
            resources :web_sites, only: [:index, :show] do
              resources :web_forms, only: [:index, :show]
              resources :web_pages, only: [:index, :show]
              resources :web_vulns, only: [:index, :show]
            end
          end
        end
      end
    end
  end

  # Wizards
  namespace :wizards do
    namespace :quick_pentest do
      resources :form
    end
    namespace :quick_web_app_scan do
      resources :form
    end
    namespace :vuln_validation do
      resources :form do
        collection do
          get  :nexpose_sites
          get  :import_run
          post :continue_exploitation
        end
      end
    end
    namespace :payload_generator do
      resources :form do
        collection do
          get :poll
          get :download
          get :upsell
        end
      end
    end
  end

  # Notifications
  namespace :notifications do
    resources :messages do
      collection do
        post 'mark_read' => 'messages#mark_read', :as => 'mark_read'
        get 'poll' => 'messages#poll', :as => 'poll'
      end
    end
  end

  # Setup & Licenses
  scope "/setup" do
    get ""           => "licenses#setup", :as => "setup"
    get "request"    => "licenses#request_key", :as => "request_key"
    get "activation" => "licenses#activation", :as => "activation"
  end

  scope "/licenses" do
    get ""        => "licenses#index",    :as => "licenses"
    post "register" => "licenses#register", :as => "register"
    post "activate" => "licenses#activate", :as => "activate"
    match "offline_activation" => "licenses#offline_activation", :as => "offline_activation", :via => [:get, :post]
  end

  # Updates
  scope "/updates" do
    get ""             => "updates#updates", :as => "updates"
    post "check"         => "updates#check"
    post "apply"         => "updates#apply"
    post "restart"       => "updates#restart"
    post "status"        => "updates#status"
    get  "offline_apply" => "updates#offline_apply"
    post "offline_apply_post" => "updates#offline_apply_post", :as => 'updates_offline_apply_post'
  end

  # NeXpose Consoles
  resources :nexpose_consoles do
    collection do
      delete "/destroy" => "nexpose_consoles#destroy", :as => "destroy"
    end
  end

  # API Keys
  resources :api_keys do
    collection do
      delete "/destroy" => "api_keys#destroy", :as => "destroy"
    end

    member do
      post "/reveal" => "api_keys#reveal"
    end
  end



  # Macros
  resources :macros
  scope "/macro" do
    delete ":id/delete_action/:action_id" => "macros#delete_action", :as => "macro_delete_action"
    delete ":id/delete_action" => "macros#delete_action", :as => "macro_delete_actions"
    post ":id/add_action" => "macros#add_action", :as => "macro_add_action"
    match "module_options" => "macros#module_options", :as => "module_options", via: [:get, :post]
  end
  delete "macros" => "macros#destroy", :as => "delete_macros"


  # Listeners
  resources :listeners
  scope "/listener" do
    post ":id/stop" => "listeners#stop", :as => "listener_stop"
    post ":id/start" => "listeners#start", :as => "listener_start"
    get "module_options" => "listeners#module_options", :as => "macro_module_options"
  end
  delete "listeners" => "listeners#destroy", :as => "delete_listeners"


  # Global Settings
  scope "/settings" do
    get "" => "settings#index", :as => "settings"
    patch "update_profile" => "settings#update_profile", :as => "update_profile"
    put "update_profile" => "settings#update_profile"
  end


  # Mdm::User settings
  resources :users

  get "login" => "user_sessions#new", :as => "login"
  get "logout" => "user_sessions#destroy", :as => "logout"

  resources :user_sessions

  # Workspaces
  get "/workspaces/search" => "workspaces#search"
  delete "/workspaces/destroy" => "workspaces#destroy"

  # Wizard related reporting routes
  get '/wizard_report_tab' => 'reports#form_for_tabbed_modal', :as => 'wizard_report_tab_form'

  resources :workspaces do
    get "sessions" => "sessions#index"
    get "events" => "events#index"
    get "web" => "web#index"

    resources :imports, only: [:new]


    # Reports and Exports
    # Standard reports
    resources :reports do
      collection do
        post  "validate_report" => "reports#validate_report", :as=> "validate_report"
        # Wrapper to allow all the other report partials
        # to be reloaded once type is selected:
        get :form_for_type
        delete :destroy_multiple
      end
      member do
        get :clone
        post :generate_format
        post :format_generation_status
      end

      # Generated report files
      resources :report_artifacts, only: [:destroy, :index] do
        member do
          get :download
          get :view
        end

        collection do
          post :email
        end
      end
    end

    # Outside of reports to avoid dealing with report IDs unnecessarily:
    get 'report_artifact_download/(:id)',       to: 'report_artifacts#download'
    get 'report_artifact_view/:id',             to: 'report_artifacts#view'

    # Custom reports
    resources :custom_reports, { :controller => :reports }

    # Resources for custom reports: logos, templates
    resources :report_custom_resources

    # Example simple template
    get 'report_template_simple_download',      to: 'reports#download_simple_sample'

    resources :exports do
      member do
        get 'download' => 'exports#download'
      end
      collection do
        delete :destroy_multiple
      end
    end

    resources :task_chains, except: [:show] do
      collection do
        post "validate_schedule" => 'task_chains#validate_schedule', :as => 'validate_schedule'
        post "validate" => 'task_chains#validate', :as=>'validate'
        delete :destroy_multiple
        post   :stop_multiple
        post   :suspend_multiple
        post   :resume_multiple
        post   :start_multiple
      end

      member do
        get :clone
      end
    end

    namespace :fuzzing do
      resources :fuzzing do
        resources :request_groups
      end
    end

    namespace :nexpose do
      namespace :result do
        # fixme: CHANGE TO POST TO PREVENT CSRF.
        get 'push_validations' => 'exceptions#push_validations'
        resources :exceptions, only: [:create, :show, :update]
        resources :validations, only: [:create, :show, :update]
      end
      namespace :data do
        resources :sites, only: [:index] do
          collection {
            get :search_operators
            get :filter_values
          }
        end
        resources :import_runs, only:[:create,:show]
      end
    end


    # NEW STYLE!
    # Social Engineering Campaigns
    namespace :social_engineering do
      resources :human_targets

      resources :web_templates, except: [:delete] do
        collection do
          post :clone_proxy
          delete "" => "web_templates#destroy", :as => "delete"
        end
      end

      resources :email_templates, except: [:delete]  do
        collection do
          delete "" => "email_templates#destroy", :as => "delete"
        end
      end

      resources :target_lists, except: [:delete]  do
        get "export" => "target_lists#export"
        post "remove_targets" => "target_lists#remove_targets"
        collection do
          delete "" => "target_lists#destroy", :as => "delete"
        end
      end

      resources :files, except: [:delete]  do
        collection do
          delete "" => "files#destroy", :as => "delete"
        end
      end

      # ------ Campaigns, components, and tracking data -----
      resources :campaigns do

        resource :web_server_config, {
            :only => [:edit, :update],
            :controller => :web_server_config
        }
        resource :email_server_config, {
            :only => [:edit, :update],
            :controller => :email_server_config
        } do
          collection do
            match :check_smtp, via: [:put, :patch]
          end
        end

        resources :visits

        resources :emails do
          get :preview
          get :preview_pane
          collection do
            post :custom_content_preview
          end
        end

        resources :web_pages do
          get :preview
          get :preview_pane
          collection do
            post :clone_proxy
            post :custom_content_preview
          end
        end

        resources :portable_files do
          member do
            get :download
          end
        end

        member do
          post :execute
          post :reset
          get  :to_task
          get  :sent_emails
          get  :opened_emails
          get  :opened_sessions
          get  :submitted_forms
          get  :links_clicked
          get  :check_for_configuration_errors
        end

        collection do
          delete "" => "campaigns#destroy", :as => "delete"
          get :logged_in
        end
        resources :phishing_results, :only => [:index, :show]
      end
    end
    # ------ RunStat -----
    resources :run_stats, :only => [:show]


    # ------ WebScan -----
    resources :web_scans, :only => [:show]


    delete "/tags/destroy" => "tags#destroy"
    resources :tags do
      collection do
        delete :destroy, :as => "destroy"
        get :search
      end
    end

    resources :loots, :only => [:index, :destroy] do
      collection do
        delete :destroy_multiple
      end
    end

    #Creds Management
    resources :creds, :only => [:index] do
    end


    # Notes, Services
    [:notes, :services].each do |res|
      resources res, :only => [:index] do
        collection do
          get :combined
          delete :destroy_multiple
        end
      end
    end

    # Note Creation
    resources :notes, only: [:create]


    # Module Detail
    resources :module_details, only:[:show]


    # Vulnerabilities
    resources :vulns, :only => [:index, :show] do
      collection do
        get :combined
        delete :destroy_multiple
        delete :destroy_multiple_groups
      end

      member do
        get  :history
        get  :related_modules
        get  :related_hosts
        get  :related_hosts_filter_values
        get  :search_operators
        put  :update_last_vuln_attempt_status
        put  :restore_last_vuln_attempt_status
      end

    end

    get "vulns/:id/details"  => "vulns#details", :as => "vuln_details"
    get "vulns/:id/attempts" => "vulns#attempts", :as => "vuln_attempts"
    get "vulns/:id/exploits" => "vulns#exploits", :as => "vuln_exploits"

    namespace :apps do

      namespace :single_password do
        resources :task_config do
          get 'show_creds', :on => :collection
        end
      end

      namespace :ssh_key do
        resources :task_config do
          get 'show_ssh_keys', :on => :collection
        end
      end

      namespace :pass_the_hash do
        resources :task_config do
          get 'show_smb_hashes', :on => :collection
        end
      end

      namespace :credential_intrusion do
        resources :task_config
      end

      namespace :passive_network_discovery do
        resources :task_config
      end

      resources :apps

      resources :app_runs, :as => :runs do
        member { put :abort }
      end
      # prevent the double "apps" in: /workspace/1/apps/apps/overview
      get 'overview' => 'apps#overview', :as => 'overview'

    end

    # New credentials data model
    namespace :metasploit do
      namespace :credential do
        root to: 'cores#index'

        resources :cores do

          collection do
            delete :destroy_multiple
            post   :quick_multi_tag
            get    :export
            get    :filter_values
            get    :search_operators
          end

          member do
            get :tags
            put :remove_tag
          end

          resource  :origin, only: [:show]

          resources :logins do

            member do
              post :get_session
              post :validate_authentication
              get :tags
              put :remove_tag
            end

            collection do
              delete :destroy_multiple
              post :quick_multi_tag
            end

          end
        end

        resources :logins do
          member do
            post :validate_authentication
            post :attempt_session
            get  :get_session
          end

        end
      end
    end

    get 'credentials' => 'metasploit/credential/cores#index', :as => 'credentials'


    namespace :shared do
      resources :payload_settings, only: [:index,:create]
    end

    namespace :brute_force do
      resources :runs

      namespace :reuse do
        resources :groups do
          member do
            get :cores
          end
        end

        resources :targets do
          collection {
            get :filter_values
            get :search_operators
          }
          member     { get :index }
        end
      end

      scope module: 'guess' do
        resources :guess
      end

      namespace :guess do
        resources :runs, only: [:create] do
          collection do
            post :target_count
          end
        end
      end

    end

  end # end Workspaces routes


  # Creds
  resources :creds do
    collection do
      delete :destroy_multiple
    end
  end

  # Mdm::Tag Search
  get "tags" => "tags#search", :as => "search_tags"

  # Hosts
  # TODO: make tag creation/deletion routes make more sense
  scope "workspaces/:workspace_id" do
    delete "hosts(/:id)" => "hosts#destroy", :as => "delete_host"    # FIXME (by deleting prototype): made id optional for named route, see hosts/index.html.erb:15
    post "hosts" => "hosts#create", :as => "create_host"
    get "hosts/json" => "hosts#index", dataTable: false
    get "hosts" => "hosts#index", :as => "hosts", dataTable: true
    get "hosts/new" => "hosts#new", :as => "new_host"
    get "hosts/map" => "hosts#map", :as => "map_host"
    post "hosts/multi_tag" => "hosts#multi_tag", :as => "multi_tag"
    post "hosts/quick_multi_tag" => "hosts#quick_multi_tag", :as => "quick_multi_tag"
    post "hosts/:id" => "hosts#create_or_update_tag", :as => "create_or_update_tag"
  end

  scope "hosts/:id" do
    get "new_loot" => "loots#new", :as => "new_loot"
    match "add_loot" => "loots#add_loot", :as => "add_loot", via: [:get, :post]
    get "" => "hosts#show", :as => "host"
    patch "" => "hosts#update", :as => "update_host"
    post "" => "hosts#create_or_update_tag"
    delete "" => "hosts#remove_tag", :as => "remove_tag"
    get "edit" => "hosts#edit", :as => "edit_host"
    get "services" => "hosts#services", :as => "host_services"
    get "sessions" => "hosts#sessions", :as => "host_sessions"
    get "vulns" => "hosts#vulns", :as => "host_vulns_tab"
    get "shares" => "hosts#shares", :as => "host_shares"
    get "loots" => "hosts#loots", :as => "host_loots"
    get "notes" => "hosts#notes", :as => "host_notes"
    get "creds" => "hosts#creds", :as => "host_creds"
    get "tags" => "hosts#tags", :as => "host_tags"
    get "attempts" => "hosts#attempts", :as => "host_attempts"
    get "details" => "hosts#details", :as => "host_details"
    get "exploits" => "hosts#exploits", :as => "host_exploits"
    get "show_alive_sessions" => "hosts#show_alive_sessions", :as => "show_alive_sessions"
    get "show_dead_sessions" => "hosts#show_dead_sessions", :as => "show_dead_sessions"
    get "show_services/json" => "hosts#show_services", dataTable:false
    get "show_services" => "hosts#show_services", :as => "show_services", dataTable:true
    get "show_history" => "hosts#show_history", :as => "show_history"
    get "show_notes" => "hosts#show_notes", :as =>"show_notes"
    patch   "update_service" => "hosts#update_service", :as => "update_service"
    delete "delete_service" => "hosts#delete_service", :as => "delete_service"
    get "show_captured_data" => "hosts#show_captured_data", :as => "show_captured_data"
    put    "update_cred" => "hosts#update_cred", :as => "update_cred"
    delete "delete_cred" => "hosts#delete_cred", :as => "delete_cred"
    post   "create_service" => "hosts#create_service", :as => "create_service"
    post   "create_cred" => "hosts#create_cred", :as => "create_cred"
    post "poll_presenter" => "hosts#poll_presenter", :as => "poll_presenter"
    get  "poll_presenter" => "hosts#poll_presenter"
  end

  # Vulns
  scope "/hosts/:host_id", :as => "host" do
    resources :vulns

    namespace :metasploit do
      namespace :credential do
        resources :cores do
          collection do
            get :accessing,   to: :index, host_origin: :accessing
            get :originating, to: :index, host_origin: :originating
          end
        end

        resources :logins do
          collection do
            get :accessing, to: :index, host_origin: :accessing
          end
        end

      end
    end

  end


  # Sessions
  scope "sessions/:id" do
    delete "" => "sessions#destroy", :as => "delete_session"
    match "route" => "sessions#route", :as => "session_route", via: [:get, :post]
    get "vnc" => "sessions#vnc", :as => "session_vnc"
    match "shell" => "sessions#shell", :as => "session_shell", via: [:get, :post]
    get "files" => "sessions#files", :as => "session_files"
    get "search" => "sessions#search", :as => "session_search"
    get "upload" => "sessions#upload", :as => "session_upload"
    get "download" => "sessions#download", :as => "session_download"
    get "delete" => "sessions#delete", :as => "session_delete"
  end

  scope "workspaces/:workspace_id" do
    get "sessions/:id" => "sessions#show", :as => "session"
    get "session_history/:id" => "sessions#history", :as => "session_history"
    match "consoles/:id" => "sessions#console_interact", :as => "session_console_interact", via: [:get, :post]
    get "console" => "sessions#console_create", :as => "session_console_create"
    get "sessions/:id/reopen" => "tasks#session_reopen", :as => "session_reopen"
  end


  # Tasks
  get "workspace/:workspace_id/tasks/update_cred_file" => "tasks#update_cred_file" # FIX ME (workspaces misspelled)
  scope "workspaces/:workspace_id/tasks" do
    get "/:id/edit" => "tasks#edit", :as => "edit"
    get "/:id/stats/:presenter" => "tasks#stats_collection"

    get "new_import_creds" => "tasks#new_import_creds", :as => "new_import_creds"
    post "start_import_creds" => "tasks#start_import_creds", :as => "start_import_creds"

    get "update_cred_file" => "tasks#update_cred_file", :as => "update_cred_file"

    match "new_scan" => "tasks#new_scan", via: [:get, :post]
    post "start_scan" => "tasks#start_scan", :as => "start_scan"
    post  "validate_scan" => "tasks#validate_scan", :as => "validate_scan"

    match "new_import" => "tasks#new_import", :as => "new_import", via: [:get, :post]
    post "start_import" => "tasks#start_import", :as => "start_import"
    post "start_scan_and_import" => "tasks#start_scan_and_import", :as => "start_scan_and_import"
    post "validate_scan_and_import" => "tasks#validate_scan_and_import", :as => "validate_scan_and_import"
    post  "validate_import" => "tasks#validate_import", :as => "validate_import"

    match "new_exploit" => "tasks#new_exploit", :as => "new_exploit", via: [:get, :post]
    post "start_exploit" => "tasks#start_exploit", :as => "start_exploit"
    post  "validate_exploit" => "tasks#validate_exploit", :as => "validate_exploit"

    match "new_webscan" => "tasks#new_webscan", :as => "new_webscan", via: [:get, :post]
    post "start_webscan" => "tasks#start_webscan", :as => "start_webscan"
    post  "validate_webscan" => "tasks#validate_webscan", :as => "validate_webscan"

    match "new_webaudit" => "tasks#new_webaudit", :as => "new_webaudit", via: [:get, :post]
    post "start_webaudit" => "tasks#start_webaudit", :as => "start_webaudit"

    match "new_websploit" => "tasks#new_websploit", :as => "new_websploit", via: [:get, :post]
    post "start_websploit" => "tasks#start_websploit", :as => "start_websploit"

    match "new_bruteforce" => "tasks#new_bruteforce", :as => "new_bruteforce", via: [:get, :post]
    post "start_bruteforce" => "tasks#start_bruteforce", :as => "start_bruteforce"
    post  "validate_bruteforce" => "tasks#validate_bruteforce", :as => "validate_bruteforce"

    match "new_collect" => "tasks#new_collect_evidence", :as => "new_collect_evidence", via: [:get, :post]
    post "start_collect" => "tasks#start_collect_evidence", :as => "start_collect_evidence"
    post  "validate_collect_evidence" => "tasks#validate_collect_evidence", :as => "validate_collect_evidence"

    match "clone_module_run/:clone_id" => "tasks#new_module_run", :as => "clone_module_run", via: [:get, :post]
    match "new_module_run/(*path)" => "tasks#new_module_run", :as => "new_module_run", via: [:get, :post]

    post "start_module_run/*path" => "tasks#start_module_run", :as => "start_module_run"
    post "validate_module_run" => "tasks#validate_module_run", :as => "validate_module_run"

    post "start_nexpose" => "tasks#start_nexpose", :as => "start_nexpose"
    post  "validate_nexpose" => "tasks#validate_nexpose", :as=> "validate_nexpose"

    match "new_replay" => "tasks#new_replay", :as => "new_replay", via: [:get, :post]
    post "start_replay" => "tasks#start_replay", :as => "start_replay"

    match "new_cleanup" => "tasks#new_cleanup", :as => "new_cleanup", via: [:get, :post]
    post "start_cleanup" => "tasks#start_cleanup", :as => "start_cleanup"

    post "new_upgrade_sessions" => "tasks#new_upgrade_sessions", :as => "new_upgrade_sessions"
    post "start_upgrade_sessions" => "tasks#start_upgrade_sessions", :as => "start_upgrade_sessions"

    post "start_campaign" => "tasks#start_campaign", :as => "start_campaign"

    match "new_tunnel" => "tasks#new_tunnel", :as => "new_tunnel", via: [:get, :post]
    post "start_tunnel" => "tasks#start_tunnel", :as => "start_tunnel"

    match "new_transport_change" => "tasks#new_transport_change", :as => "new_transport_change", via: [:get, :post]
    post "start_transport_change" => "tasks#start_transport_change", :as => "start_transport_change"

    post "new_nexpose_asset_group_push" => "tasks#new_nexpose_asset_group_push", :as => "new_nexpose_asset_group_push"
    post "start_nexpose_asset_group_push" => "tasks#start_nexpose_asset_group_push", :as => "start_nexpose_asset_group_push"

    match "new_nexpose_exception_push" => "tasks#new_nexpose_exception_push", :as => "new_nexpose_exception_push", via: [:get, :post]
    post "start_nexpose_exception_push" => "tasks#start_nexpose_exception_push", :as => "start_nexpose_exception_push"

    get "" => "tasks#index", :as => "tasks"
    get ":id" => "tasks#show", :as => "task_detail"

    get "/:id/run_stats" => "run_stats#show"
  end

  scope "tasks" do
    post "stop" => "tasks#stop", :as => "stop_task"
    match "stop_all" => "tasks#stop_all", :as => "stop_all_tasks", via: [:get, :post]
    post ":id/pause" => "tasks#pause", :as => "pause_task"
    post ":id/resume" => "tasks#resume", :as => "resume_task"
    post ":id/stop_paused" => "tasks#stop_paused", :as => "stop_paused_task"
    post ":workspace_id/stop" => "tasks#stop_in_workspace", :as => "stop_tasks_in_workspace"
    get "replay/:id" => "tasks#replay", :as => "replay_task"
    get ":id/logs" => "tasks#logs", :as => "task_log"
  end

  delete "workspace/:workspace_id/events" => "events#delete_all" # FIX ME (workspaces misspelled)
  scope "workspaces/:workspace_id" do
    delete "events" => "events#delete_all", :as => 'delete_workspace_events'

    # Modules
    get "modules" => "modules#index", :as => "modules"
    post "modules" => "modules#index"
    get "modules/*path" => "modules#show", :as => "module"

    # Web Applications
    scope "web" do
      get "sites" => "web#index", :as => "web_sites"
      get "forms/:site_id" => "web#forms", :as => "web_forms"
      get "form/:site_id" => "web#form", :as => "web_form"
      get "vulns/:site_id" => "web#vulns", :as => "web_vulns"
      get "vuln/:vuln_id" => "web#vuln", :as => "web_vuln"

      delete "vuln/delete/:site_id" => "web#delete_vulns", :as => "delete_web_vulns"
      delete "site/delete(/:id)" => "web#delete_sites", :as => "delete_web_sites"
    end
  end

  # Events
  resources :events

  # Payload information
  resources :payloads do
    collection do
      get :encoders
      get :formats
    end
  end

  # Mdm::Loot
  get "loot/:id" => "loots#get", :as => "loot"

  # Admin area
  namespace :admin do |admin|
    resources :users do
      collection do
        post "delete" => "users#destroy", :as => "mass_delete"
      end
    end
  end

  root :to => "workspaces#index"
end
