(function() {
  var NodeTypes, ParameterMissing, Utils, createGlobalJsRoutesObject, defaults, root,
    __hasProp = {}.hasOwnProperty;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  ParameterMissing = function(message) {
    this.message = message;
  };

  ParameterMissing.prototype = new Error();

  defaults = {
    prefix: "",
    default_url_options: {}
  };

  NodeTypes = {"GROUP":1,"CAT":2,"SYMBOL":3,"OR":4,"STAR":5,"LITERAL":6,"SLASH":7,"DOT":8};

  Utils = {
    serialize: function(object, prefix) {
      var element, i, key, prop, result, s, _i, _len;

      if (prefix == null) {
        prefix = null;
      }
      if (!object) {
        return "";
      }
      if (!prefix && !(this.get_object_type(object) === "object")) {
        throw new Error("Url parameters should be a javascript hash");
      }
      if (root.jQuery) {
        result = root.jQuery.param(object);
        return (!result ? "" : result);
      }
      s = [];
      switch (this.get_object_type(object)) {
        case "array":
          for (i = _i = 0, _len = object.length; _i < _len; i = ++_i) {
            element = object[i];
            s.push(this.serialize(element, prefix + "[]"));
          }
          break;
        case "object":
          for (key in object) {
            if (!__hasProp.call(object, key)) continue;
            prop = object[key];
            if (!(prop != null)) {
              continue;
            }
            if (prefix != null) {
              key = "" + prefix + "[" + key + "]";
            }
            s.push(this.serialize(prop, key));
          }
          break;
        default:
          if (object) {
            s.push("" + (encodeURIComponent(prefix.toString())) + "=" + (encodeURIComponent(object.toString())));
          }
      }
      if (!s.length) {
        return "";
      }
      return s.join("&");
    },
    clean_path: function(path) {
      var last_index;

      path = path.split("://");
      last_index = path.length - 1;
      path[last_index] = path[last_index].replace(/\/+/g, "/");
      return path.join("://");
    },
    set_default_url_options: function(optional_parts, options) {
      var i, part, _i, _len, _results;

      _results = [];
      for (i = _i = 0, _len = optional_parts.length; _i < _len; i = ++_i) {
        part = optional_parts[i];
        if (!options.hasOwnProperty(part) && defaults.default_url_options.hasOwnProperty(part)) {
          _results.push(options[part] = defaults.default_url_options[part]);
        }
      }
      return _results;
    },
    extract_anchor: function(options) {
      var anchor;

      anchor = "";
      if (options.hasOwnProperty("anchor")) {
        anchor = "#" + options.anchor;
        delete options.anchor;
      }
      return anchor;
    },
    extract_trailing_slash: function(options) {
      var trailing_slash;

      trailing_slash = false;
      if (defaults.default_url_options.hasOwnProperty("trailing_slash")) {
        trailing_slash = defaults.default_url_options.trailing_slash;
      }
      if (options.hasOwnProperty("trailing_slash")) {
        trailing_slash = options.trailing_slash;
        delete options.trailing_slash;
      }
      return trailing_slash;
    },
    extract_options: function(number_of_params, args) {
      var last_el;

      last_el = args[args.length - 1];
      if (args.length > number_of_params || ((last_el != null) && "object" === this.get_object_type(last_el) && !this.look_like_serialized_model(last_el))) {
        return args.pop();
      } else {
        return {};
      }
    },
    look_like_serialized_model: function(object) {
      return "id" in object || "to_param" in object;
    },
    path_identifier: function(object) {
      var property;

      if (object === 0) {
        return "0";
      }
      if (!object) {
        return "";
      }
      property = object;
      if (this.get_object_type(object) === "object") {
        if ("to_param" in object) {
          property = object.to_param;
        } else if ("id" in object) {
          property = object.id;
        } else {
          property = object;
        }
        if (this.get_object_type(property) === "function") {
          property = property.call(object);
        }
      }
      return property.toString();
    },
    clone: function(obj) {
      var attr, copy, key;

      if ((obj == null) || "object" !== this.get_object_type(obj)) {
        return obj;
      }
      copy = obj.constructor();
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        attr = obj[key];
        copy[key] = attr;
      }
      return copy;
    },
    prepare_parameters: function(required_parameters, actual_parameters, options) {
      var i, result, val, _i, _len;

      result = this.clone(options) || {};
      for (i = _i = 0, _len = required_parameters.length; _i < _len; i = ++_i) {
        val = required_parameters[i];
        if (i < actual_parameters.length) {
          result[val] = actual_parameters[i];
        }
      }
      return result;
    },
    build_path: function(required_parameters, optional_parts, route, args) {
      var anchor, opts, parameters, result, trailing_slash, url, url_params;

      args = Array.prototype.slice.call(args);
      opts = this.extract_options(required_parameters.length, args);
      if (args.length > required_parameters.length) {
        throw new Error("Too many parameters provided for path");
      }
      parameters = this.prepare_parameters(required_parameters, args, opts);
      this.set_default_url_options(optional_parts, parameters);
      anchor = this.extract_anchor(parameters);
      trailing_slash = this.extract_trailing_slash(parameters);
      result = "" + (this.get_prefix()) + (this.visit(route, parameters));
      url = Utils.clean_path("" + result);
      if (trailing_slash === true) {
        url = url.replace(/(.*?)[\/]?$/, "$1/");
      }
      if ((url_params = this.serialize(parameters)).length) {
        url += "?" + url_params;
      }
      url += anchor;
      return url;
    },
    visit: function(route, parameters, optional) {
      var left, left_part, right, right_part, type, value;

      if (optional == null) {
        optional = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return this.visit(left, parameters, true);
        case NodeTypes.STAR:
          return this.visit_globbing(left, parameters, true);
        case NodeTypes.LITERAL:
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
          return left;
        case NodeTypes.CAT:
          left_part = this.visit(left, parameters, optional);
          right_part = this.visit(right, parameters, optional);
          if (optional && !(left_part && right_part)) {
            return "";
          }
          return "" + left_part + right_part;
        case NodeTypes.SYMBOL:
          value = parameters[left];
          if (value != null) {
            delete parameters[left];
            return this.path_identifier(value);
          }
          if (optional) {
            return "";
          } else {
            throw new ParameterMissing("Route parameter missing: " + left);
          }
          break;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    build_path_spec: function(route, wildcard) {
      var left, right, type;

      if (wildcard == null) {
        wildcard = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return "(" + (this.build_path_spec(left)) + ")";
        case NodeTypes.CAT:
          return "" + (this.build_path_spec(left)) + (this.build_path_spec(right));
        case NodeTypes.STAR:
          return this.build_path_spec(left, true);
        case NodeTypes.SYMBOL:
          if (wildcard === true) {
            return "" + (left[0] === '*' ? '' : '*') + left;
          } else {
            return ":" + left;
          }
          break;
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
        case NodeTypes.LITERAL:
          return left;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    visit_globbing: function(route, parameters, optional) {
      var left, right, type, value;

      type = route[0], left = route[1], right = route[2];
      if (left.replace(/^\*/i, "") !== left) {
        route[1] = left = left.replace(/^\*/i, "");
      }
      value = parameters[left];
      if (value == null) {
        return this.visit(route, parameters, optional);
      }
      parameters[left] = (function() {
        switch (this.get_object_type(value)) {
          case "array":
            return value.join("/");
          default:
            return value;
        }
      }).call(this);
      return this.visit(route, parameters, optional);
    },
    get_prefix: function() {
      var prefix;

      prefix = defaults.prefix;
      if (prefix !== "") {
        prefix = (prefix.match("/$") ? prefix : "" + prefix + "/");
      }
      return prefix;
    },
    route: function(required_parts, optional_parts, route_spec) {
      var path_fn;

      path_fn = function() {
        return Utils.build_path(required_parts, optional_parts, route_spec, arguments);
      };
      path_fn.required_params = required_parts;
      path_fn.toString = function() {
        return Utils.build_path_spec(route_spec);
      };
      return path_fn;
    },
    _classToTypeCache: null,
    _classToType: function() {
      var name, _i, _len, _ref;

      if (this._classToTypeCache != null) {
        return this._classToTypeCache;
      }
      this._classToTypeCache = {};
      _ref = "Boolean Number String Function Array Date RegExp Object Error".split(" ");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        this._classToTypeCache["[object " + name + "]"] = name.toLowerCase();
      }
      return this._classToTypeCache;
    },
    get_object_type: function(obj) {
      if (root.jQuery && (root.jQuery.type != null)) {
        return root.jQuery.type(obj);
      }
      if (obj == null) {
        return "" + obj;
      }
      if (typeof obj === "object" || typeof obj === "function") {
        return this._classToType()[Object.prototype.toString.call(obj)] || "object";
      } else {
        return typeof obj;
      }
    }
  };

  createGlobalJsRoutesObject = function() {
    var namespace;

    namespace = function(mainRoot, namespaceString) {
      var current, parts;

      parts = (namespaceString ? namespaceString.split(".") : []);
      if (!parts.length) {
        return;
      }
      current = parts.shift();
      mainRoot[current] = mainRoot[current] || {};
      return namespace(mainRoot[current], parts.join("."));
    };
    namespace(root, "Routes");
    root.Routes = {
// attempt_session_workspace_metasploit_credential_login => /workspaces/:workspace_id/metasploit/credential/logins/:id/attempt_session(.:format)
  // function(workspace_id, id, options)
  attempt_session_workspace_metasploit_credential_login_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"metasploit",false]],[7,"/",false]],[6,"credential",false]],[7,"/",false]],[6,"logins",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"attempt_session",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// clone_module_run => /workspaces/:workspace_id/tasks/clone_module_run/:clone_id(.:format)
  // function(workspace_id, clone_id, options)
  clone_module_run_path: Utils.route(["workspace_id","clone_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"clone_module_run",false]],[7,"/",false]],[3,"clone_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// cores_workspace_brute_force_reuse_group => /workspaces/:workspace_id/brute_force/reuse/groups/:id/cores(.:format)
  // function(workspace_id, id, options)
  cores_workspace_brute_force_reuse_group_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"groups",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"cores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_notifications_message => /notifications/messages/:id/edit(.:format)
  // function(id, options)
  edit_notifications_message_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"notifications",false]],[7,"/",false]],[6,"messages",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_workspace_brute_force_guess => /workspaces/:workspace_id/brute_force/guess/:id/edit(.:format)
  // function(workspace_id, id, options)
  edit_workspace_brute_force_guess_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"guess",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_workspace_brute_force_reuse_group => /workspaces/:workspace_id/brute_force/reuse/groups/:id/edit(.:format)
  // function(workspace_id, id, options)
  edit_workspace_brute_force_reuse_group_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"groups",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_workspace_brute_force_reuse_target => /workspaces/:workspace_id/brute_force/reuse/targets/:id/edit(.:format)
  // function(workspace_id, id, options)
  edit_workspace_brute_force_reuse_target_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"targets",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_workspace_brute_force_run => /workspaces/:workspace_id/brute_force/runs/:id/edit(.:format)
  // function(workspace_id, id, options)
  edit_workspace_brute_force_run_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"runs",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// filter_values_workspace_brute_force_reuse_targets => /workspaces/:workspace_id/brute_force/reuse/targets/filter_values(.:format)
  // function(workspace_id, options)
  filter_values_workspace_brute_force_reuse_targets_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"targets",false]],[7,"/",false]],[6,"filter_values",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// filter_values_workspace_nexpose_data_sites => /workspaces/:workspace_id/nexpose/data/sites/filter_values(.:format)
  // function(workspace_id, options)
  filter_values_workspace_nexpose_data_sites_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"nexpose",false]],[7,"/",false]],[6,"data",false]],[7,"/",false]],[6,"sites",false]],[7,"/",false]],[6,"filter_values",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// get_session_workspace_metasploit_credential_core_login => /workspaces/:workspace_id/metasploit/credential/cores/:core_id/logins/:id/get_session(.:format)
  // function(workspace_id, core_id, id, options)
  get_session_workspace_metasploit_credential_core_login_path: Utils.route(["workspace_id","core_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"metasploit",false]],[7,"/",false]],[6,"credential",false]],[7,"/",false]],[6,"cores",false]],[7,"/",false]],[3,"core_id",false]],[7,"/",false]],[6,"logins",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"get_session",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// get_session_workspace_metasploit_credential_login => /workspaces/:workspace_id/metasploit/credential/logins/:id/get_session(.:format)
  // function(workspace_id, id, options)
  get_session_workspace_metasploit_credential_login_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"metasploit",false]],[7,"/",false]],[6,"credential",false]],[7,"/",false]],[6,"logins",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"get_session",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// history_workspace_vuln => /workspaces/:workspace_id/vulns/:id/history(.:format)
  // function(workspace_id, id, options)
  history_workspace_vuln_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"history",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// host => /hosts/:id(.:format)
  // function(id, options)
  host_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"hosts",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// host_tags => /hosts/:id/tags(.:format)
  // function(id, options)
  host_tags_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"hosts",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"tags",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// mark_read_notifications_messages => /notifications/messages/mark_read(.:format)
  // function(options)
  mark_read_notifications_messages_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"notifications",false]],[7,"/",false]],[6,"messages",false]],[7,"/",false]],[6,"mark_read",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_bruteforce => /workspaces/:workspace_id/tasks/new_bruteforce(.:format)
  // function(workspace_id, options)
  new_bruteforce_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"new_bruteforce",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_module_run => /workspaces/:workspace_id/tasks/new_module_run(/*path)(.:format)
  // function(workspace_id, options)
  new_module_run_path: Utils.route(["workspace_id"], ["path","format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"new_module_run",false]],[1,[2,[7,"/",false],[5,[3,"*path",false],false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_notifications_message => /notifications/messages/new(.:format)
  // function(options)
  new_notifications_message_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"notifications",false]],[7,"/",false]],[6,"messages",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_workspace_brute_force_guess => /workspaces/:workspace_id/brute_force/guess/new(.:format)
  // function(workspace_id, options)
  new_workspace_brute_force_guess_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"guess",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_workspace_brute_force_reuse_group => /workspaces/:workspace_id/brute_force/reuse/groups/new(.:format)
  // function(workspace_id, options)
  new_workspace_brute_force_reuse_group_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"groups",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_workspace_brute_force_reuse_target => /workspaces/:workspace_id/brute_force/reuse/targets/new(.:format)
  // function(workspace_id, options)
  new_workspace_brute_force_reuse_target_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"targets",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_workspace_brute_force_run => /workspaces/:workspace_id/brute_force/runs/new(.:format)
  // function(workspace_id, options)
  new_workspace_brute_force_run_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"runs",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// notifications_message => /notifications/messages/:id(.:format)
  // function(id, options)
  notifications_message_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"notifications",false]],[7,"/",false]],[6,"messages",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// notifications_messages => /notifications/messages(.:format)
  // function(options)
  notifications_messages_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"notifications",false]],[7,"/",false]],[6,"messages",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// pause_task => /tasks/:id/pause(.:format)
  // function(id, options)
  pause_task_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"tasks",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"pause",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// poll_notifications_messages => /notifications/messages/poll(.:format)
  // function(options)
  poll_notifications_messages_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"notifications",false]],[7,"/",false]],[6,"messages",false]],[7,"/",false]],[6,"poll",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// related_hosts_workspace_vuln => /workspaces/:workspace_id/vulns/:id/related_hosts(.:format)
  // function(workspace_id, id, options)
  related_hosts_workspace_vuln_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"related_hosts",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// related_modules_workspace_vuln => /workspaces/:workspace_id/vulns/:id/related_modules(.:format)
  // function(workspace_id, id, options)
  related_modules_workspace_vuln_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"related_modules",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// restore_last_vuln_attempt_status_workspace_vuln => /workspaces/:workspace_id/vulns/:id/restore_last_vuln_attempt_status(.:format)
  // function(workspace_id, id, options)
  restore_last_vuln_attempt_status_workspace_vuln_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"restore_last_vuln_attempt_status",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// resume_task => /tasks/:id/resume(.:format)
  // function(id, options)
  resume_task_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"tasks",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"resume",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// search_operators_workspace_brute_force_reuse_targets => /workspaces/:workspace_id/brute_force/reuse/targets/search_operators(.:format)
  // function(workspace_id, options)
  search_operators_workspace_brute_force_reuse_targets_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"targets",false]],[7,"/",false]],[6,"search_operators",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// search_operators_workspace_nexpose_data_sites => /workspaces/:workspace_id/nexpose/data/sites/search_operators(.:format)
  // function(workspace_id, options)
  search_operators_workspace_nexpose_data_sites_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"nexpose",false]],[7,"/",false]],[6,"data",false]],[7,"/",false]],[6,"sites",false]],[7,"/",false]],[6,"search_operators",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// search_workspace_tags => /workspaces/:workspace_id/tags/search(.:format)
  // function(workspace_id, options)
  search_workspace_tags_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tags",false]],[7,"/",false]],[6,"search",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// session => /workspaces/:workspace_id/sessions/:id(.:format)
  // function(workspace_id, id, options)
  session_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"sessions",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// start_bruteforce => /workspaces/:workspace_id/tasks/start_bruteforce(.:format)
  // function(workspace_id, options)
  start_bruteforce_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"start_bruteforce",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// start_import => /workspaces/:workspace_id/tasks/start_import(.:format)
  // function(workspace_id, options)
  start_import_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"start_import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// start_module_run => /workspaces/:workspace_id/tasks/start_module_run/*path(.:format)
  // function(workspace_id, path, options)
  start_module_run_path: Utils.route(["workspace_id","path"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"start_module_run",false]],[7,"/",false]],[5,[3,"*path",false],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// start_scan_and_import => /workspaces/:workspace_id/tasks/start_scan_and_import(.:format)
  // function(workspace_id, options)
  start_scan_and_import_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"start_scan_and_import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// stop_paused_task => /tasks/:id/stop_paused(.:format)
  // function(id, options)
  stop_paused_task_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"tasks",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"stop_paused",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// stop_task => /tasks/stop(.:format)
  // function(options)
  stop_task_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"tasks",false]],[7,"/",false]],[6,"stop",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// target_count_workspace_brute_force_guess_runs => /workspaces/:workspace_id/brute_force/guess/runs/target_count(.:format)
  // function(workspace_id, options)
  target_count_workspace_brute_force_guess_runs_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"guess",false]],[7,"/",false]],[6,"runs",false]],[7,"/",false]],[6,"target_count",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// task_detail => /workspaces/:workspace_id/tasks/:id(.:format)
  // function(workspace_id, id, options)
  task_detail_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// update_last_vuln_attempt_status_workspace_vuln => /workspaces/:workspace_id/vulns/:id/update_last_vuln_attempt_status(.:format)
  // function(workspace_id, id, options)
  update_last_vuln_attempt_status_workspace_vuln_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"update_last_vuln_attempt_status",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// validate_bruteforce => /workspaces/:workspace_id/tasks/validate_bruteforce(.:format)
  // function(workspace_id, options)
  validate_bruteforce_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"validate_bruteforce",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// validate_import => /workspaces/:workspace_id/tasks/validate_import(.:format)
  // function(workspace_id, options)
  validate_import_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"validate_import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// validate_module_run => /workspaces/:workspace_id/tasks/validate_module_run(.:format)
  // function(workspace_id, options)
  validate_module_run_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"validate_module_run",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// validate_scan_and_import => /workspaces/:workspace_id/tasks/validate_scan_and_import(.:format)
  // function(workspace_id, options)
  validate_scan_and_import_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"tasks",false]],[7,"/",false]],[6,"validate_scan_and_import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_guess => /workspaces/:workspace_id/brute_force/guess/:id(.:format)
  // function(workspace_id, id, options)
  workspace_brute_force_guess_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"guess",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_guess_index => /workspaces/:workspace_id/brute_force/guess(.:format)
  // function(workspace_id, options)
  workspace_brute_force_guess_index_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"guess",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_guess_runs => /workspaces/:workspace_id/brute_force/guess/runs(.:format)
  // function(workspace_id, options)
  workspace_brute_force_guess_runs_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"guess",false]],[7,"/",false]],[6,"runs",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_reuse_group => /workspaces/:workspace_id/brute_force/reuse/groups/:id(.:format)
  // function(workspace_id, id, options)
  workspace_brute_force_reuse_group_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"groups",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_reuse_groups => /workspaces/:workspace_id/brute_force/reuse/groups(.:format)
  // function(workspace_id, options)
  workspace_brute_force_reuse_groups_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"groups",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_reuse_target => /workspaces/:workspace_id/brute_force/reuse/targets/:id(.:format)
  // function(workspace_id, id, options)
  workspace_brute_force_reuse_target_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"targets",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_reuse_targets => /workspaces/:workspace_id/brute_force/reuse/targets(.:format)
  // function(workspace_id, options)
  workspace_brute_force_reuse_targets_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"reuse",false]],[7,"/",false]],[6,"targets",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_run => /workspaces/:workspace_id/brute_force/runs/:id(.:format)
  // function(workspace_id, id, options)
  workspace_brute_force_run_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"runs",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_brute_force_runs => /workspaces/:workspace_id/brute_force/runs(.:format)
  // function(workspace_id, options)
  workspace_brute_force_runs_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"brute_force",false]],[7,"/",false]],[6,"runs",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_credentials => /workspaces/:workspace_id/credentials(.:format)
  // function(workspace_id, options)
  workspace_credentials_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"credentials",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_module_detail => /workspaces/:workspace_id/module_details/:id(.:format)
  // function(workspace_id, id, options)
  workspace_module_detail_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"module_details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_nexpose_data_import_runs => /workspaces/:workspace_id/nexpose/data/import_runs(.:format)
  // function(workspace_id, options)
  workspace_nexpose_data_import_runs_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"nexpose",false]],[7,"/",false]],[6,"data",false]],[7,"/",false]],[6,"import_runs",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_nexpose_data_sites => /workspaces/:workspace_id/nexpose/data/sites(.:format)
  // function(workspace_id, options)
  workspace_nexpose_data_sites_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"nexpose",false]],[7,"/",false]],[6,"data",false]],[7,"/",false]],[6,"sites",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_nexpose_result_exceptions => /workspaces/:workspace_id/nexpose/result/exceptions(.:format)
  // function(workspace_id, options)
  workspace_nexpose_result_exceptions_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"nexpose",false]],[7,"/",false]],[6,"result",false]],[7,"/",false]],[6,"exceptions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_nexpose_result_validations => /workspaces/:workspace_id/nexpose/result/validations(.:format)
  // function(workspace_id, options)
  workspace_nexpose_result_validations_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"nexpose",false]],[7,"/",false]],[6,"result",false]],[7,"/",false]],[6,"validations",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_notes => /workspaces/:workspace_id/notes(.:format)
  // function(workspace_id, options)
  workspace_notes_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"notes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_shared_payload_settings => /workspaces/:workspace_id/shared/payload_settings(.:format)
  // function(workspace_id, options)
  workspace_shared_payload_settings_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"shared",false]],[7,"/",false]],[6,"payload_settings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_vuln => /workspaces/:workspace_id/vulns/:id(.:format)
  // function(workspace_id, id, options)
  workspace_vuln_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_vuln_attempts => /workspaces/:workspace_id/vulns/:id/attempts(.:format)
  // function(workspace_id, id, options)
  workspace_vuln_attempts_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"attempts",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_vuln_details => /workspaces/:workspace_id/vulns/:id/details(.:format)
  // function(workspace_id, id, options)
  workspace_vuln_details_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"details",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_vuln_exploits => /workspaces/:workspace_id/vulns/:id/exploits(.:format)
  // function(workspace_id, id, options)
  workspace_vuln_exploits_path: Utils.route(["workspace_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"exploits",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// workspace_vulns => /workspaces/:workspace_id/vulns(.:format)
  // function(workspace_id, options)
  workspace_vulns_path: Utils.route(["workspace_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"workspaces",false]],[7,"/",false]],[3,"workspace_id",false]],[7,"/",false]],[6,"vulns",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments)}
;
    root.Routes.options = defaults;
    return root.Routes;
  };

  if (typeof define === "function" && define.amd) {
    define([], function() {
      return createGlobalJsRoutesObject();
    });
  } else {
    createGlobalJsRoutesObject();
  }

}).call(this);

