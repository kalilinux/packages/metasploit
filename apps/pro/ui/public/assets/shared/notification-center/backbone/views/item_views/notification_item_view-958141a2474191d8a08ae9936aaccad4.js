(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/notification_center/item_views/notification_item_view.js', '/assets/templates/notification_center/item_views/_notification.js', '/assets/shared/notification-center/backbone/event_aggregators/event_aggregator.js'], function($, template, partial, EventAggregator) {
    var NotificationItemView;
    return NotificationItemView = (function(_super) {

      __extends(NotificationItemView, _super);

      function NotificationItemView() {
        this._destroy_callback = __bind(this._destroy_callback, this);
        return NotificationItemView.__super__.constructor.apply(this, arguments);
      }

      NotificationItemView.prototype.template = HandlebarsTemplates['notification_center/item_views/notification_item_view'];

      NotificationItemView.prototype.triggers = {
        'click': 'notification:link:goto',
        'click .action-bar': 'notification:close'
      };

      NotificationItemView.prototype.initialize = function() {
        return this._bind_triggers();
      };

      NotificationItemView.prototype._bind_triggers = function() {
        this.on('notification:close', this._close_notification, this);
        return this.on('notification:link:goto', this._redirect_notification_link);
      };

      NotificationItemView.prototype._redirect_notification_link = function() {
        if (this.model.get('url') != null) {
          return window.location.href = this.model.get('url');
        }
      };

      NotificationItemView.prototype._close_notification = function() {
        return this.model.destroy({
          success: this._destroy_callback
        });
      };

      NotificationItemView.prototype._destroy_callback = function(model, response) {
        return EventAggregator.trigger('notification-message:destroy');
      };

      return NotificationItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
