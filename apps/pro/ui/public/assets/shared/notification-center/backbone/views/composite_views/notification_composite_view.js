(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/notification_center/composite_views/notification_composite_view.js', '/assets/shared/notification-center/backbone/views/item_views/notification_item_view.js', '/assets/shared/notification-center/backbone/event_aggregators/event_aggregator.js'], function($, template, NotificationItemView, EventAggregator) {
    var NotificationCompositeView;
    return NotificationCompositeView = (function(_super) {

      __extends(NotificationCompositeView, _super);

      function NotificationCompositeView() {
        return NotificationCompositeView.__super__.constructor.apply(this, arguments);
      }

      NotificationCompositeView.prototype.itemView = NotificationItemView;

      NotificationCompositeView.prototype.emptyView = NotificationItemView;

      NotificationCompositeView.prototype.template = HandlebarsTemplates['notification_center/composite_views/notification_composite_view'];

      NotificationCompositeView.prototype.triggers = {
        'change .sort-options select': 'sort:changed'
      };

      NotificationCompositeView.prototype.initialize = function() {
        return this._bind_triggers();
      };

      NotificationCompositeView.prototype._bind_triggers = function() {
        return this.on('sort:changed', this._update_notification_type);
      };

      NotificationCompositeView.prototype._update_notification_type = function() {
        return EventAggregator.trigger('notification-center:change:type');
      };

      NotificationCompositeView.prototype.redraw_with_sort_preserved = function() {
        return this.collection.trigger('reset');
      };

      return NotificationCompositeView;

    })(Backbone.Marionette.CompositeView);
  });

}).call(this);
