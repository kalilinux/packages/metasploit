(function() {

  jQuery(function($) {
    return $(function() {
      var $lootsDataTable, $lootsTable, lootsPath;
      lootsPath = $('#loots-path').html();
      $lootsTable = $('#loots-table');
      $lootsDataTable = $lootsTable.table({
        analysisTab: true,
        setFilteringDelay: true,
        searchInputHint: "Search Evidence",
        datatableOptions: {
          "oLanguage": {
            "sEmptyTable": "No evidence has been captured in this Project."
          },
          "sAjaxSource": lootsPath,
          "aaSorting": [[2, 'desc']],
          "aoColumns": [
            {
              "mDataProp": "checkbox",
              "bSortable": false
            }, {
              "mDataProp": "host",
              "sWidth": "125px",
              "bSortable": false
            }, {
              "mDataProp": "type",
              "sWidth": "225px"
            }, {
              "mDataProp": "name",
              "sWidth": "150px"
            }, {
              "mDataProp": "size",
              "sWidth": "100px",
              "bSortable": false
            }, {
              "mDataProp": "info"
            }, {
              "mDataProp": "data",
              "bSortable": false,
              "sWidth": "240px"
            }
          ]
        }
      });
      $(document).on('click', 'a.loot-data-view', function(e) {
        var $dialog;
        $dialog = $("<div style='display:hidden'>" + ($(this).parent().siblings('.loot-data').html()) + "</div>").appendTo('body');
        $dialog.dialog({
          title: "Loot data",
          maxheight: 530,
          width: 670,
          buttons: {
            "Close": function() {
              return $(this).dialog('close');
            }
          }
        });
        return e.preventDefault();
      });
      return $lootsDataTable.addCollapsibleSearch();
    });
  });

}).call(this);
