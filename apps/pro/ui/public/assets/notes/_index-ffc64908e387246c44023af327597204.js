(function() {

  jQuery(function($) {
    return $(function() {
      var $notesDataTable, $notesTable, notesPath;
      notesPath = $('#notes-path').html();
      $notesTable = $('#notes-table');
      $notesDataTable = $notesTable.table({
        analysisTab: true,
        searchInputHint: "Search Notes",
        datatableOptions: {
          "oLanguage": {
            "sEmptyTable": "No Notes are associated with this Project."
          },
          "sAjaxSource": notesPath,
          "aaSorting": [[2, 'desc']],
          "aoColumns": [
            {
              "mDataProp": "checkbox",
              "bSortable": false
            }, {
              "mDataProp": "host",
              "sWidth": "125px",
              "bSortable": false
            }, {
              "mDataProp": "type",
              "sWidth": "150px"
            }, {
              "mDataProp": "data",
              "bSortable": false
            }, {
              "mDataProp": "created_at",
              "sWidth": "170px"
            }, {
              "mDataProp": "status",
              "sWidth": "20px",
              "bSortable": false
            }
          ]
        }
      });
      $(document).on('click', 'a.note-data-view', function(e) {
        var $dialog;
        $dialog = $("<div style='display:hidden'>" + ($(this).siblings('.note-data').html()) + "</div>").appendTo('body');
        $dialog.dialog({
          title: "Note data",
          buttons: {
            "Close": function() {
              return $(this).dialog('close');
            }
          },
          open: function(event, ui) {
            $(this).css({
              'max-height': 400,
              'overflow-y': 'auto'
            });
            if ($(this).parent().position().top === 0) {
              return $('.ui-resizable').css({
                'top': '100px'
              });
            }
          }
        });
        return e.preventDefault();
      });
      return $notesDataTable.addCollapsibleSearch();
    });
  });

}).call(this);
