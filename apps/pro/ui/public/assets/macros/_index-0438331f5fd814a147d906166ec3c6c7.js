(function() {

  jQuery(function($) {
    return $(document).ready(function() {
      return $('#macro-delete-submit').multiDeleteConfirm({
        tableSelector: '#macro_list',
        pluralObjectName: 'macros'
      });
    });
  });

}).call(this);
