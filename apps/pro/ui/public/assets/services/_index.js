(function() {

  jQuery(function($) {
    return $(function() {
      var $servicesDataTable, $servicesTable, servicesPath;
      servicesPath = $('#services-path').html();
      $servicesTable = $('#services-table');
      $servicesDataTable = $servicesTable.table({
        analysisTab: true,
        controlBarLocation: $('.analysis-control-bar'),
        searchInputHint: "Search Services",
        datatableOptions: {
          oLanguage: {
            sEmptyTable: "No Services are associated with this Project."
          },
          sAjaxSource: servicesPath,
          aaSorting: [[4, 'asc']],
          aoColumns: [
            {
              mDataProp: "checkbox",
              bSortable: false
            }, {
              mDataProp: "host"
            }, {
              mDataProp: "name",
              sWidth: "125px"
            }, {
              mDataProp: "protocol",
              sWidth: "60px"
            }, {
              mDataProp: "port",
              sWidth: "50px"
            }, {
              mDataProp: "info"
            }, {
              mDataProp: "state",
              sWidth: "125px"
            }, {
              mDataProp: "updated_at",
              sWidth: '200px'
            }
          ]
        }
      });
      return $servicesDataTable.addCollapsibleSearch();
    });
  });

}).call(this);
