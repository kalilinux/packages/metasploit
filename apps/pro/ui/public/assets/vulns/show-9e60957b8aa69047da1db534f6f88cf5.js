(function() {

  jQuery(function($) {
    var sortVulns;
    sortVulns = function() {
      return $("#vuln-attempts-tbl").table({
        searchInputHint: "Search exploit attempts",
        datatableOptions: {
          bDestroy: true,
          "aaSorting": [[0, 'desc']],
          "aoColumns": [null, null, null, null, null],
          "oLanguage": {
            "sEmptyTable": "No attempts have been records for this vulnerability."
          }
        }
      });
    };
    window.moduleLinksInit = function(moduleRunPathFragment) {
      var formId;
      formId = "#new_module_run";
      return $('a.module-name').click(function(event) {
        var modAction, pathPiece, theForm;
        if ($(this).attr('href') !== "#") {
          return true;
        } else {
          pathPiece = $(this).attr('module_fullname');
          modAction = "" + moduleRunPathFragment + "/" + pathPiece;
          theForm = $(formId);
          theForm.attr('action', modAction);
          theForm.submit();
          return false;
        }
      });
    };
    return $(document).ready(function() {
      return $("#tabs").tabs({
        selected: 0,
        spinner: 'Loading...',
        cache: true,
        load: function(e, ui) {
          var href;
          href = ui.tab.find('a').attr('href');
          $(ui.panel).find(".tab-loading").remove();
          if (href.indexOf("vuln") > -1) {
            return sortVulns();
          }
        },
        select: function(e, ui) {
          var $panel;
          $panel = $(ui.panel);
          if ($panel.is(":empty")) {
            return $panel.append("<div class='tab-loading'></div>");
          }
        }
      });
    });
  });

}).call(this);
