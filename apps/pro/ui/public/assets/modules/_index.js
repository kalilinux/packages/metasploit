(function() {

  jQuery(function($) {
    return window.moduleLinksInit = function(moduleRunPathFragment) {
      var formId;
      formId = "#new_module_run";
      return $('a.module-name').click(function(event) {
        var modAction, pathPiece, theForm;
        if ($(this).attr('href') !== "#") {
          return true;
        } else {
          pathPiece = $(this).attr('module_fullname');
          modAction = "" + moduleRunPathFragment + "/" + pathPiece;
          theForm = $(formId);
          theForm.attr('action', modAction);
          theForm.submit();
          return false;
        }
      });
    };
  });

}).call(this);
