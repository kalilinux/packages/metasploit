(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define(['jquery', '/assets/hosts/backbone/controllers/host_view_controller.js', '/assets/hosts/router.js'], function($, HostViewController, Router) {
    var HostViewApp;
    return HostViewApp = (function() {

      function HostViewApp() {
        this.start = __bind(this.start, this);

      }

      HostViewApp.prototype.start = function() {
        var app;
        window.HostViewAppRouter = Router;
        app = new Backbone.Marionette.Application;
        app.addInitializer(function() {
          return Backbone.history.start();
        });
        return app.start();
      };

      return HostViewApp;

    })();
  });

}).call(this);
