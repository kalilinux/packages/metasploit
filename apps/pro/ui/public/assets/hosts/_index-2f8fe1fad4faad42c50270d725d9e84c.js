(function() {

  jQuery(function($) {
    return $(function() {
      var $hostsDataTable, $hostsTable, hostsPath;
      hostsPath = $('#hosts-path').html();
      $hostsTable = $('#hosts-table');
      $hostsDataTable = $hostsTable.table({
        analysisTab: true,
        controlBarLocation: $('.analysis-control-bar'),
        searchInputHint: 'Search Hosts',
        searchable: true,
        datatableOptions: {
          "oLanguage": {
            "sEmptyTable": "No Hosts are associated with this Project. Click 'New Host' above to create a new one."
          },
          "sAjaxSource": hostsPath,
          "aaSorting": [[11, 'desc']],
          "aoColumns": [
            {
              "mDataProp": "checkbox",
              "bSortable": false
            }, {
              "mDataProp": "address",
              "sWidth": "120px"
            }, {
              "mDataProp": "name"
            }, {
              "mDataProp": "os"
            }, {
              "mDataProp": "virtual",
              "sWidth": "22px"
            }, {
              "mDataProp": "purpose",
              "sWidth": "50px"
            }, {
              "mDataProp": "services",
              "sWidth": "22px"
            }, {
              "mDataProp": "vulns",
              "sWidth": "22px"
            }, {
              "mDataProp": "attempts",
              "sWidth": "22px"
            }, {
              "mDataProp": "tags"
            }, {
              "mDataProp": "updated_at"
            }, {
              "mDataProp": "status",
              "sWidth": "80px"
            }
          ]
        }
      });
      $("#hosts-table_processing").watch('visibility', function() {
        if ($(this).css('visibility') === 'visible') {
          return $hostsTable.css({
            opacity: 0.6
          });
        } else {
          return $hostsTable.css({
            opacity: 1
          });
        }
      });
      return $hostsTable.addCollapsibleSearch();
    });
  });

}).call(this);
