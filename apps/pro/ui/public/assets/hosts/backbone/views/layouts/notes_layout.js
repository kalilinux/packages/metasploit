(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/hosts/layouts/notes.js', '/assets/moment.min.js', '/assets/shared/notification-center/backbone/event_aggregators/event_aggregator.js'], function($, Template, m, EventAggregator) {
    var NotesLayout;
    return NotesLayout = (function(_super) {

      __extends(NotesLayout, _super);

      function NotesLayout() {
        this._loadTable = __bind(this._loadTable, this);

        this._fixJSON = __bind(this._fixJSON, this);

        this.hostNotesURL = __bind(this.hostNotesURL, this);
        return NotesLayout.__super__.constructor.apply(this, arguments);
      }

      NotesLayout.prototype.template = HandlebarsTemplates['hosts/layouts/notes'];

      NotesLayout.prototype.initialize = function(_arg) {
        this.host_id = _arg.host_id;
      };

      NotesLayout.prototype.hostNotesURL = function() {
        return "/hosts/" + this.host_id + "/show_notes.json";
      };

      NotesLayout.prototype.onShow = function() {
        return EventAggregator.on("tabs_layout:change:count", this.render);
      };

      NotesLayout.prototype.onDestroy = function() {
        return EventAggregator.off("tabs_layout:change:count");
      };

      NotesLayout.prototype.onRender = function() {
        return this._loadTable();
      };

      NotesLayout.prototype._fixJSON = function(json) {
        var hash,
          _this = this;
        if (json && _.size(json) > 0 && !_.isString(json) && !_.isNumber(json)) {
          hash = {};
          _.map(json, function(v, k) {
            return hash[k] = _this._fixJSON(v);
          });
          return hash;
        } else {
          try {
            if (_.isString(json)) {
              return $.parseJSON(json || "null");
            } else {
              return json;
            }
          } catch (e) {
            return json;
          }
        }
      };

      NotesLayout.prototype._loadTable = function() {
        var _this = this;
        return helpers.loadRemoteTable({
          el: $('#notes_table', this.el),
          tableName: 'notes',
          columns: {
            ntype: {
              sTitle: 'Name'
            },
            data: {
              bSortable: false,
              sClass: 'break-all info',
              sWidth: "50%",
              fnRender: function(o) {
                if (!_.isString(o.aData.data)) {
                  return JSON.stringify(o.aData.data);
                } else {
                  return o.aData.data;
                }
              }
            },
            updated_at: {
              sTitle: "Updated",
              sWidth: "200px",
              sClass: 'time',
              fnRender: function(o) {
                var time, _ref;
                time = (_ref = o.aData) != null ? _ref.updated_at : void 0;
                return "<span title='" + (_.escape(time)) + "'>" + (_.escape(moment(time).fromNow())) + "</span>";
              }
            }
          },
          dataTable: {
            bFilter: true,
            oLanguage: {
              sEmptyTable: "No Notes were found for this host."
            },
            aaSorting: [[2, 'desc']],
            sAjaxSource: this.hostNotesURL(),
            sPaginationType: "r7Style",
            fnDrawCallback: function() {
              $('table td.time', _this.el).tooltip();
              $(_this.el).trigger('tabload');
              return _.each($('#notes_table', _this.el).find('td.info'), function(td) {
                var $data, d, node;
                try {
                  d = $.parseJSON($(td).text());
                  try {
                    d = _this._fixJSON(d);
                    node = new PrettyJSON.view.Node({
                      el: $(td),
                      level: 0,
                      data: d
                    });
                    $data = $(td).find('.node-container').first();
                    $data.addClass('inline').click(function() {
                      if ($(this).hasClass('inline')) {
                        node.expandAll();
                        return $(this).removeClass('inline');
                      }
                    });
                    return $data.css({
                      'max-width': 'none',
                      width: $(td).width() + 'px',
                      'box-sizing': 'border-box'
                    });
                  } catch (_error) {}
                } catch (_error) {}
              });
            }
          }
        });
      };

      return NotesLayout;

    })(Backbone.Marionette.LayoutView);
  });

}).call(this);
