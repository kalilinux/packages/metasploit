(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/hosts/layouts/modules.js'], function($, Template, RowDropdownLayout, Exploit) {
    var ModulesLayout;
    return ModulesLayout = (function(_super) {

      __extends(ModulesLayout, _super);

      function ModulesLayout() {
        this.hostModulesURL = __bind(this.hostModulesURL, this);

        this.initialize = __bind(this.initialize, this);
        return ModulesLayout.__super__.constructor.apply(this, arguments);
      }

      ModulesLayout.prototype.template = HandlebarsTemplates['hosts/layouts/modules'];

      ModulesLayout.prototype.initialize = function(_arg) {
        this.host_id = _arg.host_id;
      };

      ModulesLayout.prototype.hostModulesURL = function() {
        return "/hosts/" + this.host_id + "/exploits";
      };

      ModulesLayout.prototype.onRender = function() {
        return this._loadTable();
      };

      ModulesLayout.prototype._loadTable = function() {
        var _this = this;
        return $.get(this.hostModulesURL(), function(data) {
          $('.modules', _this.el).html(data);
          return $(_this.el).trigger('tabload');
        });
      };

      return ModulesLayout;

    })(Backbone.Marionette.LayoutView);
  });

}).call(this);
