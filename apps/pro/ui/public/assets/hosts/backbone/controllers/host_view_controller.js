(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/hosts/backbone/views/layouts/host_view_layout.js', '/assets/hosts/backbone/views/item_views/host_stats_overview_item_view.js', '/assets/hosts/backbone/views/layouts/tag_layout.js', '/assets/shared/backbone/layouts/row_dropdown_layout.js', '/assets/shared/backbone/layouts/tabs_layout.js', '/assets/hosts/backbone/views/layouts/services_layout.js', '/assets/hosts/backbone/views/layouts/sessions_layout.js', '/assets/hosts/backbone/views/layouts/vulnerabilities_layout.js', '/assets/hosts/backbone/views/layouts/credentials_layout.js', '/assets/hosts/backbone/views/layouts/captured_data_layout.js', '/assets/hosts/backbone/views/layouts/notes_layout.js', '/assets/hosts/backbone/views/layouts/attempts_layout.js', '/assets/hosts/backbone/views/layouts/modules_layout.js', '/assets/hosts/backbone/views/layouts/history_layout.js', '/assets/hosts/backbone/views/layouts/file_shares_layout.js', '/assets/shared/notification-center/backbone/event_aggregators/event_aggregator.js', '/assets/hosts/backbone/models/single_host_presenter.js', '/assets/hosts/backbone/models/host.js'], function($, HostViewLayout, HostStatsOverviewItemView, TagLayout, RowDropdownLayout, TabsLayout, ServicesLayout, SessionsLayout, VulnerabilitiesLayout, CredentialsLayout, CapturedDataLayout, NotesLayout, AttemptsLayout, ModulesLayout, HistoryLayout, FileSharesLayout, EventAggregator, SingleHostPresenter, Host) {
    var HostViewController;
    return HostViewController = (function(_super) {

      __extends(HostViewController, _super);

      function HostViewController() {
        this._fetchForever = __bind(this._fetchForever, this);

        this._update_url_by_tab = __bind(this._update_url_by_tab, this);

        this._display_tabs = __bind(this._display_tabs, this);

        this.Tab = __bind(this.Tab, this);
        return HostViewController.__super__.constructor.apply(this, arguments);
      }

      HostViewController.POLL_INTERVAL = 10000;

      HostViewController.prototype.initialize = function(_arg) {
        this.id = _arg.id;
        return window.HOST_ID = this.id;
      };

      HostViewController.prototype.Tab = function(tab, region) {
        this.region = region != null ? region : new Backbone.Marionette.Region({
          el: '#host-view'
        });
        this.tab = tab;
        this.show_region({
          region: this.region
        });
        this.show_host_view_layout(tab);
        return $(this.region.el).addClass('tab-loading');
      };

      HostViewController.prototype.show_region = function(_arg) {
        this.region = _arg.region;
        this.row_dropdown = new RowDropdownLayout({
          enabled: false
        });
        return this.region.show(this.row_dropdown);
      };

      HostViewController.prototype.show_host_view_layout = function(tab) {
        var presenter,
          _this = this;
        presenter = new SingleHostPresenter({
          id: this.id
        });
        return presenter.fetch({
          success: function(presenter) {
            $(_this.region.el).removeClass('tab-loading');
            _this._show_header(new Host(presenter.get('host')));
            _this._display_tabs(presenter);
            _this._bind_events();
            return _this._fetchForever(presenter);
          }
        });
      };

      HostViewController.prototype._show_header = function(host) {
        this.row_dropdown.header.show(new HostViewLayout({}));
        this.row_dropdown.header.currentView.host_stats_overview.show(new HostStatsOverviewItemView({
          model: host
        }));
        return this.row_dropdown.header.currentView.tags.show(new TagLayout({
          host: host
        }));
      };

      HostViewController.prototype._display_tabs = function(presenter) {
        var tab_layout, tab_model;
        tab_model = new Backbone.Model({
          tabs: [
            {
              name: 'Services',
              view: ServicesLayout,
              count: presenter.get('tab_counts').services
            }, {
              name: 'Sessions',
              view: SessionsLayout,
              count: presenter.get('tab_counts').sessions
            }, {
              name: 'Vulnerabilities',
              view: VulnerabilitiesLayout,
              count: presenter.get('tab_counts').vulnerabilities
            }, {
              name: 'Credentials',
              view: CredentialsLayout,
              count: presenter.get('tab_counts').credentials
            }, {
              name: 'Captured Data',
              view: CapturedDataLayout,
              count: presenter.get('tab_counts').captured_data
            }, {
              name: 'Notes',
              view: NotesLayout,
              count: presenter.get('tab_counts').notes
            }, {
              name: 'File Shares',
              view: FileSharesLayout,
              count: presenter.get('tab_counts').file_shares,
              hideOnZero: true
            }, {
              name: 'Attempts',
              view: AttemptsLayout,
              count: presenter.get('tab_counts').attempts
            }, {
              name: 'Modules',
              view: ModulesLayout,
              count: presenter.get('tab_counts').modules,
              hideOnZero: true
            }, {
              name: 'History',
              view: HistoryLayout,
              count: presenter.get('tab_counts').history,
              hideOnZero: true
            }
          ]
        });
        tab_layout = new TabsLayout({
          model: tab_model,
          host_id: this.id,
          host_address: presenter.get('host').address
        });
        this.row_dropdown.dropdown.show(tab_layout);
        return tab_layout.set_tab(this.tab || tab_model.get('tabs')[0].name);
      };

      HostViewController.prototype._bind_events = function() {
        return EventAggregator.on('tabs_layout:tab:changed', this._update_url_by_tab);
      };

      HostViewController.prototype._update_url_by_tab = function(last_tab) {
        last_tab = _.string.underscored(last_tab).toLowerCase();
        if (window.HostViewAppRouter != null) {
          return window.HostViewAppRouter.navigate(last_tab, {
            trigger: false
          });
        } else {
          return Pro.vent.trigger("host:tab:chose", last_tab);
        }
      };

      HostViewController.prototype._fetchForever = function(model) {
        var fetchAgain,
          _this = this;
        fetchAgain = function() {
          return window.setTimeout((function() {
            return _this._fetchForever(model);
          }), HostViewController.POLL_INTERVAL);
        };
        return model.fetch({
          success: fetchAgain,
          error: fetchAgain
        });
      };

      return HostViewController;

    })(Backbone.Marionette.Controller);
  });

}).call(this);
