(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/item_views/result_page_header_item_view.js'], function(Template) {
    var ResultPageHeaderItemView;
    return ResultPageHeaderItemView = (function(_super) {

      __extends(ResultPageHeaderItemView, _super);

      function ResultPageHeaderItemView() {
        return ResultPageHeaderItemView.__super__.constructor.apply(this, arguments);
      }

      ResultPageHeaderItemView.prototype.template = HandlebarsTemplates['fuzzing/item_views/result_page_header_item_view'];

      return ResultPageHeaderItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
