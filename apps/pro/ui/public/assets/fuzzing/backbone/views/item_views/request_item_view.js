(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/item_views/request_item_view.js'], function(Template) {
    var RequestItemView;
    return RequestItemView = (function(_super) {

      __extends(RequestItemView, _super);

      function RequestItemView() {
        return RequestItemView.__super__.constructor.apply(this, arguments);
      }

      RequestItemView.prototype.template = HandlebarsTemplates['fuzzing/item_views/request_item_view'];

      return RequestItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
