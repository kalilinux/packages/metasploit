(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/item_views/replayer_parameters_item_view.js'], function() {
    var ReplayerParametersItemView;
    return ReplayerParametersItemView = (function(_super) {

      __extends(ReplayerParametersItemView, _super);

      function ReplayerParametersItemView() {
        return ReplayerParametersItemView.__super__.constructor.apply(this, arguments);
      }

      ReplayerParametersItemView.prototype.template = HandlebarsTemplates['fuzzing/item_views/replayer_parameters_item_view'];

      return ReplayerParametersItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
