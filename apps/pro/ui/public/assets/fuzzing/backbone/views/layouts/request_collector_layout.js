(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/layouts/request_collector_layout.js', '/assets/fuzzing/backbone/views/item_views/request_item_view.js', '/assets/fuzzing/backbone/views/item_views/request_input_item_view.js', '/assets/fuzzing/backbone/views/collection_views/request_collection_view.js', '/assets/fuzzing/backbone/collections/request_group_collection.js'], function(Template, RequestItemView, RequestInputItemView, RequestCollectionView, RequestGroupCollection) {
    var RequestCollectorLayout;
    return RequestCollectorLayout = (function(_super) {

      __extends(RequestCollectorLayout, _super);

      function RequestCollectorLayout() {
        return RequestCollectorLayout.__super__.constructor.apply(this, arguments);
      }

      RequestCollectorLayout.prototype.template = HandlebarsTemplates['fuzzing/layouts/request_collector_layout'];

      RequestCollectorLayout.prototype.regions = {
        requests: '.requests',
        request_input: '.request-input'
      };

      RequestCollectorLayout.prototype.onRender = function() {
        this._initData();
        return this._showRequestInput();
      };

      RequestCollectorLayout.prototype._showRequestInput = function() {
        this.request_input_item_view = new RequestInputItemView;
        this.requests.show(this.request_collection_view);
        return this.request_input.show(this.request_input_item_view);
      };

      RequestCollectorLayout.prototype._initData = function() {
        this.request_collection = RequestGroupCollection;
        this.request_collection.workspace_id = 1;
        this.request_collection.fuzzing_id = 3;
        this.request_collection.fetch;
        return this.request_collection_view = new RequestCollectionView({
          itemView: RequestItemView,
          collection: this.request_collection
        });
      };

      return RequestCollectorLayout;

    })(Backbone.Marionette.LayoutView);
  });

}).call(this);
