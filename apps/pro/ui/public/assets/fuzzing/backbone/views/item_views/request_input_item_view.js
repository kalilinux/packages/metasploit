(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/fuzzing/item_views/request_input_item_view.js', '/assets/fuzzing/backbone/collections/request_group_collection.js', '/assets/fuzzing/backbone/models/request_group_model.js', '/assets/fuzzing/backbone/event_aggregator.js'], function($, Template, RequestGroupCollection, RequestGroupModel, EventAggregator) {
    var RequestInputView;
    return RequestInputView = (function(_super) {

      __extends(RequestInputView, _super);

      function RequestInputView() {
        this._save_collection = __bind(this._save_collection, this);

        this.iframeRequest = __bind(this.iframeRequest, this);
        return RequestInputView.__super__.constructor.apply(this, arguments);
      }

      RequestInputView.prototype.template = HandlebarsTemplates['fuzzing/item_views/request_input_item_view'];

      RequestInputView.prototype.triggers = {
        'click .row button': 'iframe:request'
      };

      RequestInputView.prototype.initialize = function() {
        this.on('iframe:request', this.iframeRequest);
        return this.request_group_collection = RequestGroupCollection;
      };

      RequestInputView.prototype.iframeRequest = function() {
        var attrs, proxy_url,
          _this = this;
        proxy_url = $('textarea', this.el).val();
        this.request_group = new RequestGroupModel({
          proxy_url: proxy_url
        });
        attrs = {
          workspace_id: 1,
          fuzzing_id: 3
        };
        return this.request_group.save(attrs, {
          success: function(model, response, options) {
            return _this._save_collection();
          },
          error: function(model, response, options) {
            return console.log(e);
          }
        });
      };

      RequestInputView.prototype._save_collection = function() {
        this.request_group_collection.add(this.request_group);
        return EventAggregator.trigger('iframe:update:url');
      };

      return RequestInputView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
