(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/item_views/replayer_target_browsers_item_view.js'], function(Template) {
    var ReplayerTargetBrowsersItemView;
    return ReplayerTargetBrowsersItemView = (function(_super) {

      __extends(ReplayerTargetBrowsersItemView, _super);

      function ReplayerTargetBrowsersItemView() {
        return ReplayerTargetBrowsersItemView.__super__.constructor.apply(this, arguments);
      }

      ReplayerTargetBrowsersItemView.prototype.template = HandlebarsTemplates['fuzzing/item_views/replayer_target_browsers_item_view'];

      return ReplayerTargetBrowsersItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
