(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/item_views/fuzzing_attack_header_item_view.js'], function(Template) {
    var FuzzingAttackHeaderItemView;
    return FuzzingAttackHeaderItemView = (function(_super) {

      __extends(FuzzingAttackHeaderItemView, _super);

      function FuzzingAttackHeaderItemView() {
        return FuzzingAttackHeaderItemView.__super__.constructor.apply(this, arguments);
      }

      FuzzingAttackHeaderItemView.prototype.template = HandlebarsTemplates['fuzzing/item_views/fuzzing_attack_header_item_view'];

      return FuzzingAttackHeaderItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
