(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/templates/fuzzing/layouts/fuzzing_xss_attack_layout.js', '/assets/fuzzing/backbone/views/item_views/request_item_view.js', '/assets/fuzzing/backbone/views/item_views/replayer_parameters_item_view.js', '/assets/fuzzing/backbone/views/item_views/replayer_target_browsers_item_view.js'], function(Template, RequestItemView, ReplayerParametersItemView, ReplayerTargetBrowsersItemView) {
    var FuzzingXSSAttackLayout;
    return FuzzingXSSAttackLayout = (function(_super) {

      __extends(FuzzingXSSAttackLayout, _super);

      function FuzzingXSSAttackLayout() {
        return FuzzingXSSAttackLayout.__super__.constructor.apply(this, arguments);
      }

      FuzzingXSSAttackLayout.prototype.template = HandlebarsTemplates['fuzzing/layouts/fuzzing_xss_attack_layout'];

      FuzzingXSSAttackLayout.prototype.regions = {
        request: '.request',
        parameters: '.parameters',
        target_browsers: '.target-browsers',
        xss_crafter: '.xss-crafter'
      };

      FuzzingXSSAttackLayout.prototype.onRender = function() {
        this.request.show(new RequestItemView);
        this.parameters.show(new ReplayerParametersItemView);
        return this.target_browsers.show(new ReplayerTargetBrowsersItemView);
        /*
              @.xss_crafter.show(new XssCrafterItemView)
        */

      };

      return FuzzingXSSAttackLayout;

    })(Backbone.Marionette.LayoutView);
  });

}).call(this);
