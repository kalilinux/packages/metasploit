(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/fuzzing/item_views/fuzzing_iframe_item_view.js', '/assets/fuzzing/backbone/event_aggregator.js', '/assets/fuzzing/backbone/collections/request_group_collection.js'], function($, Template, EventAggregator, RequestGroupCollection) {
    var FuzzingIframeItemView;
    return FuzzingIframeItemView = (function(_super) {

      __extends(FuzzingIframeItemView, _super);

      function FuzzingIframeItemView() {
        this._update_iframe_url = __bind(this._update_iframe_url, this);
        return FuzzingIframeItemView.__super__.constructor.apply(this, arguments);
      }

      FuzzingIframeItemView.prototype.template = HandlebarsTemplates['fuzzing/item_views/fuzzing_iframe_item_view'];

      FuzzingIframeItemView.prototype.initialize = function() {
        this.request_group_collection = RequestGroupCollection;
        return EventAggregator.on('iframe:update:url', this._update_iframe_url);
      };

      FuzzingIframeItemView.prototype._update_iframe_url = function() {
        var request_id;
        request_id = this.request_group_collection.at(this.request_group_collection.length - 1).get("id");
        return $('iframe', this.el).attr("src", "http://localhost:3791/" + request_id);
      };

      return FuzzingIframeItemView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
