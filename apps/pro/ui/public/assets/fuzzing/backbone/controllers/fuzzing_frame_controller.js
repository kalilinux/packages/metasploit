(function() {

  define(['/assets/fuzzing/backbone/views/regions/app_region.js', '/assets/fuzzing/backbone/views/layouts/fuzzing_frame_layout.js', '/assets/fuzzing/backbone/views/layouts/request_collector_layout.js', '/assets/fuzzing/backbone/views/item_views/result_page_header_item_view.js', '/assets/fuzzing/backbone/views/item_views/fuzzing_iframe_item_view.js', '/assets/fuzzing/backbone/views/layouts/fuzzing_xss_attack_layout.js', '/assets/fuzzing/backbone/views/item_views/fuzzing_attack_header_item_view.js'], function(AppRegion, FuzzingFrameLayout, RequestCollectorLayout, ResultPageHeaderItemView, FuzzingIframeItemView, FuzzingXSSAttackLayout, FuzzingAttackHeaderItemView) {
    var FuzzingFrameController;
    return FuzzingFrameController = (function() {

      function FuzzingFrameController() {
        this.app_region = new AppRegion;
        this.fuzzing_frame_layout = new FuzzingFrameLayout;
        this.app_region.show(this.fuzzing_frame_layout);
      }

      FuzzingFrameController.prototype._layout_region_reset = function() {
        this.fuzzing_frame_layout.top_row.reset();
        this.fuzzing_frame_layout.middle_row.reset();
        return this.fuzzing_frame_layout.bottom_row.reset();
      };

      FuzzingFrameController.prototype._initFuzzzingFrame = function() {
        this.fuzzing_xss_attack_layout = new FuzzingXSSAttackLayout;
        this.fuzzing_iframe_item_view = new FuzzingIframeItemView;
        return this.fuzzing_attack_header_item_view = new FuzzingAttackHeaderItemView;
      };

      FuzzingFrameController.prototype._initRequestCollector = function() {
        this.request_collector_layout = new RequestCollectorLayout;
        this.result_page_header_item_view = new ResultPageHeaderItemView;
        return this.fuzzing_iframe_item_view = new FuzzingIframeItemView;
      };

      FuzzingFrameController.prototype.showFuzzingFrame = function() {
        this._initFuzzzingFrame();
        this._layout_region_reset();
        this.fuzzing_frame_layout.top_row.show(this.fuzzing_xss_attack_layout);
        this.fuzzing_frame_layout.middle_row.show(this.fuzzing_attack_header_item_view);
        return this.fuzzing_frame_layout.bottom_row.show(this.fuzzing_iframe_item_view);
      };

      FuzzingFrameController.prototype.showRequestCollector = function() {
        this._initRequestCollector();
        this._layout_region_reset();
        this.fuzzing_frame_layout.top_row.show(this.request_collector_layout);
        this.fuzzing_frame_layout.middle_row.show(this.result_page_header_item_view);
        return this.fuzzing_frame_layout.bottom_row.show(this.fuzzing_iframe_item_view);
      };

      return FuzzingFrameController;

    })();
  });

}).call(this);
