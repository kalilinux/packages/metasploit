(function() {
  var RequireConfig;

  RequireConfig = (function() {

    function RequireConfig() {
      this.fuzzing_require = requirejs.config({
        context: "app",
        paths: {
          jquery: '/assets/shared/backbone/jquery-require-bootstrap'
        }
      });
      this.fuzzing_require(['jquery', '/assets/fuzzing/app.js'], function($, App) {
        var app;
        app = new App;
        return app.start();
      });
    }

    return RequireConfig;

  })();

  new RequireConfig();

}).call(this);
