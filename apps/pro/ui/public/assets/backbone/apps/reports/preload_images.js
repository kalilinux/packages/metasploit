(function() {
  var PRELOADED_REPORT_IMAGES, PRELOADED_RETINA_REPORT_IMAGES, RetinaDetector, reportImages;

  RetinaDetector = {
    useRetinaImages: function() {
      return this.isHighDensity() || this.isRetina();
    },
    isHighDensity: function() {
      return (window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3);
    },
    isRetina: function() {
      return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 2)) && /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
    }
  };

  PRELOADED_REPORT_IMAGES = ["/assets/reports/formats/word-icon-not-generated.png", "/assets/reports/formats/pdf-icon.png", "/assets/reports/formats/xml-icon.png", "/assets/reports/formats/word-icon.png", "/assets/reports/formats/rtf-icon.png", "/assets/reports/formats/html-icon.png", "/assets/reports/formats/xml-icon-regenerate.png", "/assets/reports/formats/html-icon-not-generated.png", "/assets/reports/formats/rtf-icon-not-generated.png", "/assets/reports/formats/xml-icon-not-generated.png", "/assets/reports/formats/rtf-icon-regenerate.png", "/assets/reports/formats/pdf-icon-regenerate.png", "/assets/reports/formats/html-icon-regenerate.png", "/assets/reports/formats/pdf-icon-not-generated.png", "/assets/reports/formats/word-icon-regenerate.png", "/assets/reports/actions/email-action-icon.png", "/assets/reports/actions/destroy-action-icon-hover.png", "/assets/reports/actions/destroy-action-icon.png", "/assets/reports/actions/email-action-icon-hover.png", "/assets/reports/actions/download-action-icon-hover.png", "/assets/reports/actions/download-action-icon.png"];

  PRELOADED_RETINA_REPORT_IMAGES = ["/assets/reports/formats/word-icon@2x.png", "/assets/reports/formats/word-icon-not-generated@2x.png", "/assets/reports/formats/html-icon@2x.png", "/assets/reports/formats/html-icon-not-generated@2x.png", "/assets/reports/formats/pdf-icon@2x.png", "/assets/reports/formats/xml-icon-not-generated@2x.png", "/assets/reports/formats/pdf-icon-not-generated@2x.png", "/assets/reports/formats/pdf-icon-regenerate@2x.png", "/assets/reports/formats/rtf-icon@2x.png", "/assets/reports/formats/xml-icon@2x.png", "/assets/reports/formats/html-icon-regenerate@2x.png", "/assets/reports/formats/xml-icon-regenerate@2x.png", "/assets/reports/formats/rtf-icon-regenerate@2x.png", "/assets/reports/formats/rtf-icon-not-generated@2x.png", "/assets/reports/formats/word-icon-regenerate@2x.png", "/assets/reports/actions/destroy-action-icon@2x.png", "/assets/reports/actions/download-action-icon-hover@2x.png", "/assets/reports/actions/email-action-icon@2x.png", "/assets/reports/actions/email-action-icon-hover@2x.png", "/assets/reports/actions/download-action-icon@2x.png", "/assets/reports/actions/destroy-action-icon-hover@2x.png"];

  reportImages = RetinaDetector.useRetinaImages() ? PRELOADED_RETINA_REPORT_IMAGES : PRELOADED_REPORT_IMAGES;

  jQuery('body').append("<div id='preload-images' style='position: absolute; left: -100000px;'></div>");

  _.each(reportImages, function(src) {
    return jQuery('#preload-images').append("<img src='" + src + "'>");
  });

}).call(this);
