(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['apps/hosts/hosts_app', 'lib/utilities/navigation'], function() {
    return this.Pro.module('VulnsApp', function(VulnsApp, App) {
      var API,
        _this = this;
      VulnsApp.Router = (function(_super) {

        __extends(Router, _super);

        function Router() {
          return Router.__super__.constructor.apply(this, arguments);
        }

        Router.prototype.appRoutes = {
          "": "show",
          "vulns/:id": "show"
        };

        return Router;

      })(Marionette.AppRouter);
      API = {
        show: function(id) {
          var loading,
            _this = this;
          loading = true;
          _.delay((function() {
            if (loading) {
              return App.execute('loadingOverlay:show');
            }
          }), 50);
          return initProRequire(['apps/vulns/show/show_controller', 'css!css/vulns'], function(ShowController) {
            var controller;
            loading = false;
            App.execute('loadingOverlay:hide');
            return controller = new VulnsApp.Show.Controller({
              id: id != null ? id : window.VULN_ID,
              workspace_id: window.WORKSPACE_ID
            });
          });
        }
      };
      App.addInitializer(function() {
        return new VulnsApp.Router({
          controller: API
        });
      });
      return App.addRegions({
        mainRegion: "#vulns-main-region"
      });
    });
  });

}).call(this);
