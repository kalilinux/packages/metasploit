(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['lib/utilities/navigation', 'apps/imports/index/index_controller'], function() {
    return this.Pro.module('ImportsApp', function(ImportsApp, App) {
      var API;
      ImportsApp.Router = (function(_super) {

        __extends(Router, _super);

        function Router() {
          return Router.__super__.constructor.apply(this, arguments);
        }

        Router.prototype.appRoutes = {
          "": "import"
        };

        return Router;

      })(Marionette.AppRouter);
      API = {
        "import": function() {
          return new ImportsApp.Index.Controller();
        }
      };
      App.addInitializer(function() {
        return new ImportsApp.Router({
          controller: API
        });
      });
      return App.addRegions({
        mainRegion: "#imports-main-region"
      });
    });
  });

}).call(this);
