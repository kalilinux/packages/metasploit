(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['base_layout', 'base_view', 'base_itemview', 'apps/imports/index/templates/index_layout'], function() {
    return this.Pro.module('ImportsApp.Index', function(Index, App, Backbone, Marionette, $, _) {
      return Index.Layout = (function(_super) {

        __extends(Layout, _super);

        function Layout() {
          this._importTypeChanged = __bind(this._importTypeChanged, this);
          return Layout.__super__.constructor.apply(this, arguments);
        }

        Layout.prototype.template = Layout.prototype.templatePath('imports/index/index_layout');

        Layout.prototype.className = 'foundation';

        Layout.prototype.ui = {
          importBtn: '.import-btn',
          tagsLabel: '.tags-label',
          tagsPane: '.tags-pane',
          expandArrow: 'span.expand',
          collapseArrow: 'span.collapse',
          autoTagOs: '[name="imports[autotag_os]"]',
          importType: '[name="imports[type]"]',
          selectedImportType: '[name="imports[type]"]:checked',
          preserveHosts: '#update_hosts'
        };

        Layout.prototype.events = {
          'click @ui.tagsLabel': '_toggleTags',
          'change @ui.importType': '_importTypeChanged'
        };

        Layout.prototype.triggers = {
          'click @ui.importBtn': 'import:start'
        };

        Layout.prototype.regions = {
          nexposeImportRegion: '.nexpose-import-region',
          tagsRegion: '.tags-region'
        };

        Layout.prototype._importTypeChanged = function() {
          this._bindUIElements();
          return this.trigger('import:typeChange', {
            view: this
          });
        };

        Layout.prototype._toggleTags = function() {
          this.ui.expandArrow.toggle();
          this.ui.collapseArrow.toggle();
          return this.ui.tagsPane.slideToggle('fast');
        };

        Layout.prototype.enableImportButton = function() {
          return this.ui.importBtn.removeClass('disabled');
        };

        Layout.prototype.disableImportButton = function() {
          return this.ui.importBtn.addClass('disabled');
        };

        Layout.prototype.isFileImport = function() {
          return this.ui.selectedImportType.val() === 'import-from-file';
        };

        Layout.prototype.expandTagSection = function() {
          this.ui.expandArrow.hide();
          this.ui.collapseArrow.show();
          return this.ui.tagsPane.show();
        };

        return Layout;

      })(App.Views.Layout);
    });
  });

}).call(this);
