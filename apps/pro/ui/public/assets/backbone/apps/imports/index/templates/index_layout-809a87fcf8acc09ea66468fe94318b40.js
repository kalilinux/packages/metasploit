(function() { this.JST || (this.JST = {}); this.JST["backbone/apps/imports/index/templates/index_layout"] = function(__obj) {
    if (!__obj) __obj = {};
    var __out = [], __capture = function(callback) {
      var out = __out, result;
      __out = [];
      callback.call(this);
      result = __out.join('');
      __out = out;
      return __safe(result);
    }, __sanitize = function(value) {
      if (value && value.ecoSafe) {
        return value;
      } else if (typeof value !== 'undefined' && value != null) {
        return __escape(value);
      } else {
        return '';
      }
    }, __safe, __objSafe = __obj.safe, __escape = __obj.escape;
    __safe = __obj.safe = function(value) {
      if (value && value.ecoSafe) {
        return value;
      } else {
        if (!(typeof value !== 'undefined' && value != null)) value = '';
        var result = new String(value);
        result.ecoSafe = true;
        return result;
      }
    };
    if (!__escape) {
      __escape = __obj.escape = function(value) {
        return ('' + value)
          .replace(/&/g, '&amp;')
          .replace(/</g, '&lt;')
          .replace(/>/g, '&gt;')
          .replace(/"/g, '&quot;');
      };
    }
    (function() {
      (function() {
      
        __out.push('<fieldset class="shared">\n  <legend><span>Import Data</span></legend>\n\n\n  <div class="row v-padding">\n    <div class="large-4 columns ');
      
        if (!this.showTypeSelection) {
          __out.push(__sanitize('hidden'));
        }
      
        __out.push('">\n      <div class="columns large-6">\n        <input type="radio" id="import-from-nexpose" name="imports[type]" checked="checked" value="import-from-nexpose"/><label for="import-from-nexpose">From Nexpose</label>\n      </div>\n\n      <div class="large-6 columns">\n        <input type="radio" id="import-from-file" name="imports[type]" value="import-from-file"/><label for="import-from-file">From file</label>\n      </div>\n\n    </div>\n\n    <div class="large-8 columns"></div>\n  </div>\n\n  <div class="row nexpose-import-region"></div>\n\n  <div class="row">\n    <div class="row tags-label">\n      <div class="columns small-2">\n          Automatic Tagging (Optional) <span class="collapse">▲</span><span class="expand">▼</span>\n      </div>\n      <div class="columns small-8 empty"></div>\n    </div>\n\n    <div class="row tags-pane">\n      <div class="row">\n        <div class="columns small-1 add-tags">Add Tags</div>\n        <div class="columns small-3 tags-region"></div>\n        <div class="columns small-8 empty"></div>\n      </div>\n\n      <div class="row">\n        <div class="columns small-1 empty"></div>\n        <div class="columns small-2">\n          <label>\n            <input type="checkbox" name="imports[autotag_os]">\n            Automatically Tag by OS\n          </label>\n        </div>\n        <div class="columns small-9 empty"></div>\n      </div>\n    </div>\n\n\n\n  </div>\n\n</fieldset>\n\n<div class="row">\n  <div class="columns large-12">\n    <div class="columns large-9 empty">\n\n    </div>\n\n    <div class="columns large-2 align-right">\n      <input id="update_hosts" type="checkbox" name="import[update_hosts]" ><label for="update_hosts">Don\'t change existing hosts</label>\n    </div>\n\n\n    ');
      
        if (this.showTypeSelection) {
          __out.push('\n      <div class="columns large-1 align-right">\n      <span class="btn disabled import-btn">\n        <input class="import" id="popup_submit" name="commit" type="submit" value="Import Data">\n      </span>\n      </div>\n    ');
        }
      
        __out.push('\n\n  </div>\n\n</div>\n');
      
      }).call(this);
      
    }).call(__obj);
    __obj.safe = __objSafe, __obj.escape = __escape;
    return __out.join('');
  };
}).call(this);
