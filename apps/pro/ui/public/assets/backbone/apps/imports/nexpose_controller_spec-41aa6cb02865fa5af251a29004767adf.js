(function() {

  define(['jquery', 'apps/imports/nexpose/nexpose_controller'], function($) {
    return describe('ImportsApp.Nexpose.Controller', function() {
      set('consoleData', function() {
        return {
          "address": "mspnexpose1.ms.scanlab.rapid7.com",
          "cached_sites": {},
          "cert": null,
          "created_at": "2015-05-15T13:45:43-05:00",
          "enabled": true,
          "id": 4,
          "name": "Test 3",
          "owner": "farias",
          "password": "msfadmin",
          "port": 3780,
          "status": "Initializing",
          "updated_at": "2015-05-15T13:45:43-05:00",
          "username": "msfadmin",
          "version": null,
          "connection_success": true
        };
      });
      set('regionEl', function() {
        return $("<div />").appendTo($('body'))[0];
      });
      set('region', function() {
        return new Backbone.Marionette.Region({
          el: regionEl
        });
      });
      beforeEach(function() {
        this.server = sinon.fakeServer.create();
        return this.server.respondWith(/^\/nexpose_consoles\.json$/, [
          200, {
            "Content-Type": "application/json"
          }, JSON.stringify(consoleData)
        ]);
      });
      afterEach(function() {
        this.server.restore();
        return region.reset();
      });
      return it('renders the view', function() {
        var controller;
        controller = new Pro.ImportsApp.Nexpose.Controller({});
        this.server.respond();
        region.show(controller._mainView);
        return expect($(regionEl).children().length).toBeGreaterThan(0);
      });
    });
  });

}).call(this);
