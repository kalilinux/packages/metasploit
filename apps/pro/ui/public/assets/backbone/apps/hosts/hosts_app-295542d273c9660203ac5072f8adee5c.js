(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define([], function() {
    return this.Pro.module('HostsApp', function(HostsApp, App) {
      var API,
        _this = this;
      HostsApp.Router = (function(_super) {

        __extends(Router, _super);

        function Router() {
          return Router.__super__.constructor.apply(this, arguments);
        }

        Router.prototype.appRoutes = {
          "hosts/:host_id": "show",
          "hosts/:host_id/:tab": "show"
        };

        return Router;

      })(Marionette.AppRouter);
      API = {
        show: function(host_id, tab) {
          var loading,
            _this = this;
          loading = true;
          _.delay((function() {
            if (loading) {
              return App.execute('loadingOverlay:show');
            }
          }), 50);
          return initProRequire(['/assets/hosts/backbone/controllers/host_view_controller.js'], function(HostViewController) {
            var hostController;
            loading = false;
            App.execute('loadingOverlay:hide');
            hostController = new HostViewController({
              id: host_id
            });
            return hostController.Tab(tab != null ? tab : 'services', App.mainRegion);
          });
        }
      };
      return App.addInitializer(function() {
        return new HostsApp.Router({
          controller: API
        });
      });
    });
  });

}).call(this);
