(function() {

  define([], function() {
    return this.Pro.module("Concerns", function(Concerns, App, Backbone, Marionette, $, _) {
      return Concerns.VulnAttemptStatuses = {
        STATUSES: {
          EXPLOITED: 'Exploited'
        },
        isExploited: function() {
          return this.get('status') === this.STATUSES.EXPLOITED || this.get('vuln_attempt_status') === this.STATUSES.EXPLOITED;
        }
      };
    });
  });

}).call(this);
