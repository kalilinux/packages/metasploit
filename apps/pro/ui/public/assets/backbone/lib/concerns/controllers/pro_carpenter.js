(function() {

  define(['lib/components/table/table_view'], function() {
    return this.Pro.module("Concerns", function(Concerns, App, Backbone, Marionette, $, _) {
      return Concerns.ProCarpenter = {
        initialize: function() {
          var _this = this;
          if (this.filterOpts) {
            this.initializeFilter();
          }
          if (this["static"]) {
            this.collection.bootstrap();
          }
          return this.carpenterRadio.on('error:search', function(message) {
            return App.execute('flash:display', {
              title: 'Error in search',
              style: 'error',
              message: message || 'There is an error in your search terms.'
            });
          });
        },
        initializeFilter: function() {
          var _this = this;
          if (this.search) {
            this.fetch = false;
            this.filterOpts.query = this.addWhiteSpaceToQuery(this.search);
          }
          this.staticFacets = this.filterOpts.staticFacets;
          this.filter = App.request('filter:component', this);
          if (this.search) {
            this.applyCustomFilter(this.search);
          }
          this.listenTo(this.filter, 'filter:query:new', function(query) {
            _this.toggleInteraction(false);
            return _this.applyCustomFilter(query);
          });
          return this.show(this.filter, {
            region: this.filterOpts.filterRegion || this.getMainView().filterRegion
          });
        },
        applyCustomFilter: function(query) {
          return this.list.setSearch({
            attributes: {
              custom_query: query
            }
          });
        },
        addWhiteSpaceToQuery: function(query) {
          return query.replace(/([^:]):([^:\s])/g, "$1: $2");
        }
      };
    });
  });

}).call(this);
