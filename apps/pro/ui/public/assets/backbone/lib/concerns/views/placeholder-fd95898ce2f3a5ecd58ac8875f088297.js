(function() {

  define(['css!css/concerns/placeholder'], function() {
    return this.Pro.module("Concerns", function(Concerns, App, Backbone, Marionette, $, _) {
      return Concerns.Placeholder = {
        events: {
          'focusout @ui.textArea': '_focusOutTextArea',
          'focusin @ui.textArea': '_focusInTextArea'
        },
        onShow: function() {
          this.ui.textArea.val(this.ui.textArea.data('placeholder'));
          return this.ui.textArea.addClass('placeholder');
        },
        _focusOutTextArea: function(e) {
          var _ref;
          if (((_ref = this.ui.textArea.val().match(/^$|^\s%/)) != null ? _ref.length : void 0) > 0) {
            this.ui.textArea.val(this.ui.textArea.data('placeholder'));
            return this.ui.textArea.addClass('placeholder');
          }
        },
        _focusInTextArea: function(e) {
          if (this.ui.textArea.data('placeholder') === this.ui.textArea.val()) {
            this.ui.textArea.removeClass('placeholder');
            return this.ui.textArea.val('');
          }
        }
      };
    });
  });

}).call(this);
