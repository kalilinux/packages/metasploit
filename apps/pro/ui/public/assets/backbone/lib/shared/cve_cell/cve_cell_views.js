(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', 'base_itemview', 'lib/shared/cve_cell/templates/view', 'lib/shared/cve_cell/templates/modal_view'], function($) {
    return this.Pro.module("Shared.CveCell", function(CveCell, App, Backbone, Marionette, $, _) {
      CveCell.View = (function(_super) {

        __extends(View, _super);

        function View() {
          return View.__super__.constructor.apply(this, arguments);
        }

        View.prototype.template = View.prototype.templatePath("cve_cell/view");

        View.prototype.className = 'shared cve-cell';

        View.prototype.triggers = {
          'click a': 'refs:clicked'
        };

        return View;

      })(App.Views.Layout);
      return CveCell.ModalView = (function(_super) {

        __extends(ModalView, _super);

        function ModalView() {
          return ModalView.__super__.constructor.apply(this, arguments);
        }

        ModalView.prototype.template = ModalView.prototype.templatePath("cve_cell/modal_view");

        ModalView.prototype.className = 'shared cve-cell modal-content';

        return ModalView;

      })(App.Views.ItemView);
    });
  });

}).call(this);
