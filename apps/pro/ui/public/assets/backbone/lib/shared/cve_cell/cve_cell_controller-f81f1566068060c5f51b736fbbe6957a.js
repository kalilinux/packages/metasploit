(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['base_controller', 'entities/module_detail', 'lib/components/modal/modal_controller', 'lib/shared/cve_cell/cve_cell_views', 'css!css/shared/cve_cell'], function() {
    return this.Pro.module("Shared.CveCell", function(CveCell, App) {
      CveCell.Controller = (function(_super) {

        __extends(Controller, _super);

        function Controller() {
          return Controller.__super__.constructor.apply(this, arguments);
        }

        Controller.prototype.defaults = function() {
          return {};
        };

        Controller.prototype.initialize = function(options) {
          var config, view,
            _this = this;
          if (options == null) {
            options = {};
          }
          config = _.defaults(options, this._getDefaults());
          this.model = new Backbone.Model(config).get('model');
          view = new CveCell.View();
          _.extend(this.model, {
            mutators: {
              parsedRefs: {
                get: function() {
                  return this.get('ref_names');
                }
              },
              refCount: {
                get: function() {
                  return this.get('ref_count');
                }
              }
            }
          });
          view.model = this.model;
          this.setMainView(view);
          return this.listenTo(this._mainView, 'refs:clicked', function() {
            var dialogView, moduleDetail;
            moduleDetail = App.request('module:detail:entity', {
              id: _this.model.id,
              refsOnly: true
            });
            dialogView = new CveCell.ModalView({
              model: moduleDetail
            });
            moduleDetail.fetch();
            return App.execute('showModal', dialogView, {
              modal: {
                title: 'References',
                description: '',
                width: 260,
                height: 300
              },
              buttons: [
                {
                  name: 'Close',
                  "class": 'close'
                }
              ],
              loading: true
            });
          });
        };

        return Controller;

      })(App.Controllers.Application);
      return App.reqres.setHandler('cveCell:component', function(options) {
        if (options == null) {
          options = {};
        }
        return new CveCell.Controller(options);
      });
    });
  });

}).call(this);
