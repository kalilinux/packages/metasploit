(function() {
  var modalViewPaths,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  modalViewPaths = {
    "domino": "/assets/domino/domino.js",
    "firewall_egress": "/assets/firewall_egress/firewall_egress.js",
    "ssh_key": "/assets/apps/backbone/views/modal_views/ssh_key.js",
    "single_password": "/assets/apps/backbone/views/modal_views/single_password.js",
    "passive_network": "/assets/apps/backbone/views/modal_views/passive_network.js",
    "pass_the_hash": "/assets/apps/backbone/views/modal_views/pass_the_hash.js",
    "credential_intrusion": "/assets/apps/backbone/views/modal_views/credential_intrusion.js"
  };

  define(['jquery', '/assets/templates/apps/views/card_view.js'], function($, Template) {
    var CardView;
    return CardView = (function(_super) {

      __extends(CardView, _super);

      function CardView() {
        this.launchClicked = __bind(this.launchClicked, this);
        return CardView.__super__.constructor.apply(this, arguments);
      }

      CardView.prototype.template = HandlebarsTemplates['apps/views/card_view'];

      CardView.prototype.events = {
        'click .primary.btn': 'launchClicked',
        'click a.title': 'launchClicked'
      };

      CardView.prototype.launchClicked = function(e) {
        var path;
        e.preventDefault();
        path = modalViewPaths[this.model.get('symbol')];
        return this.loadAssets(path);
      };

      CardView.prototype.loadAssets = function(pathToLoad) {
        var rjs;
        rjs = requirejs.config({
          context: "app"
        });
        return rjs([pathToLoad], function(ModalView) {
          return new ModalView({
            el: $('#modals')
          }).open();
        });
      };

      return CardView;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
