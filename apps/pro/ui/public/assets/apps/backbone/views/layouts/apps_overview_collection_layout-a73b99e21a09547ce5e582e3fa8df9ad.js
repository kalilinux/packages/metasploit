(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['/assets/apps/backbone/views/collection_views/cards_collection_view.js', '/assets/templates/apps/layouts/apps_overview_collection_layout.js', '/assets/apps/backbone/views/filter_view.js', '/assets/apps/backbone/models/app.js', 'jquery'], function(CardsCollectionView, Template, FilterView, App, $) {
    var AppsOverviewCollectionLayout;
    return AppsOverviewCollectionLayout = (function(_super) {

      __extends(AppsOverviewCollectionLayout, _super);

      function AppsOverviewCollectionLayout() {
        return AppsOverviewCollectionLayout.__super__.constructor.apply(this, arguments);
      }

      AppsOverviewCollectionLayout.prototype.template = HandlebarsTemplates['apps/layouts/apps_overview_collection_layout'];

      AppsOverviewCollectionLayout.prototype.regions = {
        filtersArea: '.filters',
        collectionArea: '.collection'
      };

      AppsOverviewCollectionLayout.prototype.renderRegions = function() {
        var appData, apps, collection;
        appData = $.parseJSON($('meta[name=apps]').attr('content'));
        apps = _.map(appData, function(attrs) {
          return new App(attrs);
        });
        collection = new Backbone.Collection(apps, {
          model: App
        });
        this.cardsView = new CardsCollectionView({
          collection: collection
        });
        this.collectionArea.show(this.cardsView);
        this.filterView = new FilterView({
          collectionView: this.cardsView,
          collection: collection
        });
        return this.filtersArea.show(this.filterView);
      };

      return AppsOverviewCollectionLayout;

    })(Backbone.Marionette.LayoutView);
  });

}).call(this);
