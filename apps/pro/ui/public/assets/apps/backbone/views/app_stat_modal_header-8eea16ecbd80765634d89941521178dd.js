(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/apps/app_stat_modal_header.js'], function($, Template) {
    var AppStatModalHeader, PUSH_VALIDATION_URL, VALIDATION_POLL_INTERVAL;
    PUSH_VALIDATION_URL = "/workspaces/" + WORKSPACE_ID + "/nexpose/result/push_validations.json";
    VALIDATION_POLL_INTERVAL = 500;
    return AppStatModalHeader = (function(_super) {

      __extends(AppStatModalHeader, _super);

      function AppStatModalHeader() {
        this.startPollingValidationRun = __bind(this.startPollingValidationRun, this);

        this.pushValidationsClicked = __bind(this.pushValidationsClicked, this);

        this.serializeData = __bind(this.serializeData, this);

        this.initialize = __bind(this.initialize, this);
        return AppStatModalHeader.__super__.constructor.apply(this, arguments);
      }

      AppStatModalHeader.prototype.initialize = function(_arg) {
        this.appRun = _arg.appRun;
        return this.appRun.bind('change', this.render);
      };

      AppStatModalHeader.prototype.events = {
        'click .push_validations': 'pushValidationsClicked'
      };

      AppStatModalHeader.prototype.template = HandlebarsTemplates['apps/app_stat_modal_header'];

      AppStatModalHeader.prototype.serializeData = function() {
        return _.extend({}, this, {
          stats: this.appRun.runStatHash()
        });
      };

      AppStatModalHeader.prototype.pushValidationsClicked = function(e) {
        var $link,
          _this = this;
        $link = $(e.currentTarget).find('a');
        if ($link.is('.disabled')) {
          return;
        }
        $link.addClass('disabled submitting');
        return $.ajax({
          url: PUSH_VALIDATION_URL,
          type: 'GET',
          dataType: 'json',
          data: {
            run_id: this.appRun.get('auto_exploitation_run_id')
          },
          success: function(json) {
            return _this.startPollingValidationRun(json.task_id);
          },
          error: function(ohnoes) {
            return $link.removeClass('disabled submitting');
          }
        });
      };

      AppStatModalHeader.prototype.startPollingValidationRun = function(taskId) {
        var done, poll, url,
          _this = this;
        done = false;
        url = "/workspaces/" + WORKSPACE_ID + "/tasks/" + taskId + ".json";
        poll = function() {
          return $.ajax({
            url: url,
            success: function(taskJson) {
              var $failed, $succeeded, img;
              if (taskJson.completed_at != null) {
                done = true;
                if ((taskJson.error != null) && taskJson.error.length > 0) {
                  img = '<img src="/assets/icons/incomplete.png" style="vertical-align: top;" />';
                  $failed = $('<span />');
                  $failed.css({
                    paddingRight: '10px',
                    verticalAlign: 'top'
                  });
                  $failed.html(img + (" Push failed. [<a style='vertical-align:top;' href='/workspaces/" + WORKSPACE_ID + "/tasks/" + taskId + "'>more...</a>]"));
                  return $('span.push_validations', _this.el).replaceWith($failed);
                } else {
                  img = '<img src="/assets/icons/complete.png" style="vertical-align: top" />';
                  $succeeded = $('<span />');
                  $succeeded.css({
                    paddingRight: '10px',
                    verticalAlign: 'top'
                  });
                  $succeeded.html("" + img + " Push succeeded.");
                  return $('span.push_validations', _this.el).replaceWith($succeeded);
                }
              } else {
                if (!done) {
                  return setTimeout(poll, VALIDATION_POLL_INTERVAL);
                }
              }
            },
            error: function() {
              done = true;
              return $('span.push_validations a', _this.el).removeClass('disabled submitting');
            }
          });
        };
        return setTimeout(poll, VALIDATION_POLL_INTERVAL);
      };

      return AppStatModalHeader;

    })(Backbone.Marionette.ItemView);
  });

}).call(this);
