(function() {

  define(['/assets/apps/backbone/views/layouts/app_runs_layout.js'], function(AppRunsLayout) {
    var AppRunsController;
    return AppRunsController = (function() {

      function AppRunsController(_arg) {
        this.region = _arg.region;
        this.layout = new AppRunsLayout();
        this.region.show(this.layout);
        this.layout.renderRegions();
      }

      return AppRunsController;

    })();
  });

}).call(this);
