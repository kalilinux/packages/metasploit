(function() {

  define(['/assets/apps/backbone/views/layouts/apps_overview_layout.js'], function(AppsOverviewLayout) {
    var AppsOverviewController;
    return AppsOverviewController = (function() {

      function AppsOverviewController(_arg) {
        this.region = _arg.region;
        this.layout = new AppsOverviewLayout();
        this.region.show(this.layout);
        this.layout.renderRegions();
      }

      return AppsOverviewController;

    })();
  });

}).call(this);
