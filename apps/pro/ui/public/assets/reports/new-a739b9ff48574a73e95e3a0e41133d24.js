(function() {

  jQuery(document).on('requirejs-ready', function() {
    var modal;
    modal = null;
    return window.initRequire(['jquery', '/assets/shared/backbone/layouts/modal.js', '/assets/reports/backbone/views/report_form.js', '/assets/reports/switch_report_type.js'], function($, Modal, ReportsLayoutForm) {
      var url;
      url = $('meta[name=custom_resource_create]').attr('content');
      return $('#upload_report_custom_resource').click(function(e) {
        e.preventDefault();
        if (modal != null) {
          modal.close();
        }
        modal = new Modal({
          "class": 'flat',
          title: "New Custom Resource",
          width: 500
        });
        modal.open();
        return modal.content.show(new ReportsLayoutForm({
          url: url
        }));
      });
    });
  });

}).call(this);
