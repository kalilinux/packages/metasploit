(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["tasks/new_nexpose_exception_push/item_views/vuln"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "                <option value='"
    + this.escapeExpression(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "'>\n                  "
    + this.escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"text","hash":{},"data":data}) : helper)))
    + "\n                </option>\n";
},"3":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, options, buffer = 
  "        <tr class='";
  stack1 = ((helper = (helper = helpers['even_class?'] || (depth0 != null ? depth0['even_class?'] : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"even_class?","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers['even_class?']) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "'>\n          <td>\n            <input class='host_checkbox' name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][checkbox]' type='checkbox'>\n          </td>\n          <td>\n            "
    + this.escapeExpression(((helper = (helper = helpers.host_address || (depth0 != null ? depth0.host_address : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"host_address","hash":{},"data":data}) : helper)))
    + "\n          </td>\n          <td>\n            <label>\n              Reason:\n            </label>\n            <div class='hidden-selection invisible' data-reason='"
    + this.escapeExpression(((helper = (helper = helpers.reason || (depth0 != null ? depth0.reason : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"reason","hash":{},"data":data}) : helper)))
    + "'></div>\n            <select id='reason_"
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "' name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][reason]'>\n"
    + ((stack1 = helpers.each.call(depth0,(depths[1] != null ? depths[1].reasons : depths[1]),{"name":"each","hash":{},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "            </select>\n            <span class='invisible'></span>\n          </td>\n          <td>\n            <label>\n              Comment:\n            </label>\n            <input class='comment' id='comment_"
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "' name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][comments]' value='"
    + this.escapeExpression(((helper = (helper = helpers.comments || (depth0 != null ? depth0.comments : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"comments","hash":{},"data":data}) : helper)))
    + "'>\n            <span class='invisible'></span>\n          </td>\n          <td>\n            <label>\n              Expire:\n            </label>\n            <input class='datetime' id='expire_date_"
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "_"
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "' name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][expiration_date]' value='";
  stack1 = ((helper = (helper = helpers.utc_to_datepicker || (depth0 != null ? depth0.utc_to_datepicker : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"utc_to_datepicker","hash":{},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers.utc_to_datepicker) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "'>\n            <input name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][id]' type='hidden' value='"
    + this.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "'>\n            <input name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][automatic_exploitation_match_result_id]' type='hidden' value='"
    + this.escapeExpression(((helper = (helper = helpers.automatic_exploitation_match_result_id || (depth0 != null ? depth0.automatic_exploitation_match_result_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"automatic_exploitation_match_result_id","hash":{},"data":data}) : helper)))
    + "'>\n            <input class='auto-approve' name='nexpose_result_exceptions["
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].itemIndex : depths[1]), depth0))
    + "]["
    + this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)))
    + "][approve]' type='hidden' value='"
    + this.escapeExpression(((helper = (helper = helpers.approve || (depth0 != null ? depth0.approve : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"approve","hash":{},"data":data}) : helper)))
    + "'>\n          </td>\n          <td>\n            <div class='result_code'>\n              Result Code: "
    + this.escapeExpression(((helper = (helper = helpers.result_code || (depth0 != null ? depth0.result_code : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"result_code","hash":{},"data":data}) : helper)))
    + "\n            </div>\n          </td>\n        </tr>\n";
},"4":function(depth0,helpers,partials,data) {
    var helper;

  return this.escapeExpression(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"index","hash":{},"data":data}) : helper)));
},"6":function(depth0,helpers,partials,data) {
    var helper;

  return "              <option value='"
    + this.escapeExpression(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"value","hash":{},"data":data}) : helper)))
    + "'>\n                "
    + this.escapeExpression(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"text","hash":{},"data":data}) : helper)))
    + "\n              </option>\n";
},"8":function(depth0,helpers,partials,data) {
    var helper;

  return this.escapeExpression(((helper = (helper = helpers.expiration_date || (depth0 != null ? depth0.expiration_date : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"expiration_date","hash":{},"data":data}) : helper)));
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper;

  return "<div class='vuln-exception columns'>\n  <div class='header row'>\n    <div class='left columns small-12'>\n      <div class='row'>\n        <div class='name columns small-8'>\n          "
    + this.escapeExpression(((helper = (helper = helpers.module_detail || (depth0 != null ? depth0.module_detail : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"module_detail","hash":{},"data":data}) : helper)))
    + "\n        </div>\n        <div class='columns small-4 spacer'></div>\n      </div>\n      <div class='row'>\n        <div class='columns small-6'>\n          <div class='row'>\n            <div class='radios columns'>\n              <input id='option_"
    + this.escapeExpression(((helper = (helper = helpers.itemIndex || (depth0 != null ? depth0.itemIndex : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"itemIndex","hash":{},"data":data}) : helper)))
    + "_all' name='option_"
    + this.escapeExpression(((helper = (helper = helpers.itemIndex || (depth0 != null ? depth0.itemIndex : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"itemIndex","hash":{},"data":data}) : helper)))
    + "' type='radio' value='all'>\n              <label for='option_"
    + this.escapeExpression(((helper = (helper = helpers.itemIndex || (depth0 != null ? depth0.itemIndex : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"itemIndex","hash":{},"data":data}) : helper)))
    + "_all'>\n                All Hosts with this Vulnerability\n              </label>\n              <input checked='checked' id='option_"
    + this.escapeExpression(((helper = (helper = helpers.itemIndex || (depth0 != null ? depth0.itemIndex : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"itemIndex","hash":{},"data":data}) : helper)))
    + "_single' name='option_"
    + this.escapeExpression(((helper = (helper = helpers.itemIndex || (depth0 != null ? depth0.itemIndex : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"itemIndex","hash":{},"data":data}) : helper)))
    + "' type='radio' value='single'>\n              <label for='option_"
    + this.escapeExpression(((helper = (helper = helpers.itemIndex || (depth0 != null ? depth0.itemIndex : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"itemIndex","hash":{},"data":data}) : helper)))
    + "_single'>\n                Individual Hosts with this Vulnerability\n              </label>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class='row hosts'>\n    <div class='columns small-12'>\n      <table border='1'>\n        <tr>\n          <th></th>\n          <th></th>\n          <th>\n            <div class='mass-assign invisible'>\n              <label>\n                Reason\n              </label>\n              <select name='group_reason'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.reasons : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "              </select>\n            </div>\n          </th>\n          <th>\n            <div class='mass-assign invisible'>\n              <label>\n                Comment:\n              </label>\n              <input name='group_comment'>\n            </div>\n          </th>\n          <th></th>\n          <th></th>\n        </tr>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.result_exceptions : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </table>\n    </div>\n  </div>\n</div>";
},"useData":true,"useDepths":true});
  return this.HandlebarsTemplates["tasks/new_nexpose_exception_push/item_views/vuln"];
}).call(this);
