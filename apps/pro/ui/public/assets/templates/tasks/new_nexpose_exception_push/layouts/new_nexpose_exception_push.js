(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["tasks/new_nexpose_exception_push/layouts/new_nexpose_exception_push"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<form id='exception-push-form'>\n  <div class='row exception-settings-line'>\n    <div class='columns exception-settings'>\n      <div class='header columns small-12'>\n        <div class='small-2 columns'>\n          EXCEPTION SETTINGS\n        </div>\n        <div class='console-field small-8 columns'>\n          <label class='hide-console-field'>\n            Nexponse Console\n          </label>\n          "
    + this.escapeExpression((helpers.select || (depth0 && depth0.select) || helpers.helperMissing).call(depth0,"console",((stack1 = (depth0 != null ? depth0.console : depth0)) != null ? stack1.consoles : stack1),((stack1 = (depth0 != null ? depth0.console : depth0)) != null ? stack1.console : stack1),{"name":"select","hash":{},"data":data}))
    + "\n          "
    + this.escapeExpression((helpers.checkbox || (depth0 && depth0.checkbox) || helpers.helperMissing).call(depth0,"auto_approve","yes",true,{"name":"checkbox","hash":{},"data":data}))
    + "\n          <label>\n            Automatically Approve\n          </label>\n        </div>\n        <div class='push-exceptions small-2 columns'>\n          <div class='btnRow'>\n            <span class='btn'>\n              <a class='nexpose' href='javascript:void(0)'>\n                Push Exceptions\n              </a>\n            </span>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class='tabs'></div>\n</form>";
},"useData":true});
  return this.HandlebarsTemplates["tasks/new_nexpose_exception_push/layouts/new_nexpose_exception_push"];
}).call(this);
