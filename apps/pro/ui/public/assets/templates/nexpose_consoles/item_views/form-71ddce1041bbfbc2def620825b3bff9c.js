(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["nexpose_consoles/item_views/form"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "  "
    + this.escapeExpression((helpers.hidden || (depth0 && depth0.hidden) || helpers.helperMissing).call(depth0,"_method","CREATE",{"name":"hidden","hash":{},"data":data}))
    + "\n  <li class='console_name'>\n    "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"nexpose_console[name]","Name",{"name":"label","hash":{},"data":data}))
    + "\n    "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"nexpose_console[name]",(depth0 != null ? depth0.name : depth0),{"name":"input","hash":{},"data":data}))
    + "\n  </li>\n  <li class='console_address'>\n    "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"nexpose_console[address]","Address",{"name":"label","hash":{},"data":data}))
    + "\n    "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"nexpose_console[address]",(depth0 != null ? depth0.address : depth0),{"name":"input","hash":{},"data":data}))
    + "\n  </li>\n  <li class='console_port'>\n    "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"nexpose_console[port]","Port",{"name":"label","hash":{},"data":data}))
    + "\n    "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"nexpose_console[port]",(depth0 != null ? depth0.port : depth0),{"name":"input","hash":{},"data":data}))
    + "\n  </li>\n  <li class='console_username'>\n    "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"nexpose_console[username]","Username",{"name":"label","hash":{},"data":data}))
    + "\n    "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"nexpose_console[username]",(depth0 != null ? depth0.username : depth0),{"name":"input","hash":{},"data":data}))
    + "\n  </li>\n  <li class='console_password'>\n    "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"nexpose_console[password]","Password",{"name":"label","hash":{},"data":data}))
    + "\n    "
    + this.escapeExpression((helpers.password || (depth0 && depth0.password) || helpers.helperMissing).call(depth0,"nexpose_console[password]",(depth0 != null ? depth0.password : depth0),{"name":"password","hash":{},"data":data}))
    + "\n  </li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='nexpose_console_form'>\n"
    + ((stack1 = (helpers.form || (depth0 && depth0.form) || helpers.helperMissing).call(depth0,"/",{"name":"form","hash":{"class":"form formtastic nexpose_console"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  <div class='connectivity'>\n    <div class='connection_success'>\n      Connected successfully.\n    </div>\n    <div class='connection_error'>\n      Connection failed.\n    </div>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["nexpose_consoles/item_views/form"];
}).call(this);
