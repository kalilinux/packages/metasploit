(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["tags/form"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<form action='' id='add_tags' style='padding: 3px 0'>\n  <ul>\n    <li class='name tags' style='margin-bottom: 20px'>\n      <input name='name'>\n    </li>\n  </ul>\n</form>";
},"useData":true});
  return this.HandlebarsTemplates["tags/form"];
}).call(this);
