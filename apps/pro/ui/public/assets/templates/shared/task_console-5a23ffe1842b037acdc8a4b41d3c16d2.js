(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["shared/task_console"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n  <table class='list header'></table>\n  <pre class='console logs'></pre>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["shared/task_console"];
}).call(this);
