(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["shared/modal"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "<div class='bg'></div>\n";
},"3":function(depth0,helpers,partials,data) {
    return "    <h1>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.title : depth0), depth0))
    + "</h1>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "    <p>"
    + this.escapeExpression(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n";
},"7":function(depth0,helpers,partials,data) {
    return "      <a class='"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0['class'] : depth0), depth0))
    + "' href='javascript:void(0)'>"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.name : depth0), depth0))
    + "</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.bg : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "<div class='modal "
    + this.escapeExpression(((helper = (helper = helpers['class'] || (depth0 != null ? depth0['class'] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"class","hash":{},"data":data}) : helper)))
    + "'>\n  <div class='header'>\n    <a class='close small' href='javascript:void 0'>&times;</a>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"if_present","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.description : depth0),{"name":"if_present","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n  <div class='padding'>\n    <div class='content'></div>\n    <div class='clearfix'></div>\n    <div class='modal-actions'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.buttons : depth0),{"name":"each","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["shared/modal"];
}).call(this);
