(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["shared/item_views/inline_edit"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class='container'>\n  <div class='input-container'></div>\n  <div class='field-text'>\n    "
    + this.escapeExpression(((helper = (helper = helpers.ref || (depth0 != null ? depth0.ref : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"ref","hash":{},"data":data}) : helper)))
    + "\n  </div>\n  <div class='controls'>\n    <a class='garbage' href='javascript:void(0)'></a>\n    <a class='pencil' href='javascript:void(0)'></a>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["shared/item_views/inline_edit"];
}).call(this);
