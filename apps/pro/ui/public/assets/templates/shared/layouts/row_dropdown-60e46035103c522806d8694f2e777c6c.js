(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["shared/layouts/row_dropdown"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='single-host-view-container'>\n  <div class='header'></div>\n  <div class='dropdown invisible foundation row'></div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["shared/layouts/row_dropdown"];
}).call(this);
