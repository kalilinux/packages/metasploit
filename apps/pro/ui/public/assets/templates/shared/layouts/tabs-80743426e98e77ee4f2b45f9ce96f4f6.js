(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["shared/layouts/tabs"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.hideOnZero : depth0),{"name":"if_eq","hash":{"compare":(depth0 != null ? depth0.undefined : depth0)},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.hideOnZero : depth0),{"name":"if_eq","hash":{"compare":true},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = this.invokePartial(partials['shared/layouts/_tabs'],depth0,{"name":"shared/layouts/_tabs","data":data,"indent":"    ","helpers":helpers,"partials":partials})) != null ? stack1 : "");
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_gt || (depth0 && depth0.if_gt) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.count : depth0),{"name":"if_gt","hash":{"compare":0},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='foundation row columns backbone-tabs'>\n  <ul class='nav'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.tabs : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </ul>\n  <div class='content tab-loading'></div>\n</div>";
},"usePartial":true,"useData":true});
  return this.HandlebarsTemplates["shared/layouts/tabs"];
}).call(this);
