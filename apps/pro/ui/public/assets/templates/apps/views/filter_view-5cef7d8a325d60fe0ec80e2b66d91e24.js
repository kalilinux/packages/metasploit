(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/views/filter_view"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "      <li>\n        <label>\n          <input type='checkbox'>\n          <span href='#'>\n            <span class='name'>"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n            <span class='count'>("
    + this.escapeExpression(((helper = (helper = helpers.count || (depth0 != null ? depth0.count : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"count","hash":{},"data":data}) : helper)))
    + ")</span>\n          </span>\n        </label>\n      </li>\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "      <li>\n        <a data-stars='"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "' href='#' title='"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "/5 stars'>\n          <span class='rating-stars star"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "'>\n            <span></span>\n          </span>\n          <span class='and-up'>&amp; up</span>\n          ("
    + this.escapeExpression(((helper = (helper = helpers.count || (depth0 != null ? depth0.count : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"count","hash":{},"data":data}) : helper)))
    + ")\n        </a>\n      </li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<h3 class='orange-text'>\n  MetaModules\n</h3>\n<ul class='filter-sections'>\n  <li>\n    <h6>\n      Categories:\n      <span class='reset'>\n        <a class='reset' href='#'>Reset</a>\n      </span>\n    </h6>\n    <ul class='categories'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.categories : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </ul>\n  </li>\n  <li>\n    <h6>\n      Safety Rating:\n    </h6>\n    <ul class='safety-ratings'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.safetyRatings : depth0),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </ul>\n  </li>\n</ul>";
},"useData":true});
  return this.HandlebarsTemplates["apps/views/filter_view"];
}).call(this);
