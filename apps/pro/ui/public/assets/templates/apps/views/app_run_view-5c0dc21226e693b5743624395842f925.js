(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/views/app_run_view"] = Handlebars.template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    return "    <div class='center' style='display: inline-block; margin-right: 15px; width: 100px;'>\n      <span class='stat' title='"
    + this.escapeExpression((helpers.lookupStat || (depth0 && depth0.lookupStat) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].statData : depths[1]),depth0,{"name":"lookupStat","hash":{},"data":data}))
    + "'>"
    + this.escapeExpression((helpers.formatStat || (depth0 && depth0.formatStat) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].statData : depths[1]),depth0,{"name":"formatStat","hash":{},"data":data}))
    + "</span>\n      <span class='sub'>"
    + this.escapeExpression((helpers.humanizeStat || (depth0 && depth0.humanizeStat) || helpers.helperMissing).call(depth0,(depths[1] != null ? depths[1].app : depths[1]),depth0,{"name":"humanizeStat","hash":{},"data":data}))
    + "</span>\n    </div>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return "    <div class='tab-loading'></div>\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.percentage : stack1),{"name":"if","hash":{},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    <div class='progress-text'>\n      <span class='sub'>Scan in progress</span>\n    </div>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1;

  return "    <span class='percentage'>"
    + this.escapeExpression((helpers.percentize || (depth0 && depth0.percentize) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.percentage : stack1),(depth0 != null ? depth0.statData : depth0),{"name":"percentize","hash":{},"data":data}))
    + "</span>\n";
},"6":function(depth0,helpers,partials,data) {
    return "      <li>\n        <a class='stop' href='#'>Stop</a>\n      </li>\n";
},"8":function(depth0,helpers,partials,data) {
    return "      <li>\n        <a class='delete' href='#'>Delete</a>\n      </li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper;

  return "<div class='app-run-view row'>\n  <div class='large-3 columns'>\n    <h6>"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.name : stack1), depth0))
    + "</h6>\n    <span class='sub' title='"
    + this.escapeExpression(((helper = (helper = helpers.created_at || (depth0 != null ? depth0.created_at : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"created_at","hash":{},"data":data}) : helper)))
    + "'>Start Time: "
    + this.escapeExpression(((helper = (helper = helpers.started_at || (depth0 != null ? depth0.started_at : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"started_at","hash":{},"data":data}) : helper)))
    + "</span>\n  </div>\n  <div class='large-3 columns center run-stats' style='padding-left: 0; padding-right: 0;'>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.row_stats : stack1),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n  <div class='large-4 columns center loading-wrapper'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),{"name":"if_eq","hash":{"compare":"running"},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n  <div class='large-2 columns findings'>\n    <div class='status-text'>\n      <label class='status "
    + this.escapeExpression(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"status","hash":{},"data":data}) : helper)))
    + "'>"
    + this.escapeExpression((helpers.humanize || (depth0 && depth0.humanize) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),{"name":"humanize","hash":{},"data":data}))
    + "</label>\n    </div>\n    <ul class='actions'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),{"name":"if_eq","hash":{"compare":"running"},"fn":this.program(6, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      <li>\n        <a class='findings' href='#'>Findings</a>\n      </li>\n"
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),{"name":"unless_eq","hash":{"compare":"running"},"fn":this.program(8, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </ul>\n  </div>\n</div>";
},"useData":true,"useDepths":true});
  return this.HandlebarsTemplates["apps/views/app_run_view"];
}).call(this);
