(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/views/card_view"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "      <span class='category'>\n        "
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n      </span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class='large-4 columns'>\n  <div class='card'>\n    <a class='title' href='#'>\n      <h6>\n        "
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "\n      </h6>\n    </a>\n    <div class='categories'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.app_categories : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    <div class='wrap-description'>\n      <p class='description'>\n        "
    + this.escapeExpression(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"description","hash":{},"data":data}) : helper)))
    + "\n      </p>\n    </div>\n    <a class='launch btn primary'>\n      Launch\n    </a>\n    <div class='safety-rating'>\n      Safety Rating:\n      <span class='rating-stars star"
    + this.escapeExpression(((helper = (helper = helpers.rating || (depth0 != null ? depth0.rating : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"rating","hash":{},"data":data}) : helper)))
    + "' title='"
    + this.escapeExpression(((helper = (helper = helpers.rating || (depth0 != null ? depth0.rating : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"rating","hash":{},"data":data}) : helper)))
    + "/5 stars'>\n        <span></span>\n      </span>\n    </div>\n    <div class='clearfix' style='clear:both'></div>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["apps/views/card_view"];
}).call(this);
