(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/views/generic_stats_view"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class='center app-runs'>\n  <div class='overview-header'>\n    last launched metamodule:\n    <span class='app-name'>\n      "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.name : stack1), depth0))
    + "\n    </span>\n    <label class='status finished'>"
    + this.escapeExpression(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"status","hash":{},"data":data}) : helper)))
    + "</label>\n  </div>\n</div>\n";
},"3":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = this.invokePartial(partials['apps/views/stat_views/_stat'],depth0,{"name":"apps/views/stat_views/_stat","data":data,"indent":"    ","helpers":helpers,"partials":partials})) != null ? stack1 : "");
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "<a class='show-stats' href='./app_runs/"
    + this.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "'>\n  View Findings &rarr;\n</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.showHeader : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "<div class='stat-row "
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.symbol : stack1), depth0))
    + "'>\n  <div class='center'>\n"
    + ((stack1 = helpers.each.call(depth0,((stack1 = (depth0 != null ? depth0.app : depth0)) != null ? stack1.stats : stack1),{"name":"each","hash":{},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n</div>\n"
    + ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.showHeader : depth0),{"name":"if","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
  return this.HandlebarsTemplates["apps/views/generic_stats_view"];
}).call(this);
