(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/views/no_cards_found_view"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='no-cards-found'>\n  No MetaModules were found that match your search criteria.\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["apps/views/no_cards_found_view"];
}).call(this);
