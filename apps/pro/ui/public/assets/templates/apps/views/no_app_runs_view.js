(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/views/no_app_runs_view"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='no-app-runs'>\n  There are no MetaModule Runs to display.\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["apps/views/no_app_runs_view"];
}).call(this);
