(function() {
  Handlebars.registerPartial("apps/views/stat_views/_stat", Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "  <div class='pie-chart-wrapper load-table' clickable='"
    + this.escapeExpression(((helper = (helper = helpers.clickable || (depth0 != null ? depth0.clickable : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"clickable","hash":{},"data":data}) : helper)))
    + "' label='"
    + this.escapeExpression(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"label","hash":{},"data":data}) : helper)))
    + "' name='"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "'>\n    <div class='pie-chart' name='"
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "' total='"
    + this.escapeExpression(((helper = (helper = helpers.total || (depth0 != null ? depth0.total : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"total","hash":{},"data":data}) : helper)))
    + "'></div>\n    <label class='stat run-stat' name='"
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "' total='"
    + this.escapeExpression(((helper = (helper = helpers.total || (depth0 != null ? depth0.total : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"total","hash":{},"data":data}) : helper)))
    + "'></label>\n    <label class='desc'>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.label : depth0),{"name":"if_present","hash":{},"fn":this.program(2, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "    </label>\n  </div>\n";
},"2":function(depth0,helpers,partials,data) {
    var helper;

  return "      "
    + this.escapeExpression(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"label","hash":{},"data":data}) : helper)))
    + "\n";
},"4":function(depth0,helpers,partials,data) {
    return "      "
    + this.escapeExpression((helpers.humanize || (depth0 && depth0.humanize) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.name : depth0),{"name":"humanize","hash":{},"data":data}))
    + "\n";
},"6":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "  <div class='big-stat center load-table' clickable='"
    + this.escapeExpression(((helper = (helper = helpers.clickable || (depth0 != null ? depth0.clickable : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"clickable","hash":{},"data":data}) : helper)))
    + "' label='"
    + this.escapeExpression(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"label","hash":{},"data":data}) : helper)))
    + "' name='"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "'>\n    <span class='run-stat stat' format='"
    + this.escapeExpression(((helper = (helper = helpers.format || (depth0 != null ? depth0.format : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"format","hash":{},"data":data}) : helper)))
    + "' name='"
    + this.escapeExpression(((helper = (helper = helpers.num || (depth0 != null ? depth0.num : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"num","hash":{},"data":data}) : helper)))
    + "'></span>\n    <label>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.label : depth0),{"name":"if_present","hash":{},"fn":this.program(7, data, 0),"inverse":this.program(9, data, 0),"data":data})) != null ? stack1 : "")
    + "    </label>\n  </div>\n";
},"7":function(depth0,helpers,partials,data) {
    var helper;

  return "      <span>"
    + this.escapeExpression(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"label","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"9":function(depth0,helpers,partials,data) {
    return "      <span>"
    + this.escapeExpression((helpers.humanize || (depth0 && depth0.humanize) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.name : depth0),{"name":"humanize","hash":{},"data":data}))
    + "</span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class='generic-stat-wrapper' clickable='"
    + this.escapeExpression(((helper = (helper = helpers.clickable || (depth0 != null ? depth0.clickable : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"clickable","hash":{},"data":data}) : helper)))
    + "'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.type : depth0),{"name":"if_eq","hash":{"compare":"percentage"},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "  <div class='lil-nubster'></div>\n</div>";
},"useData":true}));
}).call(this);
