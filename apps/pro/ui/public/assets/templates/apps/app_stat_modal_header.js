(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["apps/app_stat_modal_header"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.stats : depth0)) != null ? stack1.vuln_validations : stack1),{"name":"if_present","hash":{},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.stats : depth0)) != null ? stack1.vuln_exceptions : stack1),{"name":"if_present","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_gt || (depth0 && depth0.if_gt) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.stats : depth0)) != null ? stack1.vuln_validations : stack1),{"name":"if_gt","hash":{"compare":0},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"3":function(depth0,helpers,partials,data) {
    return "  <span class='push_validations btn'>\n    <a class='nexpose' href='javascript:void(0)'>\n      Push Validations\n    </a>\n  </span>\n";
},"5":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_gt || (depth0 && depth0.if_gt) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.stats : depth0)) != null ? stack1.vuln_exceptions : stack1),{"name":"if_gt","hash":{"compare":0},"fn":this.program(6, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"6":function(depth0,helpers,partials,data) {
    var stack1;

  return "  <span class='push_exceptions btn'>\n    <a class='nexpose' href='/workspaces/"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.appRun : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.workspace_id : stack1), depth0))
    + "/tasks/new_nexpose_exception_push?match_set_id="
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.appRun : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.match_set_id : stack1), depth0))
    + "'>\n      Push Exceptions\n    </a>\n  </span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<h3>"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.appRun : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.app : stack1)) != null ? stack1.name : stack1), depth0))
    + "</h3>\n<label class='status "
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = (depth0 != null ? depth0.appRun : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.status : stack1), depth0))
    + "'>\n  "
    + this.escapeExpression((helpers.humanize || (depth0 && depth0.humanize) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.appRun : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.status : stack1),{"name":"humanize","hash":{},"data":data}))
    + "\n</label>\n<div class='right'>\n  <!-- todo: move this business somewhere better -->\n"
    + ((stack1 = helpers['if'].call(depth0,((stack1 = ((stack1 = (depth0 != null ? depth0.appRun : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.auto_exploitation_run_id : stack1),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
  return this.HandlebarsTemplates["apps/app_stat_modal_header"];
}).call(this);
