(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["fuzzing/layouts/fuzzing_frame_layout"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='top-row'></div>\n<div class='middle-row'></div>\n<div class='bottom-row'></div>";
},"useData":true});
  return this.HandlebarsTemplates["fuzzing/layouts/fuzzing_frame_layout"];
}).call(this);
