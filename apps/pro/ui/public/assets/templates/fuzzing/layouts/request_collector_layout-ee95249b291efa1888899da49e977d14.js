(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["fuzzing/layouts/request_collector_layout"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<h1>Request Collector</h1>\n<div class='requests'></div>\n<div class='request-input'></div>";
},"useData":true});
  return this.HandlebarsTemplates["fuzzing/layouts/request_collector_layout"];
}).call(this);
