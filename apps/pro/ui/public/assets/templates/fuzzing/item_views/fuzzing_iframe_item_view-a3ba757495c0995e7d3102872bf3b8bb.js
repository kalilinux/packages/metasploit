(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["fuzzing/item_views/fuzzing_iframe_item_view"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<iframe class='test-frame row' sandbox='allow-forms allow-scripts' src='' style='height:800px'></iframe>";
},"useData":true});
  return this.HandlebarsTemplates["fuzzing/item_views/fuzzing_iframe_item_view"];
}).call(this);
