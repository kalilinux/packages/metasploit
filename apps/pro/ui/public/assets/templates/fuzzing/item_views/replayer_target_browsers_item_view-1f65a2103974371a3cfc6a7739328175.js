(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["fuzzing/item_views/replayer_target_browsers_item_view"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n  <h2>Targeted Browsers</h2>\n  <div>\n    Target Browser stuff goes here\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["fuzzing/item_views/replayer_target_browsers_item_view"];
}).call(this);
