(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["reports/views/report_form"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "  "
    + this.escapeExpression((helpers.hidden || (depth0 && depth0.hidden) || helpers.helperMissing).call(depth0,"_method","create",{"name":"hidden","hash":{},"data":data}))
    + "\n  <ul>\n    <li class='file'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"report_custom_resource[file_data]","Resource file",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.file || (depth0 && depth0.file) || helpers.helperMissing).call(depth0,"report_custom_resource[file_data]",{"name":"file","hash":{},"data":data}))
    + "\n    </li>\n    <li class='name'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"report_custom_resource[name]","Resource name",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"report_custom_resource[name]","",{"name":"input","hash":{},"data":data}))
    + "\n    </li>\n  </ul>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='report-form'>\n"
    + ((stack1 = (helpers.form || (depth0 && depth0.form) || helpers.helperMissing).call(depth0,"/",{"name":"form","hash":{"class":"form formtastic"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
  return this.HandlebarsTemplates["reports/views/report_form"];
}).call(this);
