(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/attempts"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='attempts-table'>\n  <div class='table'></div>\n  <div class='clearfix'></div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/attempts"];
}).call(this);
