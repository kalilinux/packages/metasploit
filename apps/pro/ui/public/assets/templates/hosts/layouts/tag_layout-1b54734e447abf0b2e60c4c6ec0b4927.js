(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/tag_layout"] = Handlebars.template({"1":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper;

  return "<a class='tag' href='/workspaces/"
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].workspace_id : depths[1]), depth0))
    + "/hosts?search=%23"
    + this.escapeExpression((helpers.encodeURIComponent || (depth0 && depth0.encodeURIComponent) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.name : depth0),{"name":"encodeURIComponent","hash":{},"data":data}))
    + "'>\n  <span class='name'>"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n</a>\n<a class='tag-close' data-id='"
    + this.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "' href='javascript:void(0)'>\n  &times;\n</a>\n";
},"3":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper;

  return "<a class='more' href='javascript:void(0)'>\n  "
    + this.escapeExpression(((helper = (helper = helpers.moreTagCount || (depth0 != null ? depth0.moreTagCount : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"moreTagCount","hash":{},"data":data}) : helper)))
    + " more…\n</a>\n<div class='under'>\n  <div class='white'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.otherTags : depth0),{"name":"each","hash":{},"fn":this.program(4, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n</div>\n";
},"4":function(depth0,helpers,partials,data,blockParams,depths) {
    var helper;

  return "    <div class='wrap'>\n      <a class='tag' href='/workspaces/"
    + this.escapeExpression(this.lambda((depths[1] != null ? depths[1].workspace_id : depths[1]), depth0))
    + "/hosts?search=%23"
    + this.escapeExpression((helpers.encodeURIComponent || (depth0 && depth0.encodeURIComponent) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.name : depth0),{"name":"encodeURIComponent","hash":{},"data":data}))
    + "'>\n        <span class='name'>"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n      </a>\n      <a class='tag-close' data-id='"
    + this.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"id","hash":{},"data":data}) : helper)))
    + "' href='javascript:void(0)'>\n        &times;\n      </a>\n    </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return "<label class='tags'>Tags</label>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.lastTags : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_gt || (depth0 && depth0.if_gt) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.tagCount : depth0),{"name":"if_gt","hash":{"compare":3},"fn":this.program(3, data, 0, blockParams, depths),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "<a class='green-add' href='javascript:void(0);' title='Add tags to this host'>+</a>";
},"useData":true,"useDepths":true});
  return this.HandlebarsTemplates["hosts/layouts/tag_layout"];
}).call(this);
