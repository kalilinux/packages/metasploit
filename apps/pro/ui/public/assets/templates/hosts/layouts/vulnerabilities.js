(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/vulnerabilities"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='vulns'></div>\n<div style='clear:both'></div>\n<div class='vuln-modal'></div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/vulnerabilities"];
}).call(this);
