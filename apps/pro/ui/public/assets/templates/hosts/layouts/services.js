(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/services"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='btnRow'>\n  <span class='btn'>\n    <a class='new' href='javascript:void(0)'>\n      New Service\n    </a>\n  </span>\n</div>\n<div id='services_table'></div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/services"];
}).call(this);
