(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/host_view_layout"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='container foundation row'>\n  <div class='host-stats-overview foundation large-6 columns'></div>\n  <div class='tags foundation large-6 columns'></div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/host_view_layout"];
}).call(this);
