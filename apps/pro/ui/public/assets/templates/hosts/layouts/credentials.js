(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/credentials"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='credentials'>\n  <h4>Logins</h4>\n  <div class='accessing-logins'></div>\n  <h4>Captured Credentials</h4>\n  <div class='originating-creds'></div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/credentials"];
}).call(this);
