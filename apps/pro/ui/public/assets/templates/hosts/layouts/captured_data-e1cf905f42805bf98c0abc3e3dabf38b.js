(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/captured_data"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='btnRow'>\n  <span class='btn'>\n    <a class='new' href='javascript:void(0)'>\n      New Captured Data\n    </a>\n  </span>\n</div>\n<div id='loot_table'></div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/captured_data"];
}).call(this);
