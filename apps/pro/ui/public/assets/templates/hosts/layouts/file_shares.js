(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/file_shares"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='shares'></div>\n<div class='clearfix'></div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/file_shares"];
}).call(this);
