(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/layouts/notes"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id='notes_table'></div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/layouts/notes"];
}).call(this);
