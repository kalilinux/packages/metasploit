(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/item_views/host_stats_overview_item_view"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "          "
    + this.escapeExpression(((helper = (helper = helpers.address || (depth0 != null ? depth0.address : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"address","hash":{},"data":data}) : helper)))
    + "\n";
},"3":function(depth0,helpers,partials,data) {
    var helper;

  return "          <div style='line-height: 47px;'>\n            "
    + this.escapeExpression(((helper = (helper = helpers.address || (depth0 != null ? depth0.address : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"address","hash":{},"data":data}) : helper)))
    + "\n          </div>\n";
},"5":function(depth0,helpers,partials,data) {
    var helper;

  return "        <div class='name'>\n          ["
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "]\n        </div>\n";
},"7":function(depth0,helpers,partials,data) {
    var helper;

  return "          <label>Flavor</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.os_flavor || (depth0 != null ? depth0.os_flavor : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_flavor","hash":{},"data":data}) : helper)))
    + "</div>\n";
},"9":function(depth0,helpers,partials,data) {
    var helper;

  return "          <label>SP</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.os_sp || (depth0 != null ? depth0.os_sp : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_sp","hash":{},"data":data}) : helper)))
    + "</div>\n";
},"11":function(depth0,helpers,partials,data) {
    var stack1;

  return "    <li class='pivot' title='VPN pivot running in Task #"
    + this.escapeExpression(this.lambda(((stack1 = (depth0 != null ? depth0.vpn_pivot : depth0)) != null ? stack1.task_id : stack1), depth0))
    + "'>\n      <div class='bottom_pin'>\n        VPN Pivot\n      </div>\n    </li>\n";
},"13":function(depth0,helpers,partials,data) {
    var stack1;

  return "    <li class='nexpose'>\n      <div class='bottom_pin'>\n        Source\n      </div>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.host_details : depth0),{"name":"each","hash":{},"fn":this.program(14, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </li>\n";
},"14":function(depth0,helpers,partials,data) {
    var helper;

  return "      <div class='menu'>\n        <div class='padding'>\n          <label>Site Name</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.nx_site_name || (depth0 != null ? depth0.nx_site_name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"nx_site_name","hash":{},"data":data}) : helper)))
    + "</div>\n          <label>Site Importance</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.nx_site_importance || (depth0 != null ? depth0.nx_site_importance : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"nx_site_importance","hash":{},"data":data}) : helper)))
    + "</div>\n          <label>Scan Template</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.nx_scan_template || (depth0 != null ? depth0.nx_scan_template : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"nx_scan_template","hash":{},"data":data}) : helper)))
    + "</div>\n          <label>Risk Score</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.nx_risk_score || (depth0 != null ? depth0.nx_risk_score : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"nx_risk_score","hash":{},"data":data}) : helper)))
    + "</div>\n          <label>Console ID</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.nx_console_id || (depth0 != null ? depth0.nx_console_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"nx_console_id","hash":{},"data":data}) : helper)))
    + "</div>\n          <label>Device ID</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.nx_device_id || (depth0 != null ? depth0.nx_device_id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"nx_device_id","hash":{},"data":data}) : helper)))
    + "</div>\n        </div>\n      </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class='container hosts-stats-overview'>\n  <ul>\n    <li class='host'>\n      <div class='inline-block'>\n        <div class='ip'>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.name : depth0),{"name":"if_present","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.name : depth0),{"name":"if_present","hash":{},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </div>\n      <a class='edit-info' href='javascript:void(0)' title='View and edit host information'></a>\n    </li>\n    <li class='state "
    + this.escapeExpression(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"status","hash":{},"data":data}) : helper)))
    + "'>\n      "
    + this.escapeExpression((helpers.upcase || (depth0 && depth0.upcase) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.status : depth0),{"name":"upcase","hash":{},"data":data}))
    + "\n    </li>\n    <li class='os'>\n      <div class='icons'>\n        "
    + ((stack1 = ((helper = (helper = helpers.virtual_host || (depth0 != null ? depth0.virtual_host : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"virtual_host","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n        "
    + ((stack1 = ((helper = (helper = helpers.os_icon || (depth0 != null ? depth0.os_icon : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_icon","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n      </div>\n      <div class='os_name bottom_pin'>\n        "
    + this.escapeExpression(((helper = (helper = helpers.os || (depth0 != null ? depth0.os : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os","hash":{},"data":data}) : helper)))
    + "\n      </div>\n      <div class='menu'>\n        <div class='padding'>\n          <label>Name</label>\n          <div class='value'>"
    + this.escapeExpression(((helper = (helper = helpers.os_name || (depth0 != null ? depth0.os_name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_name","hash":{},"data":data}) : helper)))
    + "</div>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.os_flavor : depth0),{"name":"if_present","hash":{},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.os_sp : depth0),{"name":"if_present","hash":{},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n      </div>\n    </li>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.vpn_pivot : depth0),{"name":"if_present","hash":{},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.host_details : depth0),{"name":"if_present","hash":{},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </ul>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/item_views/host_stats_overview_item_view"];
}).call(this);
