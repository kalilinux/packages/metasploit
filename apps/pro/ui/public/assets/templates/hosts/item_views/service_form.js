(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/item_views/service_form"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  <ul>\n    <li class='name'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[name]","Name",{"name":"label","hash":{},"data":data}))
    + "\n      <div>\n        "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"aaData[name]","",{"name":"input","hash":{},"data":data}))
    + "\n      </div>\n    </li>\n    <li class='port'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[port]","Port",{"name":"label","hash":{},"data":data}))
    + "\n      <div>\n        "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"aaData[port]","",{"name":"input","hash":{},"data":data}))
    + "\n      </div>\n    </li>\n    <li class='proto'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[proto]","Protocol",{"name":"label","hash":{},"data":data}))
    + "\n      <div>\n        "
    + this.escapeExpression((helpers.select || (depth0 && depth0.select) || helpers.helperMissing).call(depth0,"aaData[proto]",((stack1 = (depth0 != null ? depth0.protocols : depth0)) != null ? stack1.protocols : stack1),((stack1 = (depth0 != null ? depth0.protocols : depth0)) != null ? stack1.selected : stack1),{"name":"select","hash":{},"data":data}))
    + "\n      </div>\n    </li>\n    <li class='state'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[state]","State",{"name":"label","hash":{},"data":data}))
    + "\n      <div>\n        "
    + this.escapeExpression((helpers.select || (depth0 && depth0.select) || helpers.helperMissing).call(depth0,"aaData[state]",((stack1 = (depth0 != null ? depth0.states : depth0)) != null ? stack1.states : stack1),((stack1 = (depth0 != null ? depth0.states : depth0)) != null ? stack1.selected : stack1),{"name":"select","hash":{},"data":data}))
    + "\n      </div>\n    </li>\n    <li class='info'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[info]","Info",{"name":"label","hash":{},"data":data}))
    + "\n      <div>\n        "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"aaData[info]","",{"name":"input","hash":{},"data":data}))
    + "\n      </div>\n    </li>\n  </ul>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='services-form'>\n"
    + ((stack1 = (helpers.form || (depth0 && depth0.form) || helpers.helperMissing).call(depth0,"/",{"name":"form","hash":{"class":"form formtastic"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/item_views/service_form"];
}).call(this);
