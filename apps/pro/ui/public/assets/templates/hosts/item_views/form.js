(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/item_views/form"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "  <h5>Name & Address</h5>\n  <ul>\n    <li class='address'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[address]","Address",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[address]",(depth0 != null ? depth0.address : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[address]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.address || (depth0 != null ? depth0.address : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"address","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[address]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[address]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[address]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n    <li class='name'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[name]","Name",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[name]",(depth0 != null ? depth0.name : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[name]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[name]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[name]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[name]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n    <li class='mac'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[mac]","MAC Address",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[mac]",(depth0 != null ? depth0.mac : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[mac]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.mac || (depth0 != null ? depth0.mac : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"mac","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[mac]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[mac]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[mac]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n  </ul>\n  <h5>Operating System</h5>\n  <ul>\n    <li class='os_name'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[os_name]","Name",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[os_name]",(depth0 != null ? depth0.os_name : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[os_name]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.os_name || (depth0 != null ? depth0.os_name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_name","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[os_name]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[os_name]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[os_name]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n    <li class='os_flavor'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[os_flavor]","Flavor",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[os_flavor]",(depth0 != null ? depth0.os_flavor : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[os_flavor]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.os_flavor || (depth0 != null ? depth0.os_flavor : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_flavor","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[os_flavor]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[os_flavor]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[os_flavor]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n    <li class='os_version'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[os_sp]","SP",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[os_sp]",(depth0 != null ? depth0.os_sp : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[os_sp]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.os_sp || (depth0 != null ? depth0.os_sp : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"os_sp","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[os_sp]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[os_sp]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[os_sp]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n    <li class='purpose'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"host[purpose]","Purpose",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"host[purpose]",(depth0 != null ? depth0.purpose : depth0),{"name":"input","hash":{},"data":data}))
    + "\n      <a class='edit' for='host[purpose]' href='javascript:void(0)'>"
    + this.escapeExpression(((helper = (helper = helpers.purpose || (depth0 != null ? depth0.purpose : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"purpose","hash":{},"data":data}) : helper)))
    + "</a>\n      <div class='btns'>\n        <a class='pencil edit' for='host[purpose]' href='javascript:void(0)'></a>\n        <div class='actions'>\n          <a class='save' for='host[purpose]' href='javascript:void(0)'>Save</a>\n          <a class='cancel' for='host[purpose]' href='javascript:void(0)'>Cancel</a>\n        </div>\n      </div>\n    </li>\n  </ul>\n  <h5>\n    Comments\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.comments : depth0),{"name":"if_present","hash":{},"fn":this.program(2, data, 0),"inverse":this.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "  </h5>\n  <textarea name='host[comments]'></textarea>\n  <div class='actions comments'>\n    <a class='save' for='host[comments]' href='javascript:void(0)'>Save</a>\n    <a class='cancel' for='host[comments]' href='javascript:void(0)'>Cancel</a>\n  </div>\n  <p class='comments'>\n"
    + ((stack1 = (helpers.if_present || (depth0 && depth0.if_present) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.comments : depth0),{"name":"if_present","hash":{},"fn":this.program(6, data, 0),"inverse":this.program(8, data, 0),"data":data})) != null ? stack1 : "")
    + "  </p>\n";
},"2":function(depth0,helpers,partials,data) {
    return "    <a class='edit comments' for='host[comments]' href='javascript:void(0)'>Edit Comments</a>\n";
},"4":function(depth0,helpers,partials,data) {
    return "    <a class='edit comments' for='host[comments]' href='javascript:void(0)'>Add Comments</a>\n";
},"6":function(depth0,helpers,partials,data) {
    var helper;

  return "    <span>"
    + this.escapeExpression(((helper = (helper = helpers.comments || (depth0 != null ? depth0.comments : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"comments","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"8":function(depth0,helpers,partials,data) {
    return "    No Comments\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='host_form'>\n"
    + ((stack1 = (helpers.form || (depth0 && depth0.form) || helpers.helperMissing).call(depth0,"/",{"name":"form","hash":{"class":"form toggle-edit"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/item_views/form"];
}).call(this);
