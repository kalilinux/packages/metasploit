(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["hosts/item_views/cred_form"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return "  <ul>\n    <li class='name'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[service]","Service",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.select || (depth0 && depth0.select) || helpers.helperMissing).call(depth0,"aaData[service]",((stack1 = (depth0 != null ? depth0.services : depth0)) != null ? stack1.services : stack1),((stack1 = (depth0 != null ? depth0.services : depth0)) != null ? stack1.selected : stack1),{"name":"select","hash":{},"data":data}))
    + "\n    </li>\n    <li class='ptype'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[ptype]","Type",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.select || (depth0 && depth0.select) || helpers.helperMissing).call(depth0,"aaData[ptype]",((stack1 = (depth0 != null ? depth0.ptypes : depth0)) != null ? stack1.ptypes : stack1),((stack1 = (depth0 != null ? depth0.ptypes : depth0)) != null ? stack1.selected : stack1),{"name":"select","hash":{},"data":data}))
    + "\n    </li>\n    <li class='user'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[user]","User",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"aaData[user]","",{"name":"input","hash":{},"data":data}))
    + "\n    </li>\n    <li class='port'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"aaData[pass]","Password",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.textarea || (depth0 && depth0.textarea) || helpers.helperMissing).call(depth0,"aaData[pass]","",{"name":"textarea","hash":{},"data":data}))
    + "\n    </li>\n  </ul>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='cred-form'>\n"
    + ((stack1 = (helpers.form || (depth0 && depth0.form) || helpers.helperMissing).call(depth0,"/",{"name":"form","hash":{"class":"form formtastic"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
  return this.HandlebarsTemplates["hosts/item_views/cred_form"];
}).call(this);
