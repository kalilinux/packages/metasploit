(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/scheduler/monthly_schedule"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "    <label>\n      <input class='suspend' name='schedule_suspend' type='checkbox' value='manual'>\n      Suspend\n    </label>\n";
},"3":function(depth0,helpers,partials,data) {
    return "    <label>\n      <input checked class='suspend' name='schedule_suspend' type='checkbox' value='manual'>\n      Suspend\n    </label>\n";
},"5":function(depth0,helpers,partials,data) {
    return "      <option selected value='day'>\n        Day\n      </option>\n";
},"7":function(depth0,helpers,partials,data) {
    return "      <option value='day'>\n        Day\n      </option>\n";
},"9":function(depth0,helpers,partials,data) {
    return "      <option selected value='1'>\n        First\n      </option>\n";
},"11":function(depth0,helpers,partials,data) {
    return "      <option value='1'>\n        First\n      </option>\n";
},"13":function(depth0,helpers,partials,data) {
    return "      <option selected value='2'>\n        Second\n      </option>\n";
},"15":function(depth0,helpers,partials,data) {
    return "      <option value='2'>\n        Second\n      </option>\n";
},"17":function(depth0,helpers,partials,data) {
    return "      <option selected value='3'>\n        Third\n      </option>\n";
},"19":function(depth0,helpers,partials,data) {
    return "      <option value='3'>\n        Third\n      </option>\n";
},"21":function(depth0,helpers,partials,data) {
    return "      <option selected value='4'>\n        Fourth\n      </option>\n";
},"23":function(depth0,helpers,partials,data) {
    return "      <option value='4'>\n        Fourth\n      </option>\n";
},"25":function(depth0,helpers,partials,data) {
    return "      <option selected value='last'>\n        Last\n      </option>\n";
},"27":function(depth0,helpers,partials,data) {
    return "      <option value='last'>\n        Last\n      </option>\n";
},"29":function(depth0,helpers,partials,data) {
    return "    <input name='schedule_recurrence[monthly][day_index]' type='text' value='1'>\n";
},"31":function(depth0,helpers,partials,data) {
    var stack1;

  return "    <input name='schedule_recurrence[monthly][day_index]' type='text' value='"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_index : stack1), depth0))
    + "'>\n";
},"33":function(depth0,helpers,partials,data) {
    return "      <option selected value='sunday'>\n        Sunday\n      </option>\n";
},"35":function(depth0,helpers,partials,data) {
    return "      <option value='sunday'>\n        Sunday\n      </option>\n";
},"37":function(depth0,helpers,partials,data) {
    return "      <option selected value='monday'>\n        Monday\n      </option>\n";
},"39":function(depth0,helpers,partials,data) {
    return "      <option value='monday'>\n        Monday\n      </option>\n";
},"41":function(depth0,helpers,partials,data) {
    return "      <option selected value='tuesday'>\n        Tuesday\n      </option>\n";
},"43":function(depth0,helpers,partials,data) {
    return "      <option value='tuesday'>\n        Tuesday\n      </option>\n";
},"45":function(depth0,helpers,partials,data) {
    return "      <option selected value='wednesday'>\n        Wednesday\n      </option>\n";
},"47":function(depth0,helpers,partials,data) {
    return "      <option value='wednesday'>\n        Wednesday\n      </option>\n";
},"49":function(depth0,helpers,partials,data) {
    return "      <option selected value='thursday'>\n        Thursday\n      </option>\n";
},"51":function(depth0,helpers,partials,data) {
    return "      <option value='thursday'>\n        Thursday\n      </option>\n";
},"53":function(depth0,helpers,partials,data) {
    return "      <option selected value='friday'>\n        Friday\n      </option>\n";
},"55":function(depth0,helpers,partials,data) {
    return "      <option value='friday'>\n        Friday\n      </option>\n";
},"57":function(depth0,helpers,partials,data) {
    return "      <option selected value='saturday'>\n        Saturday\n      </option>\n";
},"59":function(depth0,helpers,partials,data) {
    return "      <option value='saturday'>\n        Saturday\n      </option>\n";
},"61":function(depth0,helpers,partials,data) {
    return "      <option selected value='year'>\n        1 Year\n      </option>\n";
},"63":function(depth0,helpers,partials,data) {
    return "      <option value='year'>\n        1 Year\n      </option>\n";
},"65":function(depth0,helpers,partials,data) {
    return "      <option selected value='never_expire'>\n        Never Expire\n      </option>\n";
},"67":function(depth0,helpers,partials,data) {
    return "      <option value='never_expire'>\n        Never Expire\n      </option>\n";
},"69":function(depth0,helpers,partials,data) {
    return "      <input checked name='task_chain[clear_workspace_before_run]' type='checkbox' value='1'>\n";
},"71":function(depth0,helpers,partials,data) {
    return "      <input name='task_chain[clear_workspace_before_run]' type='checkbox' value='1'>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='toggle-container'>\n  <div class='toggle'>\n"
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.state : stack1),{"name":"unless_eq","hash":{"compare":"suspended"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.state : stack1),{"name":"if_eq","hash":{"compare":"suspended"},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n</div>\n<form action='javascript:void(0)' method='post'>\n  <div class='option'>\n    <label>\n      Run Chain\n    </label>\n    <select id='schedule_type' name='schedule_recurrence[frequency]'>\n      <option value='once'>\n        Once\n      </option>\n      <option value='hourly'>\n        Hourly\n      </option>\n      <option value='daily'>\n        Daily\n      </option>\n      <option value='weekly'>\n        Weekly\n      </option>\n      <option selected value='monthly'>\n        Monthly\n      </option>\n    </select>\n  </div>\n  <div class='option'>\n    <div class='interval'>\n      <label>\n        Run Every\n      </label>\n      <input name='schedule_recurrence[monthly][interval]' type='text' value='"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.interval : stack1), depth0))
    + "'>\n      <span>\n        Months\n      </span>\n    </div>\n  </div>\n  <div class='option'>\n    <div class='monthly start-date'>\n      <label>\n        Start on\n      </label>\n      <input id='weekday-date' name='schedule_recurrence[monthly][start_date]' readonly='readonly' type='text' value='"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.start_date : stack1), depth0))
    + "'>\n    </div>\n    <span>\n      @\n    </span>\n    <div class='monthly start-time'>\n      <input id='weekday-time' name='schedule_recurrence[monthly][start_time]' readonly='readonly' type='text' value='"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.start_time : stack1), depth0))
    + "'>\n    </div>\n  </div>\n  <div class='option'>\n    <label>\n      On\n    </label>\n    <select name='schedule_recurrence[monthly][type]'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"if_eq","hash":{"compare":"day"},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"unless_eq","hash":{"compare":"day"},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"if_eq","hash":{"compare":"1"},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"unless_eq","hash":{"compare":"1"},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"if_eq","hash":{"compare":"2"},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"unless_eq","hash":{"compare":"2"},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"if_eq","hash":{"compare":"3"},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"unless_eq","hash":{"compare":"3"},"fn":this.program(19, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"if_eq","hash":{"compare":"4"},"fn":this.program(21, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"unless_eq","hash":{"compare":"4"},"fn":this.program(23, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"if_eq","hash":{"compare":"last"},"fn":this.program(25, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.type : stack1),{"name":"unless_eq","hash":{"compare":"last"},"fn":this.program(27, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </select>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_index : stack1),{"name":"if_eq","hash":{"compare":(depth0 != null ? depth0.undefined : depth0)},"fn":this.program(29, data, 0),"inverse":this.program(31, data, 0),"data":data})) != null ? stack1 : "")
    + "    <select name='schedule_recurrence[monthly][day_interval]'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"sunday"},"fn":this.program(33, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"sunday"},"fn":this.program(35, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"monday"},"fn":this.program(37, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"monday"},"fn":this.program(39, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"monday"},"fn":this.program(37, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"monday"},"fn":this.program(39, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"tuesday"},"fn":this.program(41, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"tuesday"},"fn":this.program(43, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"wednesday"},"fn":this.program(45, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"wednesday"},"fn":this.program(47, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"thursday"},"fn":this.program(49, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"thursday"},"fn":this.program(51, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"friday"},"fn":this.program(53, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"friday"},"fn":this.program(55, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"if_eq","hash":{"compare":"saturday"},"fn":this.program(57, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.day_interval : stack1),{"name":"unless_eq","hash":{"compare":"saturday"},"fn":this.program(59, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </select>\n  </div>\n  <div class='option'>\n    <label>\n      Max Duration\n    </label>\n    <select name='schedule_recurrence[monthly][max_duration]'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.max_duration : stack1),{"name":"if_eq","hash":{"compare":"year"},"fn":this.program(61, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.max_duration : stack1),{"name":"unless_eq","hash":{"compare":"year"},"fn":this.program(63, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.max_duration : stack1),{"name":"if_eq","hash":{"compare":"never_expire"},"fn":this.program(65, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.monthly : stack1)) != null ? stack1.max_duration : stack1),{"name":"unless_eq","hash":{"compare":"never_expire"},"fn":this.program(67, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </select>\n  </div>\n  <div class='option skip-disable'>\n    <label class='spacer'></label>\n    <div class='delete-project-data'>\n      <input name='task_chain[clear_workspace_before_run]' type='hidden' value='0'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.clear_workspace_before_run : stack1),{"name":"if_eq","hash":{"compare":true},"fn":this.program(69, data, 0),"inverse":this.program(71, data, 0),"data":data})) != null ? stack1 : "")
    + "      Delete Previous project data (Recommended)\n    </div>\n  </div>\n</form>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/scheduler/monthly_schedule"];
}).call(this);
