(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/task_chain_header"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='name columns small-6'>\n  <div class='container'>\n    <label for='name'>\n      Task Chain Name:\n    </label>\n    <div class='field-container'>\n      <input id='name' name='task_chain[name]' type='text'>\n    </div>\n  </div>\n</div>\n<div class='columns small-3 schedule-container'>\n  <div class='schedule-state schedule'></div>\n  <a class='schedule' href='javascript:void(0)'>\n    Schedule Now\n  </a>\n</div>\n<div class='actions columns small-3'>\n  <ul>\n    <li class='cancel'>\n      <span class='btn cancel link'>\n        <a href='javascript:void(0)'>\n          Cancel\n        </a>\n      </span>\n    </li>\n    <li class='save-and-run'>\n      <span class='btn save-run disabled'>\n        <a href='javascript:void(0)'>\n          Save and Run Now\n        </a>\n      </span>\n    </li>\n    <li class='save'>\n      <span class='btn save disabled'>\n        <a href='javascript:void(0)'>\n          Save\n        </a>\n      </span>\n    </li>\n  </ul>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/task_chain_header"];
}).call(this);
