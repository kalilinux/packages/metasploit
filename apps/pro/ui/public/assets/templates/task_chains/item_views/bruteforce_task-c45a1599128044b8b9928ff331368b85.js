(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/bruteforce_task"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='columns small-12'>\n  <div class='errors'></div>\n  <div class='config' id='bruteforce-main-region'></div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/bruteforce_task"];
}).call(this);
