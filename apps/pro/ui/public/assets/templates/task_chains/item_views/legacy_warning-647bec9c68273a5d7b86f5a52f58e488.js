(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/legacy_warning"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div>\n  The Nexpose and import configuration pages have been recently updated.\n  Both tasks now have a new workflow, which streamlines the import of data from Nexpose, Metasploit,\n  and third-party vendors. Due to these changes, you must reconfigure any task chain that contains a legacy Nexpose\n  or import task. For more information on these changes, see the release notes.\n  <a href='https://community.rapid7.com/docs/DOC-3105' target='_blank'>\n    https://community.rapid7.com/docs/DOC-3105\n  </a>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/legacy_warning"];
}).call(this);
