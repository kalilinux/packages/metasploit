(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/metamodule_task"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "        <option value=\""
    + this.escapeExpression(((helper = (helper = helpers.symbol || (depth0 != null ? depth0.symbol : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"symbol","hash":{},"data":data}) : helper)))
    + "\">"
    + this.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"name","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='columns small-12'>\n  <div class='errors'></div>\n  <div class='columns small-12'>\n    <div class='row spacer'></div>\n    <div class='columns small-6'></div>\n    <div class='columns small-6 mm-select-container'>\n      <label for='mm-select'>\n        Metamodule:\n      </label>\n      <select class='mm-select' id='mm-select'>\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.metamodules : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "      </select>\n    </div>\n  </div>\n  <div class='config'>\n    <div class='tab-loading'></div>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/metamodule_task"];
}).call(this);
