(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/task_chain_item"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return this.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"type","hash":{},"data":data}) : helper)));
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper, options, buffer = 
  "<div class='generic-stat-wrapper' clickable='true'>\n  <canvas class='scan' height='80px' width='80px'></canvas>\n  <div class='task'>\n    ";
  stack1 = ((helper = (helper = helpers.titleize || (depth0 != null ? depth0.titleize : depth0)) != null ? helper : helpers.helperMissing),(options={"name":"titleize","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data}),(typeof helper === "function" ? helper.call(depth0,options) : helper));
  if (!helpers.titleize) { stack1 = helpers.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/task_chain_item"];
}).call(this);
