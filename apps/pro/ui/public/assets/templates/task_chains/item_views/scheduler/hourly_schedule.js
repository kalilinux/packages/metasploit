(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/scheduler/hourly_schedule"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "    <label>\n      <input class='suspend' name='schedule_suspend' type='checkbox' value='manual'>\n      Suspend\n    </label>\n";
},"3":function(depth0,helpers,partials,data) {
    return "    <label>\n      <input checked class='suspend' name='schedule_suspend' type='checkbox' value='manual'>\n      Suspend\n    </label>\n";
},"5":function(depth0,helpers,partials,data) {
    return "      <option selected value='week'>\n        1 Week\n      </option>\n";
},"7":function(depth0,helpers,partials,data) {
    return "      <option value='week'>\n        1 Week\n      </option>\n";
},"9":function(depth0,helpers,partials,data) {
    return "      <option selected value='month'>\n        1 Month\n      </option>\n";
},"11":function(depth0,helpers,partials,data) {
    return "      <option value='month'>\n        1 Month\n      </option>\n";
},"13":function(depth0,helpers,partials,data) {
    return "      <option selected value='year'>\n        1 Year\n      </option>\n";
},"15":function(depth0,helpers,partials,data) {
    return "      <option value='year'>\n        1 Year\n      </option>\n";
},"17":function(depth0,helpers,partials,data) {
    return "      <option selected value='never_expire'>\n        Never Expire\n      </option>\n";
},"19":function(depth0,helpers,partials,data) {
    return "      <option value='never_expire'>\n        Never Expire\n      </option>\n";
},"21":function(depth0,helpers,partials,data) {
    return "      <input checked name='task_chain[clear_workspace_before_run]' type='checkbox' value='1'>\n";
},"23":function(depth0,helpers,partials,data) {
    return "      <input name='task_chain[clear_workspace_before_run]' type='checkbox' value='1'>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='toggle-container'>\n  <div class='toggle'>\n"
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.state : stack1),{"name":"unless_eq","hash":{"compare":"suspended"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.state : stack1),{"name":"if_eq","hash":{"compare":"suspended"},"fn":this.program(3, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n</div>\n<form action='javascript:void(0)' method='post'>\n  <div class='option'>\n    <label>\n      Run Chain\n    </label>\n    <select id='schedule_type' name='schedule_recurrence[frequency]'>\n      <option>\n        Once\n      </option>\n      <option selected value='hourly'>\n        Hourly\n      </option>\n      <option value='daily'>\n        Daily\n      </option>\n      <option value='weekly'>\n        Weekly\n      </option>\n      <option value='monthly'>\n        Monthly\n      </option>\n    </select>\n    <div class='hourly minute'>\n      <span>\n        at\n      </span>\n      <input name='schedule_recurrence[hourly][minute]' type='text' value='"
    + this.escapeExpression(this.lambda(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.minute : stack1), depth0))
    + "'>\n      <span>\n        minutes past the hour\n      </span>\n    </div>\n  </div>\n  <div class='option'>\n    <label>\n      Max Duration\n    </label>\n    <select name='schedule_recurrence[hourly][max_duration]'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"if_eq","hash":{"compare":"week"},"fn":this.program(5, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"unless_eq","hash":{"compare":"week"},"fn":this.program(7, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"if_eq","hash":{"compare":"month"},"fn":this.program(9, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"unless_eq","hash":{"compare":"month"},"fn":this.program(11, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"if_eq","hash":{"compare":"year"},"fn":this.program(13, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"unless_eq","hash":{"compare":"year"},"fn":this.program(15, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"if_eq","hash":{"compare":"never_expire"},"fn":this.program(17, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.unless_eq || (depth0 && depth0.unless_eq) || helpers.helperMissing).call(depth0,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.schedule_hash : stack1)) != null ? stack1.hourly : stack1)) != null ? stack1.max_duration : stack1),{"name":"unless_eq","hash":{"compare":"never_expire"},"fn":this.program(19, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </select>\n  </div>\n  <div class='option skip-disable'>\n    <label class='spacer'></label>\n    <div class='delete-project-data'>\n      <input name='task_chain[clear_workspace_before_run]' type='hidden' value='0'>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,((stack1 = (depth0 != null ? depth0.task_chain : depth0)) != null ? stack1.clear_workspace_before_run : stack1),{"name":"if_eq","hash":{"compare":true},"fn":this.program(21, data, 0),"inverse":this.program(23, data, 0),"data":data})) != null ? stack1 : "")
    + "      Delete Previous project data (Recommended)\n    </div>\n  </div>\n</form>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/scheduler/hourly_schedule"];
}).call(this);
