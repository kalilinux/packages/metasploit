(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/item_views/empty_task_config"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='columns small-12'>\n  <div class='config container'>\n    <div class='empty-message'>\n      No tasks have been added yet.\n    </div>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/item_views/empty_task_config"];
}).call(this);
