(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/collection_views/task_chain_items"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='generic-stat-wrapper' clickable='true'>\n  <canvas class='scan' height='80px' width='78px'></canvas>\n  <div class='task'></div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/collection_views/task_chain_items"];
}).call(this);
