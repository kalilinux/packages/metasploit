(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["task_chains/layouts/scheduler"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='schedule-info'>\n  <div class='title'>\n    Task Chain will occur:\n  </div>\n  <div class='run-info'>\n    Invalid Schedule\n  </div>\n</div>\n<div class='schedule-options'></div>";
},"useData":true});
  return this.HandlebarsTemplates["task_chains/layouts/scheduler"];
}).call(this);
