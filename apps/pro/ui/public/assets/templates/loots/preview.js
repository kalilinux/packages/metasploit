(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["loots/preview"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper;

  return "<div class='loot_preview'>\n  <textarea class='text_dump'></textarea>\n  <div class='img_box' style='text-align:center'>\n    <a href='"
    + this.escapeExpression(((helper = (helper = helpers.path || (depth0 != null ? depth0.path : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"path","hash":{},"data":data}) : helper)))
    + "' target='_blank'>\n      <img>\n    </a>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["loots/preview"];
}).call(this);
