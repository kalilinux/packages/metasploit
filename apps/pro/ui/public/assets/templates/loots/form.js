(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["loots/form"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "  <ul>\n    <li class='data file input'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"loot[data]","File",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.file || (depth0 && depth0.file) || helpers.helperMissing).call(depth0,"loot[data]",{"name":"file","hash":{},"data":data}))
    + "\n    </li>\n    <li class='name'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"loot[name]","Name*",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"loot[name]",(depth0 != null ? depth0.name : depth0),{"name":"input","hash":{},"data":data}))
    + "\n    </li>\n    <li class='content_type'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"loot[content_type]","Content type",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.input || (depth0 && depth0.input) || helpers.helperMissing).call(depth0,"loot[content_type]","text/plain",{"name":"input","hash":{},"data":data}))
    + "\n    </li>\n    <li class='info'>\n      "
    + this.escapeExpression((helpers.label || (depth0 && depth0.label) || helpers.helperMissing).call(depth0,"loot[info]","Info",{"name":"label","hash":{},"data":data}))
    + "\n      "
    + this.escapeExpression((helpers.textarea || (depth0 && depth0.textarea) || helpers.helperMissing).call(depth0,"loot[info]",(depth0 != null ? depth0.info : depth0),{"name":"textarea","hash":{},"data":data}))
    + "\n    </li>\n  </ul>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='loot_form'>\n  <div class='errors' style='display: none'></div>\n"
    + ((stack1 = (helpers.form || (depth0 && depth0.form) || helpers.helperMissing).call(depth0,"/",{"name":"form","hash":{"class":"form formtastic"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
  return this.HandlebarsTemplates["loots/form"];
}).call(this);
