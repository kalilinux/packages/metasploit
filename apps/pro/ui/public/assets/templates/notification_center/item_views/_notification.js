(function() {
  Handlebars.registerPartial("notification_center/item_views/_notification", Handlebars.template({"1":function(depth0,helpers,partials,data) {
    return "      <span class='system-badge'>\n        System\n      </span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class='table-container'>\n  <div class='status-bar'>\n    <div class='status-block'></div>\n  </div>\n  <div class='content-cell'>\n    <div class='message'>\n      <b>\n        "
    + this.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"title","hash":{},"data":data}) : helper)))
    + "\n      </b>\n"
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.kind : depth0),{"name":"if_eq","hash":{"compare":"system_notification"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,(depth0 != null ? depth0.kind : depth0),{"name":"if_eq","hash":{"compare":"update_notification"},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    <div class='details'>\n      <div class='text'>\n        "
    + this.escapeExpression(((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"content","hash":{},"data":data}) : helper)))
    + "\n      </div>\n      <div class='humanized'>\n        <b>\n          "
    + this.escapeExpression(((helper = (helper = helpers.humanized_created_at || (depth0 != null ? depth0.humanized_created_at : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"humanized_created_at","hash":{},"data":data}) : helper)))
    + "\n        </b>\n        ago\n      </div>\n    </div>\n  </div>\n</div>\n<div class='action-bar'>\n  <span class='close'>\n    &times;\n  </span>\n</div>";
},"useData":true}));
}).call(this);
