(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["notification_center/item_views/notification_item_view"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,true,{"name":"if_eq","hash":{"compare":(depth0 != null ? depth0.read : depth0)},"fn":this.program(2, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0,false,{"name":"if_eq","hash":{"compare":(depth0 != null ? depth0.read : depth0)},"fn":this.program(4, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "");
},"2":function(depth0,helpers,partials,data) {
    var stack1;

  return "<div class='notification-message message-read'>\n"
    + ((stack1 = this.invokePartial(partials['notification_center/item_views/_notification'],depth0,{"name":"notification_center/item_views/_notification","data":data,"indent":"  ","helpers":helpers,"partials":partials})) != null ? stack1 : "")
    + "</div>\n";
},"4":function(depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class='notification-message "
    + this.escapeExpression(((helper = (helper = helpers.kind || (depth0 != null ? depth0.kind : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"kind","hash":{},"data":data}) : helper)))
    + "'>\n"
    + ((stack1 = this.invokePartial(partials['notification_center/item_views/_notification'],depth0,{"name":"notification_center/item_views/_notification","data":data,"indent":"  ","helpers":helpers,"partials":partials})) != null ? stack1 : "")
    + "</div>\n";
},"6":function(depth0,helpers,partials,data) {
    return "<div class='no-new-notifications'>\n  No new notifications\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers['if'].call(depth0,(depth0 != null ? depth0.title : depth0),{"name":"if","hash":{},"fn":this.program(1, data, 0),"inverse":this.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
  return this.HandlebarsTemplates["notification_center/item_views/notification_item_view"];
}).call(this);
