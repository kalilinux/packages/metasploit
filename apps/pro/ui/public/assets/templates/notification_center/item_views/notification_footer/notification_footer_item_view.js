(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["notification_center/item_views/notification_footer/notification_footer_item_view"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='more-text'>\n  More\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["notification_center/item_views/notification_footer/notification_footer_item_view"];
}).call(this);
