(function() {
  this.HandlebarsTemplates || (this.HandlebarsTemplates = {});
  this.HandlebarsTemplates["notification_center/composite_views/notification_composite_view"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class='notification-type'>\n  <div class='type-title'>\n    <span>\n      Latest Notifications\n    </span>\n  </div>\n  <div class='sort-options'>\n    <span>\n      Show\n    </span>\n    <span>\n      <select name='notification_type'>\n        <option value=''>\n          All\n        </option>\n        <option value='minion_notification'>\n          Minions\n        </option>\n        <option value='system_notification'>\n          System\n        </option>\n      </select>\n    </span>\n  </div>\n</div>";
},"useData":true});
  return this.HandlebarsTemplates["notification_center/composite_views/notification_composite_view"];
}).call(this);
