(function() {

  jQuery(document).bind('requirejs-ready', function() {
    return window.initRequire(['jquery', '/assets/shared/backbone/views/task_console.js'], function($, TaskConsole, AppStatModalLayout, AppRun) {
      return $(document).ready(function() {
        var $appRunMeta, $log, $presenterMeta, appRunId, presenter, task, tc;
        $appRunMeta = $('meta[name=app_run_id]');
        $presenterMeta = $('meta[name=presenter]');
        if ($appRunMeta.length > 0) {
          appRunId = $appRunMeta.attr('content');
          return window.initRequire(['/assets/apps/backbone/views/layouts/app_stat_modal_layout.js', '/assets/apps/backbone/models/app_run.js'], function(AppStatModalLayout, AppRun) {
            var appRun,
              _this = this;
            appRun = new AppRun({
              id: appRunId
            });
            return appRun.fetch({
              success: function() {
                var region, statLayout;
                statLayout = new AppStatModalLayout({
                  appRun: appRun
                });
                region = new Backbone.Marionette.Region({
                  el: $('#appStats')
                });
                region.show(statLayout);
                return $('.app-stats').css({
                  padding: 0
                });
              }
            });
          });
        } else if ($presenterMeta.length > 0) {
          presenter = $presenterMeta.attr('content');
          return initProRequire(['apps/tasks/tasks_app'], function(TasksApp) {
            return Pro.start({
              environment: "development"
            });
          });
        } else {
          $(document).on('logUpdate', function(event, log, header) {
            return $('#taskDetails').html(header);
          });
          $log = $('#task_logs');
          task = $log.attr('task');
          tc = new TaskConsole({
            el: $log[0],
            task: task,
            prerendered: true
          });
          return tc.startUpdating();
        }
      });
    });
  });

}).call(this);
