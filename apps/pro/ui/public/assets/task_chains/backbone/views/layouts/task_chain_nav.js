(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/task_chains/layouts/task_chain_nav.js', '/assets/shared/lib/pie_chart.js', '/assets/task_chains/backbone/models/task_chain_item.js', '/assets/task_chains/backbone/views/collection_views/task_chain_items.js'], function($, Template, PieChart, TaskChainItem, TaskChainItems) {
    var TaskChainNav;
    return TaskChainNav = (function(_super) {

      __extends(TaskChainNav, _super);

      function TaskChainNav() {
        this.addExistingTask = __bind(this.addExistingTask, this);

        this._closeDropdownHandler = __bind(this._closeDropdownHandler, this);

        this.onClose = __bind(this.onClose, this);

        this.onShow = __bind(this.onShow, this);
        return TaskChainNav.__super__.constructor.apply(this, arguments);
      }

      TaskChainNav.prototype.template = HandlebarsTemplates['task_chains/layouts/task_chain_nav'];

      TaskChainNav.prototype._taskViewPaths = {
        bruteforce: '/assets/task_chains/backbone/views/item_views/bruteforce_task.js',
        cleanup: '/assets/task_chains/backbone/views/item_views/cleanup_task.js',
        collect: '/assets/task_chains/backbone/views/item_views/collect_task.js',
        exploit: '/assets/task_chains/backbone/views/item_views/exploit_task.js',
        'import': '/assets/task_chains/backbone/views/item_views/import_task.js',
        module_run: '/assets/task_chains/backbone/views/item_views/module_run_task.js',
        nexpose: '/assets/task_chains/backbone/views/item_views/nexpose_task.js',
        report: '/assets/task_chains/backbone/views/item_views/report.js',
        webscan: '/assets/task_chains/backbone/views/item_views/webscan_task.js',
        scan: '/assets/task_chains/backbone/views/item_views/scan_task.js',
        metamodule: '/assets/task_chains/backbone/views/item_views/metamodule_task.js'
      };

      TaskChainNav.prototype.events = {
        'click .add-task': '_toggleAddTask',
        'click .add-task-options': '_addTask',
        'click .action.delete': '_deleteTask',
        'click .action.reset': '_resetTask',
        'click .action.clone': '_cloneTask',
        'pieChanged .container': '_pieChanged'
      };

      TaskChainNav.prototype.ui = {
        addTaskOptions: '.add-task-options'
      };

      TaskChainNav.prototype.regions = {
        pies: '.pie-charts'
      };

      TaskChainNav.prototype.onShow = function() {
        $('.delete').tooltip();
        $('.clone').tooltip();
        $('.reset').tooltip();
        this._initPies();
        return this._bindTriggers();
      };

      TaskChainNav.prototype.onClose = function() {
        return this._unbindTriggers();
      };

      TaskChainNav.prototype._bindTriggers = function() {
        return $(document).on('click', this._closeDropdownHandler);
      };

      TaskChainNav.prototype._unbindTriggers = function() {
        return $(document).unbind('click', this._closeDropdownHandler);
      };

      TaskChainNav.prototype._closeDropdownHandler = function(e) {
        if (e.target !== $('.add-task', this.el)[0]) {
          return this._closeDropdown();
        }
      };

      TaskChainNav.prototype._closeDropdown = function() {
        return this.ui.addTaskOptions.hide();
      };

      TaskChainNav.prototype._toggleAddTask = function() {
        return this.ui.addTaskOptions.toggle();
      };

      TaskChainNav.prototype._resetTask = function(e) {
        var collection, status;
        status = confirm("Are you sure you want to reset the current task chain?");
        if (status) {
          collection = this.pies.currentView.collection;
          return collection.reset();
        }
      };

      TaskChainNav.prototype._deleteTask = function(e) {
        var collection, current_task, status;
        status = confirm("Are you sure you want to delete the current task?");
        if (status) {
          collection = this.pies.currentView.collection;
          current_task = collection.findWhere({
            selected: true
          });
          return collection.remove(current_task);
        }
      };

      TaskChainNav.prototype.addExistingTask = function(task) {
        var opts;
        if (task.kind === "collect_evidence") {
          task.kind = "collect";
        }
        if (task.kind === "scan_and_import") {
          task.kind = 'nexpose';
        }
        opts = {
          task_type: task.kind,
          task: task
        };
        return this._requireTaskConfig(this._addTaskUpdateView, opts);
      };

      TaskChainNav.prototype._addTask = function(e) {
        var opts;
        e.stopPropagation();
        opts = {
          task_type: $(e.target).data('task-type')
        };
        return this._requireTaskConfig(this._addTaskUpdateView, opts);
      };

      TaskChainNav.prototype._cloneTask = function(e) {
        var collection, current_task;
        collection = this.pies.currentView.collection;
        $(this.el).trigger('cloneTaskConfig');
        current_task = collection.findWhere({
          selected: true
        });
        return collection.add(new TaskChainItem({
          type: current_task.get('type'),
          cloned: true
        }));
      };

      TaskChainNav.prototype._pieChanged = function(e, view) {
        var opts;
        opts = {
          task_chain_item: view.model,
          task_type: view.model.get('type'),
          task: view.model.get('task')
        };
        return this._requireTaskConfig(this._updateView, opts);
      };

      TaskChainNav.prototype._addTaskUpdateView = function(opts, TaskView) {
        var show_opts, task_chain_item;
        if (opts.task_type === "collect_evidence") {
          opts.task_type = "collect";
        }
        task_chain_item = this._addPieChart({
          type: opts.task_type,
          task: opts.task
        });
        show_opts = {
          task_view: TaskView,
          form_id: task_chain_item.cid,
          task: opts.task
        };
        if (TaskView != null) {
          $(this.el).trigger('showTaskConfig', show_opts);
        }
        return this._closeDropdown();
      };

      TaskChainNav.prototype._updateView = function(opts, TaskView) {
        var show_opts;
        show_opts = {
          cloned: opts.task_chain_item.get('cloned'),
          task_view: TaskView,
          form_id: opts.task_chain_item.cid,
          task: opts.task
        };
        return $(this.el).trigger('showTaskConfig', show_opts);
      };

      TaskChainNav.prototype._requireTaskConfig = function(func, opts) {
        var rjs,
          _this = this;
        rjs = requirejs.config({
          context: "reports"
        });
        return rjs([this._taskViewPaths[opts.task_type]], function(TaskView) {
          return func.call(_this, opts, TaskView);
        });
      };

      TaskChainNav.prototype._addPieChart = function(options) {
        if (options == null) {
          options = {};
        }
        return this.tasks.add(options);
      };

      TaskChainNav.prototype._initPies = function() {
        this.tasks = new Backbone.Collection([], {
          model: TaskChainItem
        });
        return this.pies.show(new TaskChainItems({
          collection: this.tasks,
          sort: false
        }));
      };

      return TaskChainNav;

    })(Backbone.Marionette.LayoutView);
  });

}).call(this);
