(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['jquery', '/assets/templates/task_chains/item_views/empty_task_config.js', '/assets/task_chains/backbone/views/item_views/task_config.js'], function($, Template, TaskConfigView) {
    var EmptyTaskConfig;
    return EmptyTaskConfig = (function(_super) {

      __extends(EmptyTaskConfig, _super);

      function EmptyTaskConfig() {
        return EmptyTaskConfig.__super__.constructor.apply(this, arguments);
      }

      EmptyTaskConfig.prototype.template = HandlebarsTemplates['task_chains/item_views/empty_task_config'];

      return EmptyTaskConfig;

    })(TaskConfigView);
  });

}).call(this);
