(function() {

  jQuery(function($) {
    $(document).ready(function() {});
    return $(document).on('click', 'a.reveal', function(e) {
      var $dialog,
        _this = this;
      $dialog = $('<div style="display:hidden"></div>').appendTo('body');
      $dialog.load($(this).parent().parent().attr('rurl'), {
        'id': $(this).parent().parent().attr('rid')
      }, function(responseText, textStatus, xhrRequest) {
        return $dialog.dialog({
          title: "Unobfuscated API Key",
          height: 100,
          width: 300
        });
      });
      return e.preventDefault();
    });
  });

}).call(this);
