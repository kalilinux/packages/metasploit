(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Teaspoon.Reporters.Console = (function(_super) {

    __extends(Console, _super);

    function Console() {
      this.bindScenarioOutput = __bind(this.bindScenarioOutput, this);
      Console.__super__.constructor.apply(this, arguments);
      angular.scenario.output("teaspoon", this.bindScenarioOutput);
    }

    Console.prototype.bindScenarioOutput = function(context, runner, model) {
      var _this = this;
      model.on("runnerBegin", function() {
        return _this.reportRunnerStarting({
          total: angular.scenario.Describe.specId
        });
      });
      model.on("specEnd", function(spec) {
        return _this.reportSpecResults(spec);
      });
      return model.on("runnerEnd", function() {
        return _this.reportRunnerResults();
      });
    };

    return Console;

  })(Teaspoon.Reporters.Console);

}).call(this);
