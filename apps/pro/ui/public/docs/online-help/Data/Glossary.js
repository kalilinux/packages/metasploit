MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'Glossary',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<html xmlns:MadCap=\"http://www.madcapsoftware.com/Schemas/MadCap.xsd\" MadCap:tocPath=\"\" MadCap:InPreviewMode=\"false\" MadCap:PreloadImages=\"false\" MadCap:RuntimeFileType=\"Glossary\" MadCap:TargetType=\"WebHelp2\" lang=\"en-us\" xml:lang=\"en-us\" MadCap:PathToHelpSystem=\"../\" MadCap:HelpSystemFileName=\"index.xml\">' +
	'    <head>' +
	'        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />' +
	'        <link href=\"../Skins/Default/Stylesheets/TextEffects.css\" rel=\"stylesheet\" type=\"text/css\" />' +
	'        <link href=\"../Skins/Default/Stylesheets/Topic.css\" rel=\"stylesheet\" type=\"text/css\" />' +
	'        <title>Glossary</title>' +
	'        <link href=\"../Content/Resources/Stylesheets/Styles.css\" rel=\"stylesheet\" type=\"text/css\" />' +
	'        <script src=\"../Resources/Scripts/jquery.min.js\" type=\"text/javascript\">' +
	'        </script>' +
	'        <script src=\"../Resources/Scripts/plugins.min.js\" type=\"text/javascript\">' +
	'        </script>' +
	'        <script src=\"../Resources/Scripts/MadCapAll.js\" type=\"text/javascript\">' +
	'        </script>' +
	'    </head>' +
	'    <body style=\"background-color: #fafafa;\">' +
	'        <ul>' +
	'            <li class=\"GlossaryPageEntry\">' +
	'                <div class=\"GlossaryPageTerm\">' +
	'                    <a name=\"4196621782_anchor1\" href=\"../Content/zz-glossary/def-administrator.html\" target=\"topic\">Administrator</a>' +
	'                </div>' +
	'            </li>' +
	'        </ul>' +
	'        <p>&#160;</p>' +
	'    </body>' +
	'</html>'
);
