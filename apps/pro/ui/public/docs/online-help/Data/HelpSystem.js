MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add(
	'HelpSystem',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<WebHelpSystem DefaultUrl=\"Content/00-welcome/welcome-screen.html\" Toc=\"Data/Toc.xml\" Index=\"Data/Index.xml\" Concepts=\"Data/Concepts.xml\" Glossary=\"Data/Glossary.xml\" SearchDatabase=\"Data/Search.xml\" Alias=\"Data/Alias.xml\" Synonyms=\"Data/Synonyms.xml\" SearchFilterSet=\"Data/Filters.xml\" SkinName=\"HTML5-CustomSkin\" Skins=\"HTML5-CustomSkin,HTML5\" BuildTime=\"12/4/2014 5:00:05 PM\" BuildVersion=\"8.1.2.0\" TargetType=\"WebHelp2\" SkinTemplateFolder=\"Skin/\" InPreviewMode=\"false\" MoveOutputContentToRoot=\"false\" MakeFileLowerCase=\"false\" UseCustomTopicFileExtension=\"true\" CustomTopicFileExtension=\"html\">' +
	'    <CatapultSkin Version=\"1\" SkinType=\"WebHelp2\" Comment=\"HTML5 skin\" Anchors=\"Left,Right,Top,Bottom\" Width=\"1024px\" Height=\"768px\" Top=\"216px\" Left=\"1920px\" Bottom=\"176px\" Right=\"0\" Tabs=\"TOC\" DefaultTab=\"TOC\" UseBrowserDefaultSize=\"true\" UseDefaultBrowserSetup=\"false\" NavigationLinkTop=\"true\" BrowserSetup=\"Toolbar,LocationBar\" DisplayNotificationOptions=\"false\" Title=\"Metasploit Online Help | Metasploit Documentation | Metasploit Videos and Tutorials\" AutoSyncTOC=\"false\" NavigationLinkBottom=\"false\" Name=\"HTML5-CustomSkin\">' +
	'        <WebHelpOptions NavigationPanePosition=\"Left\" NavigationPaneWidth=\"300\" HideNavigationOnStartup=\"true\" />' +
	'        <Toolbar EnableCustomLayout=\"true\" Buttons=\"HomePage|Print|Separator|Separator|Filler|PreviousTopic|CurrentTopicIndex|NextTopic\">' +
	'            <Script />' +
	'        </Toolbar>' +
	'    </CatapultSkin>' +
	'    <CatapultSkin Version=\"1\" SkinType=\"WebHelp2\" Comment=\"HTML5 skin\" Anchors=\"Left,Right,Top,Bottom,Width,Height\" Width=\"1028px\" Height=\"764px\" Top=\"136px\" Left=\"1920px\" Bottom=\"260px\" Right=\"0\" Tabs=\"TOC,Index\" DefaultTab=\"TOC\" UseBrowserDefaultSize=\"false\" UseDefaultBrowserSetup=\"false\" BrowserSetup=\"Menu,LocationBar\" NavigationLinkTop=\"false\" Name=\"HTML5\">' +
	'        <WebHelpOptions HideNavigationOnStartup=\"false\" />' +
	'    </CatapultSkin>' +
	'</WebHelpSystem>'
);
