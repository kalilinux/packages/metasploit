#!/bin/sh

# Get a reference to our current directory and script name
SOURCE="$0"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  test ${SOURCE#/} = ${SOURCE} && SOURCE="$DIR/$SOURCE"
done
BASE="$( cd -P "$( dirname "$SOURCE" )" && pwd )"


# Set up the environment
. "${BASE}/scripts/setenv.sh"

/etc/init.d/metasploit stop
exec "${BASE}/apps/pro/ui/script/diagnostic_logs.rb" "$@"
